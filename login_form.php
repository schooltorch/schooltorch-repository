<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'placeholder' =>'joe@example.com',
	'class'  => 'form-control input-lg',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
	
	'required' => true
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'placeholder' =>'',
	'class'  => 'form-control input-lg',
	'size'	=> 30,
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'placeholder' =>'',
	
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'placeholder' =>'',
	'class'  => 'form-control input-lg',
	'maxlength'	=> 8,
);
?>

<div class="container">
	<div class="">
		<div class="form-wrapper">
		
			<?php echo form_open($this->uri->uri_string(),array("class"=>"form","role"=>"form")); ?>

			<div class="form-signin-heading header-image">

    			<a href="<?=site_url()?>" title="Home"><img class="img-responsive" alt="" src="<?=base_url()?>assets/images/logo-black.png"></a>

   			 </div>

			<div class="form-group">
				<?php echo form_label($login_label, $login['id']); ?>
				<?php echo form_input($login); ?>
				<span class="help-block error-block"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></span>
			</div>

			<div class="form-group">
				<?php echo form_label('Password', $password['id']); ?>
				<?php echo form_password($password); ?>
				<span class="help-block error-block"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></span>
			</div>

			<?php if ($show_captcha) {
				if ($use_recaptcha) { ?>
			<div class="form-group">
				
					<div id="recaptcha_image"></div>
					<a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
					<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
					<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
				
			</div>
			<div class="form-group">

				<div class="recaptcha_only_if_image">Enter the words above</div>
				<div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
				
				<input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
				<span class="help-block error-block"><?php echo form_error('recaptcha_response_field'); ?></span>
				<?php echo $recaptcha_html; ?>
			</div>
			<?php } else { ?>
			<div class="form-group">
					<p>Enter the code exactly as it appears:</p>
					<?php echo $captcha_html; ?>
			</div>

			<div class="form-group">
				<?php echo form_label('Confirmation Code', $captcha['id']); ?>
				<?php echo form_input($captcha); ?>
				<span class="help-block error-block"> <?php echo form_error($captcha['name']); ?></span>
			</div>
			<?php }
			} ?>

		
			<div class="form-group">
					<?php echo form_checkbox($remember); ?>
					<?php echo form_label('Remember me', $remember['id']); ?>
					<span class="pull-right"><?php echo anchor('/auth/forgot_password/', 'Forgot password'); ?></span>
			</div>
			
			<?php echo form_submit(array('name'=>'submit', 'value' => 'Login','class' =>'btn btn-success btn-lg btn-block' )); ?>
			<?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Register',array("class"=>"btn btn-default btn-lg btn-block")); ?>
			<?php echo form_close(); ?>
			
			<!-- <div class="fb-login-button btn btn-success" data-max-rows="1" data-show-faces="false" data-auto-logout-link="false" onlogin='window.location="https://graph.facebook.com/oauth/authorize?client_id=<?=$this->config->item('appId'); ?>&redirect_uri=<?=site_url('auth_other/fb_signin'); ?>&amp;r="+window.location.href;'></div> -->

		</div>
	</div><!--/row-->
</div><!--/container-->