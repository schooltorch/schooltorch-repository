function FILES() {

	var me 	=	this;

	this.init = function(){

		

	}

	this.bindUploader = function(){

		var jobId 		=	$('#pickfiles').attr('data-job-id');

		var uploader 	= 	new plupload.Uploader({
			runtimes 		: 	'html5,flash,html4',
			browse_button 	: 	'pickfiles',
			filters			: 	{
								  	mime_types : "image/*"
								},
			max_file_size 	: 	'50mb',
			url 			: 	schoolApp.site_url+'admin/blog/upload',
			chunk_size		: 	'5mb',
			force_chunk_size: 	true, 
			autostart 		: 	true,
			unique_names	: 	false,
			multi_selection : 	false,
			flash_swf_url 	: 	'../assets/js/plupload.flash.swf',
			preinit : {
				Init: function(up, info) {
				},
		
				UploadFile: function(up, file) {
	
					// You can override settings before the file is uploaded
					// up.setOption('url', 'upload.php?id=' + file.id);
					// up.setOption('multipart_params', {param1 : 'value1', param2 : 'value2'});
				}
			},
	
			// Post init events, bound after the internal events
			init : {
				PostInit: function() {
					
					// Called after initialization is finished and internal event handlers bound

				},

				Browse: function(up) {
					// Called when file picker is clicked
				},

				Refresh: function(up) {
					// Called when the position or dimensions of the picker change
				},
	
				StateChanged: function(up) {
					// Called when the state of the queue is changed
				},
	
				QueueChanged: function(up) {
					// Called when queue is changed by adding or removing files
				},

				OptionChanged: function(up, name, value, oldValue) {
					// Called when one of the configuration options is changed
				},

				BeforeUpload: function(up, file) {
					// Called right before the upload for a given file starts, can be used to cancel it if required
				},
	
				UploadProgress: function(up, file) {

					$('#filesProgress').addClass('active');
					
					$('#filesProgress .progress-bar').css({'width': file.percent+'%'});

					$('#filesProgress .progress .sr-only').html(file.percent+' %');

					// Called while file is being uploaded
				},

				FileFiltered: function(up, file) {
					// Called when file successfully files all the filters
				},
	
				FilesAdded: function(up, files) {
					$('#pickfiles').hide();
					
					plupload.each(files, function(file) {
						
						uploader.start();

					});

				},
	
				FilesRemoved: function(up, files) {
					// Called when files are removed from queue
	
					plupload.each(files, function(file) {
						
					});
				},
	
				FileUploaded: function(up, file, info) {
					// Called when file has finished uploading
					
				},
	
				ChunkUploaded: function(up, file, info) {
					// Called when file chunk has finished uploading
				},

				UploadComplete: function(up, files) {

					// Called when all files are either uploaded or failed
					$('#pickfiles').show();
					$('#filesProgress').removeClass('active');

				},

				Destroy: function(up) {
					// Called when uploader is destroyed
				},
	
				Error: function(up, args) {
					// Called when error occurs

					console.log(args);

				}
			}
		});
	
		$('#uploadfiles').unbind('click');
		$('#uploadfiles').click(function() {
			
			uploader.start();
			return false;

		});

		uploader.init();

	}

}

var files = new FILES();
