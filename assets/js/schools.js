
var callIsGoing     =   0;

function delteReview()
{
    $('.deleteReview').unbind('click');
    $('.deleteReview').click(function(e){

        e.preventDefault();
        e.stopPropagation();

       // alert("clicker delete");

        

        var target = $(this);
        var reviewId = target.attr("data-id");

            if(confirm('Are you sure you want to delete the review?'))
            {
    
               $.ajax({
                        url:globalSiteUrl+'admin/reviewManagement/delete',
                        method:'POST',
                        data: {reviewId:reviewId},
                        success:function(response)
                        {
                            var result = JSON.parse(response);

                            if(result.status != "success")
                            {
                                alert(response);
                            }
                            else
                            {
                                //$("#"+"approve-id-"+reviewId).html("Yes");

                                console.log($("#"+"review-"+reviewId));

                                $('#review-details-modal').modal('hide');

                                $("#"+"review-"+reviewId).remove();
                                if($(".reviews").length == 0)
                                {
                                    $("#reviewsContainer").html("<h4>No Reviews Present </h4>");
                                }
                            }
                            
                        },
                        error:function()
                        {
                            console.log("The ajax request failed");
                        }
                   
                });
            }
    });

}


function approveReview()
{
    $('.approveReview').unbind('click');
    $('.approveReview').click(function(e){

        e.preventDefault();
        e.stopPropagation();

        var target      =   $(this);

        var reviewId    =   target.attr("data-id");

        $("#approve-link-"+ reviewId).html('Loading').removeClass('approveReview').addClass('hide');

        $("#approve-id-"+reviewId).html("Loading...");
    
        $.ajax({
                url:globalSiteUrl+'admin/reviewManagement/approveReview',
                method:'POST',
                data: {reviewId:reviewId},
                success:function(response)
                {
                    var result = JSON.parse(response);

                    if(result.status != "success")
                    {
                        $("#approve-id-"+reviewId).html("No");
                        $(target).html('Approve').addClass('approveReview').removeClass('hide');
                        alert(response);
                    }
                    else
                    {
                        $("#approve-id-"+reviewId).html("Yes");

                        $("#approve-link-"+reviewId).text("Approved").removeClass('approveReview');

                        $("#approve-link-"+reviewId).attr("disabled",'disabled');

                        $("#approve-link-"+reviewId).remove();

                        $('#review-details-modal').modal('hide');

                    }
                    
                },
                error:function()
                {
                    console.log("The ajax request failed");
                }
           
        });
    });

}

function monkeyPatchAutocomplete() {

  // Don't really need to save the old fn, 
  // but I could chain if I wanted to
  var oldFn = $.ui.autocomplete.prototype._renderItem;

    /*$.ui.autocomplete.prototype._renderItem = function( ul, item) {
      
        var re = new RegExp(this.term, "g");

        var t = item.label.replace(re,"<span class='selected'>" + this.term + "</span>");
        return $( "<li></li>" )
          .data( "item.autocomplete", item )
          .append( "<a>" + t + "</a>" )
          .appendTo( ul );
    };*/

    $.ui.autocomplete.prototype._renderItem = function( ul, item) {
      
        var re      =   new RegExp( "(" + this.term + ")", "gi" ),
        template    =   "<span class='selected'>$1</span>",
        t           =   item.label.replace( re, template );

        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + t + "</a>" )
            .appendTo( ul );
  };
}



$(document).ready(function() {

    schoolApp.loginCheck = function(){

        $('.loginCheck').unbind('click');
        $('.loginCheck').click(function(event){

            if(!isLoggedIn())
            {
                event.preventDefault();
                $('#loginPopup').trigger('click');
            }

        });
    }

    schoolApp.htmlspecialchars_decode = function(string, quote_style) {
      //       discuss at: http://phpjs.org/functions/htmlspecialchars_decode/
      //      original by: Mirek Slugen
      //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      //      bugfixed by: Mateusz "loonquawl" Zalega
      //      bugfixed by: Onno Marsman
      //      bugfixed by: Brett Zamir (http://brett-zamir.me)
      //      bugfixed by: Brett Zamir (http://brett-zamir.me)
      //         input by: ReverseSyntax
      //         input by: Slawomir Kaniecki
      //         input by: Scott Cariss
      //         input by: Francois
      //         input by: Ratheous
      //         input by: Mailfaker (http://www.weedem.fr/)
      //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      // reimplemented by: Brett Zamir (http://brett-zamir.me)
      //        example 1: htmlspecialchars_decode("<p>this -&gt; &quot;</p>", 'ENT_NOQUOTES');
      //        returns 1: '<p>this -> &quot;</p>'
      //        example 2: htmlspecialchars_decode("&amp;quot;");
      //        returns 2: '&quot;'

      var optTemp = 0,
        i = 0,
        noquotes = false;
      if (typeof quote_style === 'undefined') {
        quote_style = 2;
      }
      string = string.toString()
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>');
      var OPTS = {
        'ENT_NOQUOTES': 0,
        'ENT_HTML_QUOTE_SINGLE': 1,
        'ENT_HTML_QUOTE_DOUBLE': 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE': 4
      };
      if (quote_style === 0) {
        noquotes = true;
      }
      if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
        quote_style = [].concat(quote_style);
        for (i = 0; i < quote_style.length; i++) {
          // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
          if (OPTS[quote_style[i]] === 0) {
            noquotes = true;
          } else if (OPTS[quote_style[i]]) {
            optTemp = optTemp | OPTS[quote_style[i]];
          }
        }
        quote_style = optTemp;
      }
      if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
        string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
        // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
      }
      if (!noquotes) {
        string = string.replace(/&quot;/g, '"');
      }
      // Put this in last place to avoid escape being double-decoded
      string = string.replace(/&amp;/g, '&');

      return string;
    }

    function createModal(message, showLogin, currentElement)
    {
        $('#loginPopup').trigger('click');
    }

    if(typeof schoolApp == 'undefined')
    {
        schoolApp   =   {};
    }

    if(typeof schoolApp.peopleChartData != 'undefined')
    {

        var total       =   schoolApp.peopleChartData.total;
        var percent     =   ((schoolApp.peopleChartData.totalGraduationStud * 100) / total).toFixed(0);

        $('#demographics1').html('<h3 style="text-align:center;">% Grad students</h3>'+
                                    '<div style="display:table;margin: auto;">'+
                                        '<input type="text" data-readOnly="true" data-fgColor="#ff6b45" data-width="200" data-height="200" value="'+ percent +'" id="demographics1dial" class="dial" style="">'+
                                    '</div>'
                                );

        $("#demographics1dial").knob({
            'draw' : function () { 
                $(this.i).val(this.cv + '%')
            }
        });

        total           =   0;

        percent         =   0;

        total           =   schoolApp.peopleChartData.total;
        percent         =   ((schoolApp.peopleChartData.totalGradNonResident * 100) / total).toFixed(0);

        $('#demographics2').html('<h3 style="text-align:center;">% International Grad students</h3>'+
                                    '<div style="display:table;margin: auto;">'+
                                        '<input type="text" data-readOnly="true" data-fgColor="#ff6b45" data-width="200" data-height="200" value="'+ percent +'" id="demographics2dial" class="dial" style="">'+
                                    '</div>'
                                );

        $("#demographics2dial").knob({
            'draw' : function () { 
                $(this.i).val(this.cv + '%')
            }
        });

    }
    else
    {

    }

    

    $('#search').val('');
		
	if(document.getElementById('map-canvas'))
    {

	    function initialize() {
		    
            var myLatlng = new google.maps.LatLng(document.getElementById('map-canvas').getAttribute("latitude"),document.getElementById('map-canvas').getAttribute("longitude"));
		    
            var mapOptions = {
		        zoom: 14,
		        center: myLatlng
		    }

		    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

		    var marker = new google.maps.Marker({
		        position: myLatlng,
		        map: map,
		        title: 'Hello World!'
		    });

		}

		google.maps.event.addDomListener(window, 'load', initialize);

	}

    monkeyPatchAutocomplete();
	
	//HOME PAGE CODE	
	$("#search").autocomplete({
        source: schoolApp.site_url+"search",
        minLength: 1,
        select: function( event, ui ) {

            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
            callIsGoing     =   1;
            window.location = schoolApp.site_url+'home/school/'+ui.item.id+'/'+ui.item.URLTITLE;
        }
    });

    $("#searchCity").autocomplete({
        source: schoolApp.site_url+"search?isCity=1",
        minLength: 1,
        select: function( event, ui ) {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
            callIsGoing     =   1;
            window.location = schoolApp.site_url+'home/schools?city='+ui.item.id;
        }
    });

    /*$("#searchMajor").autocomplete({
        source: schoolApp.site_url+"search?isMajor=1",
        minLength: 1,
        select: function( event, ui ) {
            window.location = schoolApp.site_url+'home/schools?major='+ui.item.id;
        }
    });*/

    /*$("#search").keyup(function(event){

        if($(this).val() != '')
        {
            $('.ui-autocomplete .ui-menu-item').highlight($(this).val());
        }
        
    });*/

    var icons = {
      header: "ui-icon-circle-arrow-e",
      activeHeader: "ui-icon-circle-arrow-s"
    };

	//SCHOOLS LIST
    $( "#accordion" ).accordion({
      heightStyle: "content",
      active:"false",
      collapsible: true,
      icons: icons
    });

    /*$('.search_form').submit(function(event){

        event.preventDefault();

        if($('.ui-autocomplete').length > 0)
        {
            if($('.ui-autocomplete .ui-menu-item').length > 0)
            {
                $('.ui-autocomplete .ui-menu-item').first().trigger('click');
            }
        }

    });*/

    $(document).off('keyup', '.search_form input[type="text"]')
    $(document).on('keyup', '.search_form input[type="text"]', function(event){

        if(event.which == 13) 
        {
            if($('.ui-autocomplete').length > 0)
            {

                if(+callIsGoing == 0)
                {
                    if($('.ui-autocomplete .ui-menu-item.ui-state-focus').length > 0)
                    {
                        $('.ui-autocomplete .ui-menu-item.ui-state-focus').first().trigger('click');
                    }
                    else if($('.ui-autocomplete .ui-menu-item').length > 0)
                    {
                        $('.ui-autocomplete .ui-menu-item').first().trigger('click');
                    }
                }
                else
                {
                    callIsGoing = 0;
                }

                
            }
        }

    });

    $('#disconnectFacebook').unbind('click');
    $('#disconnectFacebook').click(function(event){

        event.preventDefault();

        var answer = confirm('Are you sure you want to disconnect facebook?');

        if(answer)
        {

            var req = new XMLHttpRequest();
            req.open("POST",schoolApp.site_url+"home/disconnectFacebook",true);
            req.onload = function(data){

                var response = JSON.parse(data.target.response);
                
                if(response.status == "success")
                {
                   window.location.reload();
                }
                
            }
            
            req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            req.send(null);

        }    

    })


    schoolApp.schoolAction = function(){

        $('.saveSchool').unbind('click');
        $('.saveSchool').click(function(event){

            event.preventDefault();

            var currentElement = $(this);

            if(currentElement.attr('isSaved') != 1)
            {   

                var userId = currentElement.attr('dataUserId');

                if(+userId != 0)
                {

                    var schoolId = currentElement.attr('dataSchoolId');

                    //$(this).text('Saving...');

                    var req = new XMLHttpRequest();
                    req.open("POST",schoolApp.site_url+"home/saveSchool",true);
                    req.onload = function(data){

                        var response = JSON.parse(data.target.response);
                                     
                        if(response.status == "success")
                        {
                            currentElement.addClass('btn-link-success');
                            currentElement.attr('isSaved',1);

                            if(currentElement.attr('isAct') == 2)
                            { 
                                currentElement.text('Remove from saved schools');
                            }
                            else
                            {
                                currentElement.parent().html('<span class="glyphicon glyphicon-ok savedSchool" title="School Saved"></span><span class="following">Saved</span>');
                            }   
                        }

                    }
                    
                    req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    req.send("userId="+userId+
                    "&schoolId="+schoolId);

                }
                else
                {

                    createModal('Please Login/Register to Save this school!', 1, currentElement);
                }

            }
            else if(currentElement.attr('isAct') == 1 || currentElement.attr('isAct') == 2)
            {   
                
                var userId = currentElement.attr('dataUserId');
                
                if(+userId != 0)
                {   
                    var schoolId = currentElement.attr('dataSchoolId');
                    
                    //$(this).text('Removing...');

                    var req = new XMLHttpRequest();
                    req.open("POST",schoolApp.site_url+"home/saveSchool",true);
                    req.onload = function(data){

                        var response = JSON.parse(data.target.response);
                            
                        if(response.status == "success")
                        {   

                            if(currentElement.attr('isAct') == 1)
                            { 
                                //currentElement.attr('data-id');

                                $("#"+currentElement.attr('data-id')).remove();
                                if($('#mySavedSchoolstable tr>td:visible').length == 0)
                                {
                                   $('#mySavedSchoolstable').html(
                                            '<div class="col-heading">Saved Schools</div>'+
                                            '<div class="form-wrapper no-data">'+
                
                                                '<div class="icon-bullhorn icon"></div>'+
                                                
                                                '<div class="msg"> You do not have any saved schools.</div>'+


                                           '</div>'
                                   

                                        ) 
                                }
                               // currentElement.parent().parent().parent().parent().hide();
                            }
                            else
                            {
                                currentElement.text("Save School");
                                currentElement.attr('isSaved',0);
                            }  

                        }

                    }
                        
                    req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    
                    req.send("userId="+userId+
                        "&schoolId="+schoolId+
                        "&isDelete=1");

                }
                else
                {
                    createModal('Please Login/Register to Save this school!', 1, currentElement);
                }
            }    

        })


        $('.voteSchool').unbind('click');
        $('.voteSchool').click(function(event){

            console.log("clicked vote school");

            event.preventDefault();

            var currentElement = $(this);

            var userId = currentElement.attr('dataUserId');

            var schoolId = currentElement.attr('dataSchoolId');

            if(currentElement.attr('isVoted') != 1)
            {
                if(+userId != 0)
                {

                    var req = new XMLHttpRequest();
                    req.open("POST",schoolApp.site_url+"home/voteSchool",true);
                    req.onload = function(data){

                        var response = JSON.parse(data.target.response);
                                
                        if(response.status == "success")
                        {
                            
                            currentElement.addClass('Voted');
                            //currentElement.find('.glyphicon-thumbs-up').removeClass('glyphicon-thumbs-up').addClass('glyphicon-thumbs-down');
                            currentElement.attr('isVoted',1);
                                
                            var countElem   =   currentElement.parent().find('.count');
                            var voteText    =   currentElement.parent().find('.voteText');
                            
                            countElem.addClass('voted')
                            
                            if(countElem.text() == '')
                            {
                                countElem.text(1);
                                voteText.text('Vote');
                            }
                            else
                            {   

                                var lastCount = $.trim(countElem.text());

                                if(lastCount == "")
                                {
                                   lastCount = 0;
                                }
                                    
                                countElem.text(parseInt(lastCount)+1);
                                //if(parseInt(lastCount)+1 == 1)
                                {
                                    //voteText.text('Vote');
                                    voteText.html('<i class="glyphicon glyphicon-thumbs-up"></i>');
                                }
                               /* else
                                {
                                    voteText.text('Votes');
                                }*/
                                

                            }

                           
                            console.log(currentElement.attr("school-page"));
                            if (currentElement.attr("data-page") != "undefined" && currentElement.attr("data-page")=="school-page") 
                            {

                                //console.log(currentElement);
                               currentElement.remove();
                            }
                            else{
                                //console.log("in the else");
                                console.log(currentElement);
                            }

                           
                            
                        }

                    }
                    
                    req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    req.send("userId="+userId+
                    "&schoolId="+schoolId);

                }
                else
                {
                    createModal('Please Login/Register to vote!', 1, currentElement);
                }
            }
            else
            {
                /*var userId = currentElement.attr('dataUserId');

                if(+userId != 0)
                {
                    var req = new XMLHttpRequest();
                    req.open("POST",schoolApp.site_url+"home/downVoteSchool",true);
                    req.onload = function(data){

                        var response = JSON.parse(data.target.response);
                                
                        if(response.status == "success")
                        {
                            currentElement.removeClass('Voted');
                            currentElement.attr('isVoted',0);
                            currentElement.find('.glyphicon-thumbs-down').removeClass('glyphicon-thumbs-down').addClass('glyphicon-thumbs-up');
                            var countElem = currentElement.parent().find('.count');
                            
                            countElem.removeClass('voted')
                            
                            var finalCount  =   parseInt(countElem.text())-1;

                            if(finalCount > 0)
                            {
                                countElem.text(parseInt(countElem.text())-1);
                            }
                            else
                            {
                                countElem.text('');
                            }
                        }

                    }
                    
                    req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    req.send("userId="+userId+
                    "&schoolId="+schoolId);
                }
                else
                {
                    createModal('Please Login/Register to vote!', 1);
                }*/
            }

        })

        $('.voteSchoolTemp').unbind('click');
        $('.voteSchoolTemp').click(function(event){

            $('.voteSchool').trigger('click');

        });

    }

    schoolApp.schoolAction();

})	