<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Google extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->client = new Google_Client();

		$this->client->setClientId($this->config->item('google_client_id'));
		$this->client->setClientSecret($this->config->item('google_client_secret'));
		$this->client->setRedirectUri(site_url('google/callback'));
		$this->client->setAccessType('offline');
		$this->client->setScopes("profile email");

		$this->load->model('user_model');
		$this->load->model('tank_auth/users');
	}

	public function auth()
	{
		$authUrl = $this->client->createAuthUrl();

		header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));

	}

	public function callback()
	{
		if (isset($_GET['code'])) {

			try{
				$this->client->authenticate($_GET['code']);

				$token_data = $this->client->verifyIdToken()->getAttributes();

				

				$oauth = new Google_Service_Oauth2($this->client);

        		$googleData 	=	$oauth->userinfo->get();

        		$googleId 		=	$googleData['id'];

        		$googleData 	=	json_encode($googleData);

				$this->session->set_userdata('google_open_id', $googleId);

				$user = $this->user_model->get_user_by_sm(array('google_open_id' => $googleId), 'google_open_id');

				$user_id = $this->session->userdata('user_id');

				if( sizeof($user) == 0  && !$user_id) 
				{
					$this->db->insert('profileStore', array(
															'profileId'	=>	$googleId,
															'data'		=>	$googleData
														)
									);

					$insertId 	=	$this->db->insert_id();

					$this->session->set_userdata('google_insert_id', $insertId);
					
					redirect('auth_other/fill_user_info'); 
				}
				else if(sizeof($user) == 0 && $user_id)
				{

					$usersCount	=	$this->db->select('id')
												->from('user_profiles')
													->where('user_id', $user_id)
														->get()
															->result_array();



					if(count($usersCount) > 0)
					{
						$this->db->where('user_id', $user_id)
									->set('google_open_id', $googleId)
										->set('google_data', $googleData)
											->where('user_id', $user_id)
												->update('user_profiles');
					}
					else
					{
						$insertArray 	=	array(
													'user_id'			=>	$user_id,
													'google_open_id'	=>	$googleId,
													'googleData'		=>	$googleData
												);

						$this->db->insert('user_profiles', $insertArray);

					}

					redirect('profile');
				}
				else if(!$user_id)
				{

					$this->db->where('user_id', $user[0]->id)
								->set('google_data', $googleData)
									->update('user_profiles');

					$user 		=	$user[0];

					$picture 	=	getUserPicture($user);

					$this->session->set_userdata(array(
							'user_id'	=> $user->id,
							'email'		=> $user->email,
							'username'	=> $user->username,
							'role'		=> $user->role,
							'picture'	=>	$picture,
							'facebook_id'	=> $user->facebook_id,
							'onlyGradSchools'	=> $user->onlyGradSchools,
							'status'	=> ($user->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED,
					));
					
					//$this->tank_auth->clear_login_attempts($user[0]->email); can't run this when doing FB
					$this->users->update_login_info( $user->id, $this->config->item('login_record_ip'), 
													 $this->config->item('login_record_time'));

					if($user->role == 1)
					{
						redirect('auth', 'refresh');
					}	
					else
					{
						redirect('admin', 'school');
					}
				}
				else if($user_id != $user[0]->id)
				{
					$usersCount	=	$this->db->select('id')
												->from('user_profiles')
													->where('user_id', $user_id)
														->get()
															->result_array();

					if(count($usersCount) > 0)
					{
						$this->db->where('user_id', $user_id)
									->set('google_open_id', $googleId)
										->set('google_data', $googleData)
											->where('user_id', $user_id)
												->update('user_profiles');
					}
					else
					{
						$insertArray 	=	array(
													'user_id'			=>	$user_id,
													'google_open_id'	=>	$googleId,
													'google_Data'		=>	$googleData
												);

						$this->db->insert('user_profiles', $insertArray);

					}

					$this->db->set('google_open_id', NULL)
								->set('google_Data', NULL)
									->where('google_open_id', $googleId)
										->where('user_id', $user[0]->id)
											->update('user_profiles');

					redirect('profile');

				}
				else
				{

					$this->db->where('user_id', $user[0]->id)
								->set('google_open_id', $googleId)
									->set('google_data', $googleData)
										->update('user_profiles');

					$user 		=	$user[0];

					$picture 	=	getUserPicture($user);

					$this->session->set_userdata(array(
							'user_id'	=> $user->id,
							'email'		=> $user->email,
							'username'	=> $user->username,
							'role'		=> $user->role,
							'picture'	=>	$picture,
							'facebook_id'	=> $user->facebook_id,
							'onlyGradSchools'	=> $user->onlyGradSchools,
							'status'	=> ($user->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED,
					));
					
					//$this->tank_auth->clear_login_attempts($user[0]->email); can't run this when doing FB
					$this->users->update_login_info( $user->id, $this->config->item('login_record_ip'), 
													 $this->config->item('login_record_time'));

					if($user->role == 1)
					{
						redirect('auth', 'refresh');
					}	
					else
					{
						redirect('admin', 'school');
					}

				}
			}
			catch(Exception $e)
			{
				$this->session->set_flashdata('message', 'googleLoginError');
				redirect(site_url());
			}

		}
		else
		{
			$this->session->set_flashdata('message', 'googleLoginError');
			redirect(site_url());
		}
	}

	public function disconnect()
	{

		$data = $this->user_model->disconnectGoogle();

		redirect('profile');

	}

}