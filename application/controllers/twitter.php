<?php
/**
 * Twitter OAuth library.
 * Sample controller.
 * Requirements: enabled Session library, enabled URL helper
 * Please note that this sample controller is just an example of how you can use the library.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Twitter extends CI_Controller
{
	/**
	 * TwitterOauth class instance.
	 */
	private $connection;
	
	/**
	 * Controller constructor
	 */
	function __construct()
	{
		parent::__construct();
		// Loading TwitterOauth library. Delete this line if you choose autoload method.
		$this->load->library('twitteroauth');
		// Loading twitter configuration.

		$this->load->model('user_model');
		$this->load->model('tank_auth/users');
		
		if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			// If user already logged in
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('access_token'),  $this->session->userdata('access_token_secret'));
		}
		elseif($this->session->userdata('request_token') && $this->session->userdata('request_token_secret'))
		{
			// If user in process of authentication
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('request_token'), $this->session->userdata('request_token_secret'));
		}
		else
		{
			// Unknown user
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'));
		}
	}
	
	/**
	 * Here comes authentication process begin.
	 * @access	public
	 * @return	void
	 */
	public function auth()
	{

		if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			$this->reset_session();
			redirect(site_url('/twitter/auth'));
		}
		else
		{
			// Making a request for request_token
			$request_token = $this->connection->getRequestToken(site_url('twitter/callback'));

			$this->session->set_userdata('request_token', $request_token['oauth_token']);
			$this->session->set_userdata('request_token_secret', $request_token['oauth_token_secret']);
			
			if($this->connection->http_code == 200)
			{
				$url = $this->connection->getAuthorizeURL($request_token);
				redirect($url);
			}
			else
			{
				// An error occured. Make sure to put your error notification code here.
				$this->session->set_flashdata('errorMessage', 'twitterLoginError');

				redirect(site_url());
			}
		}
	}
	
	/**
	 * Callback function, landing page for twitter.
	 * @access	public
	 * @return	void
	 */
	public function callback()
	{

		if($this->input->get('oauth_token') && $this->session->userdata('request_token') !== $this->input->get('oauth_token'))
		{
			$this->reset_session();
			redirect(site_url('/twitter/auth'));
		}
		else
		{
			if($this->input->get('oauth_verifier'))
			{
				$access_token = $this->connection->getAccessToken($this->input->get('oauth_verifier'));
			
				if ($this->connection->http_code == 200)
				{

					$settings 	= 	array(
											'oauth_access_token' => $access_token["oauth_token"],
											'oauth_access_token_secret' => $access_token["oauth_token_secret"],
											'consumer_key' => $this->config->item('twitter_consumer_token'),
											'consumer_secret' => $this->config->item('twitter_consumer_secret')
										);


					$url = 'https://api.twitter.com/1.1/users/show.json?screen_name='. $access_token['screen_name'];

					$userData 	=	$this->connection->get($url, $settings);

					if(isset($userData))
					{
						if(isset($userData->profile_image_url_https))
						{
							$userData->picture 	= 	$userData->profile_image_url_https;
						}
					}

					$userData 	=	json_encode($userData);

					$this->session->set_userdata('twitter_id', $access_token['user_id']);	
					
					$user = $this->user_model->get_user_by_sm(array('twitter_id' => $access_token['user_id']), 'twitter_id');

					$user_id = $this->session->userdata('user_id');

					$this->reset_session();

					if(sizeof($user) == 0 && !$user_id)
					{
						$this->db->insert('profileStore', array(
																'profileId'	=>	$access_token['user_id'],
																'data'		=>	$userData
															)
										);

						$insertId 	=	$this->db->insert_id();

						$this->session->set_userdata('twitter_insert_id', $insertId);

						redirect('auth_other/fill_user_info');
					}
					else if(sizeof($user) == 0 && $user_id)
					{

						$usersCount	=	$this->db->select('id')
													->from('user_profiles')
														->where('user_id', $user_id)
															->get()
																->result_array();



						if(count($usersCount) > 0)
						{
							$this->db->where('user_id', $user_id)
										->set('twitter_id', $access_token['user_id'])
											->set('twitter_data', $userData)
												->where('user_id', $user_id)
													->update('user_profiles');
						}
						else
						{
							$insertArray 	=	array(
														'user_id'		=>	$user_id,
														'twitter_id'	=>	$access_token['user_id'],
														'twitter_data'	=>	$userData
													);

							$this->db->insert('user_profiles', $insertArray);

						}

						redirect('profile');
					}
					else if(!$user_id)
					{

						$this->db->set('twitter_data', $userData)
									->where('user_id', $user[0]->id)
										->update('user_profiles');

						$user 		=	$user[0];

						$picture 	=	getUserPicture($user);

						$this->session->set_userdata(array(
								'user_id'	=> $user->id,
								'email'		=> $user->email,
								'username'	=> $user->username,
								'role'		=> $user->role,
								'picture'	=>	$picture,
								'facebook_id'	=> $user->facebook_id,
								'onlyGradSchools'	=> $user->onlyGradSchools,
								'status'	=> ($user->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED,
						));
						
						//$this->tank_auth->clear_login_attempts($user[0]->email); can't run this when doing FB
						$this->users->update_login_info( $user->id, $this->config->item('login_record_ip'), 
														 $this->config->item('login_record_time'));

						if($user->role == 1)
						{
							redirect('auth', 'refresh');
						}	
						else
						{
							redirect('admin', 'school');
						}
					}
					else if($user_id != $user[0]->id)
					{

						$usersCount	=	$this->db->select('id')
												->from('user_profiles')
													->where('user_id', $user_id)
														->get()
															->result_array();

						if(count($usersCount) > 0)
						{
							$this->db->where('user_id', $user_id)
										->set('twitter_id', $access_token['user_id'])
											->set('twitter_data', $userData)
												->where('user_id', $user_id)
													->update('user_profiles');
						}
						else
						{
							$insertArray 	=	array(
														'user_id'		=>	$user_id,
														'twitter_id'	=>	$access_token['user_id'],
														'twitter_data'	=>	$userData
													);

							$this->db->insert('user_profiles', $insertArray);

						}

						$this->db->set('twitter_id', NULL)
									->set('twitter_data', NULL)
										->where('twitter_id', $access_token['user_id'])
											->where('user_id', $user[0]->id)
												->update('user_profiles');

						redirect('profile');

					}
					else
					{
						$this->db->set('twitter_data', $userData)
									->where('user_id', $user[0]->id)
										->update('user_profiles');
										
						$user 		=	$user[0];

						$picture 	=	getUserPicture($user);

						$this->session->set_userdata(array(
								'user_id'	=> $user->id,
								'email'		=> $user->email,
								'username'	=> $user->username,
								'role'		=> $user->role,
								'picture'	=>	$picture,
								'facebook_id'	=> $user->facebook_id,
								'onlyGradSchools'	=> $user->onlyGradSchools,
								'status'	=> ($user->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED,
						));
						
						//$this->tank_auth->clear_login_attempts($user[0]->email); can't run this when doing FB
						$this->users->update_login_info( $user->id, $this->config->item('login_record_ip'), 
														 $this->config->item('login_record_time'));

						if($user->role == 1)
						{
							redirect('auth', 'refresh');
						}	
						else
						{
							redirect('admin', 'school');
						}

					}
					
				}
			}
			else
			{
				redirect(site_url());
			}
			
		}
	}
		
	/**
	 * Reset session data
	 * @access	private
	 * @return	void
	 */
	private function reset_session()
	{
		$this->session->unset_userdata('access_token');
		$this->session->unset_userdata('access_token_secret');
		$this->session->unset_userdata('request_token');
		$this->session->unset_userdata('request_token_secret');
		$this->session->unset_userdata('twitter_user_id');
		$this->session->unset_userdata('twitter_screen_name');
	}


	public function disconnect()
	{

		$data = $this->user_model->disconnectTwitter();

		redirect('profile');

	}

}
/* End of file twitter.php */
/* Location: ./application/controllers/twitter.php */