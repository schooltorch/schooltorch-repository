<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("school_model");
	}

	/* Updated And Moved By
		NAME: Ranjit Randive
		DESC: All Profile Get Calls
		update: can be removed..31/1/15
	*/
	public function index($section = 1)
	{

		$this->basicInfo();

		/*
		if($this->tank_auth->is_logged_in()) 
		{
			//$section = $this->input->get('section');

			$data['user_id'] = $this->session->userdata('user_id');
			$data['user'] = $this->user_model->get_user_details( $this->session->userdata('user_id'));

			if($section == 1){

				$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');				
				$this->form_validation->set_rules('firstName', 'First Name', 'trim|required|xss_clean');				
				$this->form_validation->set_rules('lastName', 'Last Name', 'trim|required|xss_clean');

				if ($this->form_validation->run()) {

					$data = $this->user_model->update_basic_info();

				}
				

			}else if($section == 2){

				$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
				$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length').']|max_length['.$this->config->item('password_max_length').']|alpha_dash');
				$this->form_validation->set_rules('confirm_new_password', 'Confirm Password', 'trim|required|xss_clean|matches[new_password]');


				if ($this->form_validation->run()) {
										// validation ok
					if ($this->tank_auth->change_password(
						$this->form_validation->set_value('old_password'),
						$this->form_validation->set_value('new_password')
						)) {	

						$data['status'] = 'success';
						$data['message'] = 'Password Updated';
						
					} 
					else 
					{	
						
						// fail
						$errors = $this->tank_auth->get_error_message();
						
						foreach ($errors as $k => $v)
						{
							$data['errors'][$k] = $this->lang->line($v);
						}

					}
				
				}


			}else if($section == 5){

			
			}
			else if($section == 3){

				$data['mySchools'] = $this->school_model->readSchools(1);


			
			}else
			{

				$this->form_validation->set_rules('location', 'Location', 'trim|xss_clean');				
				$this->form_validation->set_rules('gmat', 'GMAT score', 'trim|xss_clean|float');				
				$this->form_validation->set_rules('gre', 'GRE score', 'trim|xss_clean|float');

				$this->form_validation->set_rules('gpa', 'Undergrad GPA on a 4.0 scale', 'trim|xss_clean|float');
				$this->form_validation->set_rules('undergradInstitute', 'Undergrad instituition', 'trim|xss_clean');
				$this->form_validation->set_rules('budget', 'Budget', 'trim|xss_clean');

				$this->form_validation->set_rules('yearsOfExp', 'Years of Experience', 'trim|xss_clean|numeric');

				


				if($this->form_validation->run()) 
				{
					$result = $this->user_model->updateBackGroundSection();

					$result = $this->user_model->getBackGroundSection();

					if($result['status'] == "success")
					{
						$data = $result['data'];
						$data['dataFlag'] = 1;
					}
					else
					{
						redirect("/home/profile");
					}

					

				}
				else
				{

					if(count($_POST) > 0)
					{
						$errors = $this->tank_auth->get_error_message();
						
						foreach ($errors as $k => $v)
						{
							$data['errors'][$k] = $this->lang->line($v);
						}


						
					}
					else
					{
						$result = $this->user_model->getBackGroundSection();

						if($result['status'] == "success")
						{
							$data = $result['data'];
							$data['dataFlag'] = 1;
						}
						else
						{
							redirect("/home/profile");
						}
						
					}
					
				}


			}



			$data['page_class'] = 'page_settings';
			$data['page_id'] = '';
			$data['section'] = $section;

		

			$this->load->view('common/inc_header', $data);
			$this->load->view('common/menu_app', $data);
			$this->load->view('profile/profile', $data);
			$this->load->view('common/inc_footer', $data);


			

		}else{

			redirect('home/index');
		}*/

	}

	function basicInfo()
	{
		if($this->tank_auth->is_logged_in()) 
		{

			$data['user_id'] = $this->session->userdata('user_id');
			$data['user'] = $this->user_model->get_user_details( $this->session->userdata('user_id'));
			
			//Updated By Jayant Wankhede. Removed required attribute
			$this->form_validation->set_rules('firstName', 'First Name', 'trim|xss_clean');				
			$this->form_validation->set_rules('lastName', 'Last Name', 'trim|xss_clean');

			if ($this->form_validation->run()) {

				$data = $this->user_model->update_basic_info();

			}

			$data['user'] = $this->user_model->get_user_details( $this->session->userdata('user_id'));

			$data['page_class'] = 'page_settings';
			$data['page_id'] = '';
			$data['section'] = 1 ;

			$data['page_title']     = pageTitleGenerator(array("Profile Settings","Login Settings"));
			$this->load->view('common/inc_header', $data);
			$this->load->view('common/menu_app', $data);
			$this->load->view('profile/profile', $data);
			$this->load->view('common/inc_footer', $data);
		}
		else{

			redirect('home/index');
		}

	}

	function changePassword()
	{
		if($this->tank_auth->is_logged_in()) 
		{
			$data['user_id'] = $this->session->userdata('user_id');
			$data['user'] = $this->user_model->get_user_details( $this->session->userdata('user_id'));

			$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length').']|max_length['.$this->config->item('password_max_length').']|alpha_dash');
			$this->form_validation->set_rules('confirm_new_password', 'Confirm Password', 'trim|required|xss_clean|matches[new_password]');


			if($this->form_validation->run()) 
			{
				if ($this->tank_auth->change_password(
					$this->form_validation->set_value('old_password'),
					$this->form_validation->set_value('new_password')
					)) {	

					$data['status'] = 'success';
					$data['message'] = 'Password Updated';
					
					redirect('profile');

				} 
				else 
				{	
					
					// fail
					$errors = $this->tank_auth->get_error_message();
					
					foreach ($errors as $k => $v)
					{
						$data['errors'][$k] = $this->lang->line($v);
					}

				}
			
			}

			$data['page_class'] = 'page_settings';
			$data['page_id'] = '';
			$data['section'] = 2 ;

			$data['page_title']     = pageTitleGenerator(array("Profile Settings","Login Settings"));
			$this->load->view('common/inc_header', $data);
			$this->load->view('common/menu_app', $data);
			$this->load->view('profile/profile', $data);
			$this->load->view('common/inc_footer', $data);
		}
		else{

			redirect('home/index');
		}

	}

	function background()
	{	
		if($this->tank_auth->is_logged_in()) 
		{

			$data['user_id'] = $this->session->userdata('user_id');
			$data['user'] = $this->user_model->get_user_details( $this->session->userdata('user_id'));

			$this->form_validation->set_rules('location', 'Location', 'trim|xss_clean');				
			$this->form_validation->set_rules('gmat', 'GMAT score', 'trim|xss_clean|float');				
			$this->form_validation->set_rules('gre', 'GRE score', 'trim|xss_clean|float');

			$this->form_validation->set_rules('gpa', 'Undergrad GPA on a 4.0 scale', 'trim|xss_clean|float');
			$this->form_validation->set_rules('undergradInstitute', 'Undergrad instituition', 'trim|xss_clean');
			$this->form_validation->set_rules('budget', 'Budget', 'trim|xss_clean');

			$this->form_validation->set_rules('yearsOfExp', 'Years of Experience', 'trim|xss_clean|numeric');

			


			if($this->form_validation->run()) 
			{
				$result = $this->user_model->updateBackGroundSection();

				$result = $this->user_model->getBackGroundSection();

				if($result['status'] == "success")
				{
					$data = $result['data'];
					$data['dataFlag'] = 1;
				}
				else
				{
					redirect("/home/profile");
				}

			}
			else
			{

				if(count($_POST) > 0)
				{
					$errors = $this->tank_auth->get_error_message();
					
					foreach ($errors as $k => $v)
					{
						$data['errors'][$k] = $this->lang->line($v);
					}
					
				}
				else
				{
					$result = $this->user_model->getBackGroundSection();

					if($result['status'] == "success")
					{
						$data = $result['data'];
						$data['dataFlag'] = 1;
					}
					else
					{
						redirect("/home/profile");
					}
					
				}
			}
			$data['page_class'] = 'page_settings';
			$data['page_id'] = '';
			$data['section'] = 4 ;

			$data['page_title']     = pageTitleGenerator(array("Profile Settings","Background Section"));
			$this->load->view('common/inc_header', $data);
			$this->load->view('common/menu_app', $data);
			$this->load->view('profile/profile', $data);
			$this->load->view('common/inc_footer', $data);
		}
		else{

			redirect('home/index');
		}


	}

	function savedSchools()
	{
		if($this->tank_auth->is_logged_in()) 
		{

			$data['user_id'] = $this->session->userdata('user_id');
			$data['user'] = $this->user_model->get_user_details( $this->session->userdata('user_id'));

			$data['mySchools'] = $this->school_model->readSchools(1);

			$data['page_class'] = 'page_settings';
			$data['page_id'] = '';
			$data['section'] = 3 ;

			$data['page_title']     = pageTitleGenerator(array("Profile Settings","Saved Schools"));
			$this->load->view('common/inc_header', $data);
			$this->load->view('common/menu_app', $data);
			$this->load->view('profile/profile', $data);
			$this->load->view('common/inc_footer', $data);


		}
		else
		{

			redirect('home/index');
		}


	}

	/*
		NAME: Neeraj
		DESC: to update the user email settings.29/1/15.
	*/

	public function updateEmailSettings()
	{
		$data 	=	validate_my_params(
										array(
												'key'	=>	'required|valid_opts[all,blogUpdatesEmail,reviewUpdatesEmail]',
												'value'	=>	'required|valid_opts[1,0]'
											)
									);

		if($this->tank_auth->is_logged_in()) 
		{
			if($data['status']	==	'success')
			{

				$data 	=	$data['data'];

				$result 	=	$this->user_model->updateEmailSettings($data);

				echo json_encode($result);
			}
			else
			{
				echo json_encode($data);
			}
		}
		else
		{
			redirect('home/index');
		}
	}

	/*
		NAME: Ranjit Randive
		DESC: Getting Current Email Settings
	*/
	public function emailSettings()
	{
		if($this->tank_auth->is_logged_in()) 
		{
			$data =		array();

			$data['page_class'] = 'page_settings';
			$data['page_id'] = '';
			$data['section'] = 6;

			$permissions	=	$this->user_model->getCurrentUserPermissions();

			$data['blogUpdatesEmail']	=	$permissions[0]['blogUpdatesEmail'];
			$data['reviewUpdatesEmail']	=	$permissions[0]['reviewUpdatesEmail'];

			$data['page_title']     = pageTitleGenerator(array("Profile Settings","Email Settings"));

			$this->load->view('common/inc_header', $data);
			$this->load->view('common/menu_app', $data);
			$this->load->view('profile/profile', $data);
			$this->load->view('common/inc_footer', $data);
		}
		else
		{
			redirect('home/index');
		}
	}


	/*
		NAME: Ranjit Randive
		DESC: Update Profile Photo
	*/
	public function profilePhotoSettings($flag=0)
	{
		if($this->tank_auth->is_logged_in()) 
		{
			
			$result 	=	$this->user_model->profilePhotoSettings($flag);

			if($result['status'] == 'success')
			{
				redirect('profile');
			}
			else
			{
				redirect('home/index');
			}
			
		}
		else
		{
			redirect('home/index');
		}
	}

}
