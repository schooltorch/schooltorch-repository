<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index(){

		$this->form_validation->set_rules('name', 'Full Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('subject', 'Subject','required');
		$this->form_validation->set_rules('message', 'Message', 'required');

		$data['show_captcha'] = TRUE;
		$data['use_recaptcha'] = TRUE;
	
		if ($data['use_recaptcha'])
			$this->form_validation->set_rules('recaptcha_response_field', 'Confirmation Code', 'trim|xss_clean|required|callback__check_recaptcha');
		else
			$this->form_validation->set_rules('captcha', 'Confirmation Code', 'trim|xss_clean|required|callback__check_captcha');
		

		if($this->form_validation->run() == FALSE)
		{
			$data['page_class'] = 'page_settings';
			$data['page_id'] 	= ''; 

		
			if ($data['use_recaptcha']) {
				$data['recaptcha_html'] = $this->_create_recaptcha();
			} else {
				$data['captcha_html'] = $this->_create_captcha();
			}

			$data['page_title']     = pageTitleGenerator(array("Contact Us")); 
			$this->load->view('common/inc_header', $data);
			$this->load->view('common/menu_app', $data);
			$this->load->view('home/contact-us',$data);
			$this->load->view('common/inc_footer', $data);
		}
		else
		{	
			$data 	=	array();
			$data 	=	array(
								'name'		=>	$this->input->post('name'),
								'subject'		=>	$this->input->post('subject'),
								'email'		=>	$this->input->post('email'),
								'message'	=>	$this->input->post('message'),
								'site_name'	=>	$this->config->item('siteName')
							);

			$email 	=	$this->input->post('email');
			$type	=	'contactUs';

			send_email($type, $this->config->item('siteEmail'), $data);

			$data = array();
			$data['page_class'] = 'page_settings
					';
			$data['page_id'] 	= ''; 
			$data['page_title']     = pageTitleGenerator(array("Contact Us")); 

			$this->load->view('common/inc_header', $data);
			$this->load->view('common/menu_app', $data);
			$this->load->view('email/thankYou',$data);
			$this->load->view('common/inc_footer', $data);	

		}

	}


		/**
	 * Create CAPTCHA image to verify user as a human
	 *
	 * @return	string
	 */
	function _create_captcha()
	{
		$this->load->helper('captcha');

		$cap = create_captcha(array(
			'img_path'		=> './'.$this->config->item('captcha_path'),
			'img_url'		=> base_url().$this->config->item('captcha_path'),
			'font_path'		=> './'.$this->config->item('captcha_fonts_path'),
			'font_size'		=> $this->config->item('captcha_font_size'),
			'img_width'		=> $this->config->item('captcha_width'),
			'img_height'	=> $this->config->item('captcha_height'),
			'show_grid'		=> $this->config->item('captcha_grid'),
			'expiration'	=> $this->config->item('captcha_expire'),
		));

		// Save captcha params in session
		$this->session->set_flashdata(array(
				'captcha_word' => $cap['word'],
				'captcha_time' => $cap['time'],
		));

		return $cap['image'];
	}

	/**
	 * Callback function. Check if CAPTCHA test is passed.
	 *
	 * @param	string
	 * @return	bool
	 */
	function _check_captcha($code)
	{
		$time = $this->session->flashdata('captcha_time');
		$word = $this->session->flashdata('captcha_word');

		list($usec, $sec) = explode(" ", microtime());
		$now = ((float)$usec + (float)$sec);

		if ($now - $time > $this->config->item('captcha_expire')) {
			$this->form_validation->set_message('_check_captcha', $this->lang->line('auth_captcha_expired'));
			return FALSE;

		} elseif (($this->config->item('captcha_case_sensitive') AND
				$code != $word) OR
				strtolower($code) != strtolower($word)) {
			$this->form_validation->set_message('_check_captcha', $this->lang->line('auth_incorrect_captcha'));
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * Create reCAPTCHA JS and non-JS HTML to verify user as a human
	 *
	 * @return	string
	 */
	function _create_recaptcha()
	{
		$this->load->helper('recaptcha');

		// Add custom theme so we can get only image
		$options = "<script>var RecaptchaOptions = {theme: 'custom', custom_theme_widget: 'recaptcha_widget'};</script>\n";

		// Get reCAPTCHA JS and non-JS HTML
		$html = recaptcha_get_html($this->config->item('recaptcha_public_key'));

		return $options.$html;
	}

	/**
	 * Callback function. Check if reCAPTCHA test is passed.
	 *
	 * @return	bool
	 */
	function _check_recaptcha()
	{
		$this->load->helper('recaptcha');

		$resp = recaptcha_check_answer($this->config->item('recaptcha_private_key'),
				$_SERVER['REMOTE_ADDR'],
				$_POST['recaptcha_challenge_field'],
				$_POST['recaptcha_response_field']);

		if (!$resp->is_valid) {
			$this->form_validation->set_message('_check_recaptcha', $this->lang->line('auth_incorrect_captcha'));
			return FALSE;
		}
		return TRUE;
	}
}
