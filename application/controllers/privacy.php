<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Privacy extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		
		$data['page_class'] = 'innerPage';
		$data['page_id'] 	= 'terms'; 
		$data['isPrivacy']  = 1;//'privacy';

		$this->load->view('common/inc_header', $data);
		$this->load->view('common/menu_app', $data);
		$this->load->view('home/combinedView', $data);
		$this->load->view('common/inc_footer', $data);

	}
}
