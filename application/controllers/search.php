<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model("search_model");
	}
	
	
	public function index()
	{	
		if($this->input->get('term'))
		{
			$data = $this->search_model->get($this->input->get('term'));
			
			$return = array();

			if(count($data)>0){

				foreach ($data as $key => $value) {

					if(!isset($value['category']))
					{
						$value['category'] 	=	'';
					}
					
					$temp = array(	
									'id' 		=>	$key,
									'label'		=>	$value['name'],
									'URLTITLE'	=>	url_title($value['name']),
									'value'		=>	$value['name'],
									//"photo" 	=>	$value['photo'],
									"category"	=>	$value['category']
								);

					$return[] = $temp;

				}

			}

			echo json_encode($return);

		}
		else
		{
			echo json_encode(array());
		}
	}
	
	public function advanced()
	{	

		
		if($this->input->get('onlyGraduate') == 1)
		{
			$cookie = 	array(
								'name'   => 'showGraduateOnly',
								'value'  => 1,
	    						'expire' => '86500'
						    );

			$this->input->set_cookie($cookie);
		}
		else
		{
			$cookie = 	array(
								'name'   => 'showGraduateOnly',
								'value'  => 0,
								'expire' => '86500'
						    );

			$this->input->set_cookie($cookie);
		}
		
	
		echo json_encode($this->search_model->advanced(
					$this->input->get('instsize'),
					$this->input->get('locale'),
					$this->input->get('state'),
					$this->input->get('tuition'),
					$this->input->get('selectivity'),
					$this->input->get('city'),
					$this->input->get('major'),
					$this->input->get('limit'),
					$this->input->get('offset'),
					$this->input->get('orderby'),
					$this->input->get('order'),
					array(),
					$this->input->get('onlyGraduate')
				));
		
	}

}