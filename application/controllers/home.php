<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model("school_model");
		$this->load->model("search_model");
		$this->load->model("user_model");
	}
	
	
	public function index()
	{	
		
		$data 				= 	array();

		$data['page_id'] 	= 	'home_page';

		if ($this->tank_auth->is_logged_in())
		{			
			$data['username'] 	=	$this->session->userdata('username');
			$data['role'] 		=	$this->session->userdata('role');
			$data['user_id'] 	=	$this->session->userdata('user_id');
		}

		if($message = $this->session->flashdata('errorMessage'))
		{
			if($message == 'twitterLoginError')
			{
				$data['errorMessage']	=	'Unfortunately we are unable to connect with Twitter.Please try after some time.';
			}
			else if($message == 'facebookLoginError')
			{
				$data['errorMessage']	=	'Unfortunately we are unable to connect with Facebook.Please try after some time.';
			}
			else if($message == 'googleLoginError')
			{
				$data['errorMessage']	=	'Unfortunately we are unable to connect with Google.Please try after some time.';
			}
		}

		$data['popularSchools']	=	$this->school_model->getPopularSchools();
		$data['page_title']     = pageTitleGenerator(array("Home")); 
		
		$this->load->view('common/inc_header', $data);
		$this->load->view('common/menu_app', $data);
		$this->load->view('home/home', $data);
		$this->load->view('common/inc_footer', $data);
		
	}
	

	public function school($id,$is_test=0){
		
		
		if(isset($id))
		{
			$data = $this->school_model->getSchool($id);

			if(!$data)
			{
				redirect("/home/schools");
			}


			$data['visitorCookie'] = $this->visitorCookie();



			if ($this->tank_auth->is_logged_in())
			{
				
				$data['username'] 	=	$this->session->userdata('username');
				$data['role'] 		=	$this->session->userdata('role');
				$data['page_id'] 	= 	'schools'; 
				$data['user_id'] 	=	$this->session->userdata('user_id');

			}
			
			// echo "<pre>";
			// print_r($data['schools']);
			// die;
			
			$data['school'] 	= $data['schools'][0]; 
			$data['likedPhotos']= $data['likedPhotos'];
			$data['page_id'] 	= ''; 

			$data['locale'] 	= $this->config->item('locale');

			$data['stateList'] 	= $this->config->item('state');
			$data['stateList']  = array_flip($data['stateList']);

			$data['chart'] 		= $this->school_model->getHeadCountCharts($id);

			$data['averageReviewRating'] = $this->school_model->getAverageReviewRating($id);

			
			//Call for view count increment!!!
			$this->school_model->updateSchoolCount($id);

			if(isset($data['schools'][0]['INSTNM']))
			{
				$data['page_title']  = 	pageTitleGenerator(array($data['schools'][0]['INSTNM'],"Schools")) ;
			}
			else{
				$data['page_title']  = 	pageTitleGenerator(array("Schools")) ;
			}
			

			$this->load->view('common/inc_header', $data);
			$this->load->view('common/menu_app', $data);
			$this->load->view('home/school_page', $data);			
			$this->load->view('common/inc_footer', $data);

		}
		else
		{
			redirect('');
		}	

	}
	
	//----------//

	public function survey($id,$is_test=0){
		
		if(isset($id))
		{
			if ($this->tank_auth->is_logged_in())
			{

				$data = $this->school_model->getSurvey($id);
				
				$data['username'] 	=	$this->session->userdata('username');
				$data['role'] 		=	$this->session->userdata('role');
				$data['page_id'] 	= 	'survey'; 
				$data['user_id'] 	=	$this->session->userdata('user_id');

				$this->load->view('common/inc_header', $data);
				$this->load->view('common/menu_app', $data);
				$this->load->view('home/survey', $data);
				$this->load->view('common/inc_footer', $data);
			}
			else
			{
				redirect('/auth/login');
			}

		}
		else
		{
			redirect('');
		}	

	}


	public function schools(){
					
		$data = array();
		
		$data = $this->school_model->readSchools();

		

		if($this->tank_auth->is_logged_in())
		{
			
			$data['username'] 			=	$this->session->userdata('username');
			$data['role'] 				=	$this->session->userdata('role');
			$data['page_id'] 			= 	'schools'; 
			$data['user_id'] 			=	$this->session->userdata('user_id');

		}	

		$data['filters_students'] 		= $this->config->item('students');
		$data['filters_setting'] 		= $this->config->item('setting');
		$data['filters_selectivity'] 	= $this->config->item('selectivity');
		$data['filters_tuition'] 		= $this->config->item('tuition');
		$data['filters_state'] 			= $this->config->item('state');
		$data['locale'] 				= $this->config->item('locale');

		$data['page_id'] 				= '';
		$data['page_title']     		= pageTitleGenerator(array("Schools"));

		$this->load->view('common/inc_header', $data);
		$this->load->view('common/menu_app', $data);
		$this->load->view('home/schools', $data);
		$this->load->view('common/inc_footer', $data);
		
	}

	public function likePhoto()
	{
		if ($this->tank_auth->is_logged_in()) 
		{
			if($this->input->get_post('UNITID') && $this->input->get_post('imgUrl'))
			{
				if($this->school_model->likePhoto($this->input->get_post('UNITID'), $this->input->get_post('imgUrl')))
				{
					echo json_encode(array('status' => 'success'));
				}
				else
				{
					echo json_encode(array('status' => 'failure', 'message' => 'Something went wrong! Please try again!'));
				}
			}
			else
			{	
				echo json_encode(array('status' => 'failure', 'message' => 'UNITID & imgUrl are required'));
			}
		}
		else
		{
			echo json_encode(array('status' => 'failure', 'message' => 'Please login to like a photo!'));
		}
	}

	public function unlikePhoto()
	{
		if ($this->tank_auth->is_logged_in()) 
		{
			if($this->input->get_post('UNITID') && $this->input->get_post('imgUrl'))
			{
				if($this->school_model->unlikePhoto($this->input->get_post('UNITID'), $this->input->get_post('imgUrl')))
				{
					echo json_encode(array('status' => 'success'));
				}
				else
				{
					echo json_encode(array('status' => 'failure', 'message' => 'Something went wrong! Please try again!'));
				}
			}
			else
			{	
				echo json_encode(array('status' => 'failure', 'message' => 'UNITID & imgUrl are required'));
			}
		}
		else
		{
			echo json_encode(array('status' => 'failure', 'message' => 'Please login to like a photo!'));
		}
	}

	// users Reviews with pagination [for profile]
	public function usersReviews($pageNo= 1)
	{
		$data 	=	validate_my_params(
										array(
												'limit'         =>    'integer',
	                                          	'offset'        =>    'integer',
	                                          	'page_no'       =>    'integer'
											)
									);

		if($this->tank_auth->is_logged_in()) 
		{
			if($data['status'] == 'success')
		    {

		    	$data 				=	$data['data'];

		    	$data['page_no']	=	$pageNo;

		    	if(!isset($data['limit']))
		    	{
		    		$data['limit'] 	=	10;
		    	}

		    	if(!isset($data['offset']))
		    	{
		    		$data['offset'] 	=	0;
		    	}

		    	if(isset($data['page_no']))
				{
			    	if($data['page_no'])
					{
						$data['offset'] 	=	$data['limit'] * ($data['page_no'] - 1);
					}
				}

		    	if(!isset($data['page_no']))
		    	{
		    		$data['page_no'] 	=	1;
		    	}
				
				$result  = $this->school_model->readProfileReviews($data);	  

				//dump($result);



		    	if($result['status'] == 'success' && isset($result['data']) && count($result['data'])>0)
			    {
			       	$data['status'] = 'success';

			        $data['data'] 	= $result['data'];
			      	
			        if(isset($result['total_count']) && $result['total_count'] > 0)
					{
			        	//$data['total_count'] 	= 	$query['total_count'];
			       		//dump( $data['total_count']);
						$this->load->config('pagination');

						$config 	=	$this->config->item('pageConfig');

						$config['total_rows'] 			= 	$result['total_count'];
						$config['uri_segment']  		= 	3;
						$config['per_page'] 			= 	$data['limit'];
						$config['use_page_numbers'] 	= 	TRUE;
						$config['base_url'] 			=   site_url().'home/usersReviews';
						//$config['suffix'] 				= 	$urlSuffix;
						//$config['first_url'] 			= 	$config['base_url'];///*.$config['suffix']*/;

						$this->pagination->initialize($config); 
					
						$data['create_link'] 	= 	$this->pagination->create_links();
					}

					if($result['status'] == "success")
					{
						$data['myReviews'] = $data['data'];

					}

					$data['page_class'] = 'page_settings';
					$data['page_id'] = '';
					$data['section'] = 5;

					$data['page_title']     = pageTitleGenerator(array("Profile Settings","My Reviews"));

					$this->load->view('common/inc_header', $data);
					$this->load->view('common/menu_app', $data);
					$this->load->view('profile/profile', $data);
					$this->load->view('common/inc_footer', $data);
				}
				else
			    {
			    	$data['status'] 	= 'success';

			        $data['data'] 		= $result['data']; 
			        $data['myReviews'] 	= $data['data']; 

			        $data['page_class'] = 'page_settings';
					$data['page_id'] = '';
					$data['section'] = 5;

					//$data['page_title']     = pageTitleGenerator(array("Reviews"));
					$data['page_title']     = pageTitleGenerator(array("Profile Settings","My Reviews"));

					$this->load->view('common/inc_header', $data);
					$this->load->view('common/menu_app', $data);
					$this->load->view('profile/profile', $data);
					$this->load->view('common/inc_footer', $data);
      
			    	//$this->session->set_flashdata('errorMessage', 'No data');
			    }   

				
			}
			else
		    {
				//$this->session->set_flashdata('errorMessage', $data['errors']);
				echo json_encode($data['message']);
		    }
		}
		else
		{
			redirect('auth/login');
		}
	}

	// only the user who is logged in can add the image
	public function addImage($id=0, $name= '')
	{
		//$this->form_validation->set_rules('id', 'ID', 'trim|required|xss_clean|integer');		
		$this->form_validation->set_rules('schoolId', 'schoolId', 'trim|required|xss_clean');			

		$data =array();

		if(count($_POST) > 0)
		{
			if($_FILES['photo1']['name'] == null && $_FILES['photo2']['name'] == null && $_FILES['photo3']['name'] == null && $_FILES['photo4']['name'] == null && $_FILES['photo5']['name'] == null)
			{
				$data['errors']['photoSelect'] = "You have not selected any photo, please select atleat one photo.";
			}

		}

		if($this->form_validation->run() && !isset($data['errors']['photoSelect']) )
		{	
			// When the form is success
			if(!$this->tank_auth->is_logged_in())
			{
				redirect("auth/logout");
			}

			$result =  $this->school_model->addImage();

			if($result['status'] == "failure")
			{
				$data['errors']['photoSelect'] = $result['errors'];
			}
			else{

				$data['status'] = "success";
			}

		}
		else
		{
			if(count($_POST) > 0)
			{
				if($_FILES['photo1']['name'] == null && $_FILES['photo2']['name'] == null && $_FILES['photo3']['name'] == null && $_FILES['photo4']['name'] == null && $_FILES['photo5']['name'] == null)
				{
					$data['errors']['photoSelect'] = "You have not selected any photo, please select atleat one photo.";
				}
			}
		}

		if($id != 0)
		{
			$data['schoolId'] = $id;
			$data['schoolName'] = $name;		
		}
		else
		{
			$data['schoolId']   = $this->input->post('schoolId');
			$data['schoolname'] =  $this->input->post('schoolname');
		}

		if($data['schoolId'])
		{
			$data['schoolName']	=	getSchoolName($data['schoolId']);
		}
		// echo "Control goes here...";
		// echo "<pre>";
		// print_R($data);
		// die;

		$this->load->view('common/inc_header', $data);
		$this->load->view('common/menu_app', $data);
		$this->load->view('home/addImage',$data);
		$this->load->view('common/inc_footer', $data);

	}

	public function saveSchool(){

		$return =  $this->school_model->saveSchool();

		echo json_encode($return);

	}

	public function voteSchool(){

		$return =  $this->school_model->voteSchool();

		echo json_encode($return);

	}

	public function downVoteSchool(){

		$return =  $this->school_model->downVoteSchool();

		echo json_encode($return);

	}

	//Please do not delete this function // has to move in helper
	function visitorCookie(){

		$CI=&get_instance();

		//Set the hours here
		$hours 			= 1; //1:Lifetime, 24:One day
		$schoolsLimit 	= 3; 

		if (!$CI->tank_auth->is_logged_in() && $hours != 1)
		{	

			$CI->load->helper('cookie');

			$cookieName = 'unregisterdVisitor';
			$seconds 	= $hours * 3600;
			$counter 	= 1;
			$prefix		= 'myschool_';
			$firstVisit = date('Y-m-d H:i:s');

			$cookieData = $CI->input->cookie($prefix.''.$cookieName);

			if($cookieData)
			{
				$cookieData = json_decode($cookieData);
				
				if(count($cookieData) > 0){

					$firstVisit = $cookieData['firstVisit'];
					$counter 	= $cookieData['counter'];
					$counter 	= $counter+1;

				}

			}

			$return = 'success';

			if($counter > $schoolsLimit){

				if((strtotime(date('Y-m-d H:i:s'))-strtotime($firstVisit)) > $seconds){

					delete_cookie($cookieName);

					$counter 	= 1;
					$firstVisit = date('Y-m-d H:i:s');

				}
				else
				{
					$return = 'failure';
				}

			}

			$cookie = array();

			$cookie['name'] 	= $cookieName;
			$cookie['value'] 	= json_encode(array('counter'=>$counter,
											'firstVisit'=>$firstVisit));
			$cookie['prefix'] 	= $prefix;
			$cookie['secure'] 	= TRUE;

			$CI->input->set_cookie($cookie);

			return $return;

		}
		else
		{
			return "success";
		}	

	}

	/*
		NAME: Neeraj
		DESC: to get all the reviews connected to specific schlool.29/1/15.
	*/
	public function reviews($pageNo = 1)
	{
		$data = validate_my_params(
	                                  array(
	                                          	'schoolId'  	=>   'integer|required',
	                                          	'reviewId'		=>	 'integer',
												'limit'         =>   'integer',
												'offset'        =>   'integer',
												'page_no'       =>   'integer',
												'order_by'     	=>   'valid_opts[rating,flagCount,helpfulFlagCount,createdDate]',
												'order'        	=>   'valid_opts[asc,desc]',
	                                           	/*'filterBy'	  	=>	 'valid_opts[dates,schoolId]',
											   	'filter'		=>	 '',
											   	'since'     	=>   'valid_date',
												'until'    	  	=>   'valid_date',*/
												/*'filterFrom'   	=>   'valid_date',
												'filterTo'    	=>   'valid_date'*/
	                                        )
                                 );

	    if($data['status'] == 'success')
	    {

	    	$data 				=	$data['data'];


	    	$data['page_no']	=	$pageNo;

	    	if(!isset($data['limit']))
	    	{
	    		$data['limit'] 	=	4;
	    	}

	    	if(!isset($data['offset']))
	    	{
	    		$data['offset'] =	0;
	    	}

	    	if(isset($data['page_no']))
			{
		    	if($data['page_no'])
				{
					$data['offset'] 	=	$data['limit'] * ($data['page_no'] - 1);
				}
			}

	    	if(!isset($data['page_no']))
	    	{
	    		$data['page_no'] 	=	1;
	    	}		    

	    	$tempdata	= $this->school_model->getReviews($data);
	   			
	    	if($tempdata['status'] == 'success' && isset($tempdata['data']))
		    {
		       	$data['status'] = 'success';

		        $data['data'] 	= $tempdata['data'];

		        $urlSuffix		=	array();
				$newUrlSuffix	=	array();
		      
		      
		        if(isset($tempdata['total_count']))
				{
					$this->load->config('pagination');

					$config 	=	$this->config->item('pageConfig');
					if($data['order_by'] != '')
					{
						$urlSuffix[]	=	'order_by='.$data['order_by'];
					}

					if($data['order'] != '')
					{
						$urlSuffix[]   =	'order='.$data['order'];
					}

					if(isset($_GET['restaurantId']))
					{
						$urlSuffix[]	=	'restaurantId='.$_GET['restaurantId'];
						$newUrlSuffix[]	=	'restaurantId='.$_GET['restaurantId'];
					}

					if(count($urlSuffix) > 0)
					{
						$urlSuffix 	   =	'?'.implode('&', $urlSuffix);
						$newUrlSuffix  =	'?'.implode('&', $newUrlSuffix);
					}
					else
					{
						$urlSuffix		=	'';
						$newUrlSuffix	=	'';
					}
					
			        if($message = $this->session->flashdata('errorMessage'))
			        {
			            $data['errorMessage'] = $message;
			        }

			        $data['urlSuffix'] 	=	$newUrlSuffix;
					
					//dump($data);
					$config['total_rows'] 			= 	$tempdata['total_count'];
					$config['uri_segment']  		= 	4;
					$config['per_page'] 			= 	$data['limit'];
					$config['use_page_numbers'] 	= 	TRUE;
					$config['base_url'] 			=   site_url().'home/reviews2';
					$config['suffix'] 				= 	$urlSuffix;
					$config['first_url'] 			= 	$config['base_url'].$config['suffix'];

					$this->pagination->initialize($config); 
				
					$data['create_link'] 	= 	$this->pagination->create_links();
					$data['total_count']	=	$tempdata['total_count'];
					$data['count']			=	$tempdata['count'];
				}
				

				//dump($data);

				echo json_encode($data);
			

		    }
		    else
		    {
		    	
		      
		    	$this->session->set_flashdata('errorMessage', $tempdata['errors']);
		    	echo 'error1';
				//redirect('admin/purchaseOrders');
		    }   
     		
   		}
	    else
	    {
			//$this->session->set_flashdata('errorMessage', $data['errors']);
			echo json_encode($data);
	    }
	}
	
	
	

	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */