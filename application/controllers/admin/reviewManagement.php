<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ReviewManagement extends CI_Controller {


	function __construct()
	{
		parent::__construct();

		$this->load->model("admin/reviewManagement_model");
		$this->load->helper('form');
		$this->load->helper('sail_helper');
		isAdmin();
	}

	/*
        NAME: Neeraj
        DESC: to get the information about all the reviews.
    */
	function index($pageNo = 1)
	{

		$data = validate_my_params(
	                                  array(
	                                          	'schoolId'  	=>   'integer',
												'limit'         =>   'integer',
												'offset'        =>   'integer',
												'page_no'       =>   'integer',
												'order_by'     	=>   'valid_opts[rating,flagCount,helpfulFlagCount,createdDate]',
												'order'        	=>   'valid_opts[asc,desc]',
	                                           	'filterBy'	  	=>	 'valid_opts[dates,schoolId]',
											   	'filter'		=>	 '',
											   	'since'     	=>   'valid_date',
												'until'    	  	=>   'valid_date',
												'isAll'    	  	=>   'valid_opts[1]',
												'reviewId'      =>   'integer',
												/*'filterFrom'   	=>   'valid_date',
												'filterTo'    	=>   'valid_date'*/
	                                        )
                                 );

	    if($data['status'] == 'success')
	    {

	    	$data 				=	$data['data'];

	    	//dump($data);

	    	$data['page_no']	=	$pageNo;

	    	if(!isset($data['limit']))
	    	{
	    		$data['limit'] 	=	20;
	    	}

	    	if(!isset($data['offset']))
	    	{
	    		$data['offset'] =	0;
	    	}

	    	if(isset($data['page_no']))
			{
		    	if($data['page_no'])
				{
					$data['offset'] 	=	$data['limit'] * ($data['page_no'] - 1);
				}
			}

	    	if(!isset($data['page_no']))
	    	{
	    		$data['page_no'] 	=	1;
	    	}		    

	    	$tempdata	= $this->reviewManagement_model->get($data);
			
	    	if($tempdata['status'] == 'success' && isset($tempdata['data']))
		    {
		       	$data['status'] = 'success';

		        $data['data'] 	= $tempdata['data'];

		        $urlSuffix		=	array();
				$newUrlSuffix	=	array();
		      
		      	//dump($tempdata);
		        if(isset($tempdata['total_count']) && $tempdata['total_count']>0)
				{

		        	//$data['total_count'] 	= 	$query['total_count'];
		       		//dump( $data['total_count']);
					$this->load->config('pagination');

					$config 	=	$this->config->item('pageConfig');

			    	if($data['filterBy'] != '' && $data['filter'] != '')
					{
						$urlSuffix[]	=	'filterBy='.$data['filterBy'];
						$urlSuffix[]	=	'filter='.$data['filter'];
						$newUrlSuffix[]	=	'filterBy='.$data['filterBy'];
						$newUrlSuffix[]	=	'filter='.$data['filter'];
					}

					if($data['isAll'] != '')
					{
						$urlSuffix[]	=	'isAll='.$data['isAll'];
						$newUrlSuffix[]	=	'isAll='.$data['isAll'];
					}

					if($data['filterBy'] != '' && $data['since'] != '' && $data['until'])
					{
						$urlSuffix[]	=	'filterBy='.$data['filterBy'];
						$urlSuffix[]	=	'since='.$data['since'];
						$urlSuffix[]	=	'until='.$data['until'];
						$newUrlSuffix[]	=	'filterBy='.$data['filterBy'];
						$newUrlSuffix[]	=	'since='.$data['since'];
						$newUrlSuffix[]	=	'until='.$data['until'];
					}

					if($data['order_by'] != '')
					{
						$urlSuffix[]	=	'order_by='.$data['order_by'];
					}

					if($data['order'] != '')
					{
						$urlSuffix[]   =	'order='.$data['order'];
					}

					if(count($urlSuffix) > 0)
					{
						$urlSuffix 	   =	'?'.implode('&', $urlSuffix);
						$newUrlSuffix  =	'?'.implode('&', $newUrlSuffix);
					}
					else
					{
						$urlSuffix		=	'';
						$newUrlSuffix	=	'';
					}
					
			        if($message = $this->session->flashdata('errorMessage'))
			        {
						$data['errorMessage'] = $message;
			        }

			        $data['urlSuffix'] 	=	$newUrlSuffix;
					
					$config['total_rows'] 			= 	$tempdata['total_count'];
					$config['uri_segment']  		= 	4;
					$config['per_page'] 			= 	$data['limit'];
					$config['use_page_numbers'] 	= 	TRUE;
					$config['base_url'] 			=   site_url().'admin/reviewManagement/index';
					$config['suffix'] 				= 	$urlSuffix;
					$config['first_url'] 			= 	$config['base_url'].$config['suffix'];

					$this->pagination->initialize($config); 
				
					$data['create_link'] 	= 	$this->pagination->create_links();
				}

		    	$data['filterAction'] 	= 	site_url().'admin/reviewManagement/index';

				$data['filterOption']   = 	array(
												 	'schoolId' 	=> 	array('type'=>'input', 'value'=>'School'),
												 		
												 	'dates' 	=> 	array('type'=>'date', 'value'=>'Dates')
												);

				$data['page_title']  	= 	pageTitleGenerator(array("Reviews"));
								
				$this->load->view('admin/inc_header',$data);
				$this->load->view('admin/menu',$data);	
				$this->load->view('reviews/review',$data);
				$this->load->view('admin/inc_footer',$data);

		    }
		    else
		    {
		    	
		       //redirect('admin/notificationGroups');        
		    	$this->session->set_flashdata('errorMessage', $tempdata['errors']);
		    	echo 'error1';
				//redirect('admin/purchaseOrders');
		    }   
     		
   		}
	    else
	    {
			//$this->session->set_flashdata('errorMessage', $data['errors']);
			echo json_encode($data['message']);
	    }
	}

	/*
		NAME: Neeraj
		DESC: to delete the review.27/1/15.
	*/
	public function delete()
	{
		$data	=	validate_my_params(
										array(
												'reviewId'	=>	'integer|required'
											)	
									);
		//dump($data);
		if($data['status']	==	'success')
		{
			$result	=	$this->reviewManagement_model->deleteReview($data['data']['reviewId']);

			if($result['status']	==	'success')
			{
				echo json_encode($result);
			}
			else
			{
				echo json_encode($result);
			}
		}
		else
		{
			echo json_encode($data);
		}
		
	}

	/*
        NAME: Neeraj
        DESC: to approve review.
    */
	public function approveReview()
	{
		$data	=	validate_my_params(
										array(
												'reviewId'	=>	'integer|required'
											)	
									);
		//dump($data);

		if($data['status']	==	'success')
		{	
			
			$result	=	$this->reviewManagement_model->approveReview($data['data']['reviewId']);

			if($result['status']	==	'success')
			{
				echo json_encode($result);
			}
			else
			{
				echo json_encode($result);
			}
		}
		else
		{
			echo json_encode($data);
		}
	}



	/*
	*	Name : Ather Parvez
	*	Date : 30 JAN 2015
	*	DESC : it display the details of specific id
	*/
	function view()
	{
		$data = validate_my_params(
										array(
												'id' 	=>	'required|integer'
												
											)
									);

		if($data['status'] == 'success')
		{
			$data = $this->reviewManagement_model->view($data['data']['id']);

			

			if($data['status'] == 'success')
			{

       			$this->load->view('reviews/view',$data['data']);
			}
		}
	}

}