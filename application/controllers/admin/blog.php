<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

	//private $has_category = null ;//$this->config->item('has_category');

	function __construct()
	{
		parent::__construct();

		//$this->load->helper('gateway');
		$this->load->helper(array('form', 'url'));

		$this->load->model('admin/blogs_model');
		$this->load->helper('sail_helper');
		//$this->has_category  = $this->config->item('has_category');

	}
	
	
	
	function index($pageNo=1)
	{

		$data['active_menu']	=	'blog';
		$data['subMenuActive']	=	'';
		
		$data['categories'] = $this->blogs_model->get_categories();
		

		//$data['users'] = $this->blogs_model->get_users();
		
		

		
    	$data['limit'] 	=	20;
    	

    	

    	
		$data['offset'] 	=	$data['limit'] * ($pageNo - 1);

    	//$this->load->config('pagination');

		//$pageConfig  = $this->config->item('pageConfig');//  $config['pageConfig']

		$pageConfig['base_url'] 			= 	site_url().'admin/blog/index/'; 
		$pageConfig['total_rows'] 			= 	$this->db->from('blog-articles')->where('isDeleted',NULL)->order_by('id','desc')->count_all_results();




    	$pageConfig['per_page'] 			= 	$data['limit'];
		$pageConfig['uri_segment'] 			= 	4;
		$pageConfig['num_links'] 			= 	5;
		$pageConfig['first_link'] 			= 	'First';
		$pageConfig['last_link'] 			= 	'Last';
		$pageConfig['use_page_numbers'] 	= 	TRUE;
		$pageConfig['full_tag_open'] 		= 	'<ul class="pagination">';
		$pageConfig['full_tag_close'] 		= 	'</ul>';

		$pageConfig['num_tag_open'] 		= 	'<li>';
		$pageConfig['num_tag_close'] 		= 	'</li>';

		$pageConfig['cur_tag_open'] 		= 	"<li class='active'><a href='#'>";
		$pageConfig['cur_tag_close'] 		= 	'</a></li>';

		$pageConfig['prev_tag_open'] 		= 	'<li>';
		$pageConfig['prev_tag_close'] 		= 	'</li>';

		$pageConfig['next_tag_open'] 		= 	'<li>';
		$pageConfig['next_tag_close'] 		= 	'</li>';

		$pageConfig['first_tag_open'] 		= 	'<li>';
		$pageConfig['first_tag_close'] 		= 	'</li>';

		$pageConfig['last_tag_open'] 		= 	'<li>';
		$pageConfig['last_tag_close']	 	= 	'</li>';

		$this->pagination->initialize($pageConfig);

		$data['articles'] 	= $this->blogs_model->get_articles($data);

		$data['total_rows'] 	= 	$pageConfig['total_rows'];

		$data['paginationString']  =  $this->pagination->create_links();
		//dump($data['paginationString']);
		$data['page_title']  	= 	pageTitleGenerator(array("Blog"));

		//echo $this->pagination->create_links();
		$this->load->view('admin/inc_header',$data);	
		$this->load->view('admin/menu',$data);	
		$this->load->view('blog/blog',$data);
		$this->load->view('admin/inc_footer',$data);

	}


	function categories($id=0)
	{	
		$data['active_menu']	=	'blog';
		$data['subMenuActive']	=	'';
		
		$data['categories'] = $this->blogs_model->get_categories();

		//dump($data);

		//add_log(json_encode($data['categories']));
		$data['page_title']  	= 	pageTitleGenerator(array("Categories","Blog"));

		$this->load->view('admin/inc_header',$data);	
		$this->load->view('admin/menu',$data);	
		$this->load->view('blog/categories',$data);
		$this->load->view('admin/inc_footer',$data);

	}




	function add_category()
	{	
		$data = array();

		$this->form_validation->set_rules('category_name', 'Category name', 'required');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red ;">', '</div>');

		

		if ($this->form_validation->run() == FALSE)
		{
			$data['page_title']  	= 	pageTitleGenerator(array("Create Category","Category","Blog"));

			$this->load->view('admin/inc_header',$data);
			$this->load->view('admin/menu',$data);	
			$this->load->view('blog/add_category',$data);
			$this->load->view('admin/inc_footer',$data);
		}
		else
		{
			$this->blogs_model->add_category($data);
			redirect('admin/blog/categories');
			
		}

	}

	function update_category($id = 0)
	{	
		$data = array();

		$this->form_validation->set_rules('category_name', 'Category name', 'required');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red ;">', '</div>');

		
		$data['id'] 	 = $id;
		$data['is_edit'] = 1;

		if ($this->form_validation->run() == FALSE)
		{

			$data['data'] = $this->blogs_model->get_category($data);

			$data['page_title']  	= 	pageTitleGenerator(array("Update Category","Category","Blog"));

			$this->load->view('admin/inc_header',$data);
			$this->load->view('admin/menu',$data);	
			$this->load->view('blog/add_category',$data);
			$this->load->view('admin/inc_footer',$data);
		}
		else
		{

			//print_r($_POST);

			if($id != 0)
			{
				$this->blogs_model->update_category($data);
			}else
			{
				echo "here";
			}
		
			redirect('admin/blog/categories');
			
		}

	}


	function delete_category($category_id){

		
		if($category_id != null && $category_id != ""){
			$result = $this->blogs_model->delete_category($category_id);

		}
		redirect('admin/blog/categories');
	}

	function test()
	{
		$this->load->view('blog/testFile');
	}

	/*
		updated by - Gaurav sharma
		date - 30/01/2015
	*/

	function add_article(){
		
		$data = array();
		$data['active_menu']	=	'blog';
		$data['subMenuActive']	=	'';
		$data['is_create']		=   1;
		//$data['has_category']   =   $this->has_category;

		$this->load->helper(array('form', 'url'));
		

		

		//if($this->has_category == "yes")
		{
			$data['categories'] 	= 	$this->blogs_model->get_categories();
			$data['authors'] 		= 	$this->blogs_model->getAuthors();

			$this->form_validation->set_rules('categoryId', 'Category', 'required');

			//dump($data['categories']);
		}

		$data['users']  = $this->blogs_model->get_users();

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		//$this->form_validation->set_rules('coverImage', 'Cover Image', 'required');
		//$this->form_validation->set_rules('user_id', 'user_id', 'required');

		$this->form_validation->set_error_delimiters('<div class="error" style="color:red ;">', '</div>');

		

		if ($this->form_validation->run() == FALSE)
		{
			$data['page_title']  	= 	pageTitleGenerator(array("Create Article","Blog"));

			$this->load->view('admin/inc_header',$data);
			$this->load->view('admin/menu',$data);	
			$this->load->view('blog/add_article',$data);
			$this->load->view('admin/inc_footer',$data);
		}
		else
		{	
			
			//dump($_FILES['coverImage']['name'],1);
			$result = $this->blogs_model->create_article($data);

			if($result['status'] == "success")
			{
				redirect('admin/blog');
			}
			else
			{

				$data['errors'] =  $result['error']['error'];

				$data['page_title']  	= 	pageTitleGenerator(array("Create Article","Blog"));

				$this->load->view('admin/inc_header',$data);
				$this->load->view('admin/menu',$data);	
				$this->load->view('blog/add_article',$data);
				$this->load->view('admin/inc_footer',$data);
			}


			
			
		}
	}

	/*
		updated by - Gaurav sharma
		date - 31/01/2015
	*/
	function update_article($id = 0 ,$post = 0){

		$data['id'] = $id;
		$data['is_edit'] = 1;
		//$data['has_category']	= $this->has_category;
		$data['is_post']		= $post;


		//if($this->has_category == "yes")
		{
			$data['categories'] = $this->blogs_model->get_categories();	
		}
		
		$data['authors'] 		= 	$this->blogs_model->getAuthors();
		$data['users']  = $this->blogs_model->get_users();

		
		$temp = $this->blogs_model->read_article($id);
		if($id != 0 && $post == 0)
		{
			if($temp['status'] == "success")
			{
				$data['data']  = $temp['data'];
			}
			else
			{

				redirect("admin/blog/update_article/".$id."/");

			}
		}
		else
		{
			$data['coverImage'] = $temp['data']['coverImage'];
		}
		
		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('content', 'content', 'required');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red ;">', '</div>');
	
				
		if($this->form_validation->run() == FALSE)
		{	
			$data['page_title']  	= 	pageTitleGenerator(array("Update Article","Blog"));

			$this->load->view('admin/inc_header',$data);
			$this->load->view('admin/menu',$data);
			$this->load->view('blog/add_article',$data);
			$this->load->view('admin/inc_footer',$data);
		}
		else
		{

		
		
			/*echo  '<pre>';
			print_r($_POST);
			echo '</pre>';*/

			$result = $this->blogs_model->update_article($data);

			if($result['status'] == "failure")
			{

				$data['errors'] =  $result['error']['error'];

				$data['page_title']  	= 	pageTitleGenerator(array("Update Article","Blog"));
				
				$this->load->view('admin/inc_header',$data);
				$this->load->view('admin/menu',$data);
				$this->load->view('blog/add_article',$data);

				$this->load->view('admin/inc_footer',$data);

			}
			else{

				if($this->input->get('is_apply') != null && $this->input->get('is_apply') == 1)
				{
					//echo "coming here";
					$data['page_title']  	= 	pageTitleGenerator(array("Update Article","Blog"));

					$this->load->view('admin/inc_header',$data);
					$this->load->view('admin/menu',$data);	

					$this->load->view('blog/add_article',$data);

					$this->load->view('admin/inc_footer',$data);
				}
				else{
					
					redirect("/admin/blog");
				}
			}
			//redirect("/admin/blogs/update_article/".$id);
			
		}
	}

	function delete_article($id = 0 ){
		
		$this->blogs_model->delete_article($id);

		redirect('admin/blog');
	}

	/*
	NAME: Ranjit Randive
	DESC: File Upload
	*/

	function upload()
	{

		$config['upload_path'] 		= 	'./blogPics';
		$config['allowed_types'] 	= 	'jpeg|jpg|png';
		$config['overwrite']		= 	FALSE;
		$config['max_size']			= 	'10000';
		$config['max_width']  		= 	'20000';
		$config['max_height']  		= 	'10000';
		
		$this->load->library('upload', $config);

		$imageName 	=	'';
		$thumbName 	=	'';

		if (!$this->upload->do_upload('file'))
		{
			$error = $this->upload->display_errors();
			echo json_encode(array('status' => 'failure'));
		}
		else
		{
			$imageData 	= 	$this->upload->data();
			echo json_encode(array('status' => 'success', 'data' => $imageData));
		}
	}
   
}

/* End of file admin.php */
/* Location: ./application/modules/blog/controllers/admin.php */