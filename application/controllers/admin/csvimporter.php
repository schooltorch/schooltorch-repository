<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Csvimporter extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('csvImport');
		$this->load->config('csvImportTableNames');
	}
	
	
	public function index()
	{
		$data = array('errorMessage' => '', 'successMessage' => '');

		$this->load->view('admin/inc_header',$data);
		$this->load->view('admin/menu',$data);	
		$this->load->view('csvImport/index',$data);
		$this->load->view('admin/inc_footer',$data);
	}

	public function go()
	{
		ini_set('memory_limit', '-1');
		ini_set('max_execution_time', '600');
		ini_set('max_input_time', '600');
		ini_set('default_socket_timeout', '600');
		ini_set('post_max_size', '128M');
		ini_set('upload_max_filesize', '128M');
		$data = array('errorMessage' => '', 'successMessage' => '');
		

		if(!isset($_FILES['csvFile']))
		{
			 $data['errorMessage'] = 'File missing';
		}
		elseif(!in_array($this->input->post('tableName'), $this->config->item('csvImportTableNames')))
		{
			 $data['errorMessage'] = "Invalid table name!";
		}
		elseif($_FILES['csvFile']['tmp_name'] == '')
		{
			$data['errorMessage'] = "CSV file required!";
		}
		elseif(getFileExt($_FILES["csvFile"]["name"]) != 'csv')
		{
			$data['errorMessage'] = "CSV file required!";
		}
		else
		{
			$response = importCSVHub(array(
							'csvFile' 	=>	$_FILES['csvFile']['tmp_name'],
							'dbName' 	=> 	$this->input->post('tableName'),
							'chunkSize' => 	25
						));

			if($response['status'] == 'failure')
			{
				$data['errorMessage'] = $response['message'];
			}
			else
			{
				$data['successMessage'] = "Updated";
			}
			
		}


		$this->load->view('admin/inc_header',$data);
		$this->load->view('admin/menu',$data);	
		$this->load->view('csvImport/index',$data);
		$this->load->view('admin/inc_footer',$data);
	}

	public function updateAcceptance()
	{
		ini_set('memory_limit', '-1');

		$query = $this->db->from('schoolCharacteristics')->get()->result();

		$updateArr = array();

		foreach($query as $item)
		{	
			if(($item->APPLCNM + $item->APPLCNW) > 0)
			{
				$acceptanceVal = (($item->ADMSSNM + $item->ADMSSNW)/($item->APPLCNM + $item->APPLCNW))*100;
			}
			else
			{
				$acceptanceVal = 0;
			}
			$updateArr[] = 	array(
									'UNITID' => $item->UNITID,
									'TOTALSTUDENTS' => ($item->ADMSSNM + $item->ADMSSNW),
									'ACCEPTANCE' => $acceptanceVal
								);

			if(count($updateArr) == 20)
			{
				$this->db->update_batch('schools', $updateArr, 'UNITID'); 
				$updateArr = array();
			}
		}

		if(count($updateArr) > 0)
		{
			$this->db->update_batch('schools', $updateArr, 'UNITID'); 
			$updateArr = array();
		}

		redirect('admin/csvimporter');
	}

	public function updateSelectivity()
	{
		ini_set('memory_limit', '-1');
		
		$query = $this->db->from('schoolCharacteristics')->get()->result();

		$updateArr = array();

		foreach($query as $item)
		{

			$value 	=	(($item->SATVR25 + $item->SATVR75 + $item->SATMT25 + $item->SATMT75)/4);

			
			$updateArr[] = 	array(
								'UNITID' 		=> 	$item->UNITID,
								'SELECTIVITY' 	=> 	$value
							);

			if(count($updateArr) == 20)
			{
				$this->db->update_batch('schools', $updateArr, 'UNITID'); 
				$updateArr = array();
			}
		}

		if(count($updateArr) > 0)
		{
			$this->db->update_batch('schools', $updateArr, 'UNITID'); 
			$updateArr = array();
		}

		redirect('admin/csvimporter');
	}
}

/* End of file csvimporter.php */
/* Location: ./application/controllers/csvimporter.php */