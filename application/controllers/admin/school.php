<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class School extends CI_Controller {

	function __construct()
	{	
		parent::__construct();
		$this->load->model("school_model");
	}	

	function index($pageNo=1)
	{	
		$this->show($pageNo);
	}

	function show($pageNo=1)
	{
		
		if ($this->tank_auth->is_logged_in())
		{
			$data = array();

			$role 	=	$this->session->userdata('role');
		
			if($role == 2)
			{
				$data 	=	validate_my_params(	
													array(
															'limit' 	=>	'integer',
															'offset' 	=>	'integer',
															'page_no' 	=>	'integer',
															'filterBy'	=>	 'valid_opts[name]',
											   				'filter'	=>	 '',
														)
												);
				
				if($data['status'] == 'success')
				{

					$data 				=	$data['data'];

					$data['page_no']	=	$pageNo;

					if(!isset($data['limit']))
			    	{
			    		$data['limit'] 	=	20;
			    	}

			    	if(!isset($data['offset']))
			    	{
			    		$data['offset'] 	=	0;
			    	}

			    	if(isset($data['page_no']))
					{
				    	if($data['page_no'])
						{
							$data['offset'] 	=	$data['limit'] * ($data['page_no'] - 1);
						}
					}

			    	if(!isset($data['page_no']))
			    	{
			    		$data['page_no'] 	=	1;
			    	}

					$data = $this->school_model->readAdminSchools($data);

					$urlSuffix		=	array();
					$newUrlSuffix	=	array();

					if(isset($data['total_count']))
					{
						if($data['filterBy'] != '' && $data['filter'] != '')
						{
							$urlSuffix[]	=	'filterBy='.$data['filterBy'];
							$urlSuffix[]	=	'filter='.$data['filter'];
							$newUrlSuffix[]	=	'filterBy='.$data['filterBy'];
							$newUrlSuffix[]	=	'filter='.$data['filter'];
						}

						if(count($urlSuffix) > 0)
						{
							$urlSuffix 	   =	'?'.implode('&', $urlSuffix);
							$newUrlSuffix  =	'?'.implode('&', $newUrlSuffix);
						}
						else
						{
							$urlSuffix		=	'';
							$newUrlSuffix	=	'';
						}

						$data['urlSuffix'] 	=	$newUrlSuffix;

						$pageConfig['base_url'] 			= 	site_url('/admin/school/index');
						$pageConfig['total_rows'] 			= 	$data['total_count'];
						$pageConfig['per_page'] 			= 	$data['limit'];
						$pageConfig['suffix'] 				= 	$urlSuffix;
						$pageConfig['first_url'] 			= 	$pageConfig['base_url'].$pageConfig['suffix'];
						$pageConfig['uri_segment'] 			= 	4;
						$pageConfig['num_links'] 			= 	5;
						$pageConfig['first_link'] 			= 	'First';
						$pageConfig['last_link'] 			= 	'Last';
						$pageConfig['use_page_numbers'] 	= 	TRUE;
						$pageConfig['full_tag_open'] 		= 	'<ul class="pagination">';
						$pageConfig['full_tag_close'] 		= 	'</ul>';

						$pageConfig['num_tag_open'] 		= 	'<li>';
						$pageConfig['num_tag_close'] 		= 	'</li>';

						$pageConfig['cur_tag_open'] 		= 	"<li class='active'><a href='#'>";
						$pageConfig['cur_tag_close'] 		= 	'</a></li>';

						$pageConfig['prev_tag_open'] 		= 	'<li>';
						$pageConfig['prev_tag_close'] 		= 	'</li>';

						$pageConfig['next_tag_open'] 		= 	'<li>';
						$pageConfig['next_tag_close'] 		= 	'</li>';

						$pageConfig['first_tag_open'] 		= 	'<li>';
						$pageConfig['first_tag_close'] 		= 	'</li>';

						$pageConfig['last_tag_open'] 		= 	'<li>';
						$pageConfig['last_tag_close']	 	= 	'</li>';

						$this->pagination->initialize($pageConfig);


						$data['pagination_str'] 	=	$this->pagination->create_links();

					}

					if(!isset($data['pagination_str']))
					{
						$data['pagination_str'] 	=	'';
					}

					$data['filterAction'] 	= 	site_url().'admin/school';

					$data['filterOption']   = 	array(
													 	'name' 	=> 	array('type'=>'input', 'value'=>'Name'),
													);
					$data['page_title']  = 	pageTitleGenerator(array("Schools"));

					
					$this->load->view('admin/inc_header',$data);
					$this->load->view('admin/menu',$data);	
					$this->load->view('school/schools',$data);
					$this->load->view('admin/inc_footer',$data);
				}
				else
				{
					redirect('');
				}
			}
			else
			{
				redirect('');
			}
		}
		else
		{
			redirect('auth/login');
		}
		
	}

	function addNew()
	{

		$data 	=	$this->school_model->getRequiredData();

		if(!isset($data['states']))
		{
			$data['states'] 	=	array();
		}
		

		if(!isset($data['programs']))
		{
			$data['programs'] 	=	array();
		}
		
		if(!isset($data['survey']))
		{
			$data['survey'] 	=	array();
		}
		$data['page_title']  = 	pageTitleGenerator(array("Add"));
		
		$this->load->view('admin/inc_header',$data);	
		$this->load->view('admin/menu',$data);
		$this->load->view('school/addSchoolNew',$data);
		$this->load->view('admin/inc_footer',$data);
	}

	function add()
	{

		$role 	=	$this->session->userdata('role');
		
		if($role == 2)
		{	
			$data = array();

			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			
			$this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean');
				
			$this->form_validation->set_rules('area', 'Area', 'trim|required|xss_clean');

			$this->form_validation->set_rules('latitude', 'Latitude', 'trim|required|xss_clean');

			$this->form_validation->set_rules('longitude', 'Longitude', 'trim|required|xss_clean');

			$this->form_validation->set_rules('enrollmentSize', 'Enrollment Size', 'trim|required|xss_clean');

			$this->form_validation->set_rules('programs', 'Programs', 'trim|required|xss_clean');

			$this->form_validation->set_rules('state', 'State', 'trim|required|xss_clean');

			$this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean');

			$this->form_validation->set_rules('demographicsMale', 'Demographics Male', 'trim|required|xss_clean');

			$this->form_validation->set_rules('demographicsFemale', 'Demographics Female', 'trim|required|xss_clean');

			$this->form_validation->set_rules('demographicsInternational', 'Demographics International', 'trim|required|xss_clean');

			$this->form_validation->set_rules('keyFeatures', 'Key Features', 'trim|xss_clean');

			$this->form_validation->set_rules('tuition', 'Tuition', 'trim|required|xss_clean');

			$this->form_validation->set_rules('selectivity', 'Selectivity', 'trim|required|xss_clean');

			$this->form_validation->set_rules('survey', 'Survey', 'trim|required|xss_clean');

			if($this->form_validation->run())
			{
				$params 	=	array(
										'name' 		=>	$this->form_validation->set_value('name'),
										'location' 	=>	$this->form_validation->set_value('location'),
										'area' 	=>	$this->form_validation->set_value('area'),
										'latitude' 	=>	$this->form_validation->set_value('latitude'),
										'longitude' 		=>	$this->form_validation->set_value('longitude'),
										'enrollmentSize' 	=>	$this->form_validation->set_value('enrollmentSize'),
										'programs' 	=>	$this->form_validation->set_value('programs'),
										'state' 	=>	$this->form_validation->set_value('state'),
										'city' 	=>	$this->form_validation->set_value('city'),
										'demographicsMale' 	=>	$this->form_validation->set_value('demographicsMale'),
										'demographicsFemale' 	=>	$this->form_validation->set_value('demographicsFemale'),
										'demographicsInternational' 	=>	$this->form_validation->set_value('demographicsInternational'),
										'keyFeatures' 	=>	$this->form_validation->set_value('keyFeatures'),
										'tuition' 	=>	$this->form_validation->set_value('tuition'),
										'selectivity' 	=>	$this->form_validation->set_value('selectivity'),
										'survey' 	=>	$this->form_validation->set_value('survey'),

									);

				

			}

			$data 	=	$this->school_model->getRequiredData();

			if(!isset($data['states']))
			{
				$data['states'] 	=	array();
			}
			

			if(!isset($data['programs']))
			{
				$data['programs'] 	=	array();
			}
			
			if(!isset($data['survey']))
			{
				$data['survey'] 	=	array();
			}

			$data['page_title']  = 	pageTitleGenerator(array("Add","Schools"));

			$this->load->view('admin/inc_header',$data);	
			$this->load->view('admin/menu',$data);
			$this->load->view('school/addSchool',$data);
			$this->load->view('admin/inc_footer',$data);
		}
		else
		{
			redirect('');
		}

	}

	function getCities()
	{

		$data 	=	validate_my_params(	
											array(
													'stateCode'	=>	'required'
												)
										);

		if($data['status'] == 'success')
		{
			$data 	=	$data['data'];
			$data 	=	$this->school_model->getCities($data);
			echo json_encode($data);
		}
		else
		{
			echo json_encode($data);
		}

	}

	function edit($id= 0)
	{	
		$this->school_model->edit($id);
		

	}

	function delete($id = 0)
	{
		$this->school_model->delete($id);

	}

	function programs($page_no=1)
	{
		$role 	=	$this->session->userdata('role');
		
		if($role == 2)
		{
			$data 	= 	array();

			$data 	=	validate_my_params(	
												array(
														'limit' 	=>	'integer',
														'offset' 	=>	'integer',
														'page_no' 	=>	'integer'
													)
											);

			if($data['status'] == 'success')
			{
				$data 	=	$data['data'];

				$data['page_no']	=	$page_no;

				if(!isset($data['limit']))
		    	{
		    		$data['limit'] 	=	20;
		    	}

		    	if(!isset($data['offset']))
		    	{
		    		$data['offset'] 	=	0;
		    	}

		    	if(isset($data['page_no']))
				{
			    	if($data['page_no'])
					{
						$data['offset'] 	=	$data['limit'] * ($data['page_no'] - 1);
					}
				}

		    	if(!isset($data['page_no']))
		    	{
		    		$data['page_no'] 	=	1;
		    	}

				$data 	=	$this->school_model->getAllPrograms($data);

				if(isset($data['total_count']))
				{
					$pageConfig['base_url'] 			= 	site_url('/admin/school/programs');
					$pageConfig['total_rows'] 			= 	$data['total_count'];
					$pageConfig['per_page'] 			= 	$data['limit'];
					$pageConfig['uri_segment'] 			= 	4;
					$pageConfig['num_links'] 			= 	5;
					$pageConfig['first_link'] 			= 	'First';
					$pageConfig['last_link'] 			= 	'Last';
					$pageConfig['use_page_numbers'] 	= 	TRUE;

					$this->pagination->initialize($pageConfig);

					$data['pagination_str'] 	=	$this->pagination->create_links();

				}



				if(!isset($data['pagination_str']))
				{
					$data['pagination_str'] 	=	'';
				}
				
				$data['page_title']  = 	pageTitleGenerator(array("Programs","Schools"));
				$this->load->view('admin/inc_header',$data);	
				$this->load->view('admin/menu',$data);
				$this->load->view('school/programs',$data);
				$this->load->view('admin/inc_footer',$data);
			}
			else
			{
				$data['error'] 	=	$data['message'];
				$this->load->view('admin/inc_header',$data);
				$this->load->view('admin/menu',$data);	
				$this->load->view('school/programs',$data);
				$this->load->view('admin/inc_footer',$data);
			}
		}
		else
		{
			redirect('');
		}
	}

	function addPrograms()
	{
		$role 	=	$this->session->userdata('role');
		
		if($role == 2)
		{
			$data 	=	validate_my_params(	
												array(
														'name'	=>	'required|alpha'
													)
											);
			
			if($data['status'] == 'success')
			{
				$data	=	$data['data'];
				$data 	=	$this->school_model->addPrograms($data);
				redirect('/admin/school/programs');
			}
			else
			{
				
				$data['page_title']  = 	pageTitleGenerator(array("Add Programs","Schools"));
				$data['error'] 	=	'Please enter Program name';
				$this->load->view('admin/inc_header',$data);
				$this->load->view('admin/menu',$data);	
				$this->load->view('school/programs',$data);
				$this->load->view('admin/inc_footer',$data);
			}
		}
		else
		{
			redirect('');
		}

	}

	function searchImages()
	{

		$data  = array();
		
		$data['page_title']  = 	pageTitleGenerator(array("Search Images","Schools"));

		$this->load->view('admin/inc_header',$data);
		$this->load->view('admin/menu',$data);	
		$this->load->view('school/searchImages',$data);
		$this->load->view('admin/inc_footer',$data);
	}

	function manageImages($id = 0)
	{

		$this->load->helper("school");

		$data  = array();

		$temp = $this->school_model->getSchoolData($id,1);

		$data["imageArr"] = $temp['imageArr'];

		$data['userImageArr'] 	=	array();

		if(isset($temp['userImageArr']) && count($temp['userImageArr']) > 0)
		{
			$data['userImageArr'] 	=	$temp['userImageArr'];
		}

		$data['name']	  = $temp['name'];	

		$data['id']		  = $id;
		$data['page_title']  = 	pageTitleGenerator(array("Manage Images","Schools"));

		
		$this->load->view('admin/inc_header',$data);
		$this->load->view('admin/menu',$data);	
		$this->load->view('school/manageImages',$data);
		$this->load->view('admin/inc_footer',$data);
	}

	function fetchImages($id = 0)
	{
		$this->load->helper("school");
		$data  = array();

		
		
		$schoolInfo =$this->school_model->getSchoolData($id);

		$data["imageArr"] = schoolLocationPics($schoolInfo);
		
		echo json_encode($data);

	}

	function saveImages($id)
	{
		$this->load->helper("school");

		$imageArr 	= 	array();

		if($this->input->post("url"))
		{

			if(is_array($this->input->post("url")) )
			{
				$imageArr = $this->input->post("url");
			}
		}

		setSchoolPhotos($imageArr,$id);

		
		redirect("/admin/school/manageImages/".$id);
	}

	function deleteImage($id)
	{
		$this->load->helper("school");

		$imageArr 	= 	'';

		if($this->input->post("url"))
		{

			if($this->input->post("url") != '')
			{
				$imageArr = $this->input->post("url");
			}
		}

		deleteSchoolPhotos($imageArr,$id);

		echo json_encode(array('status'=>'success'));

	}

	//email test view.
	function emailTestView()
	{

		$data  = array();

		$data['site_name'] = "Schools Directory";
		$data['user_id'] = 2132134;
		$data['new_email_key'] ="NEW EMAIL KEY";
		$data['activation_period'] = "NEW ACT PERIOD";
		$data['email'] = "hari@gdiz.com";
		$data['username'] = "TOMCRUISE";
		$data['new_email'] ="new@gmail.com";
		$data['new_pass_key'] ="NEW PASS KEY";

		$this->load->view('admin/inc_header',$data);
		$this->load->view('admin/menu',$data);	
		$this->load->view('email/welcome-html',$data);
		$this->load->view('admin/inc_footer',$data);
	}

	/*
		Name:HARI
		DESC:Manage Popular Schools
	*/
	function popularSchools($id=0)
	{
		$this->load->helper("school");

		$data  = array();

			

		$data['id']		 	 = $id;

		$data['data']        = $this->school_model->getPopularSchools();

		//dump($data['data']);

		$data['page_title']  = 	pageTitleGenerator(array("Popular schools","Schools"));

		
		$this->load->view('admin/inc_header',$data);
		$this->load->view('admin/menu',$data);	
		$this->load->view('school/managePopularSchools',$data);
		$this->load->view('admin/inc_footer',$data);


	}

	/*
		Name:HARI
		DESC:Save popular schools
	*/

	function savePopularSchools()
	{

		$data 	=	validate_my_params(	
										array(
												'school1'	=>	'required|integer',
												'school2'	=>	'required|integer',
												'school3'	=>	'required|integer',
											)
									);

		if($data['status'] == "success")
		{

			$data  = $data['data'];

			$this->school_model->updatePopularSchools($data);

			echo json_encode(array("status"=>"success"));
		}
		else
		{
			echo $data;
		}
	}


}
