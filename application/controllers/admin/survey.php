<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Survey extends CI_Controller
{

	/*
		Written By: Alok Banjare
		Created Date: 6 September, 2014	
		Last Updated:
		Description: Survey Management
	*/

	function __construct()
	{
		parent::__construct();
		$this->load->model('survey_model');
		$this->load->helper('validation_helper');
	}

	/*
		Survey Functions Starts
	*/

	function index()
	{	
		
		$role 	=	$this->session->userdata('role');
		
		if($role == 2)
		{
			
			$data['page_title']  = 	pageTitleGenerator(array('Survey'));
			$data['page_header']  = "Survey Management";
			
			$data['surveys'] = $this->survey_model->readSurveys();		

			$this->load->view('admin/inc_header', $data);
			$this->load->view('admin/menu', $data);
			$this->load->view('app/survey_management', $data);
			$this->load->view('admin/inc_footer', $data);
			
		}
		else
		{
			redirect('');
		}

	}

	function addSurvey(){

		$role 	=	$this->session->userdata('role');
		
		if($role == 2)
		{

			$data['page_title']  = 	pageTitleGenerator(array('Manage Survey'));
			
			$this->form_validation->set_rules('name', 'Survey Name', 'trim|required');
			$this->form_validation->set_rules('category[]', 'Survey Category', 'trim|required');
			
			if ($this->form_validation->run())
			{
				$data['response'] = $this->survey_model->addSurvey();
			}

			redirect('admin/survey');
			
		}
		else
		{
			redirect('');
		}

	}

	function editSurvey(){

	}

	function deleteSurvey(){



	}

	/*
		Survey Functions Ends
	*/	


		

	/*
		Category Functions Starts
	*/

	function categories()
	{
		$role 	=	$this->session->userdata('role');
		
		if($role == 2)
		{
			
			$data['page_title'] = 'Survey Categories';
				
			$data['categories'] = $this->survey_model->readCategories();

			$this->load->view('admin/inc_header', $data);
			$this->load->view('admin/menu', $data);
			$this->load->view('app/survey_categories', $data);
			$this->load->view('admin/inc_footer', $data);

		}
		else
		{
			redirect('');
		}
	}



	function addCategories(){

		$role 	=	$this->session->userdata('role');
		
		if($role == 2)
		{
		
			$data['page_title'] = 'Survey Categories';
			
			$this->form_validation->set_rules('name', 'Category Name', 'trim|required');
			
			if ($this->form_validation->run())
			{
				$data['response'] = $this->survey_model->addCategories();
			}

			redirect('admin/survey/categories');
			
		}
		else
		{
			redirect('');
		}

	}

	function editCategory(){

		
	}

	function deleteCategories(){



	}

	/*
		Category Functions Ends
	*/


	/*
		Question Functions Starts
	*/	

	function questions($id=0){

		$role 	=	$this->session->userdata('role');
		
		if($role == 2)
		{
			
			$data['survey']  	= $this->survey_model->readSurveys($id);
			$data['questions']  = $this->survey_model->readQuestions($id);

			$data['page_title'] = site_title($data['survey']['name'],'Questions');
			$data['survey_id']	= $id;	

			$this->load->view('admin/inc_header', $data);
			$this->load->view('admin/menu', $data);
			$this->load->view('app/survey_questions', $data);
			$this->load->view('admin/inc_footer', $data);

		}
		else
		{
			redirect('');
		}


	}	

	function question($id=0){

		$role 	=	$this->session->userdata('role');
			
		if($role == 2)
		{
			
			$data['surveyId']   = $id;
			
			$data['survey']  	= $this->survey_model->readSurveys($id);
			$data['page_title'] = site_title($data['survey']['name'],'Questions','Add');
			
			$this->load->view('admin/inc_header', $data);
			$this->load->view('admin/menu', $data);
			$this->load->view('app/survey_add_questions', $data);
			$this->load->view('admin/inc_footer', $data);
			
		}
		else
		{
			redirect('');
		}

	}

	function addQuestion(){

		$this->form_validation->set_rules('surveyId', 'Survey Id', 'trim|required');
		$this->form_validation->set_rules('questionOptions', 'Questions Options', 'trim|required');
		$this->form_validation->set_rules('question', 'Question', 'trim|required');
		$this->form_validation->set_rules('questionType', 'Question Type', 'trim|required');
		
		if($this->form_validation->run())
		{
			$returnArray = $this->survey_model->addQuestion();
		}
		else
		{
			$returnArray = array('status'=>'failure');
		}

		echo json_encode($returnArray);

	}

	function editQuestions(){

	}

	function deleteQuestions(){

	}

	/*
		Question Functions Ends
	*/	

}