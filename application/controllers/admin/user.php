<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('tank_auth/users');
	}

	function index($page_no=1)
	{	

		$role 	=	$this->session->userdata('role');
		
		if($role == 2)
		{
			$data 	=	validate_my_params(	
												array(
														'limit' 	=>	'integer',
														'offset' 	=>	'integer',
														'page_no' 	=>	'integer',
														'filterBy'	=>  'valid_opts[name,email,role]',
											   			'filter'	=>	 '',
													)
											);
			
			if($data['status'] == 'success')
			{

				$data 				=	$data['data'];
				$data['page_no']	=	$page_no;
				if(!isset($data['limit']))
		    	{
		    		$data['limit'] 	=	20;
		    	}

		    	if(!isset($data['offset']))
		    	{
		    		$data['offset'] 	=	0;
		    	}

		    	if(isset($data['page_no']))
				{
			    	if($data['page_no'])
					{
						$data['offset'] 	=	$data['limit'] * ($data['page_no'] - 1);
					}
				}

		    	if(!isset($data['page_no']))
		    	{
		    		$data['page_no'] 	=	1;
		    	}

				
				$data['user_id']	= 	$this->tank_auth->get_user_id();
				$data['username']	= 	$this->tank_auth->get_username();

				$data	=	$this->user_model->read($data);

				$urlSuffix		=	array();
				$newUrlSuffix	=	array();

				//$data['page_title'] = site_title('Users');
				$data['page_title']  = 	pageTitleGenerator(array('Users'));

				if(isset($data['total_count']))
				{
					if($data['filterBy'] != '' && $data['filter'] != '')
					{
						$urlSuffix[]	=	'filterBy='.$data['filterBy'];
						$urlSuffix[]	=	'filter='.$data['filter'];
						$newUrlSuffix[]	=	'filterBy='.$data['filterBy'];
						$newUrlSuffix[]	=	'filter='.$data['filter'];
					}

					if(count($urlSuffix) > 0)
					{
						$urlSuffix 	   =	'?'.implode('&', $urlSuffix);
						$newUrlSuffix  =	'?'.implode('&', $newUrlSuffix);
					}
					else
					{
						$urlSuffix		=	'';
						$newUrlSuffix	=	'';
					}

					$data['urlSuffix'] 	=	$newUrlSuffix;

					$pageConfig['base_url'] 			= 	site_url('/admin/user/index');
					$pageConfig['total_rows'] 			= 	$data['total_count'];
					$pageConfig['per_page'] 			= 	$data['limit'];
					$pageConfig['uri_segment'] 			= 	4;
					$pageConfig['num_links'] 			= 	5;
					$pageConfig['suffix'] 				= 	$urlSuffix;
					$pageConfig['first_url'] 			= 	$pageConfig['base_url'].$pageConfig['suffix'];

				
					$pageConfig['first_link'] 			= 	'First';
					$pageConfig['last_link'] 			= 	'Last';
					$pageConfig['use_page_numbers'] 	= 	TRUE;
					$pageConfig['full_tag_open'] 		= 	'<ul class="pagination">';
					$pageConfig['full_tag_close'] 		= 	'</ul>';

					$pageConfig['num_tag_open'] 		= 	'<li>';
					$pageConfig['num_tag_close'] 		= 	'</li>';

					$pageConfig['cur_tag_open'] 		= 	"<li class='active'><a href='#'>";
					$pageConfig['cur_tag_close'] 		= 	'</a></li>';

					$pageConfig['prev_tag_open'] 		= 	'<li>';
					$pageConfig['prev_tag_close'] 		= 	'</li>';

					$pageConfig['next_tag_open'] 		= 	'<li>';
					$pageConfig['next_tag_close'] 		= 	'</li>';

					$pageConfig['first_tag_open'] 		= 	'<li>';
					$pageConfig['first_tag_close'] 		= 	'</li>';

					$pageConfig['last_tag_open'] 		= 	'<li>';
					$pageConfig['last_tag_close']	 	= 	'</li>';

					

					$this->pagination->initialize($pageConfig);

					$data['pagination_str'] 	=	$this->pagination->create_links();

				}

				if(!isset($data['pagination_str']))
				{
					$data['pagination_str'] 	=	'';
				}

				$filterOptionsArr			=	array(
													array(
															'id'=>'2',
															'value'	=>	'Admin',
															
														),
													array(
															'id'=>'1',
															'value'	=>	'Normal User',
															
														),

												);
				$data['filterAction'] = site_url().'admin/user';
				$data['filterOption'] = 	array(
												 	'email' => 	array('type'=>'input', 'value'=>'Email'),
								 					'name' 	=> 	array('type'=>'input', 'value'=>'Name'),
							 						
							 						'role'	=>	array('type'	=>'select', 
							 										  'value'	=>'Role', 
							 										   'options'=>	$filterOptionsArr)
												);

				$data['page_title']  	= 	pageTitleGenerator(array("User Management"));

				$this->load->view('admin/inc_header', $data);
				$this->load->view('admin/menu', $data);
				$this->load->view('app/user_management', $data);
				$this->load->view('admin/inc_footer', $data);
			}
			else
			{
				//$data['page_title'] = site_title('Users');
				
				$data['page_title']  	= 	pageTitleGenerator(array("User Management"));

				$this->load->view('admin/inc_header', $data);
				$this->load->view('admin/menu', $data);
				$this->load->view('app/user_management', $data);
				$this->load->view('admin/inc_footer', $data);
			}
		}
		else
		{
			redirect('');
		}
	}

	function update($update_role=0)
	{
		$role 	=	$this->session->userdata('role');
		
		if($role == 2)
		{
			$data 	=	validate_my_params(	
												array(
														'id' 		=>	'required|integer',
														'role' 		=>	'required|integer|valid_opts[1,2]'
													)
											);

			if($data['status'] == 'success')
			{
				$data 				=	$data['data'];
				$data['user_id']	= 	$this->tank_auth->get_user_id();
				$data['username']	= 	$this->tank_auth->get_username();

				$data	=	$this->user_model->update($data);

				redirect('/admin/user');
			}
			else
			{
				redirect('/admin/user');
			}

		}
		else
		{
			redirect('');
		}
	}

	function delete($userId=0)
	{
		$role 	=	$this->session->userdata('role');
		
		if($role == 2 && $userId != 0)
		{
			$this->users->delete_user($userId);

			redirect('/admin/user');
		}
		else
		{
			redirect('');
		}
	}

}