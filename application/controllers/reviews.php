<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reviews extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('reviews_model');
		$this->load->config("reviews");
		$this->load->helper('sail_helper');
		
	}
	
	
	public function index()
	{

		$this->createReview();
		
	}

		

	// HARi
	public function createReview($id= 0,$name='')
	{

		
		$this->form_validation->set_rules('schoolId', 'School Id','required|integer');
		$this->form_validation->set_rules('rating',   'Rating',    'integer|required');
		$this->form_validation->set_rules('userType', 'userType',  'required|integer'); 
		$this->form_validation->set_rules('studentType', 'studentType',  'required|integer'); 
		$this->form_validation->set_rules('gradYear', 'Graduation year','required|integer');
		$this->form_validation->set_rules('major',   'Major', 'required');
		$this->form_validation->set_rules('title', 'Title',  'required'); 
		$this->form_validation->set_rules('good', 'Good','required|trim');
		$this->form_validation->set_rules('bad',   'Bad',    'required|trim');
		$this->form_validation->set_rules('features',   'Features',    'required');
		$this->form_validation->set_rules('culture',   'Culture',    'required');

		if($this->input->get_post('major') == "Other")
		{
			$this->form_validation->set_rules('otherMajor',   'Major', 'required|trim');
		}
		else
		{
			$this->form_validation->set_rules('major',   'Major', 'required');
		}

		$data = array();

		if($id != 0)
		{
			$data['schoolId'] = $id;
			$data['schoolName'] = $name;		
		}
		else
		{
			$data['schoolId']   = $this->input->post('schoolId');
			$data['schoolname'] = $this->input->post('schoolname');
		}

		if($data['schoolId'])
		{
			$data['schoolName']	=	getSchoolName($data['schoolId']);
		}

		$data['reviewUserType']  = $this->config->item('reviewUserType');
		$data['reviewGradYear']  = $this->config->item('reviewGradYear');
		$data['reviewFeatures']  = $this->config->item('reviewFeatures');
		$data['reviewCulture']  = $this->config->item('reviewCulture');

		if ($this->form_validation->run() == FALSE)
		{
			$data['page_title']     = pageTitleGenerator(array("Write a review")); 
			$this->load->view('common/inc_header', $data);
			$this->load->view('common/menu_app', $data);
			$this->load->view('home/reviewAdd', $data);
			$this->load->view('common/inc_footer', $data);	

		}
		else
		{

			if($this->tank_auth->is_logged_in()	!=	TRUE)
			{
				redirect("auth/login");
			}
			// check the errors from the model
			$result = $this->reviews_model->create();


			
			if($result['status'] == "success")
			{
				
				redirect("home/school/".$data['schoolId']."/". url_title($data['schoolName']));

			}
			else
			{	
				// failure[LOAD THE FORM WITH ERROR]
				$data['errors']  = $result['errors'];


				
				$data['page_title']     = pageTitleGenerator(array("Write a review"));
				$this->load->view('common/inc_header', $data);
				$this->load->view('common/menu_app', $data);
				$this->load->view('home/reviewAdd', $data);
				$this->load->view('common/inc_footer', $data);

			}
			

		}
	
	}

	
	//Neeraj
	public function countFlags()
	{
		$data 	=	validate_my_params(	
										array(
												'schoolId' 		=>	'integer|required',
												'reviewId'		=>	'integer|required',
												'flag'			=>	'required|valid_opts[flag,helpfulFlag]',
												//'helpfulFlag'	=>	'integer'
											)
										);
		
		if($this->tank_auth->is_logged_in()	!=	TRUE)
		{
			//redirect("auth/login");
			echo json_encode(array("status"=>"failure","errors"=>"User is not logged in"));
		}
		else{
		
			if($data['status']	==	'success')
			{
				$result	=	$this->reviews_model->countFlags($data['data']);

				if($result['status'] == 'success')
				{
					echo json_encode($result);
				}
				else
				{
					echo json_encode($result);
				}
			}
			else
			{
				echo json_encode($data);
			}
		}

	}

	/*
	*	Name : HARi
	*	Date : 30 JAN 2015
	*	DESC : Logged in user can 
	*/
	function view()
	{	

		if($this->tank_auth->is_logged_in()	!=	TRUE)
		{
			//redirect("auth/login");
			echo json_encode(array("status"=>"failure","errors"=>"User is not logged in"));
		}
		else
		{

			$data = validate_my_params(
										array(
												'id' 	=>	'required|integer'
											)
									);



			if($data['status'] == 'success')
			{
				$data = $this->reviews_model->view($data['data']['id']);

				if($data['status'] == 'success')
				{

	       			$this->load->view('reviews/view',$data['data']);
				}
				else{
					echo $data['errors'];
				}
			}
			else
			{
				echo "<h3> No data received</h3>";
			}

		}

		
	}

	/*
	*	Name : HARi
	*	Date : 30 JAN 2015
	*	DESC : Delete review for the FE
	*/
	public function delete()
	{
		if($this->tank_auth->is_logged_in()	!=	TRUE)
		{
			//redirect("auth/login");
			echo json_encode(array("status"=>"failure","errors"=>"User is not logged in"));
		}
		else
		{
			$data	=	validate_my_params(
											array(
													'reviewId'	=>	'integer|required'
												)	
										);
			//dump($data);
			if($data['status']	==	'success')
			{
				$result	=	$this->reviews_model->deleteReview($data['data']['reviewId']);

				if($result['status']	==	'success')
				{
					echo json_encode($result);
				}
				else
				{
					echo json_encode($result);
				}
			}
			else
			{
				echo json_encode($data);
			}
		}
			
	}
	public function getMappings(){
		$mapping_entry = $this->input->post('mappingValue');
		$mapping_values = $this->config->item($mapping_entry.'-major');
		$final_str = "";
		$cc = 1;
		asort($mapping_values);
		foreach($mapping_values as $value){
			if($cc != count($mapping_values)){
				$final_str = $final_str . $value.",";
			}else{
				$final_str = $final_str . $value;
			}
			$cc++;
		}
		echo $final_str;
	}
}