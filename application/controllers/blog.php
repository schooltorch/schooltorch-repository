<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model("blog_model");
	}

	/* Updated By 
		NAME: Ranjit Randive
		DESC: On 30/01/2015
	*/
	public function index($pageNo = 0, $year= 0, $month = 0)
	{
		/// show the first page

		$limit = 10; //Default

		$data['page_no']	=	$pageNo;

		if(!isset($offset))
		{
    		$offset 	=	0;
    	}

		if(isset($data['page_no']))
		{
	    	if($data['page_no'])
			{
				$offset 	=	$limit * ($data['page_no'] - 1);
			}
		}

		$data = array();

		$data['limit']	=	$limit;
		$data['offset']	=	$offset;

		$data['year']	=	$year;
		$data['month']	=	$month;
		
		$data['articlesInfo']  	= 	$this->blog_model->articles($data);
		
		$config['base_url'] 	= 	site_url().'/blog/index/';

		$config['total_rows'] 	= 	$data['articlesInfo']['total_rows'];


		$config['per_page'] = $limit; 
		$config['full_tag_open'] = "<div><ul class='pager'>";
		$config['full_tag_close'] ="</ul></div>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		//$config['display_pages'] = FALSE;//[this is to show only the first and next thing]
		//$config['anchor_class']  = 'class="btn btn-link btn-lg"';
		$config['use_page_numbers'] = TRUE;
		/*
		$config['first_link'] = 'FIRST';
		$config['last_link'] = 'LAST';
		$config['next_link'] = 'NEXT';
		$config['prev_link'] = 'PREV';*/

		if($year != 0 && $month != 0)
		{
			$config['suffix'] = '/'. $year.'/'. $month;
		}

		$this->pagination->initialize($config); 

		$data['categories']    =  $this->blog_model->categories();
		$data['paginationString']  =  $this->pagination->create_links();

		if ($this->tank_auth->is_logged_in())
		{			
			$data['username'] 	=	$this->session->userdata('username');
			$data['role'] 		=	$this->session->userdata('role');
			$data['user_id'] 	=	$this->session->userdata('user_id');
		}

		$data['year']	=	$year;
		$data['month']	=	$month;

		$data['page_title']  	= 	pageTitleGenerator(array("Blog"));

		$this->load->view('common/inc_header', $data);
		$this->load->view('common/menu_app', $data);
		$this->load->view('blog/blogs', $data);
		$this->load->view('common/inc_footer', $data);
		
	}


	/*
		NAME: Neeraj
		DESC: to return all the categories.31/10/14
	*/
	public function categories()
	{
		$returnArr	=	$this->blog_model->categories();

		//dump($returnArr);
	}

	/*/*
		NAME: Neeraj
		DESC: to return all the articles.31/10/14
	*/
	/*public function articles($offset=0)
	{


		$limit = 2; //Default
		$config['base_url'] = site_url().'/blog/articles'; 
		$config['total_rows'] = $this->db->count_all_results('blog-articles');
		$config['per_page'] = $limit; 
		
		$returnArr	=	$this->blog_model->articles($limit,$offset);


		

		$this->pagination->initialize($config); 

		$data = array();

		$this->load->view('common/inc_header', $data);
		$this->load->view('common/menu_app', $data);
		$this->load->view('blog/blogs', $data);
		$this->load->view('common/inc_footer', $data);
		
		echo $this->pagination->create_links();

		dump($this->pagination->create_links());
		//dump($returnArr);
		
	}*/

	/*
		NAME: Neeraj
		DESC: to get all articles belonging to given category Id.31/10/14
	*/
	public function category($id = 0,$offset= 0)
	{
		
		$limit = 10; //Default
		$config['base_url'] = site_url().'/blog/category/'.$id."/"; 
		$config['total_rows'] = $this->db->like('categoryId',"|".$id."|")
									 		->where('isDeleted',NULL)
									 			->where('isPublished',1)
									 				->order_by('timestamp','desc')
														->count_all_results('blog-articles');
		$config['per_page'] = $limit;
		$config['uri_segment'] = 4; 
		$config['full_tag_open'] = "<div><ul class='pager'>";
		$config['full_tag_close'] ="</ul></div>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$config['display_pages'] = FALSE;
		$config['anchor_class']  = 'class="btn btn-link btn-lg"';
		//$config['use_page_numbers'] = TRUE;

		$config['first_link'] = 'FIRST';
		$config['last_link'] = 'LAST';
		$config['next_link'] = 'NEXT';
		$config['prev_link'] = 'PREV';


		$this->pagination->initialize($config); 


		$data = array();

		$tempArticlesInfo = $this->blog_model->category($id,2,$offset);
		$data['articlesInfo']   = $tempArticlesInfo['data']['articles'];
		$data['categories']    =  $this->blog_model->categories();
		$data['paginationString']  =  $this->pagination->create_links();

		$data['page_title']  	= 	pageTitleGenerator(array("Category","Blog"));

		$this->load->view('common/inc_header', $data);
		$this->load->view('common/menu_app', $data);
		$this->load->view('blog/blogs', $data);
		$this->load->view('common/inc_footer', $data);

		//dump($returnArr);
	}

	/*
		NAME: Neeraj
		DESC: to get the article belonging to given article Id.31/10/14
	*/
	public function article($id = 0)
	{
		
		$data = array();
		$data['articlesInfo']  = $this->blog_model->article($id);

		if($data['articlesInfo']['status']	==	'success')
		{
			$data['isArticle']  = 1;
		

			$data['categories']    =  $this->blog_model->categories();

			//dump($data);

			$data['page_title']  	= 	pageTitleGenerator(array($data['articlesInfo']['data'][0]['title']/*,"Article"*/,"Blog"));

			$this->load->view('common/inc_header', $data);
			$this->load->view('common/menu_app', $data);
			$this->load->view('blog/outerBlogArticle', $data);
			$this->load->view('common/inc_footer', $data);
		}
		else
		{
			redirect('/blog');
		}		
	}	
}