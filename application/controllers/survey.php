<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Survey extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model("survey_model");
		 $this->load->model('school_model');
		$this->load->config("survey");
	}
	
	
	public function index()
	{	
		echo $this->session->flashdata('message');
		//echo "in the index function";
	}

	public function add($id = 0)
	{
		if ($this->tank_auth->is_logged_in())
		{
	
			if($id == 0)
			{
				$this->session->set_flashdata('message','The school does not exist');
				echo "the id does not exist";
				//redirect(site_url('/home/school?is_survey=1'));	
			}
			else
			{
					$this->form_validation->set_rules('q1', 'Question1', 'required');
					$this->form_validation->set_rules('q2', 'Question2', 'required');
					$this->form_validation->set_rules('q3', 'Question3', 'required');
					$this->form_validation->set_rules('q4', 'Question4', 'required');
					$this->form_validation->set_rules('q5', 'Question5', 'required');
					$this->form_validation->set_rules('q6', 'Question6', 'required');
					$this->form_validation->set_rules('q7', 'Question7', 'required');
					$this->form_validation->set_rules('q8', 'Question8', 'required');
					$this->form_validation->set_rules('q9', 'Question9', 'required');
					$this->form_validation->set_rules('q10', 'Notes', '');
					


					if($this->form_validation->run() === FALSE) 
					{
						$data = array();

						$data['questions'] 	= 	$this->survey_model->readQuestions($id);

						//dump($data['questions']);


						if($data['questions']['status'] == 'failure')
						{
							$this->session->set_flashdata('message','The school does not exist');
							redirect(site_url('/home/school/'.$id.'?is_survey=1'));
						}
						else
						{
							$data['questions'] 	=	$data['questions']['data'];
						}
						
						$schoolInfo = $this->db->select('INSTNM')->from("schools")->where('UNITID',$id)->limit(1)->get()->row();

						if(count($schoolInfo) == 0)
						{
							redirect(site_url('/home/school/'.$id.'?is_survey=1'));
						}
						else
						{

							$data['username'] 	=	$this->session->userdata('username');
							$data['role'] 		=	$this->session->userdata('role');
							$data['user_id'] 	=	$this->session->userdata('user_id');
							$data['schoolName'] = 	$schoolInfo->INSTNM;

							$data['id']			= 	$id;

						
							$this->load->view('common/inc_header', $data);
							$this->load->view('common/menu_app', $data);
							$this->load->view('home/surveyAdd', $data);
							$this->load->view('common/inc_footer', $data);

						}
					}
					else
					{
						
						$data   = array();
						$data['id']  = $id;
						$data['userId'] 	=	$this->session->userdata('user_id');
						$result = $this->survey_model->createSurvey($data);

						$schoolInfo = $this->db->select('INSTNM')->from("schools")->where('UNITID',$id)->limit(1)->get()->row();
						
						if($result['status'] == "success")
						{
							redirect(site_url('/home/school/'.$id.'?is_survey=1'));
							//redirect(site_url('/home/schools?is_survey='.$id));
						}
						else
						{
							redirect(site_url('/home/school/'.$id.'?is_survey=1'));
							//echo "Already rated the school cannot ,rate again !!";
							//redirect(site_url('/home/schools?is_survey='.$id));
						}
					}
			}
		}
		else
		{
			redirect("auth/login");

		}
	}

	public function test()
	{
		echo  "<pre>";
		print_r($_POST);
		echo  "</pre>";

	}


}