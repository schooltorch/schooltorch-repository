<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		
		$data['page_id'] = ''; 

		$this->load->view('common/inc_header', $data);
		$this->load->view('common/menu_app', $data);
		$this->load->view('settings/settings', $data);
		$this->load->view('common/inc_footer', $data);

	}
}
