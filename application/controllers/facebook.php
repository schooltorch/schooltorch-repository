<?php
defined('BASEPATH') OR exit('No direct script access allowed');
session_start();
use Facebook\FacebookSession;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;

class Facebook extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('user_model');
		$this->load->model('tank_auth/users');
		FacebookSession::setDefaultApplication($this->config->item('appId'), $this->config->item('secret'));

	}

	public function auth()
	{
		$session = FacebookSession::newAppSession();


		try
		{
			$session->validate();

			$helper = new FacebookRedirectLoginHelper(site_url('/facebook/callback'));

			$loginUrl = $helper->getLoginUrl();

			redirect($loginUrl);

		}
		catch(FacebookRequestException $ex)
		{
			// Session not valid, Graph API returned an exception with the reason.
			echo $ex->getMessage();
		}
		catch(\Exception $ex)
		{
			// Graph API returned info, but it may mismatch the current app or have expired.
		  	echo $ex->getMessage();
		}



	}

	public function callback()
	{
		
		$helper = new FacebookRedirectLoginHelper(site_url('facebook/callback'));
		
		$session 	=	NULL;

		try{
			$session = $helper->getSessionFromRedirect();
		}
		catch(FacebookRequestException $ex){
			// When Facebook returns an error
			echo $ex->getMessage();
			$this->session->set_flashdata('message', 'facebookLoginError');
			redirect(site_url());
		}
		catch(\Exception $ex){
			// When validation fails or other local issues
			$this->session->set_flashdata('message', 'facebookLoginError');
			redirect(site_url());
		}
		
		if($session)
		{
			$request 		= 	new FacebookRequest($session, 'GET', '/me', array('fields' => 'id,name,picture,email,first_name,last_name'));

			$response 		= 	$request->execute();

			$graphObject 	= 	$response->getGraphObject();

			$userData 		=	array(
										'id'		=>	$graphObject->getProperty('id'),
										'name'		=>	$graphObject->getProperty('name'),
										'picture'	=>	$graphObject->getProperty('picture'),
										'email'		=>	$graphObject->getProperty('email')
									);

			$userData['picture']	=	$userData['picture']->getProperty('url');

			$userData 		=	json_encode($userData);

			$fb_id 			=	$graphObject->getProperty('id');

			$this->session->set_userdata('facebook_id', $fb_id);

			$user 			= 	$this->user_model->get_user_by_sm(array('facebook_id' => $fb_id), 'facebook_id');

			$user_id = $this->session->userdata('user_id');

			if( sizeof($user) == 0  && !$user_id) 
			{

				$this->db->insert('profileStore', array(
														'profileId'	=>	$fb_id,
														'data'		=>	$userData
													)
								);

				$insertId 	=	$this->db->insert_id();

				$this->session->set_userdata('facebook_insert_id', $insertId);

				redirect('auth_other/fill_user_info');
			}
			else if(sizeof($user) == 0 && $user_id)
			{

				$usersCount	=	$this->db->select('id')
											->from('user_profiles')
												->where('user_id', $user_id)
													->get()
														->result_array();

				if(count($usersCount) > 0)
				{
					$this->db->where('user_id', $user_id)
								->set('facebook_id', $fb_id)
									->set('facebook_data', $userData)
										->where('user_id', $user_id)
											->update('user_profiles');
				}
				else
				{
					$insertArray 	=	array(
												'user_id'		=>	$user_id,
												'facebook_id'	=>	$fb_id,
												'facebook_data'	=>	$userData
											);

					$this->db->insert('user_profiles', $insertArray);

				}

				redirect('profile');
			}
			else if(!$user_id)
			{

				$this->db->set('facebook_data', $userData)
							->where('user_id', $user[0]->id)
								->update('user_profiles');

				$user 		=	$user[0];

				$picture 	=	getUserPicture($user);

				$this->session->set_userdata(array(
						'user_id'	=> $user->id,
						'email'		=> $user->email,
						'username'	=> $user->username,
						'role'		=> $user->role,
						'picture'	=>	$picture,
						'facebook_id'	=> $user->facebook_id,
						'onlyGradSchools'	=> $user->onlyGradSchools,
						'status'	=> ($user->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED,
				));
				
				//$this->tank_auth->clear_login_attempts($user[0]->email); can't run this when doing FB
				$this->users->update_login_info( $user->id, $this->config->item('login_record_ip'), 
												 $this->config->item('login_record_time'));

				if($user->role == 1)
				{
					redirect('auth', 'refresh');
				}	
				else
				{
					redirect('admin', 'school');
				}
			}
			else if($user_id != $user[0]->id)
			{

				$usersCount	=	$this->db->select('id')
											->from('user_profiles')
												->where('user_id', $user_id)
													->get()
														->result_array();

				if(count($usersCount) > 0)
				{
					$this->db->where('user_id', $user_id)
								->set('facebook_id', $fb_id)
									->set('facebook_data', $userData)
										->where('user_id', $user_id)
											->update('user_profiles');
				}
				else
				{
					$insertArray 	=	array(
												'user_id'		=>	$user_id,
												'facebook_id'	=>	$fb_id,
												'facebook_data'	=>	$userData
											);

					$this->db->insert('user_profiles', $insertArray);

				}

				$this->db->set('facebook_id', NULL)
							->set('facebook_data', NULL)
								->where('facebook_id', $fb_id)
									->where('user_id', $user[0]->id)
										->update('user_profiles');

				redirect('profile');

			}
			else
			{

				$this->db->set('facebook_data', $userData)
							->where('user_id', $user[0]->id)
								->update('user_profiles');

				/*// simulate what happens in the tank auth
				$this->session->set_userdata(array(	'user_id' => $user[0]->id, 'role' => $user[0]->role, 'username' => $user[0]->username,
													'status' => ($user[0]->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED));*/
	
				$user 		=	$user[0];
				$picture 	=	getUserPicture($user);

				$this->session->set_userdata(array(
						'user_id'	=> $user->id,
						'email'		=> $user->email,
						'username'	=> $user->username,
						'role'		=> $user->role,
						'picture'	=>	$picture,
						'facebook_id'	=> $user->facebook_id,
						'onlyGradSchools'	=> $user->onlyGradSchools,
						'status'	=> ($user->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED,
				));
				
				//$this->tank_auth->clear_login_attempts($user[0]->email); can't run this when doing FB
				$this->users->update_login_info( $user->id, $this->config->item('login_record_ip'), 
												 $this->config->item('login_record_time'));

				if($user->role == 1)
				{
					redirect('auth', 'refresh');
				}	
				else
				{
					redirect('admin', 'school');
				}

			}

		}
		else
		{
			
		}

	}

	public function disconnect()
	{

		$data = $this->user_model->disconnectFacebook();

		redirect('profile');

	}

}