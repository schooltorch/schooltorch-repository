<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Localweather extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		if($this->input->get('lat') && $this->input->get('lon'))
		{
			$response = json_decode(file_get_contents('http://forecast.weather.gov/MapClick.php?lat='. $this->input->get('lat') .'&lon='. $this->input->get('lon') .'&FcstType=json'));
			
			$data =	array(
						'period' 	=> $response->time->startValidTime,
						'text' 		=> $response->data->weather,
						'icon' 		=> $response->data->iconLink
					);
			$this->load->view('app/localweather', $data);
		}	
		else
		{
			echo 'lat and lon required';
		}
	}

	function getImage()
	{
		$response = getSchoolImage($this->input->get('UNITID'));
		
		if($response != '')
		{
			foreach($response as $imgsrc)
			{
				echo '<img src="'. $imgsrc .'" border="0" /><br />';
			}
		}
		
	}

	function def()
	{
		$this->load->model('school_model');
		dump($this->school_model->compareSchools(array('100724', '384306')));
	}

	function t()
	{	
		dump(parseLocale(12));
		echo count($this->db->where('isGoogleLocationPicsProcessed is NULL')->from('schools')->get()->result());
	}

	function importSchoolToLocal()
	{
		$order = 'asc';
		if($this->input->get('order'))
		{
			$order = 'desc';
		}

		$processedIds = array();
		$query = $this->db->where('isGoogleLocationPicsProcessed is NULL')->from('schools')->order_by('INSTNM',$order)->limit(50)->get()->result();

		foreach($query as $school)
		{
			dump($school->INSTNM);
			$response = getSchoolLocationPics($school);
			dump($response);
			$processedIds[] = $school->ID;
			if($response != '')
			{
				foreach($response as $imgsrc)
				{
					echo '<img src="'. locationPicsBaseUrl().$imgsrc .'" border="0" /><br />';
				}
			}
		}	

		
		if(count($processedIds) > 0)
		{
			$this->db->set('isGoogleLocationPicsProcessed', 1)->where_in('ID', $processedIds)->update('schools');
		}
		

		dump("ALL DONE!!!");
	}

	
}

//AIzaSyDUIJCoUiNA9x1egNNGyZmvxWLc2XvlbtY
//https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyDUIJCoUiNA9x1egNNGyZmvxWLc2XvlbtY&location=42.445291,-76.482690&rankby=distance&types=school|university&keyword=Cornell University


//https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyDUIJCoUiNA9x1egNNGyZmvxWLc2XvlbtY&maxwidth=1296&photoreference=CoQBcQAAAAQW7XFrIEnXvqX_B40dvkuGjNqyFTPmXY6dGc8FuTGl1Mbsik-7v8IBKmZtKxvXE3Bx4ZeRvQGMpcJOBpb4ZYk0slXAWU6ZFvdnbFzvMELmNV2YVvJMJ0Uo9rVXaCppTqKbFhYgaYOxC3Bb6VleY_0N0R_MsQRPUNW4trep4iDrEhAdZNwyRcamB7uGK-2_-LmvGhTF7HmO0cjKuktHzvKyJwnuTD7DrA