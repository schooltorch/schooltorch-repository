<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * validate all parameters
 * Kenrick Vaz
 * 20 Sept, 2011
 *
*/



function validate_my_params($params = array(),$is_post = 0)
{

	$CI = &get_instance();

	$errors 	=	array();

	if(count($params) > 0)
	{

		$return_vars = array();

		foreach ($params as $param => $options)
		{

			$i = trim($CI->input->get_post($param, TRUE));

			/*if($is_post == 1)
			{

				$i = trim($CI->post($param, TRUE));

			}
			elseif($is_post == 0)
			{

				$i = trim($CI->get($param, TRUE));

			}
			elseif($is_post == 2)
			{

				$i = trim($CI->input->get_post($param, TRUE));

			}*/

			$options = explode('|',$options);

			$content_flag = 0;
			$valid_content_fields = array('content','comment','description','name','title','message');
			

			if(in_array($param, $valid_content_fields) && $is_post == 1)
			{
				$content_flag = 1;

				//replacing % with html code (Kenrick Vaz, 16 April 2013)
				$i = str_replace('%', '&#37;', $i);
			}
		

			foreach($options as $option)
			{
				
				/*if($i == '0' && $content_flag == 0)
				{

					$i = NULL;

				}*/
				
				
				if($i == '')
				{

					$i = NULL;

				}
					
				if($option != "" && $option != NULL)
				{

					if (($i === FALSE || $i == "" || $i == NULL) && $option == "required")
					{
						$errors['status'] 	=	'failure';
						$errors['message'] 	=	'Missing or empty parameter `'. $param .'`';
						
						return $errors;
					}
					elseif($i != NULL && $i !== FALSE && $i != NULL)
					{
							
						if($option == 'valid_email')
						{
							$i = strtolower($i);
						}	
						
						if($CI->form_validation->valid_email($i) == 0 && $option == 'valid_email')
						{

							$errors['status'] 	=	'failure';
							$errors['message'] 	=	'Invalid Email Address passed';
							
							return $errors;

						}
						elseif($option == 'valid_date' && valid_datetime($i) == FALSE )
						{

							$errors['status'] 	=	'failure';
							$errors['message'] 	=	'Invalid Date format passed';
							
							return $errors;

						}
						elseif($option == 'valid_datetime' && valid_datetime($i,1) == FALSE )
						{
							$errors['status'] 	=	'failure';
							$errors['message'] 	=	'Invalid Date format passed';
							
							return $errors;

						}
						elseif($option == 'valid_phone' && my_valid_phone($i) == 0)
						{
								
							$errors['status'] 	=	'failure';
							$errors['message'] 	=	'Invalid phone number passed';
							
							return $errors;

						}
						elseif($CI->form_validation->numeric_dash($i) == FALSE && $option == 'numeric_dash')
						{

							$errors['status'] 	=	'failure';
							$errors['message'] 	=	'Only numbers with underscore allowed for `'. $param .'`';
							
							return $errors;

						}
						elseif($CI->form_validation->integer($i) == FALSE && $option == 'integer')
						{

							$errors['status'] 	=	'failure';
							$errors['message'] 	=	'Only integers allowed for `'. $param .'`';
							
							return $errors;

						}
						elseif($CI->form_validation->numeric($i) == FALSE && $option == 'numeric')
						{

							$errors['status'] 	=	'failure';
							$errors['message'] 	=	'Only numbers allowed for `'. $param .'`';
							
							return $errors;

						}
						elseif($CI->form_validation->alpha($i) == FALSE && $option == 'alpha')
						{
							$errors['status'] 	=	'failure';
							$errors['message'] 	=	'Only alphabets allowed for `'. $param .'`';
							
							return $errors;
							
						}
						elseif($CI->form_validation->alpha_numeric($i) == FALSE && $option == 'alpha_numeric')
						{
	
							$errors['status'] 	=	'failure';
							$errors['message'] 	=	'Only alphabets and numbers allowed for `'. $param .'`';
							
							return $errors;
	
						}
						else
						{

							if(strstr($option,'valid_opts'))
							{
								$option = explode('[',$option);
								$option = explode(']',$option[1]);
									
								$valid_opts = explode(',',$option[0]);
									
								if(!in_array($i,$valid_opts))
								{
									$errors['status'] 	=	'failure';
									$errors['message'] 	=	'Match required `'. $option[0] .'` `'. $param .'`';
									
									return $errors;
								}
									
							}
								
						}
					}

				}
					
					
				if($option == 'valid_date')
				{
					$i = valid_datetime($i);
				}
				elseif($option == 'valid_datetime')
				{

					$i = valid_datetime($i,1);

				}

			}

			$return_vars[$param] = $i;

		}

		$return_array 	=	array();

		$return_array['status'] 	=	'success';

		$return_array['data'] 		=	$return_vars;

		return $return_array;

	}

}


/**
 * valid phone
 */
function my_valid_phone($param)
{
	//echo $param;
	$param	=	trim($param , '+');
	
	if(is_numeric($param))
	{
		return 1;
	}
	else
	{
		return 0;
	}


	//return ( ! preg_match("/^\+?\(?[0-9]{0,2}\)?\-?[0-9]{10}$/i", $param)) ? 0 : 1;

}

//date validation
function valid_datetime( $str, $is_time = 0 )
{
	if($str == null)
	{

		return FALSE;

	}
	else
	{
			
		if(is_numeric($str) == 1 || strlen($str) == 0  )
		{
			return FALSE;
		}
			
			
		$stamp = strtotime( $str );
			
		if($stamp	==	 NULL)
		{
			return FALSE;
		}

		$month = date( 'm', $stamp );
		$day   = date( 'd', $stamp );
		$year  = date( 'Y', $stamp );
		 
		if(checkdate($month, $day, $year))
		{
			if($is_time == 0)
			{

				return $year.'-'.$month.'-'.$day;
					
			}
			elseif($is_time == 1)
			{
				return date('Y-m-d H:i:s', $stamp);
			}

		}

			
		 
		return FALSE;

	}
	 
}

function show_common_success($data)
{
	$CI= &get_instance();
	
	$CI->output->set_output(json_encode($return_array));
}



function show_common_error($message , $code, $response_code = 200,$field_name = '')
{

		
	$CI= &get_instance();

	$return_array = array();

	$return_array['status']		=	'failure';
	$return_array['error']		=	$message;

	if($field_name != '')
	{
		$return_array['field_name'] = 	$field_name;
	}

	$return_array['error_code'] = 	$code;

	$CI->output->set_output(json_encode($return_array));

}

function show_custom_error($message , $code, $response_code = 403,$field_name = '')
{

	$CI= &get_instance();

	if($response_code == 401 && $CI->input->get_post('gdiz_strike_web') == '1')
	{
		//set_access_ip_error();
	}

	$return_array = array();

	$return_array['status']		=	'failure';
	$return_array['error']		=	$message;

	if($field_name != '')
	{
		$return_array['field_name'] = 	$field_name;
	}

	$return_array['error_code'] = 	$code;
	
	//$CI->response($return_array, $response_code);
	//echo json_encode( $return_array );
	$CI->output->set_output(json_encode($return_array));
	//return false;
}

?>