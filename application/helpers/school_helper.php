<?php
	
	function parseLocale($val)
	{
		if($val > 10 && $val < 20)
		{
			return 'City';
		}
		elseif($val > 20 && $val < 30)
		{
			return 'Suburban';
		}
		elseif($val > 30 && $val < 40)
		{
			return 'Town';
		}
		elseif($val > 40 && $val < 50)
		{
			return 'Rural';
		}
		else
		{
			return '-';
		}
	}

	function parseAcceptance($val)
	{
		$CI=&get_instance();

		$opts = $CI->config->item('schoolAcceptance');

		if($val == 0)
		{
			return $opts[0];
		}
		else if($val > 0 && $val < 20)
		{
			return $opts[1];
		}
		else if($val > 20 && $val < 40)
		{
			return $opts[2];
		}
		else if($val > 4&& $val < 60)
		{
			return $opts[3];
		}
		else if($val > 60 && $val < 80)
		{
			return $opts[4];
		}
		else if($val > 80)
		{
			return $opts[4];
		}
	}

	function parseSelectivity($val)
	{
		$CI=&get_instance();

		$opts 	= 	$CI->config->item('schoolAcceptance');
		
		if($val >= 700)
		{
			//return $opts[0];
			return 5;
		}
		else if($val <= 700 && $val > 600)
		{
			//return $opts[1];
			return 4;
		}
		else if($val <= 600 && $val > 550)
		{
			//return $opts[2];
			return 3;
		}
		else if($val <= 550 && $val > 500)
		{
			//return $opts[3];
			return 2;
		}
		else if($val <= 500)
		{
			//return $opts[4];
			return 1;
		}
		else
		{
			//return $opts[4];
			return 1;
		}
		
	}

	function parseCountry($val)
	{
		if($val < 1000)
		{
			return 'United States';
		}
		elseif($val == 1000)
		{
			return 'Canada';
		}
		elseif($val == 1001)
		{
			return 'Australia';
		}
		elseif($val == 1002)
		{
			return 'New Zealand';
		}
		elseif($val == 1003)
		{
			return 'United Kingdom';
		}
		else
		{
			return 'United States';
		}
	}
	
	function getSchoolImage($unitid, $isForce = 0)
	{
		$CI=&get_instance();

		if($unitid)
		{
			//first check if photo is present!
			$query = $CI->db->select('LONGITUD, LATITUDE, INSTNM, ADDR, CITY, photo, schools.UNITID AS SCHOOLID')
								->from('schools')
									->join('schoolsImages', 'schools.UNITID = schoolsImages.UNITID')
										->where('schools.UNITID', $unitid)
											->limit(1)
												->get()
													->row();

			if($query->photo)
			{
				return explode($CI->config->item('schoolPhotoGlue'), $query->photo);
			}
			else
			{
				try{

					return getSchoolLocationPics($query);
					

				}
				catch(Exception $e){
					return '';
				}
				
			}
			

		}
		else
		{
			return '';
		}
	}

	
	function getSchoolLocationPics($query)
	{
		$CI=&get_instance();

		/*
		try{
			$nameFrags = $query->INSTNM .' '. $query->ADDR .' '. $query->CITY;
			$url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?key='. urlencode($CI->config->item('googlePlacesKey')) .'&location='. urlencode($query->LATITUDE) .','. urlencode($query->LONGITUD) .'&rankby=distance&types=school|university&query='. urlencode($nameFrags);
			$ch = curl_init();
				
		        curl_setopt($ch, CURLOPT_URL, $url);
		        curl_setopt($ch, CURLOPT_HEADER, 0);
		       	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 

			$result = json_decode(curl_exec($ch));

			//dump($url);
			//dump($result);
			curl_close($ch); 
			
			if(count($result->results) > 0)
			{
				$imageRef = array();
				$nameFrags = explode(' ', $nameFrags);

				$nameFrags 	=	array_filter($nameFrags);

				foreach($result->results as $item)
				{
					$counter = 0;
					foreach($nameFrags as $nf)
					{
						if(strpos($item->name, $nf) !== false)
						{	
							$counter++;
						}
					}

					if(isset($item->photos) && $counter != 0)
					{
						foreach($item->photos as $photo)
						{
							if($photo->width > 500)
							{
								$imageRef[] = downloadRemoteFile($query->UNITID, 'https://maps.googleapis.com/maps/api/place/photo?key='. urlencode($CI->config->item('googlePlacesKey')) .'&maxwidth='. $photo->width .'&photoreference='. $photo->photo_reference);
							}
						}
						
					}
				}

				if($imageRef == '')
				{
					return '';
				}
				else
				{
					setSchoolPhotos($imageRef, $query->SCHOOLID);

					return $imageRef;
				}
			}
			else
			{
				return '';
			}
		}
		catch(Exception $e){
			return '';
		}
		*/
		return '';
	}

	function schoolLocationPics($query)
	{
		$CI=&get_instance();
		
		try{
			$nameFrags = $query->INSTNM .' '. $query->ADDR .' '. $query->CITY;
			$url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?key='. urlencode($CI->config->item('googlePlacesKey')) .'&location='. urlencode($query->LATITUDE) .','. urlencode($query->LONGITUD) .'&rankby=distance&types=school|university&query='. urlencode($nameFrags);
			$ch = curl_init();
				
		        curl_setopt($ch, CURLOPT_URL, $url);
		        curl_setopt($ch, CURLOPT_HEADER, 0);
		       	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 

			$result = json_decode(curl_exec($ch));

			//dump($url);
			//dump($result);
			curl_close($ch); 
			
			if(count($result->results) > 0)
			{
				$imageRef = array();
				$nameFrags = explode(' ', $nameFrags);

				$nameFrags 	=	array_filter($nameFrags);

				foreach($result->results as $item)
				{
					$counter = 0;
					foreach($nameFrags as $nf)
					{
						if(strpos($item->name, $nf) !== false)
						{	
							$counter++;
						}
					}

					if(isset($item->photos) && $counter != 0)
					{
						foreach($item->photos as $photo)
						{
							if($photo->width > 500)
							{
								$imageRef[] = downloadRemoteFile($query->UNITID, 'https://maps.googleapis.com/maps/api/place/photo?key='. urlencode($CI->config->item('googlePlacesKey')) .'&maxwidth='. $photo->width .'&photoreference='. $photo->photo_reference);
							}
						}
						
					}
				}

				if($imageRef == '')
				{
					return '';
				}
				else
				{
					return $imageRef;
				}
			}
			else
			{
				return '';
			}
		}
		catch(Exception $e){

			return '';
		}
		
	}

	function setSchoolPhotos($imgArr, $unitid)
	{

		$CI 	=	&get_instance();

		$images =	$CI->db->select('UNITID,photo')
								->from('schoolsImages')
									->where('UNITID', $unitid)
										->get()
											->result_array();

		if(count($images) > 0)
		{
			$images 	=	$images[0];

			if($images['photo'] != NULL && $images['photo'] != '')
			{

				$images['photo'] 	=	explode($CI->config->item('schoolPhotoGlue'), $images['photo']);

				$imgArr 			=	array_merge($images['photo'], $imgArr);

			}

		}

		$imgArr 	=	array_filter(array_unique($imgArr));

		$CI->db->set('photo', implode($CI->config->item('schoolPhotoGlue'),$imgArr))->where('UNITID', $unitid)->update('schoolsImages');



	}

	function deleteSchoolPhotos($url, $unitid)
	{

		if($url != '')
		{
			$CI 	=	&get_instance();

			$images =	$CI->db->select('UNITID,photo')
									->from('schoolsImages')
										->where('UNITID', $unitid)
											->get()
												->result_array();

			if(count($images) > 0)
			{
				$images 	=	$images[0];

				if($images['photo'] != NULL && $images['photo'] != '')
				{

					$images['photo'] 	=	explode($CI->config->item('schoolPhotoGlue'), $images['photo']);

				}
				else
				{
					$images['photo'] 	=	array();
				}

				$key 	=	array_search($url, $images['photo']);

				unset($images['photo'][$key]);

				$images['photo'] 	=	array_filter(array_unique($images['photo']));

				if(count($images['photo']) > 0)
				{
					$CI->db->set('photo', implode($CI->config->item('schoolPhotoGlue'),$images['photo']));
				}
				else
				{
					$CI->db->set('photo', '');
				}

				$CI->db->where('UNITID', $unitid)->update('schoolsImages');

			}
		}

	}


	function getSchoolExtraData($schoolIds=array())
	{
		$CI=&get_instance();

		$viewCountResultArray = array();
		if ($CI->tank_auth->is_logged_in())
		{
			
			if(count($schoolIds) > 0)
			{

				$data = array();

				$userId	   = $CI->session->userdata('user_id');

				$savedSchoolArray = array();
				$votedSchoolArray = array();
				


				$savedResult = $CI->db->select('id,schoolId')	
										->from('schoolSavedList')	
											->where_in('schoolId',$schoolIds)
												->where('userId',$userId)
													->where('isDeleted IS NULL')
														->get()
															->result_array();
				
				if(count($savedResult) > 0){

					foreach ($savedResult as $value) {
						
						$savedSchoolArray[$value['schoolId']] = $value['id'];

					}

				}	

				$votedResult = $CI->db->select('id,schoolId')	
									->from('schoolVotedList')	
										->where_in('schoolId',$schoolIds)
											->where('userId',$userId)
												->where('isDeleted IS NULL')
													->get()
														->result_array();

				if(count($votedResult) > 0){

					foreach ($votedResult as $value) {
						
						$votedSchoolArray[$value['schoolId']] = $value['id'];

					}

				}	

				//Get the view count for the schoolids 

				$viewCountResult = $CI->db->select('id,schoolId,count')	
										->from('schoolsViewCount')	
											->where_in('schoolId',$schoolIds)
												->where('isDeleted IS NULL')
													->get()
														->result_array();

				if(count($viewCountResult) > 0){

					foreach ($viewCountResult as $value) {
						
						$viewCountResultArray[$value['schoolId']] = $value['count'];

					}

				}	


				$data['savedSchools'] = $savedSchoolArray;
				$data['votedSchools'] = $votedSchoolArray;
				$data['viewsCount']   = $viewCountResultArray;

				return $data;

			}
			else
			{
				return array();
			}

		}
		else
		{

			$data = array();
		
            if(count($schoolIds) > 0)
		    {
		   		$viewCountResult = $CI->db->select('id,schoolId,count')	
									->from('schoolsViewCount')	
										->where_in('schoolId',$schoolIds)
											->where('isDeleted IS NULL')
												->get()
													->result_array();
				$viewCountResultArray = array();
													
				if(count($viewCountResult) > 0){

					foreach ($viewCountResult as $value) {
						
						$viewCountResultArray[$value['schoolId']] = $value['count'];

					}

				}	

				$data['viewsCount']   = $viewCountResultArray;
			}

			return $data;
		}

	}	


	function downloadRemoteFile($uid, $url)
	{
		$fileName = "locationPics/$uid-".randomNoGen() .".jpg";
		$fp = fopen("./".$fileName, "w");
		fwrite($fp, getRemoteFile($url));
		fclose($fp);

		return $fileName;
	}

	function randomNoGen()
	{
		return time() . rand (1001, 9999);
	}

	function locationPicsBaseUrl()
	{
		return base_url();
	}

	function getRemoteFile($url)
	{
		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	    $tmp = curl_exec($ch);
	    curl_close($ch);
	    if ($tmp != false){
	        return $tmp;
	    }
	}
?>