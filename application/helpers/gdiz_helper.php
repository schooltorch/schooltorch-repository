<?php
	
	/*
	name - Gaurav sharma
	desc - getting thumb url
	date - 02/02/2015
	*/
	function getThumbUrl($fileName)
	{

	    $CI         =&  get_instance();
	    if(!($fileName == NULL || $fileName == ''))
	    {
	        $ext = strrchr($fileName, '.');
	        $name = ($ext === FALSE) ? $fileName : substr($fileName, 0, -strlen($ext));
	        $fileName   =   $name.'_thumb'.$ext;

	        return array(
	                        'status' => 'success',
	                        'data' => $fileName
	                    );
	    }
	    else
	    {
	        return array(
	                        'status' => 'failure',
	                        'errors' => 'Empty or missing parameter fileName.'
	                    );
	    }
	}  
	/*
		NAME: Ranjit Randive
		DESC: Get School Name
	*/

	function getSchoolName($schoolId)
	{
		$CI=& get_instance();

		$schoolName 		=	'';

		if($schoolId)
		{
			$school 		=	$CI->db->select('INSTNM')
											->from('schools')
												->where('UNITID', $schoolId)
													->limit(1)
														->get()
															->result_array();

			if(count($school) > 0)
			{
				$schoolName 	=	$school[0]['INSTNM'];
			}

		}

		return $schoolName;

	}

	function add_log($data)
	{	

		/*
		$CI=& get_instance();

		
		if($comment != NULL)
		{
			$comment_data =  array();
			$comment_data =  str_split($comment,740000);
			
			foreach($comment_data as $row)
			{
				$data=array(					
								'name'		=>	$row,
								'url'		=>	uri_string(),
							);			
				$CI->db->insert('logs',$data);
			}	
			
		}*/

		$CI=&get_instance();
			
		$postData = array();

		if(is_array($data)){
			$postData['name'] 	= json_encode($data);	
		}
		else
		{
			$postData['name'] 	= $data;
		}
		
		$postData['url']	= uri_string();	

		$CI->db->insert('logs',$postData);		


	}

	function updateSessionData()
	{

		$CI=& get_instance();

		$CI->load->model('tank_auth/users');

		$user = $CI->users->get_user_by_email($CI->session->userdata('email'));

		if (!is_null($user = $CI->users->get_user_by_email($CI->session->userdata('email')))) {

			$picture 	=	getUserPicture($user);

			$CI->session->set_userdata(array(
				'user_id'	=> $user->id,
				'email'		=> $user->email,
				'username'	=> $user->username,
				'role'		=> $user->role,
				'picture'	=>	$picture,
				'facebook_id'	=> $user->facebook_id,
				'onlyGradSchools'	=> $user->onlyGradSchools
			));

			return array('status' => 'success');

		}
		else
		{
			return array('status' => 'failure');
		}
	}

	function dump($opt)
	{
		echo '<pre>';
		print_r($opt);
		echo '</pre>';
		echo('*****************************');
	}


	function getFileExt($filename)
	{
		return end((explode(".", $filename))); # extra () to prevent notice
	}

	function site_title($title="",$sub_title="",$sub_sub_title=""){

		$CI=&get_instance();

		if($sub_sub_title != "")
		{
			return $title." <span>-</span> ".$sub_title." <span>-</span> ".$sub_sub_title;	
		}
		else if($sub_title != "")
		{
			return $title." <span>-</span> ".$sub_title;	
		}
		else if($title != "")
		{
			return $CI->config->item('website_name').' - '.$title;
		}
		else
		{
			return $CI->config->item('website_name');
		}

	}

	function gateman()
	{
		$CI=& get_instance();

		if($CI->tank_auth->is_logged_in())
		{
			return TRUE;
		}
		else
		{
			redirect('auth/login');
		}
	}

	function isAdmin()
	{
		$CI=& get_instance();

		if($CI->tank_auth->is_logged_in())
		{
			$role 	=	$CI->session->userdata('role');
		
			if($role == 2)
			{
				return TRUE;
			}
			else
			{
				redirect('auth/login');
			}
		}
		else
		{
			redirect('auth/login');
		}
	}

	function formatDate($date){

			return date('d M, y \a\t H:i a',strtotime($date));

	}

	function pageTitleGenerator($pageArr= array())
	{
		$CI=& get_instance();
		
		$pageTitle = "";

		$separator = $CI->config->item('titleSeparator');
		$siteName =  $CI->config->item('siteName');


		if(count($pageArr) > 0)
		{
			$pageTitle = implode($separator,$pageArr).$separator.$siteName;
		}
		else
		{
			$pageTitle = $siteName;
		}

		return $pageTitle;

	}

	function getUserPicture($user)
	{
		$getPicFrom 	=	0;

		if(isset($user->profilePicFlag))
		{
			if(in_array($user->profilePicFlag, array(1,2,3)))
			{
				$getPicFrom 	=	$user->profilePicFlag;
			}
		}

		if(isset($user->picture))
		{
			$uploadedPic 	=	base_url('profilePics/'.$user->picture);
		}
		else
		{
			$uploadedPic 	=	'';
		}

		$fbData 			=	array();
		$twitterData 		=	array();
		$googleData 		=	array();

		if(isset($user->facebook_data))
		{
			if($user->facebook_data)
			{
				try{
					$fbData 	=	json_decode($user->facebook_data, true);
				}
				catch(Excaption $e)
				{

				}
			}
		}

		if(isset($user->twitter_data))
		{
			if($user->twitter_data)
			{
				try{
					$twitterData 	=	json_decode($user->twitter_data, true);
				}
				catch(Excaption $e)
				{
					
				}
			}
		}

		if(isset($user->google_data))
		{
			if($user->google_data)
			{
				try{
					$googleData 	=	json_decode($user->google_data, true);
				}
				catch(Excaption $e)
				{
					
				}
			}
		}

		if(isset($fbData['picture']))
		{
			$fbPic 			=	$fbData['picture'];
		}
		else
		{
			$fbPic 			=	'';
		}

		if(isset($twitterData['picture']))
		{
			$twitterPic 	=	$twitterData['picture'];
		}
		else
		{
			$twitterPic 	=	'';
		}

		if(isset($googleData['picture']))
		{
			$googlePic 		=	$googleData['picture'];
		}
		else
		{
			$googlePic 		=	'';
		}

		switch($getPicFrom)
		{
			case 0:

				if($uploadedPic != '')
				{
					return $uploadedPic;
				}
				else if($fbPic != '')
				{
					return $fbPic;
				}
				else if($twitterPic != '')
				{
					return $twitterPic;
				}
				else if($googlePic != '')
				{
					return $googlePic;
				}

				break;

			case 1:

				if($fbPic != '')
				{
					return $fbPic;
				}
				else if($uploadedPic != '')
				{
					return $uploadedPic;
				}
				else if($twitterPic != '')
				{
					return $twitterPic;
				}
				else if($googlePic != '')
				{
					return $googlePic;
				}

				break;

			case 2:

				if($twitterPic != '')
				{
					return $twitterPic;
				}
				else if($uploadedPic != '')
				{
					return $uploadedPic;
				}
				else if($fbPic != '')
				{
					return $fbPic;
				}
				else if($googlePic != '')
				{
					return $googlePic;
				}

				break;

			case 3:

				if($googlePic != '')
				{
					return $googlePic;
				}
				else if($uploadedPic != '')
				{
					return $uploadedPic;
				}
				else if($fbPic != '')
				{
					return $fbPic;
				}
				else if($twitterPic != '')
				{
					return $twitterPic;
				}

				break;

			default:

				return '';

				break;

		}


	}

	/**
	* Send email message of given type (activate, forgot_password, etc.)
	*
	* @param	string
	* @param	string
	* @param	array
	* @return	void
	*/
	function send_email($type, $email, &$data)
	{
		$CI=& get_instance();
		$CI->load->library('email');
		$CI->email->from($CI->config->item('webmaster_email'), $CI->config->item('website_name'));
		$CI->email->reply_to($CI->config->item('webmaster_email'), $CI->config->item('website_name'));
		$CI->email->to($email);
		$CI->email->subject('New message from '.$data['name'] );
		$CI->email->message($CI->load->view('email/'.$type.'-html', $data, TRUE));
			/*
		add_log( $CI->load->view('email/'.$type.'-html', $data, TRUE));
		add_log( $CI->load->view('email/'.$type.'-txt', $data, TRUE));
		add_log('New message from '.$data['name']);*/

		$CI->email->set_alt_message($CI->load->view('email/'.$type.'-txt', $data, TRUE));
		$CI->email->send();

	}

?>