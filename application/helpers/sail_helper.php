<?php

	/*
		NAME: Ranjit Randive
		DESC: 
	*/

	function emailHeader()
	{

		$CI 	=	&get_instance();

		return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
					<html>
						<head>
							<title>'. $CI->config->item("website_name") .'</title>
						</head>
						<body>	
							<div style="max-width: 800px; margin: 0; background-color: #f3f3f3; font: 13px/18px Arial, Helvetica, sans-serif;">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td align="center" style="padding: 15px">
												<img src="'.base_url().'assets/images/logo-black-email.png" alt="SchoolTorch" width="200"/>
												<table width="600" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ddd;background: #fff; padding:30px;margin:10px 0 0">
													<tr>
														<td align="left" width="100%" style="font: 13px/18px Arial, Helvetica, sans-serif;">';
	}

	function emailFooter()
	{
		return 		'	</td>
					</tr>
				</table>
				<table width="600" border="0" cellpadding="0" cellspacing="0" style=" color: #999;
				    line-height: 22px;">
												<tbody>
													<tr>
														<td valign="top">
															<p align="center" style="font-size:11px;line-height:16px;color:#8c93a0;">
																<a href="http://on.fb.me/1EOmUjV"><img width="30" height="auto"src="'.base_url().'assets/images/email/fb.png"/></a>
																<a href="https://twitter.com/getschooltorch"><img width="30" height="auto"src="'.base_url().'assets/images/email/twitter.png"/></a>
																<a href="http://bit.ly/1zIZisA"><img width="30" height="auto"src="'.base_url().'assets/images/email/google+.png"/></a>
															</p>

															<p align="center" style="font-size:11px;line-height:16px;color:#8c93a0;">
																<a href="'.site_url().'profile/emailSettings">Unsubscribe</a>
															</p>

															<p align="center" style="font-size:11px;line-height:16px;color:#8c93a0;">© 2015 SchoolTorch
															</p>
														</td>
														
													</tr>
												</tbody>
											</table>
											<!-- /footer-->



										</td>
									</tr>
								</tbody>	
							</table>



						</div>
					</body>
				</html>';
	}


	/*
		NAME: Saurabh Joshi
		DESC: To retrieve emailId for blog or review
	*/
	function emailNotifications($type,$data = NULL)
	{
		$CI=& get_instance();

		$getEmailIds = array();

		if($type == 'blog')
		{
			$getEmailIds = $CI->db->select('email')
										->from('users')
											->where('blogUpdatesEmail',1)
												->where('activated',1)
													->where('isArchived',NULL)
														->get()
															->result_array();

		}
		elseif($type == 'review')
		{
			$schoolId = $data['schoolId'];

			$getSchoolUsersConnected = $CI->db->select('userId')
													->from('schoolSavedList')
														->where('schoolId',$schoolId)
															->where('isDeleted is NULL')
																->get()
																	->result_array();
			
			$userIds	   = __::uniq(__::pluck($getSchoolUsersConnected, 'userId'));

			if(count($userIds) > 0)
			{
				$getEmailIds = $CI->db->select('email')
											->from('users')
												->where_in('id',$userIds)
													->where('reviewUpdatesEmail',1)
														->where('activated',1)
															->where('isArchived',NULL)
																->get()
																	->result_array();

				//$emailIds	   = __::uniq(__::pluck($getEmailIds, 'email'));	
			
			}
			
		}
		else
		{
			return array(
							'status'	=> 'failure',
							'errors'	=> 'Type can only be review or blog'
						);
		}

		$emailIds	   = __::uniq(__::pluck($getEmailIds, 'email'));

		return array(
							'status'	=> 'success',
							'data'		=> $emailIds
						);

	}


	function emailBody($data)
	{
		$data = $data['data'];
		
		$emailSubmjectText = "New review for ". $data['school']['INSTNM'] ." by ". $data['user']['firstName'] ." ". $data['user']['lastName'];

		$emailBodyText = "<b>".$data['school']['INSTNM']."</b></br></br>"."Creator Name: ".$data['user']['firstName'] ." ". $data['user']['lastName']."</br>"."Created On: ".$data['review']['createdDate']."</br><br/>"."<h3>Review Details</br><br/>"."Title: ".$data['review']['title']."</br>"."Major: ".$data['review']['major']."</br>"."Good: ".$data['review']['good']."</br>"."Bad: ".$data['review']['bad']."</br>"."Features: ".$data['review']['features']."</br>"."Culture: ".$data['review']['culture']."</br>";

		$data = array();
		$data = array(
						'subject' => $emailSubmjectText,
						'body'	  => $emailBodyText
					);

		return $data;
	}


	function send_emailNotification($email, &$data)
	{
		
		$CI=& get_instance();
		$CI->load->library('email');
		$CI->email->from($CI->config->item('webmaster_email'), $CI->config->item('website_name'));
		$CI->email->reply_to($CI->config->item('webmaster_email'), $CI->config->item('website_name'));
		

		if(is_array($email))
		{
			/*$to = $email[0];
			unset($email[0]);*/
			$bcc = array_values($email);
			// add_log($to);
			// add_log(json_encode($bcc));
			

			$CI->email->to($CI->config->item('webmaster_email'));



			$CI->email->bcc(implode(', ', $bcc));
		}
		else
		{
			$CI->email->to($email);
		}
		
		//$CI->email->cc('neil@gdiz.com');
		$CI->email->subject($data['subject']);
		$CI->email->message($data['body']);

		//$CI->email->send();

		if ( ! $CI->email->send() )
		{
		    add_log('ERROR!!! while Email Sending');

		    // Loop through the debugger messages.
		    add_log($CI->email->print_debugger());

		    // Remove the debugger messages as they're not necessary for the next attempt.
		    //$this->email->clear_debugger_messages();
		}
		else
		{
		    add_log($CI->email->print_debugger());
		}

	}


	function bodyFormat($type,$data,$flag = 0)
	{
		$data = $data['data'];

		if($flag == 1)
		{
			$emailSubmjectText = "Review flagged by user";
		}
		else
		{
			$emailSubmjectText = "New review posted for school ". $data['school']['INSTNM'];
		}

		if($type == 'html')
		{
			
			$ratingSrt ="";
			
			if($data['review']['rating'] != null)
			{

				for ($i = 1; $i <= 5; $i++) 
				{
					if($i <= $data['review']['rating'])
					{
						$ratingSrt.="<img src='".base_url()."assets/images/full-star.png"."' width='20' height='20'/>";
					}
					else{
						$ratingSrt.="<img src='".base_url()."assets/images/star.png"."' width='20' height='20'/>";;
					}
					
				};
			}

			$flagCountHtml="";

			if(isset($data['review']['helpfulFlagCount']))
			{
				$helpfulFlag = $data['review']['helpfulFlagCount'] ;
			}
			else
			{
				$helpfulFlag = 0;
			}

			if($flag == 1)
			{
				$backLink 	   =   site_url("admin/reviewManagement/index")."?reviewId=". $data['review']['id'];

				$flagCountHtml = 	"<tr>
										<td style='vertical-align: top;' >
										</td>    
										<td style='padding: 10px;font-size: 16px;'>
											<span style='color: #5cb85c;font-size: 16px;'>". $helpfulFlag."</span> people found this helpful<br>
											<span style='color: #d9534f;font-size: 16px;'>". $data['review']['flagCount']."</span> people flagged this review
										</td>
									</tr>";

			}
			else
			{
				$backLink 	   =   site_url("home/school")."/".$data['school']['UNITID']."/".url_title($data['school']['INSTNM']);
			}

			$htmlbody	= 		"<table border='0' cellpadding='0' cellspacing='0'>
									<tr>
										<td colspan='2'><h2 style='color: #777'>"
											.$data['school']['INSTNM'].
										"</h2></td>
									</tr>
									<!--<tr>
										<td  colspan='2'>"
											.date('d F, Y', strtotime($data['review']['createdDate'])).
										"</td>
									</tr>-->
									<tr>
										<td valign='top' style='padding: 10px;'>
											<img src='".base_url()."assets/images/profile.png"."' width='48' height='48'/>
										</td>
										<td style='padding: 10px;'>
											<table  border='0' cellpadding='0' cellspacing='5'>
												<tr>
													<td  colspan='2'><h3>". $data['review']['title']."</h3></td> 
												</tr>
												<!--<tr>
													<td  colspan='2'>". $data['user']['firstName']." ".$data['user']['lastName']."</td> 
												</tr>-->
												<tr>
													<td  colspan='2' style='padding: 10px;'>". $ratingSrt."</td> 
												</tr>
												<tr>
													<td valign='top' style='padding: 10px;'>
														<b>Good</b>
													</td>    
													<td style='font-size: 15px; line-height: 24px;padding: 10px;'>
														".$data['review']['good']."
													</td>
												</tr>
												<tr>
													<td valign='top' style='padding: 10px;'>
														<b>Bad</b>
													</td>    
													<td style='font-size: 15px; line-height: 24px;padding: 10px;'>
														".$data['review']['bad']."
													</td>
												</tr>
												<tr>
													<td valign='top' style='padding: 10px;'>
														<b>Best Features</b>
													</td>    
													<td style='font-size: 15px; line-height: 24px;padding: 10px;'>
														". $data['review']['features']. "
													</td>
												</tr>
												<tr>
													<td valign='top' style='padding: 10px;'>
														<b>Cultures</b>
													</td>    
													<td style='font-size: 15px; line-height: 24px;padding: 10px;'>
														". $data['review']['culture']." 
													</td>
												</tr>
												".$flagCountHtml."
												<tr>
												<td valign='top' style='padding: 10px;'>
														
													</td>   
													<td>
														<a href='".$backLink."' style='background-color:#ff6b45;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:15px;font-weight:normal;line-height:40px;text-align:center;text-decoration:none;width:150px'>Read More</a>

														
													</td>
												</tr>
											</table>	
										</td>
									</tr>

								</table>";


			$htmlbody 	=	emailHeader() .$htmlbody. emailFooter();

			
			
		}
		else
		{
			
			$htmlbody 	=	$data['school']['INSTNM'].
							"Creator Name :". $data['user']['firstName']." ".$data['user']['lastName'].
							"Created On :". $data['review']['createdDate'].
							"Review Detail";

			if(isset($data['review']['title']))
			{
				$htmlbody 	.=	"Title :".$data['review']['title'];
			}

			if(isset($data['review']['major']))
			{
				$htmlbody 	.=	"Major :".$data['review']['major'];
			}

			if(isset($data['review']['good']))
			{
				$htmlbody 	.=	"Good :".$data['review']['good'];
			}

			if(isset($data['review']['bad']))
			{
				$htmlbody 	.=	"Bad :".$data['review']['bad'];
			}

			if(isset($data['review']['features']))
			{
				$htmlbody 	.=	"Features :".$data['review']['features'];
			}

			if(isset($data['review']['culture']))
			{
				$htmlbody 	.=	"Culture :".$data['review']['culture'];
			}
		}
		
		return array(
					 'subject'  =>	$emailSubmjectText,
					 'body'     =>	$htmlbody
					);
	}


	function ReviewData($reviewId)
    {
    	$CI=& get_instance();

        $CI->config->load('reviews');


        $data['review'] = __::flatten($CI->db->select('id,schoolId,userId, rating, major, title, good, bad, features, culture, isApproved,createdDate,flagCount,helpfulFlagCount')
                           ->where('id',$reviewId)
                               ->get('reviews',1)
                                   ->result_array(),true);
        //dump($data,1);
        $data['review']['features'] = __::compact(explode("|",$data['review']['features']));

        $features = $CI->config->item('reviewFeatures');

        foreach ($data['review']['features'] as $key => $feature)
        {
          $data['review']['features'][$key] = $features[$feature];
        }

        $data['review']['features'] = implode(", ",$data['review']['features']);


        $data['review']['culture'] = __::compact(explode("|",$data['review']['culture']));

        $cultures = $CI->config->item('reviewCulture');

        foreach ($data['review']['culture'] as $key => $culture)
        {
          $data['review']['culture'][$key] = $cultures[$culture];
        }
        
        $data['review']['culture'] = implode(", ",$data['review']['culture']);


        if($data['review']['isApproved'] == 1)
        {
            $data['review']['isApproved'] = 'Approved';
        }
        else
        {
            $data['review']['isApproved'] = 'Not Approved';
        }
        
        
        $data['school'] = __::flatten($CI->db->select('ID, INSTNM, UNITID')
                                                    ->where('UNITID',$data['review']['schoolId'])
                                                        ->get('schools',1)
                                                            ->result_array(),true);

        
        $data['user'] = __::flatten($CI->db->select('id, firstName, lastName, email,picture')
                                                    ->where('id',$data['review']['userId'])
                                                        ->get('users',1)
                                                            ->result_array(),true);
        

        if(count($data) > 0)
        {
            return array(
                            'status' => 'success',
                            'data'   => $data
                        );
        }
        else
        {
            return array(
                            'status' => 'failure',
                            'errors' => 'NO RECORD PRESENT.'
                        );
        }

    }


   function blogData($blogId)
   {
   		$data = array();
   		$CI=& get_instance();
   		$blogData = $CI->db->select('id,categoryId,title,content,userId,isEmailSend,coverImage,isPublished,createdDate')
   								->from('blog-articles')
   									->where('isEmailSend is NULL')
   										//->where('isPublished is NULL')
   										 ->where('id',$blogId)	
   										 	->where('isEmailSend is NULL')
   												->where('isDeleted is NULL')	
   													->get()
   														->result_array();
   	
   		if(count($blogData) > 0)
   		{

   			$data['blog']  = $blogData; 
   			$userIds	=	__::compact(__::uniq(__::pluck($blogData,'userId')));

			if(isset($userIds) && count($userIds) > 0)
			{
				$userData 	 =	 $CI->db->select('id,email,firstName,lastName,username')
												->where_in('id',$userIds)
													->where('blogUpdatesEmail',1)
														->where('activated',1)
															->get('users')
																->result_array();
    			$data['user']  = $userData; 																
			}
			else
			{
				$data['user'] = array('firstName' => 'SchoolTorch Team');
			}

   		}

   		if(count($data) > 0)
        {
            return array(
                            'status' => 'success',
                            'data'   => $data
                        );
        }
        else
        {
            return array(
                            'status' => 'failure',
                            'errors' => 'NO RECORD PRESENT.'
                        );
        }
   }

   function blogBodyFormat($type,$data)
   {
   		$CI=& get_instance();
   		$data = $data['data'];

   		if(isset($data['user'][0]['firstName']) && isset($data['user'][0]['lastName']))
   		{
   			$name = $data['user'][0]['firstName'].' '.$data['user'][0]['lastName'];
   		}
   		else
   		{
   			$name = 'SchoolTorch Team';
   		}
   		
   		$safeTitle = preg_replace('/[^A-Za-z0-9-]+/', '-',filter_var($data['blog'][0]['title'],FILTER_SANITIZE_SPECIAL_CHARS));

   		$backLink 	   =   site_url("blog/article")."/".$data['blog'][0]['id']."/".$safeTitle;
   		
   		//add_log($safeTitle);
   		$emailSubjectText = $CI->config->item('website_name') .'Blog - '. $data['blog'][0]['title'];

   		
   		if($type == 'html')
		{	
			$htmlbody = 	"<table border='0' cellpadding='0' cellspacing='0' width='100%'>
								<tr>
									<td> 
										<img src='". base_url() ."blogPics/".$data['blog'][0]['coverImage'] ."' style='max-width: 600px;'>
									</td>
								</tr>
								<tr>
									<td>
										<table  border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td colspan='2'>
													<h2>".$data['blog'][0]['title']."</h2> 
												</td>
											</tr>
											
											<tr>
												<td colspan='2' style='font-size: 16px; font-weight: normal; line-height: 24px;' >
													".$data['blog'][0]['content']."
												</td>
											</tr>

											<tr>
												<td colspan='2'>
													<a href='".$backLink."' style='background-color:#ff6b45;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:15px;font-weight:normal;line-height:40px;text-align:center;text-decoration:none;width:150px'>Read more</a>
												</td>
											</tr>

										</table>
									</td> 
								</tr>

							</table>";

						
			$htmlbody 	=	emailHeader() .$htmlbody. emailFooter();
			
		}
		else
		{
			$htmlbody 	=  $data['blog'][0]['coverImage'];

			if(isset($data['blog'][0]['title']))
			{
				$htmlbody 	.=	$data['blog'][0]['title'];
			}

			if(isset($data['user'][0]['firstName']) && isset($data['user'][0]['lastName']))
			{
				$htmlbody 	.=	" by ". $data['user'][0]['firstName']." ".$data['user'][0]['lastName'];
			}

			if(isset($data['blog'][0]['createdDate']))
			{
				$htmlbody 	.=	date('d F, Y', strtotime($data['blog'][0]['createdDate']));
			}

			if(isset($data['blog'][0]['content']))
			{
				$htmlbody 	.=	$data['blog'][0]['content'];
			}				
			
			
		}
		
		return array(
					 'subject'  =>	$emailSubjectText,
					 'body'     =>	$htmlbody
					);
   }


?>