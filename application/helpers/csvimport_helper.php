<?php
	
	function importCSVHub($opts)
	{
		$CI=&get_instance();

		$prefixStr = '';

		$returnArr = array('status' => 'failure', 'message' => '');

		//check if all required opts passed
		if(isset($opts['csvFile']) && isset($opts['dbName']) && isset($opts['chunkSize']))
		{
			if(strpos($opts['dbName'], ':') !== false)
			{
				$opts['dbName'] = explode(':', $opts['dbName']);

				$prefixStr 		= $opts['dbName'][1];
				$opts['dbName'] = $opts['dbName'][0];
			}

			$CI->load->library('csvimport');
		
			$data = $CI->csvimport->get_array($opts['csvFile'], "", TRUE);

			
			if(sizeof($data) > 0)
			{
				//array walk if prefix not blank
				if($prefixStr != '')
				{
					function addPrefix(&$opt, $key, $prefixStr)
					{
						$opt['table'] = $prefixStr;
					}

					array_walk($data, 'addPrefix', $prefixStr);
				}

				//first get keys
				$csvColumns = array_keys($data[0]);
				
				//get current columns
				$currentColumns = $CI->db->list_fields($opts['dbName']);
				
				//check if all columns in csv are present in db
				$isPresentCount = 0;
				$missingColumns = array();
				foreach($csvColumns as $value)
				{
					if(in_array($value, $currentColumns))
					{
						$isPresentCount++;
					}
					else
					{
						$missingColumns[] = $value;
					}
				}

				if($isPresentCount < count($csvColumns))
				{
					$returnArr['message'] = 'Missing columns! '.  implode(', ', $missingColumns);
				}
				else
				{
					/* */
					if($prefixStr != '')
					{
						$CI->db->delete($opts['dbName'], array('table' => $prefixStr)); 
					}
					else if($opts['dbName'] != 'schoolCompletions')
					{
						//empty table
						
					}

					$CI->db->truncate($opts['dbName']);

					//make chunks of array for batch inset
					$data = array_chunk($data, $opts['chunkSize']);
					
					foreach($data as $insertData)
					{
						$CI->db->insert_batch($opts['dbName'], $insertData); 
					}

					$returnArr['status'] = "success";
				}			
			}
			else
			{
				$returnArr['message'] = 'No data!';
			}
		}
		else
		{
			$returnArr['message'] = 'Missing opts!';
		}

		return $returnArr;		
	}
	
?>