<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reviews_model extends CI_Model{

	function countFlags($data = array())
	{
		$flagsHelpfulFlags	=	$this->db->select('id,flagCount,helpfulFlagCount')
										 	->where('id',$data['reviewId'])
										 		->where('isArchived',NULL)
										 			->where('isDeleted',NULL)
										 				->limit(1)
										 					->get('reviews')
										 						->result_array();

		if(count($flagsHelpfulFlags) > 0)
		{
			$flags  		=	$flagsHelpfulFlags[0]['flagCount'];
			$helpfulFlags 	=	$flagsHelpfulFlags[0]['helpfulFlagCount'];
		}
		else
		{
			return array(
							'status' => 'failure',
							'errors' => 'ReviewId : '.$data['reviewId'].' is invalid.'
						);
		}
		//dump($data);
		//exit();
		$flagCount 	=	$this->db->select('id,flag,helpfulFlag,isArchived')
										 	->where('schoolId',$data['schoolId'])
										 		->where('reviewId',$data['reviewId'])
										 			->where('userId',$this->session->userdata('user_id'))
										 				->where('isDeleted',NULL)
										 					->where('isArchived',NULL)
										 						->limit(1)
										 							->get('reviewFlagConnect')
										 								->result_array();
		if(!isset($data['flag']))
		{
			return array(
								'status' => 'failure',
								'errors' => '1 out of the Helpful FLag and Flag must be passed.'
							);
		}

		if(count($flagCount) > 0)
		{
			
			if($flagCount[0]['flag'] == NULL)
			{
				
				if($data['flag'] == 'flag')
				{
					//add_log("step 1".date("Y-m-d H:i:s"));

					if($flagCount[0]['helpfulFlag'] == NULL)
					{
						$updateData	=	array(
												'flag'			=>	1,
											);

					
						$this->db->where('id',$flagCount[0]['id'])
								 	->update('reviewFlagConnect',$updateData);

						$updateData	=	array(
												'flagCount'			=>	$flags + 1,
											);

						$this->db->where('id',$data['reviewId'])
								 	->update('reviews',$updateData);
								 	
						$flags 	=	$flags 	+	1;
						//add_log("Step 2".date("Y-m-d H:i:s"));
									
					}
					else if($flagCount[0]['helpfulFlag'] != NULL)
					{
						$updateData	=	array(
												'flag'			=>	1,
												'helpfulFlag'	=>	NULL
											);

					
						$this->db->where('id',$flagCount[0]['id'])
								 	->update('reviewFlagConnect',$updateData);

						$updateData	=	array(
												'flagCount'			=>	$flags + 1,
												'helpfulFlagCount'	=>	$helpfulFlags - 1
											);

						$this->db->where('id',$data['reviewId'])
								 	->update('reviews',$updateData);
								 	
						//dump($flags);
						$flags 			=	$flags 	+	1;
						$helpfulFlags 	=	$helpfulFlags - 1;
					}
						$reviewInfo = ReviewData($data['reviewId']);
						
						$email = 'getschooltorch@gmail.com';
						//$email = 'neil@gdiz.com';
						//$email = 'saurabh@gdiz.com';

						$emailMessage =  bodyFormat('html',$reviewInfo,1);

						//dump($emailMessage);
						//add_log("Step 3".date("Y-m-d H:i:s"));
						send_emailNotification($email, $emailMessage);
						//add_log("Step 4".date("Y-m-d H:i:s"));
				}
				else
				{
					if($flagCount[0]['helpfulFlag'] == NULL)
					{
						$updateData	=	array(
												'helpfulFlag'			=>	1,
											);

					
						$this->db->where('id',$flagCount[0]['id'])
								 	->update('reviewFlagConnect',$updateData);

						$updateData	=	array(
												'helpfulFlagCount'			=>	$helpfulFlags + 1,
											);

						$this->db->where('id',$data['reviewId'])
								 	->update('reviews',$updateData);
								 	
						
						$helpfulFlags 	=	$helpfulFlags 	+	1;
			
					}
					else if($flagCount[0]['helpfulFlag'] != NULL)
					{
						$updateData	=	array(
												'helpfulFlag'	=>	NULL
											);

					
						$this->db->where('id',$flagCount[0]['id'])
								 	->update('reviewFlagConnect',$updateData);

						$updateData	=	array(
												'helpfulFlagCount'	=>	$helpfulFlags - 1
											);

						$this->db->where('id',$data['reviewId'])
								 	->update('reviews',$updateData);
								 	
						$helpfulFlags 	=	$helpfulFlags - 1;
					}
				}

			}
			else if($flagCount[0]['flag'] != NULL)
			{
				if($data['flag'] == 'flag')
				{
					$updateData	=	array(
												'flag'	=>	NULL
											);

					
					$this->db->where('id',$flagCount[0]['id'])
							 	->update('reviewFlagConnect',$updateData);

					$updateData	=	array(
											'flagCount'	=>	$flags - 1
										);

					$this->db->where('id',$data['reviewId'])
							 	->update('reviews',$updateData);
							 	
					$flags 	=	$flags - 1;
				}
				else
				{
					$updateData	=	array(
												'flag'			=>	NULL,
												'helpfulFlag'	=>	1
											);

					
					$this->db->where('id',$flagCount[0]['id'])
							 	->update('reviewFlagConnect',$updateData);

					$updateData	=	array(
											'flagCount'			=>	$flags - 1,
											'helpfulFlagCount'	=>	$helpfulFlags + 1
										);

					$this->db->where('id',$data['reviewId'])
							 	->update('reviews',$updateData);
							 	
					$flags 			=	$flags 	-	1;
					$helpfulFlags 	=	$helpfulFlags + 1;
				}
				
			}
			
		}
		else if(count($flagCount) == 0)
		{
			if($data['flag'] ==	'flag')
			{
				
				$insertData	=	array(
											'flag'			=>	1,
											'helpfulFlag'	=>	NULL,
											'schoolId'		=>	$data['schoolId'],
											'reviewId'		=>	$data['reviewId'],
											'userId'		=>  $this->session->userdata('user_id')
										);

				$this->db->insert('reviewFlagConnect',$insertData);

				$updateData	=	array(
											'flagCount'			=>	$flags + 1,
										);

				$this->db->where('id',$data['reviewId'])
						 	->update('reviews',$updateData);

				$flags 			=	$flags + 1;

				$reviewInfo = ReviewData($data['reviewId']);
						
				$email = 'getschooltorch@gmail.com';
				//$email = 'neil@gdiz.com';
				//$email = 'saurabh@gdiz.com';
				
				$emailMessage =  bodyFormat('html',$reviewInfo,1);

				send_emailNotification($email, $emailMessage);
			}
			else
			{
				$insertData	=	array(
												'flag'			=>	NULL,
												'helpfulFlag'	=>	1,
												'schoolId'		=>	$data['schoolId'],
												'reviewId'		=>	$data['reviewId'],
												'userId'		=>  $this->session->userdata('user_id')
											);

				$this->db->insert('reviewFlagConnect',$insertData);

				$updateData	=	array(
										'helpfulFlagCount'			=>	$helpfulFlags + 1,
									);

				$this->db->where('id',$data['reviewId'])
						 	->update('reviews',$updateData);

				$helpfulFlags 	=	$helpfulFlags + 1;
			}		
					
		}


		if($flags == NULL)
		{
			$flags = 0;
		}

		if($helpfulFlags == NULL)
		{
			$helpfulFlags = 0;
		}

		return array(
						'status' 			=> 'success',
						'flagCount'			=>	$flags,
						'helpfulFlagCount'	=>	$helpfulFlags
						
					);
	
	}

	

	function create()
	{
		$params = array();

		$params['userId']   	=	$this->session->userdata('user_id');
		$params['schoolId']		=	$this->input->post('schoolId');

		
		$params['rating']		=	$this->input->post('rating');
		$params['userType']		=	$this->input->post('userType');
		$params['gradType']		=	$this->input->post('gradType');
		$params['studentType']	=	$this->input->post('studentType');

		
		if($this->input->post('major') == "Other")
		{
			$params['major']		=	$this->input->post('otherMajor');
		}
		else
		{
			$params['major']		=	$this->input->post('major');
		}
		
		$params['title']		=	$this->input->post('title');
		$params['good']			=	$this->input->post('good');
		$params['bad']			=	$this->input->post('bad');

		$params['features'] 	=	$this->input->post('features');
		$params['culture'] 		=	$this->input->post('culture');

		$params['userId']   	=	$this->session->userdata('user_id');
		
		$params['createdDate'] 	=   date('Y-m-d H:i:s');
  
		

		$result = $this->_validateReviewParams($params);

		if($result['status'] == "failure")
		{
			return $result;
		}

		if(count($params['features']) >= 3)
		{
			$params['features'] =  "|". implode("|", $params['features'])."|";
		}
		
		
		if(count($params['culture']) >= 1)
		{
			$params['culture'] =  "|". implode("|", $params['culture'])."|";
		}

		$this->db->insert('reviews',$params);

		
		$reviewId = $this->db->insert_id();

		$reviewConnectdata =  array(
										'schoolId' => $params['schoolId'],
										'reviewId' => $reviewId,
										'userId'   => $params['userId']
									);

		$this->db->insert('reviewConnect',$reviewConnectdata);

		return array("status"=> "success");
		
	}


	private function _validateReviewParams($params)
	{

	 	$returnArray = array();

	 	/*$hasUserReview			=	$this->db->select('id,schoolId,userId')
											 	->where('schoolId',$params['schoolId'])
											 		->where('userId',$params['userId'])
											 			->where('isArchived',NULL)
											 				->where('isDeleted',NULL)
											 					->limit(1)
											 						->get('reviewConnect')
											 							->result_array();

		
		if(count($hasUserReview) > 0)
		{
			$returnArray['errors']['schoolId']  = 'A user cannot write more than 1 reviews for the same school.';
			
		}*/

	 	if(isset($params['good']))
	 	{
	 		$output = preg_replace('/\s+/', ' ',$params['good']);

	 		$temp = explode(" ", $output);

	 		if(count($temp) < 19)
	 		{	
	 			$returnArray['errors']['good'] = '20 words minimum for good';

	 		}

	 		if(count($temp) > 100)
	 		{	
	 			$returnArray['errors']['good'] = '100 words maximum for good';

	 		}
	 	}

	 	if(isset($params['bad']))
	 	{
	 		$output = preg_replace('/\s+/', ' ',$params['bad']);

	 		$temp = explode(" ", $output);

	 		if(count($temp) < 19)
	 		{

	 			$returnArray['errors']['bad'] = '20 words minimum for bad';
	 			

	 		}
	 		if(count($temp) > 100 )
	 		{

	 			$returnArray['errors']['bad'] = '100 words maximum for bad';
	 			

	 		}
	 	}

	 	if(count($params['features']) >= 3)
		{
			$params['features'] =  "|". implode("|", $params['features'])."|";
		}
		else
		{
			$returnArray['errors']['features'] = 'You must pick at least 3 features.';
			
			
		}
		
		if(count($params['culture']) >= 1)
		{
			$params['culture'] =  "|". implode("|", $params['culture'])."|";
		}
		else
		{
			$returnArray['errors']['culture'] = 'You must pick at least 3 features.';
			
		}

		if(isset($returnArray['errors']))
		{
			$returnArray['status'] = "failure";

			return $returnArray;
		}

	}

	/*
        NAME: Neeraj
        DESC: to delete the review.
    */
    function deleteReview($reviewId)
    {
        $isReviewPresent    =   $this->db->select('id')
                                            ->where('id',$reviewId)
                                                ->where('isArchived',NULL)
                                                    ->where('isDeleted',NULL)
                                                    	 ->where('userId',$this->session->userdata('user_id'))		
	                                                        ->limit(1)
	                                                            ->get('reviews')
	                                                                ->result_array();
        if(count($isReviewPresent) == 0)
        {
            return  array(
                            'status'    =>  'failure',
                            'errors'    =>  'Invalid reviewId'
                        );
        }

        $this->db->where('id',$reviewId)
                    ->update('reviews',array('isArchived'   =>  1));

        return  array(
                        'status'    =>  'success',
                        'message'   =>  'Review with id : '.$reviewId.' is deleted successfully.'
                    );
    }



    /*
    *   Name : Ather Parvez
    *   Date : 30 JAN 2015
    *   DESC : it display the details of specific id
    */
    function view( $reviewId )
    {

        $this->config->load('reviews');

        $reviewUserTypeArr   = $this->config->item('reviewUserType');

        $data['review'] = __::flatten($this->db->select('id,schoolId,userId, rating, major, title, userType, good, bad, features, culture, isApproved,createdDate,flagCount,helpfulFlagCount')
                           		->where('id',$reviewId)
                         		 ->where('userId',$this->session->userdata('user_id'))
                               			->get('reviews',1)
                                   			->result_array(),true);


       
        if(count($data['review']) > 0)
        {
	        //dump($data,1);
	        $data['review']['features'] = __::compact(explode("|",$data['review']['features']));

	        $features = $this->config->item('reviewFeatures');

	        foreach ($data['review']['features'] as $key => $feature)
	        {
	          $data['review']['features'][$key] = $features[$feature];
	        }

	        $data['review']['features'] = implode(", ",$data['review']['features']);


	        $data['review']['culture'] = __::compact(explode("|",$data['review']['culture']));

	        $cultures = $this->config->item('reviewCulture');

	        foreach ($data['review']['culture'] as $key => $culture)
	        {
	          $data['review']['culture'][$key] = $cultures[$culture];
	        }
	        
	        $data['review']['culture'] = implode(", ",$data['review']['culture']);


	        if($data['review']['isApproved'] == 1)
	        {
	            $data['review']['isApproved'] = 'Approved';
	        }
	        else
	        {
	            $data['review']['isApproved'] = 'Not Approved';
	        }

	        $data['review']['userTypeName'] = $reviewUserTypeArr[$data['review']['userType']];
	        
	        
	        $data['school'] = __::flatten($this->db->select('ID, INSTNM, UNITID')
	                                                    ->where('UNITID',$data['review']['schoolId'])
	                                                        ->get('schools',1)
	                                                            ->result_array(),true);

	        
	        $data['user'] = __::flatten($this->db->select('id, firstName, lastName, email,picture')
	                                                    ->where('id',$data['review']['userId'])
	                                                        ->get('users',1)
	                                                            ->result_array(),true);
        

            return array(
                            'status' => 'success',
                            'data'   => $data
                        );
        }
        else
        {
            return array(
                            'status' => 'failure',
                            'errors' => 'NO RECORD PRESENT.'
                        );
        }

    }


}