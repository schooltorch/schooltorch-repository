<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class ReviewManagement_model extends CI_Model{

   /* function __construct()
    {        
        parent::__construct();
        //$this->accountId    =   getAccountId();
    }*/

	

    /*
        NAME: Neeraj
        DESC: to delete the review.
    */
    function deleteReview($reviewId)
    {
        $isReviewPresent    =   $this->db->select('id')
                                            ->where('id',$reviewId)
                                                ->where('isArchived',NULL)
                                                    ->where('isDeleted',NULL)
                                                        ->limit(1)
                                                            ->get('reviews')
                                                                ->result_array();
        if(count($isReviewPresent) == 0)
        {
            return  array(
                            'status'    =>  'failure',
                            'errors'    =>  'Invalid reviewId'
                        );
        }

        $this->db->where('id',$reviewId)
                    ->update('reviews',array('isArchived'   =>  1));

        return  array(
                        'status'    =>  'success',
                        'message'   =>  'Review with id : '.$reviewId.' is deleted successfully.'
                    );
    }

    /*
        NAME: Neeraj
        DESC: to approve review.
    */
	function approveReview($reviewId)
    {
        $isReviewPresent    =   $this->db->select('id,schoolId')
                                            ->where('id',$reviewId)
                                                ->where('isArchived',NULL)
                                                    ->where('isDeleted',NULL)
                                                        ->limit(1)
                                                            ->get('reviews')
                                                                ->result_array();

        if(count($isReviewPresent) == 0)
        {
            return  array(
                            'status'    =>  'failure',
                            'errors'    =>  'Invalid reviewId'
                        );
        }
        
        $this->db->where('id',$reviewId)
                    ->update('reviews',array('isApproved'   =>  1));

        $relatedEmails  =    emailNotifications('review', $isReviewPresent[0]);

        if(!empty($relatedEmails['data']))
        {
            $reviewData         =   ReviewData($reviewId);

            $emailMessage       =   bodyFormat('html',$reviewData);

           // $email = 'saurabh@gdiz.com';
           // send_emailNotification($email,$emailMessage);
            send_emailNotification($relatedEmails['data'],$emailMessage);
         
        }
        

        return  array(
                        'status'    =>  'success',
                        'message'   =>  'Review with id : '.$reviewId.' is approved.'
                    );


    }

    /*
        NAME: Neeraj
        DESC: to get the information about all the reviews.
    */
	public function get($data)
    {
        //dump($data);
        $this->db->start_cache();
    
        //filter result 
        if(isset($data['schoolId']))
        {
            $this->db->where('schoolId',$data['schoolId'])
                        ->limit(1);
                        
        }

        if($data['filterBy'] == 'dates' && isset($data['since']) && isset($data['until']))
        {
            if(strtotime($data['since']) <= strtotime($data['until']))
            {
                $this->db->where('createdDate <', $data['until'].' 23:59:59');
           
                $this->db->where('createdDate >= ', $data['since'].' 00:00:00');  
            }
            else
            {
                
                return array(
                              'status'  =>  'failure',
                              'errors'  =>  'Since should be less than until',
                              //'data'    =>   $data['restaurantId']    
                            );  
            } 
        }

        if($data['filterBy'] == 'schoolId')
        {

            $this->db->where('schoolId',$data['filter'])
                             ->limit(1);

        }



        if($data['since'] != null && $data['until'] != null)
        {
            $this->db->where('createdDate <', $data['until'].' 23:59:59');
           
            $this->db->where('createdDate >= ', $data['since'].' 00:00:00'); 

        }
        else if($data['since'] != null)
        {
             $this->db->where('createdDate >= ', $data['since'].' 00:00:00'); 
        }
        if($data['until'] != null)
        {
             $this->db->where('createdDate <', $data['until'].' 23:59:59');
        }




        //$this->db->stop_cache();
        //$data   =   array();

        $order      =   'desc';

        if(isset($data['order']))
        {
            $order  =   $data['order'];
        }

        $order_by   =   'rating';

        if(isset($data['order_by']))
        {
            if($data['order_by'] == 'rating')
            {
                $this->db->order_by('rating', $order);
            }
            else if($data['order_by'] == 'flagCount')
            {
                $this->db->order_by('flagCount', $order);
            }
            else if($data['order_by'] == 'helpfulFlagCount')
            {
                $this->db->order_by('helpfulFlagCount', $order);
            }
            else
            {
                $this->db->order_by('createdDate', $order);
            }
        }
        else
        {
            $this->db->order_by('rating', $order);
        }
        $this->db->where('isArchived',NULL)
                    ->where('isDeleted',NULL);

        if($this->input->get_post('isAll')  != 1)
        {
            $this->db->where('isApproved',NULL);
        }
                                
        $this->db->stop_cache();

        $query   =   $this->db->select('id,schoolId,userId,rating,userType,studentType,gradYear,gradType,major,title,good,bad,features,
                                        culture,isApproved,flagCount, helpfulFlagCount,createdDate')                                                   
                                ->limit($data['limit'], $data['offset'])    
                                    ->get('reviews')
                                        ->result_array();
       //dump($this->db->last_query());
       //exit(); */
        $totalCount             =   $this->db->from('reviews')
                                                ->count_all_results();    
        $this->db->flush_cache();

         // dump($this->db->last_query());

       
        //exit();

        if(count($query) > 0)
        {
            $schoolIds  =   __::uniq(__::pluck($query,'schoolId'));
            $userIds    =   __::uniq(__::pluck($query,'userId'));
            //dump($schoolIds);

            if(count($userIds) > 0)
            {
                $userNames =   $this->db->select('id,username,email,firstName,lastName')
                                            ->where_in('id',$userIds)
                                                ->limit(count($userIds))
                                                    ->get('users')
                                                        ->result_array();
                //dump($schoolNames);
                //exit();
                foreach ($userNames as $key => $value) 
                {
                    //Updated By Jayant Wankhede
                    if (strlen(trim($value['firstName'])) == 0) 
                    {
                        $value['firstName']              =   'Anonymous';
                        $userNames[$key]                =   $value;
                    }                    
                    $userNameWithIds[$value['id']]      =   $value;
                }

                foreach ($query as $key => $value) 
                {
                    if(isset($userNameWithIds[$value['userId']]))
                    {
                        $query[$key]['userName']  =   $userNameWithIds[$value['userId']]['firstName'].' '.$userNameWithIds[$value['userId']]['lastName'].' '.$userNameWithIds[$value['userId']]['email'];
                    }
                    else
                    {
                        $query[$key]['userName']  =   '--';  
                    }
                }
            }

            if(count($schoolIds) > 0)
            {
                $schoolNames =   $this->db->select('UNITID,INSTNM')
                                            ->where_in('UNITID',$schoolIds)
                                                ->limit(count($schoolIds))
                                                    ->get('schools')
                                                        ->result_array();
                //dump($schoolNames);
                //exit();

                if(count($schoolNames) > 0)
                {
                    foreach ($schoolNames as $key => $value) 
                    {
                        $schoolNameWithIds[$value['UNITID']]    =   $value;
                    }

                    foreach ($query as $key => $value) 
                    {
                        if(isset($schoolNameWithIds[$value['schoolId']]))
                        {
                            $query[$key]['schoolName']  =   $schoolNameWithIds[$value['schoolId']]['INSTNM'];
                        }
                        else
                        {
                            $query[$key]['schoolName']  =   '--';  
                        }
                    }
                }
                else
                {
                   foreach ($query as $key => $value) 
                    {
                        $query[$key]['schoolName']  =   '--';
                    } 
                }
            }
            /*dump($query);
            exit();*/

            foreach ($query as $key => $value) 
            {
                if($value['flagCount'] ==  NULL)
                {
                    $query[$key]['flagCount']  =   0;
                }
                if($value['helpfulFlagCount'] ==  NULL)
                {
                    $query[$key]['helpfulFlagCount']  =   0;
                }
            }

            $returnArr  =   array(
                                    'status'        => 'success',
                                    'data'          =>  $query,
                                    'total_count'   =>  $totalCount
                                );
            return $returnArr;                                     
        }
        else
        {
            return array(
                            'status'        => 'success',
                            'data'          => array(),
                            'total_count'   =>  $totalCount
                        );
        }
        
    }




    /*
    *   Name : Ather Parvez
    *   Date : 30 JAN 2015
    *   DESC : it display the details of specific id
    */
    function view( $reviewId )
    {

        $this->config->load('reviews');


        $data['review'] = __::flatten($this->db->select('id,schoolId,userId, rating, major, title, good, bad, features, culture, isApproved,createdDate,flagCount,helpfulFlagCount,major,userType,gradType')
                           ->where('id',$reviewId)
                               ->get('reviews',1)
                                   ->result_array(),true);
        //dump($data,1);
        $data['review']['features'] = __::compact(explode("|",$data['review']['features']));

        $features = $this->config->item('reviewFeatures');
        $reviewUserTypeArr = $this->config->item('reviewUserType');

        $data['review']['userTypeName'] = $reviewUserTypeArr[ $data['review']['userType']];
        foreach ($data['review']['features'] as $key => $feature)
        {
          $data['review']['features'][$key] = $features[$feature];
        }

        $data['review']['features'] = implode(", ",$data['review']['features']);


        $data['review']['culture'] = __::compact(explode("|",$data['review']['culture']));

        $cultures = $this->config->item('reviewCulture');

        foreach ($data['review']['culture'] as $key => $culture)
        {
          $data['review']['culture'][$key] = $cultures[$culture];
        }
        
        $data['review']['culture'] = implode(", ",$data['review']['culture']);


        if($data['review']['isApproved'] == 1)
        {
            $data['review']['isApproved'] = 'Approved';
        }
        else
        {
            $data['review']['isApproved'] = 'Not Approved';
        }
        
        
        $data['school'] = __::flatten($this->db->select('ID, INSTNM, UNITID')
                                                    ->where('UNITID',$data['review']['schoolId'])
                                                        ->get('schools',1)
                                                            ->result_array(),true);

        
        $data['user'] = __::flatten($this->db->select('id, firstName, lastName, email,picture')
                                                    ->where('id',$data['review']['userId'])
                                                        ->get('users',1)
                                                            ->result_array(),true);
        

        if(count($data) > 0)
        {
            return array(
                            'status' => 'success',
                            'data'   => $data
                        );
        }
        else
        {
            return array(
                            'status' => 'failure',
                            'errors' => 'NO RECORD PRESENT.'
                        );
        }

    }
}