<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Blogs_model extends CI_Model{

	//private $has_category = "yes";

	/*
		name - Gaurav sharma
		desc - to get authors
		date - 30/01/2015
	*/
	public function getAuthors()
	{
		$query = $this->db->select('id,role,email,username,firstName,lastName')
								->from('users')
									->where('role',2)
										->where('activated',1)
											->where('banned',0)
												->where('isArchived',NULL)
													->get()
														->result_array();

		
		return $query;
	}

	// this function is when we are interested in ordering of the articles..
	public function add_category2($category_name,$category_id){


		$first_flag = 0;
		//set the order for the new category added
		$categories_order_info = $this->db->select('order')
										->from('support_category_order')
											->where('id',1)
												->get();
														

		$temp_order = "";

		if($categories_order_info->num_rows() > 0 )
		{
	  
		    $temp_order_arr = $categories_order_info->result_array();
		    $temp_order      = $temp_order_arr[0]['order'];
		}
		else
		{
			$categories_info = $this->db->select('id')
										->from('blog-categories')
											->where("is_deleted",NULL)
												->get();

			if($categories_info->num_rows() > 0)
			{
				$cat_temp  = array();

				foreach ($categories_info->result_array() as $value) {
					$cat_temp [] = $value['id'];
				}
				
				$temp_order  = $temp_order . implode("_", $cat_temp);
			}

			$first_flag = 1; 
		}


		
		if($category_id != null && $category_id != undefined && $category_id != ""){


			
			$this->db->set('name',$category_name)
							->where('id',$category_id)
								->update('blog-categories',$data);
								
		}	
		else
		{
			$data = array('name'=>$category_name);
			$this->db->insert('blog-categories',$data);

			$last_inserted_id  = $this->db->insert_id();

			if($first_flag == 1)
			{
				//dump($temp_order);

				if($temp_order != "")
				{
					$order  = $last_inserted_id."_".$temp_order ;
				}
				else{
					$order  = $last_inserted_id;

				}
				
				$this->db->insert('support_category_order',array("order"=> $order));
							   
								
			}
			else
			{
				$order  = $last_inserted_id."_".$temp_order ;
			

			   $this->db->set('order',$order)
							   ->where('id',1)
								->update('support_category_order');


			}

		}
		
		return true;
		
	}

	public function add_category($data)
	{

		
		$data   = array();

		$data['name'] =  $this->input->get_post('category_name');

		// if($this->input->get_post('is_active'))
		// {
		// 	$data['is_active'] = 1;
		// }
		// else
		// {

		// 	$data['is_active'] = 2;
		// }

		$data['createdDate']  = date('Y-m-d H:i:s');
		
		$this->db->insert('blog-categories',$data);

		

		$last_inserted_id  = $this->db->insert_id();

		return ;
	}

	public function update_category($data)
	{

		/*if($this->input->post('is_active'))
		{
			$this->db->set('is_active',$this->input->post('is_active'));
		}
		else
		{
			$this->db->set('is_active',2);
			
		}*/

		$data['name'] = $this->input->post('category_name');

	
		$this->db->set('name',$data['name'])
					
						->where('id',$data['id'])
							->update('blog-categories');


		

		//print_r($this->db->last_query());

		return ;
	}

	public function get_category($data)
	{

		$categories = $this->db
						->from('blog-categories')
							->where('isDeleted',null)
								->where('id',$data['id'])
									->get()
										->result_array();

		return $categories[0];

	}

	public function get_categories(){

		$categories = $this->db->select('*')
						->from('blog-categories')
							->where('isDeleted',null)
								->order_by('id','desc')
									->get()
										->result_array();

		$categories_row = array();							

		foreach($categories as $row){

			$categories_row[$row['id']] =  $row;//array('id'=> $row->id,'name'=> $row->name/*,'is_active'=> $row->is_active*/);
			$categories_row[$row['id']]['createdDate'] = date($this->config->item('dateFormat'),strtotime($row['createdDate']));

		}	

		return $categories_row;

	}

	public function get_users()
	{

		$temp_users = $this->db->select('id,role,email,username')
						->from('users')
							->where('banned',0)
								 ->where("role",1)
									->get()
										->result();

		$users = array();							

		foreach($temp_users as $row){

			$users[$row->id] = $row;

		}	

		return $users;

	}

	

	public function get_articles($params){


			$articles = array();

			$articles = $this->db->select('*')
							->from('blog-articles')
								->where('isDeleted',NULL)
									->order_by('id','desc')
										->limit($params['limit'], $params['offset'])
											->get()
												->result_array();

			$articles_row = array();							

			foreach($articles as $row){

				$articles_row[$row['id']]= $row;

			}


		return $articles_row;

	}

	public function delete_category($category_id){


		$this->db->set('isDeleted',1)
					->where('id',$category_id)
						 ->update('blog-categories');

		return ;

	}


	public function delete_categoryWithOrder($category_id){

		$count = $this->db->select('id')
										->from('blog-articles')
											->where('category_id',$category_id)
											  ->where('is_deleted',NULL)
												->get()
												 ->num_rows();

		$return_array  = array();

		if($count < 1 )
		{



			$this->db->where('id',$category_id)
							->delete('blog-categories');

		   // get the order and update the order
		   $categories_order_info = $this->db->select('order')
											->from('support_category_order')
												->where('id',1)
													->get();
															

			$temp_order = "";
			$order  = array();
			if($categories_order_info->num_rows() > 0 )
			{
		  
			    $temp_order_arr = $categories_order_info->result_array();
			    $temp_order      = $temp_order_arr[0]['order'];
			    $order  = explode("_", $temp_order);
			}



			$key = array_search($category_id,$order);
			if($key!==false){
			    unset($order[$key]);
			}


			$order_string  = implode("_", $order);

			$this->db->set('order',$order_string)
								   ->where('id',1)
									->update('support_category_order');


			$return_array['status'] = "success";
			return $return_array;
		}
		else
		{	
			
			$return_array['status'] = "failure";
			return $return_array;

		}

	}


	/*
	 *to create the articles ..which inserts the posted data in the DB..
	 *
	 */
	function create_article($param)
	{
		$data = array();
		
		if($this->input->post('title'))
		{
			$data['title'] 		= $this->input->post('title');
		}
		
		if($this->input->post('content'))
		{
			$data['content']  = $this->input->post('content');
		}
		
		if($this->input->post('isFavorite'))
		{
			$data['isFavorite']  	= $this->input->post('isFavorite');
		}

		if($this->input->post('isPublished'))
		{
			if($this->input->post('isPublished') == "no")
			{
				$data['isPublished']  	= NULL;
			}
			else{
				$data['isPublished']  	= 1;
			}
		}

		//if($param['has_category'] == "yes")
		{
			if($this->input->post('categoryId'))
			{
				$data['categoryId']  	= "|".implode("|",$this->input->post('categoryId'))."|";
			}
		}
		//created by gaurav
		if($this->input->post('authorId'))
		{
			$data['userId']  	= $this->input->post('authorId');
		}


		/*if($this->input->post('userId'))
		{
			$data['userId']  	= $this->input->post('userId');
		}*/

		$data['createdDate']  = date('Y-m-d H:i:s');


		if(!isset($_FILES['coverImage']['name']) || $_FILES['coverImage']['name'] == "" || $_FILES['coverImage']['name'] == null) 
		{

			return  array('status' => "failure",'error' => array('error'=>"You have not selected any files"));
		}
		else
		{

			$result = $this->uploadFile();



			if($result['status'] != "success")
			{
				return $result;
			}
			else
			{
					$data['coverImage'] = $result['data']['file_name'];

					$this->db->insert('blog-articles',$data);

					$last_inserted_id  = $this->db->insert_id();


					/*dump($this->db->last_query());
					exit();
					*/

					if($this->input->post('isPublished') == 'yes')
					{
						$this->sendEmail($last_inserted_id);

						$this->db->set('isEmailSend', 1)
									->update('blog-articles');
					}

					return  array('status' => "success");

			}
		}
		
	}


	function uploadFile()
	{
		$fileTemp   = explode(".",$_FILES['coverImage']['name']);

		$config['file_name']  = $fileTemp[0]."_".strtotime("now").".".$fileTemp[1];
		$config['upload_path'] = './blogPics';
		$config['allowed_types'] = 'gif|jpg|png';
		/*$config['max_width']  = '1024';
		$config['max_height']  = '768';*/
		$config['max_size']			= 	5124;

		$this->load->library('upload');

		$this->upload->initialize($config);

		if(! $this->upload->do_upload('coverImage'))
		{
			$error = array('error' => $this->upload->display_errors());

			return  array('status' => "failure",'error' => $error);
		}
		else
		{
			return  array('status' => "success", 'data' => $this->upload->data());
		}

	}

	/*
	 * read particular article
	 */
	function read_article($id = 0)
	{
	
		$return_array   		= array();
		$return_array['status']  = "success";

		
		$articles_data = $this->db->from('blog-articles')
									->where('id',$id)
										 ->limit(1)
											 ->get();
	
	
		if($articles_data->num_rows() > 0)
		{
	
			$articles_data =  $articles_data->result_array();

			$return_array['data'] = $articles_data[0];
						

			return $return_array;
				
				
		}
		else
		{	
			$return_array['status'] = "failure";

			return $return_array;
			
		}
	}

	/*
		updated by - Gaurav sharma
		date - 31/01/2015
	*/
	function update_article($param)
	{
		$articles_data = $this->db->from('blog-articles')
									->where('id',$param['id'])
										->limit(1)
											->get();
		
		$articles_info = array();
		$old_category_id = 0;

		$data = array();

		if($articles_data->num_rows() > 0)
		{
	
			$articles_data =  $articles_data->result_array();
			$articles_info  = $articles_data[0];

			$old_category_id   = $articles_info['categoryId'];
		}

		//dump($old_category_id);

		$new_category_flag = 0;

		$new_category_id  = $this->input->post('categoryId');

		if($old_category_id != $this->input->post('categoryId') )
		{
			//dump("new flag is set");
			$new_category_flag = 1;

		}
		
		
		if($this->input->post('title'))
		{
			$data['title'] 		= $this->input->post('title');
		}
	
		if($this->input->post('content'))
		{
			$data['content']  = $this->input->post('content');
		}
		
		
		
		if($this->input->post('isPublished'))
		{
			//$data['isPublished']  	= $this->input->post('isPublished');
			if($this->input->post('isPublished') == "no")
			{
				$data['isPublished']  	= NULL;
			}
			else{
				$data['isPublished']  	= 1;
			}
		}
		else
		{
			$data['isPublished']   = NULL;
			
		}
		if($this->input->post('isFavorite'))
		{
			$data['isFavorite']  	= $this->input->post('isFavorite');
		}
		else
		{ 
			$data['isFavorite']   = NULL;
			
		}

		//if($param['has_category']  == 'yes')
		{
			if($this->input->post('categoryId'))
			{	
				$str = '';
				if(count($this->input->post('categoryId')) > 0)
				{
					$str = "|". implode("|",$this->input->post('categoryId')). "|";
				}
				$data['categoryId']  	=	$str;
			}
		}

		/*if($this->input->post('userId'))
		{
			$data['userId']  	= $this->input->post('userId');
		}*/

		//created by gaurav
		if($this->input->post('authorId'))
		{
			$data['userId']  	= $this->input->post('authorId');
		}

		if($this->input->post('isPublished') == 'yes')
		{
			$this->sendEmail($param['id']);
			$data['isEmailSend']  	= 1;
		}

		if(!isset($_FILES['coverImage']['name']) || $_FILES['coverImage']['name'] == "" || $_FILES['coverImage']['name'] == null) 
		{
			$this->db->where('id',$param['id'])
				->update('blog-articles', $data);

			

			return  array('status' => "success");
			
		
		}
		else
		{
			$result = $this->uploadFile();

			if($result['status'] == "success")
			{
				$data['coverImage'] = $result['data']['file_name'];

				$this->db->where('id',$param['id'])
							->update('blog-articles', $data);

				return  array('status' => "success");



			}
			else{


				return $result;
			}

		

		}
		
	}

	

	function delete_article($id)
	{
		
		$this->db->set('isDeleted',1)
					//->set('is_deleted',1)
						->where('id', $id)
							->where('isDeleted',NULL)
								->update('blog-articles');
		
		return;
	}


	function order_articles($id= 0 ,$order= 0)
	{

		$return_array  = array();

		$return_array['status'] = "success";

		if($order != 0)
		{
			$temp_order  = explode("_", $order);

			$temp_order   = array_unique($temp_order);

			$order  = implode("_", $temp_order);
			// check if the ids are from the category id passed

			$articles_info = $this->db->select('id')
							->from('blog-articles')
								->where('is_deleted',NULL)
									->where('category_id',$id)
										->get()
											->result_array();

			 if(count($articles_info) > 0)
			 {

			 	//dump("When the count is greatee");
			 	

			 	foreach($articles_info as $row){

					$articles[]= $row['id'];

				}

			 	if(count(array_unique($temp_order))  == count(array_intersect($temp_order, $articles)))
			 	{

			 		//dump("When coming inside");

			 		// then for the catgory update the order
			 		$this->db->set('article_order',$order)
									->where('id', $id)
										->where('is_deleted',NULL)
											->update('blog-categories');

					//dump($this->db->last_query());
			 	}
			 	else
			 	{
			 		$return_array['status'] = "failure";

			 	}

			 }
			 

			 return $return_array;

		}

	}

	function  update_category_order($order= 0)
	{
		$return_array  = array();

		$return_array['status'] = "success";

		if($order != 0)
		{
			$temp_order  = explode("_", $order);

			$temp_order   = array_unique($temp_order);

			$order  = implode("_", $temp_order);
			// check if the ids are from the category id passed

			$category_info = $this->db->select('id')
							->from('blog-categories')
								->where('is_deleted',NULL)
										->get()
											->result_array();

			 if(count($category_info) > 0)
			 {

			 	//dump("When the count is greatee");
			 	

			 	$categories  = array();

			 	foreach($category_info as $row){

					$categories[]= $row['id'];

				}

			 	if(count(array_unique($temp_order))  == count(array_intersect($temp_order, $categories)))
			 	{

			 		//dump("When coming inside");

			 		// then for the catgory update the order
			 		$this->db->set('order',$order)
									->where('id', 1)
											->update('support_category_order');

					//dump($this->db->last_query());
			 	}
			 	else
			 	{
			 		$return_array['status'] = "failure";

			 	}

			 }
			 

			 return $return_array;

		}

	}

	public function get_categories_to_order(){



		$categories = $this->db->select('name,id')
						->from('blog-categories')
							->where('is_deleted',null)
								->order_by('id','desc')
									->get()
										->result();

		$categories_row = array();							

		foreach($categories as $row){

			$categories_row[$row->id] = $row->name;

		}	



		$categories_order_info = $this->db->select('order')
						->from('support_category_order')
							->where('id',1)
								->get();
										

		if($categories_order_info->num_rows() > 0 )
		{
		  
		    $temp_order_arr = $categories_order_info->result_array();
		    $temp_order      = $temp_order_arr[0]['order'];

		    //dump($temp_order);

		    $temp_order   = explode("_", $temp_order);

		   // dump($temp_order);

		    $return_array  = array();

		    foreach($temp_order as $row){

				$return_array[$row] = $categories_row[$row];
			}



			return $return_array;
		}
		else
		{
			return $categories_row;

		}

		

	}

	public function get_articles_to_order($id= 0){


			/*if($id != 0)
			{
				 $this->db->where('category_id',$id);
			}

			$articles = $this->db->select('*')
							->from('blog-articles')
								->where('is_deleted',NULL)
									//->order_by('id','desc')
										->get()
											->result_array();

			$articles_row = array();							

			foreach($articles as $row){

				$articles_row[$row['id']]= $row;

			}


			return $articles_row;*/




			$articles = $this->db->select('title,id')
							->from('blog-articles')
								->where('is_deleted',null)
									->order_by('id','desc')
										->get()
											->result();

			$articles_row = array();							

			foreach($articles as $row){

				$articles_row[$row->id] = $row;

			}	

			//dump($articles_row);

			if($id != 0)
			{
				 $this->db->where('id',$id);
			}

			$articles_order_info = $this->db->select('article_order')
									->from('blog-categories')
											->get();



		//dump($articles_order_info->result_array());
													

			if($articles_order_info->num_rows() > 0 )
			{
			  
			    $temp_order_arr = $articles_order_info->result_array();
			    $temp_order      = $temp_order_arr[0]['article_order'];

			  //  dump($temp_order);

			    $temp_order   = array_unique(explode("_", $temp_order));

			   // dump($temp_order);

			    $return_array  = array();

			    foreach($temp_order as $row){

			    	if(isset($articles_row[$row]))
			    	{
			    		$return_array[$row] = $articles_row[$row];
			    	}
					
				}


				//dump($return_array);
				return $return_array;
			}
			else
			{
				return $articles_row;

			}


	}

	/*
		NAME: Neeraj
		DESC: to check if category is present.31/10/14
	*/

	function checkCategoryPresent()
	{
		
	}

/*
	NAME: Saurabh Joshi
	DESC: To send email to users if a blog is published
*/
	function sendEmail($blogId)
	{	
		$relatedEmails  =    emailNotifications('blog');
		//add_log('Related Emails');	
		//add_log(json_encode($relatedEmails));
		
		//$relatedEmails    =  'saurabh@gdiz.com';
		
        if(!empty($relatedEmails['data']))
        {
			$blogInfo         =   blogData($blogId);
			
			if($blogInfo['status'] == 'success')
			{
				$emailMessage       =   blogBodyFormat('html',$blogInfo);
				
				//add_log("RESPONSE:Blog email");
				//add_log($emailMessage['body']);
				send_emailNotification($relatedEmails['data'],$emailMessage);
				
			}
		}

	}

		

}