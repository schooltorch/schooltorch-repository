<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class UploadImages_model extends CI_Model{

   /* function __construct()
    {        
        parent::__construct();
        //$this->accountId    =   getAccountId();
    }*/

	function test()
	{
		echo 'inside model';
		return true;
	}

	/*
        NAME: Neeraj
        DESC: to delete the review.
    */
    function deleteImage($imageId)
    {
        $isImagePresent    =   $this->db->select('id')
                                            ->where('id',$imageId)
                                                ->where('isArchived',NULL)
                                                    ->where('isDeleted',NULL)
                                                        ->limit(1)
                                                            ->get('schoolsUserImages')
                                                                ->result_array();
        if(count($isImagePresent) == 0)
        {
            return  array(
                            'status'    =>  'failure',
                            'errors'    =>  'Invalid imageId'
                        );
        }

        $this->db->where('id',$imageId)
                    ->update('schoolsUserImages',array('isArchived'   =>  1));

        return  array(
                        'status'    =>  'success',
                        'message'   =>  'Image with id : '.$imageId.' is deleted successfully.'
                    );
    }

    /*
        NAME: Neeraj
        DESC: to approve review.
    */
	function approveImage($imageId)
    {
        $isImagePresent    =   $this->db->select('id')
                                            ->where('id',$imageId)
                                                ->where('isArchived',NULL)
                                                    ->where('isDeleted',NULL)
                                                        ->limit(1)
                                                            ->get('schoolsUserImages')
                                                                ->result_array();
        if(count($isImagePresent) == 0)
        {
            return  array(
                            'status'    =>  'failure',
                            'errors'    =>  'Invalid imageId'
                        );
        }

        $this->db->where('id',$imageId)
                    ->update('schoolsUserImages',array('isApproved'   =>  1));

        return  array(
                        'status'    =>  'success',
                        'message'   =>  'Image with id : '.$imageId.' is approved.'
                    );


    }

	/*
        NAME: Neeraj
        DESC: to get the information about all the reviews.
    */
	public function getUploadedImages($data)
    {

        if($data['filterBy'] == 'dates' && isset($data['since']) && isset($data['until']))
        {
            if(strtotime($data['since']) > strtotime($data['until']))
            {
                return array(
                              'status'  =>  'failure',
                              'errors'  =>  'Since should be less than until.',  
                            );  
            }
        }
        //dump($data);
        $this->db->start_cache();

        if(isset($data['filterBy']) &&  isset($data['filter']))
        {
            //$params['filter']   =   strtolower($params['filter']);
            
            if($data['filterBy'] == 'isApproved') 
            {

                //dump($data['filter']);
                if($data['filter'] == 1)
                {
                    $this->db->where("isApproved",1);
                }
                else if($data['filter'] == 0)
                {
                    $this->db->where("isApproved",NULL);
                }
            }
            else if($data['filterBy'] == 'schoolId') 
            {
                $this->db->where('schoolId', $data['filter']);
            }
        }
        
        if($this->input->get_post('isAll')  != 1)
        {
            $this->db->where('isApproved',NULL);
        }

        if($data['filterBy'] == 'dates' && isset($data['since']) && isset($data['until']))
        {
            $this->db->where('createdDate <', $data['until'].' 23:59:59');
       
            $this->db->where('createdDate >= ', $data['since'].' 00:00:00');  
        }

        //$this->db->stop_cache();
        //$data   =   array();

        $order      =   'desc';

        if(isset($data['order']))
        {
            $order  =   $data['order'];
        }

        $order_by   =   'isApproved';

        if(isset($data['order_by']))
        {
            if($data['order_by'] == 'isApproved')
            {
                $this->db->order_by('isApproved', $order);
            }
            else
            {
                $this->db->order_by('createdDate', $order);
            }
        }
        else
        {
            $this->db->order_by('isApproved', $order);
        }
        $this->db->where('isArchived',NULL)
                    ->where('isDeleted',NULL);

        /*if($this->input->get_post('isAll')  != 1)
        {
            $this->db->where('isApproved',NULL);
        }*/
                                
        $this->db->stop_cache();

        $query   =   $this->db->select('id,schoolId,userId,fileName,isApproved,createdDate')                                                   
                                
                                ->limit($data['limit'], $data['offset'])    
                                    ->get('schoolsUserImages')
                                        ->result_array();
       //dump($this->db->last_query());
       //exit(); */
        $totalCount             =   $this->db->from('schoolsUserImages')
                                                ->count_all_results();    
        $this->db->flush_cache();

          //dump($this->db->last_query());

       
        //exit();

        if(count($query) > 0)
        {
            
            foreach($query as $key => $schoolImage)
            {
                $fileUrl                   =   base_url().'schoolsImages/'.$schoolImage['fileName'];
                $query[$key]['fileUrl']    =   $fileUrl;
                $thumbUrl                  =   explode('.',$fileUrl);
                $thumbUrl[2]               =   $thumbUrl[2].'_thumb';
                $thumbUrl                  =   implode('.',$thumbUrl);  
                $query[$key]['thumbUrl']   =   $thumbUrl;
              
            }
            
            $schoolIds  =   __::uniq(__::pluck($query,'schoolId'));
            $userIds    =   __::uniq(__::pluck($query,'userId'));
            //dump($schoolIds);

            if(count($userIds) > 0)
            {
                $userNames =   $this->db->select('id,username,email')
                                            ->where_in('id',$userIds)
                                                ->limit(count($userIds))
                                                    ->get('users')
                                                        ->result_array();
                //dump($schoolNames);
                //exit();

                foreach ($userNames as $key => $value) 
                {
                    $userNameWithIds[$value['id']]    =   $value;
                }

                foreach ($query as $key => $value) 
                {
                    if(isset($userNameWithIds[$value['userId']]))
                    {
                        $query[$key]['userName']  =   $userNameWithIds[$value['userId']]['username'].'('.$userNameWithIds[$value['userId']]['email'].')';
                    }
                    else
                    {
                        $query[$key]['userName']  =   '--';  
                    }
                }
            }

            if(count($schoolIds) > 0)
            {
                $schoolNames =   $this->db->select('UNITID,INSTNM')
                                            ->where_in('UNITID',$schoolIds)
                                                ->limit(count($schoolIds))
                                                    ->get('schools')
                                                        ->result_array();
                //dump($schoolNames);
                //exit();

                if(count($schoolNames) > 0)
                {
                    foreach ($schoolNames as $key => $value) 
                    {
                        $schoolNameWithIds[$value['UNITID']]    =   $value;
                    }

                    foreach ($query as $key => $value) 
                    {
                        //base_url()."schoolsImages/".$value['fileName']
                        if(isset($schoolNameWithIds[$value['schoolId']]))
                        {
                            $query[$key]['schoolName']  =   $schoolNameWithIds[$value['schoolId']]['INSTNM'];
                        }
                        else
                        {
                            $query[$key]['schoolName']  =   '--';  
                        }
                    }
                }
                else
                {
                   foreach ($query as $key => $value) 
                    {
                        $query[$key]['schoolName']  =   '--';
                    } 
                }
            }

            $returnArr  =   array(
                                    'status'        => 'success',
                                    'data'          =>  $query,
                                    'total_count'   =>  $totalCount
                                );
            return $returnArr;                                     
        }
        else
        {
            return array(
                            'status'        => 'success',
                            'data'          => array(),
                            'total_count'   =>  $totalCount
                        );
        }
        
    }

}