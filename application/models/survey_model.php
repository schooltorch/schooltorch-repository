<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Survey_model extends CI_Model 
{

	function __construct()
    {
    	parent::__construct();
    }

    function readSurveys($id=0)
    {
        
        if(isset($id) && $id != 0 && $id != NULL){
            $this->db->where('id',$id);
        }

        $surveysResult = $this->db->select('id,name,updatedDate')
                            ->where('isDeleted IS NULL')
                                ->get('survey')
                                    ->result();

        $surveys = array();                            

        if(count($surveysResult) > 1){

            foreach ($surveysResult as $key => $value) {
                $surveys[] = array('id'=>$value->id,
                                           'name'=>$value->name,
                                                'updatedDate'=>$value->updatedDate);
            }
            

        }  
        else
        {
            foreach ($surveysResult as $key => $value) {
                $surveys = array('id'=>$value->id,
                                           'name'=>$value->name,
                                                'updatedDate'=>$value->updatedDate);
            }
        } 

        return $surveys;

    }

    function addSurvey()
    {   

        $surveyCategory =    array();

        $surveyName     =    $this->input->post('name');
        $surveyCategory =    $this->input->post('category');

        $duplicateCheck = $this->db->select('name')
                                ->from('survey')
                                    ->where('name',$surveyName)
                                        ->where('isDeleted IS NULL')
                                            ->count_all_results();

        if($duplicateCheck != 0){

            return array('status'=>'failure',
                            'error'=>'Survey '.$surveyName.' already exist');

        }  
        else
        {
            
            $data['name']       = $surveyName;
            $data['category']   = implode('_',$surveyCategory);
            $data['createDate'] = date('Y-m-d H:i:s');
            
            $this->db->insert('survey',$data);
            
            return array('status'=>'success');
            
        }                              

    }

    function readCategories(){

        $categoriesResult = $this->db->select('id,name,updatedDate')
                            ->where('isDeleted IS NULL')
                                ->get('surveyCategory')
                                    ->result();

        $categories = array();                            

        if(count($categoriesResult) > 0){

            foreach ($categoriesResult as $key => $value) {
                $categories[] = array('id'=>$value->id,
                                           'name'=>$value->name,
                                                'updatedDate'=>$value->updatedDate);
            }
            

        }   

        return $categories;

    }

    function addCategories()
    {
        $categoryName        =    $this->input->post('name');


        $duplicateCheck = $this->db->select('name')
                                ->from('surveyCategory')
                                    ->where('name',$categoryName)
                                    ->where('isDeleted IS NULL')
                                        ->count_all_results();

        if($duplicateCheck != 0){

            return array('status'=>'failure',
                            'error'=>'Category '.$categoryName.' already exist');

        }  
        else
        {

            $data['name'] = $categoryName;
            $data['createDate'] = date('Y-m-d H:i:s');

            $this->db->insert('surveyCategory',$data);

            return array('status'=>'success');

        }                              

    }


    /*function readQuestions(){

        $returnData = array();
 
        return $returnData;                            

    }*/


    function readParentQuestions(){

        $result = $this->db->select('id,label')
                                        ->where('parentId',0)
                                            ->where('isDeleted IS NULL')
                                                ->get('surveyQuestions')
                                                    ->result();

        $returnData = array();                                                

        if(count($result)>0){

            foreach ($result as $key => $value) {
                
                $returnData[] = array('id'=>$value->id,
                                        'label'=>$value->label);

            }

        }    

        return $returnData;                                        


    }


    function addQuestion(){

        // $data = array(
        //     'label'         =>  $this->input->post('questionLabel'),
        //     'category'      =>  $this->input->post('questionCategory'),
        //     'type'          =>  $this->input->post('questionType'),
        //     'parentId'      =>  $this->input->post('questionParent'),
        //     'value'         =>  $this->input->post('questionValue'),
        //     'defaultValue'  =>  $this->input->post('questionDefaultValue'),
        //     'helpText'      =>  $this->input->post('questionHelpText'),
        //     'errorText'     =>  $this->input->post('questionErrorText'),
        //     'minRequired'   =>  $this->input->post('questionMinRequired'),
        //     'createdDate'   =>  date('Y-m-d H:i:s')
        // );

        // $this->db->insert('surveyQuestions',$data);

        $returnArray = array();

        $questionData['surveyId']           = $this->input->post('surveyId');
        $questionData['question']           = $this->input->post('question');
        $questionData['questionType']       = $this->input->post('questionType');
        $questionData['createdDate']        = date('Y-m-d H:i:s');

        $questionOptions    = json_decode($this->input->post('questionOptions'));

        add_log($questionData);
        add_log($questionOptions);

        if(count($questionOptions)==0){
            $returnArray = array('status'=>'failure');
        }
        else{

           // $this->db->insert('surveyQuestions',$questionData);

        }

        //$this->db->insert($questionData);

        //$this->db->insert_id();

        return $returnArray;

    }


    //--------------

    /*
    *   Get Question for the add survey form
    */

    function readQuestions($id){

        $userId         =   $this->session->userdata('user_id');

        $count          =   $this->db->from('surveyAnswers')
                                        ->where('userId', $userId)
                                            ->where('schoolId', $id)
                                                ->count_all_results();

        if($count == 0)
        {
            $returnArray    =   array();
        
            $result         =   $this->db->select('id,question,questionType')
                                            ->get('surveyQuestions')
                                                ->result_array();

            
                                       // dump($result);
            if(count($result)>0)
            {

                // for all the Question get the values for the Question from config files

                //dump($this->config->item("surveyOptions"));

                $surveyOptions  = $this->config->item("surveyOptions");

                foreach ($result as $key => $value) {
                    
                    $temp = array(
                                    'id'=>$value['id'],
                                    'question'=>$value['question'],
                                    'questionType'  => $value['questionType']
                                );

                    if( isset($surveyOptions[$value['id']]))
                    {
                        $temp['option'] = $surveyOptions[$value['id']];
                    }
                    else{

                        $temp['option'] = NULL; 
                    }

                    $returnArray[] = $temp;
                }

            }   


            return array('status' => 'success', 'data' => $returnArray);
        }
        else
        {
            return array('status' => 'failure');
        }

    }

    function createSurvey($data)
    {   

        $returnArray['status']  = "success";

        $count = $this->db->from('surveyAnswers')
                            ->where('userId',$data['userId'])
                            ->where("schoolId",$data['id'])
                                ->count_all_results();
        
        if($count ==  0)
        {
            $array = array(1,2,3,4,5,6,7,8,9,10);

            foreach ($array as $key => $value) 
            {

                if($this->input->post('q'.$value))
                {
                   
                    $insert_array = array();
                    
                    $insert_array['userId'] = $data['userId'];
                    $insert_array['schoolId'] = $data['id'];
                    $insert_array['questionId'] = $value;
                    $insert_array['createdDate']   =  date('Y-m-d H:i:s', time());

                    if(is_array($this->input->post('q'.$value)))
                    {

                       foreach ($this->input->post('q'.$value) as $key2 => $value2) {

                              $insert_array['answer']        = $value2;


                              $this->db->insert("surveyAnswers",$insert_array);
                        }
                       

                    }
                    else
                    {
                        if($this->input->post('q'.$value))
                        {
                            $insert_array['answer']        = $this->input->post('q'.$value);
                            $this->db->insert("surveyAnswers",$insert_array);
                        }
                        
                    }
                   
                }
            }
        }
        else
        {
              $returnArray['status']  = "failure";
        }

        return $returnArray;

                                 
    }

}