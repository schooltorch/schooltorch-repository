<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends CI_Model 
{

	function __construct()
    {
    	parent::__construct();
    }

    /*
		NAME: Neeraj
		DESC: to return all the categories.31/10/14
	*/
	public function categories()
	{
		$returnArr	=	array();

		$temp	=	$this->db->select('id,name,createdDate,updated,isDeleted')
							 	->where('isDeleted',NULL)
							 		->get('blog-categories')
							 			->result_array();
		

		if(count($temp) > 0)
		{


			$articlesInfo	=	$this->db->select('*')
											->where('isPublished',1)
										 		->where('isDeleted',NULL)
										 			->get('blog-articles')
										 				->result_array();

			
			
			$fCount      = array();

			foreach ($articlesInfo as $key => $value) 
			{	
				if($value['categoryId'] != NULL)
				{
					$temp2  = explode("|",$value['categoryId']);
					$temp2 = array_filter($temp2);

					//dump($temp2);

					
					foreach ($temp2 as $key2 => $value2) {
						
						if(isset($fCount[$value2]))
						{
							array_push($fCount[$value2],$value['id']);
						}
						else
						{
							$fCount[$value2] = array();
							array_push($fCount[$value2],$value['id']);

						}
					}
					
				}
				//$categoryIds[]  = $value['id'];
			}


			//dump($fCount);


			foreach ($temp as $key => $value) 
			{
				if(isset($fCount[$value['id']]))
				{
					$temp[$key]['count'] =  count($fCount[$value['id']]);
				}
				else{
					$temp[$key]['count'] = 0;
				}

				$temp[$key]['safeTitle'] =  url_title($value['name']);// urlencode(strtolower(str_replace(" ","-",$value['name'])));
				
			}

			$temp2  = array();
			foreach ($temp as $key3 => $value3) 
			{
			 	$temp2[$value3['id']] = $value3 ;
			}

			//dump($temp);
			
			$returnArr['count']	=	count($temp2);
			$returnArr['data']	=	$temp2;

			
		}
		else{

			$returnArr['count']	=	0;
			$returnArr['data']	=	array();


		}

		

		//dump($returnArr);
		return $returnArr;

	}

	/*
		NAME: Neeraj
		DESC: to return all the articles.31/10/14
		updated by: gaurav
	*/
	public function articles($data)
	{
		$returnArr	=	array();

		$categoriesData	=	$this->db->select('id,name,createdDate,updated,isDeleted')
							 	->where('isDeleted',NULL)
							 		->get('blog-categories')
							 			->result_array();
		

		$dbcategories = array();

		if(count($categoriesData) > 0)
		{

			$this->db->start_cache();

			$tempQuery = array();
			foreach ($categoriesData as $key => $value) 
			{
				$dbcategories[] = $value['id'];

				$tempQuery[]  = "(`categoryId` LIKE '%|".$value['id']."|%')";
			}

			$this->db->where('('.implode('OR', $tempQuery).')');

			$this->db->where('isPublished',1);

			if(isset($data['year']) && isset($data['month']))
			{
				if($data['year'] != 0 && $data['month'] != 0)
				{
					$since 		= 	date($data['year'] .'-'. $data['month'] .'-01 00:00:00' );

					$lastDate	=	date('Y-m-t', strtotime($since));

					$until 		= 	$lastDate .' 23:59:59';

					$this->db->where('createdDate >', $since);
					$this->db->where('createdDate <', $until);
				}
			}

			$this->db->where('isDeleted IS NULL');



			$this->db->stop_cache();

			$totalCount 	=	$this->db->from('blog-articles')->count_all_results();

			//dump($this->db->last_query());

			$temp	=	$this->db->select('id,categoryId,title,content,userId,isPublished,isFavorite,createdDate,coverImage,isDeleted,timestamp')
									->order_by('timestamp','desc')
										->limit($data['limit'],$data['offset'])
											->get('blog-articles')
												->result_array();
			//echo $this->db->last_query();



			$this->db->flush_cache();

			//created by gaurav
			$userIds	=	__::compact(__::uniq(__::pluck($temp,'userId')));

			if(isset($userIds) && count($userIds) > 0)
			{

				$query 		=	$this->db->select('id,email,firstName,lastName,username')
												->where_in('id',$userIds)
													->get('users')
														->result_array();
				
			}

			
			
			foreach($temp as $key => $value)
			{
				if(isset($query))
				{
					if(count($query) > 0)
					{
						if($value['userId'] == $query[0]['id'])
						{
							
							$temp[$key]['firstName'] 	=	$query[0]['firstName'];
							$temp[$key]['lastName'] 	=	$query[0]['lastName'];
							$temp[$key]['email'] 		=	$query[0]['email'];
						}
					}
				}
				$temp[$key]['safeTitle'] =  url_title($value['title']);//urlencode($value['title']);
			}
			//dump($temp);
			$returnArr 					=	$data;

			$returnArr['total_rows'] 	=	$totalCount;
			$returnArr['count']			=	count($temp);
			$returnArr['data']			=	$temp;

		}
		else{

			$returnArr 					=	array();

			$returnArr['total_rows'] 	=	0;
			$returnArr['count']			=	0;
			$returnArr['data']			=	array();

		}

		//dump($returnArr['total_rows'] );

		return $returnArr;

	}

	/*
		NAME: Neeraj
		DESC: to get all articles belonging to given category Id.31/10/14
	*/
	public function category($id,$limit=10,$offset = 0 )
	{
		$returnArr	=	array();
 		
		$temp 	=	$this->db->select('id,name,createdDate,updated,isDeleted')
							 	->where('id',$id)
							 		->limit(1)
							 			->get('blog-categories')
							 				->row();
		
		if(count($temp) > 0)
		{
			$finalArr['status'] = "success";
			$finalArr['data']   = array(
										 'id' 			=> 	$temp->id,
										 'name'			=> 	$temp->name,
										 'createdDate' 	=> 	$temp->createdDate,
										 'updated'		=> $temp->createdDate,
										 'articles'     => array('count' => 0, 'data'=> array()),
										 
  										);


		}


 		$temp2	=	$this->db->select('id,categoryId,title,content,userId,isPublished,coverImage, isFavorite,createdDate,isDeleted,timestamp')
 							 	->like('categoryId',"|".$id."|")
							 		->where('isDeleted',NULL)
							 			->where('isPublished',1)
							 				->order_by('timestamp','desc')
							 					->limit($limit,$offset)
							 						->get('blog-articles')
							 							->result_array();

		//echo $this->db->last_query();
		foreach($temp2 as $key => $value) {												

			$temp2[$key]['safeTitle'] =  url_title($value['title']);//     urlencode(strtolower(str_replace(" ","-",$value['title'])));
														

		}



		
		//dump("test");							 							
		//dump($this->db->last_query());
		//dump($temp2);
		
		$finalArr['data']['articles']['count']	=	count($temp2);
		$finalArr['data']['articles']['data']	=	$temp2;

			
		//dump($finalArr);
	
		return $finalArr;
	}

	/*
		NAME: Neeraj
		DESC: to get the article belonging to given article Id.31/10/14
	*/
	public function article($id)
	{
		$returnArr	=	array();
 		
 		$temp	=	$this->db->select('id,categoryId,title,content,userId,isPublished,coverImage,isFavorite,createdDate,isDeleted,timestamp')
 							 	->where('id',$id)
							 		->where('isDeleted',NULL)
							 			->where('isPublished',1)
							 				->limit(1)
							 					->get('blog-articles')
							 						->result_array();

		
		// get the similar article
		$similarArticles = array(); 							 								
		if(count($temp) > 0)
		{							 						
   			$categoryIds  = array_values(array_filter(explode("|",$temp[0]['categoryId'])));

   			//dump($categoryIds);

   			$similarArticlesTemp  = $this->category($categoryIds[0],$limit=5);

   			//dump($similarArticlesTemp['data']['articles']);

   			

   			if($similarArticlesTemp['status'] == "success" )
   			{	
   				if(count($similarArticlesTemp['data']['articles']['data']) > 0)
   				{
   					$similarArticles  = $similarArticlesTemp['data']['articles']['data'];
   				}

   			}
   		}
   		else
   		{
   			return array(
   							'status'	=>	'failure',
   							'errors'	=>	'Blog Id not present in the database.'
   						);
   		}	

   		$temp[0]['safeTitle'] = url_title($temp[0]['title']);  //urlencode(strtolower(str_replace(" ","-",$temp[0]['title'])));					 							



   		$returnArr['status']			=	'success';	
		$returnArr['count']				=	count($temp);
		$returnArr['data']				=	$temp;
		$returnArr['similarArticles']	=	$similarArticles;

		//dump($returnArr);
		return $returnArr;
	}



}