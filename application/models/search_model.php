<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends CI_Model{

	function get($q)
	{

		$returnArr = array();

		if($this->input->get('isCity') == 1)
		{
			$query = $this->db->select('city, state_code')
								->from('cities_extended')
									->like('city', $q)
										->distinct('city')
											->order_by('city', 'asc')
												->limit(10)
													->get()
														->result();

			foreach($query as $result)
			{
				$returnArr[$result->city] = array('name' => $result->city, "photo" => '',"category"	=>	'city');
			}
		}
		/*else if($this->input->get('isMajor') == 1)
		{

			$query = $this->db->select('codevalue, valuelabel')
								->from('schoolFrequencies')
									->where('varname', 'CIPCODE')
										->where('table', 'schoolCompletions')
											->like('valuelabel', $q)
												->limit(10)
													->get()
														->result();

			foreach($query as $result)
			{
				$returnArr[$result->codevalue] = array('name' => $result->valuelabel, "photo" => '',"category"	=>	'major');
			}

		}*/
		else
		{
			$query = $this->db->select('UNITID, INSTNM, CITY')
								->from('schools')
									->like('INSTNM', $q)
										->order_by('INSTNM', 'asc')
										//->or_like('CITY', $q)
											->limit(20)
												->get()
													->result();

			foreach($query as $result)
			{
				/*if($result->photo != '' && $result->photo != null)
				{
					$photo = explode($this->config->item('schoolPhotoGlue'), $result->photo);

					$photo = $photo[0];
				}
				else
				{
					$photo = '';
				}*/
				
				$returnArr[$result->UNITID] = array('name' => $result->INSTNM, "category"	=>	'school');
			}
		}

		return $returnArr;
		
	}

	
	function advanced($instsize=0, $locale, $state, $tuition, $selectivity, $city, $major, $limit, $offset, $orderby = '', $order = '',$savedSchools=array(),$onlyGraduate=0)
	{
		$majorList 	=	$this->config->item('major');
		
		$stateList 	= $this->config->item('state');

		$stateList  = array_flip($stateList);

		$returnArr = array();

		$majorSchoolIds 	=	array();

		if(isset($major) && isset($majorList[$major]))
		{
			$cookie = 	array(
								'name'   => 'showGraduateOnly',
								'value'  => 1,
								'expire' => '86500'
							);

			$this->input->set_cookie($cookie);

			$onlyGraduate 	=	1;

			$maxMajor 		=	(int)$major + 1;

			$majorSchools	=	$this->db->select('UNITID')
											->from('schoolPrograms')
												->where('schoolPrograms.CIPCODE >= '. $major .' AND schoolPrograms.CIPCODE < '. $maxMajor)
													->where('ISARCHIVED IS NULL')
														->where('ISDELETED IS NULL')
															->get()
																->result_array();

			if(count($majorSchools) > 0)
			{

				$majorSchoolIds 	=	$this->__->pluck($majorSchools, 'UNITID');

				//ADDED ON 17/04/2015 
				$majorSchools	=	$this->db->select('UNITID')
												->from('schoolCompletions')
													->where_in('AWLEVEL', array(7 , 17 , 18 , 19))
														->where_in('UNITID', $majorSchoolIds)
															->where('schoolCompletions.CIPCODE >= '. $major .' AND schoolCompletions.CIPCODE < '. $maxMajor)
															->where('is_archived IS NULL')
																->where('is_deleted IS NULL')
																	->get()
																		->result_array();

				if(count($majorSchools) > 0)
				{
					$majorSchoolIds 	=	$this->__->pluck($majorSchools, 'UNITID');
				}
				else
				{
					$majorSchoolIds 	=	array();
				}
				
			}
			else
			{
				return 	array(
								'totalCount' => 0,
								'count' => 0,
								'data' 	=> $returnArr
							);
			}

		}
		else if(isset($major) && !isset($majorList[$major]))
		{
			unset($_REQUEST['major']);
		}

		$this->db->start_cache();

		$selects = 'schools.UNITID as UNITID, schools.GROFFER as GROFFER,schools.INSTNM as INSTNM,schools.ADDR as ADDR,schools.CITY as CITY,schools.STABBR as STABBR,schools.STABBR as STATENAME,schools.ZIP as ZIP,schools.GENTELE as GENTELE,schools.WEBADDR as WEBADDR,schools.LOCALE as LOCALE,schools.LONGITUD as LONGITUD,schools.LATITUDE as LATITUDE,schools.SELECTIVITY as SELECTIVITY,schools.ACCEPTANCE as ACCEPTANCE, schools.TOTALSTUDENTS AS TOTALSTUDENTS,schoolStudentChargesYear.TUITION7 as TUITION,schools.SELECTIVITY as SELECTIVITY,schools.OBEREG as OBEREG,schoolsViewCount.count as VIEWSCOUNT';
		
		$this->db->select($selects)
							->from('schools')
								->join('schoolStudentChargesYear', 'schools.UNITID = schoolStudentChargesYear.UNITID')
									->join('schoolsViewCount', 'schools.UNITID = schoolsViewCount.schoolId', 'LEFT');

		//adding instsize filter

		if(!isset($orderby))
		{
			$orderby 	=	'selectivity';
		}

		if(!isset($order))
		{
			$order 		=	'desc';
		}

		if($orderby == NULL)
		{
			$orderby 	=	'selectivity';
		}

		if(!in_array($orderby, array('selectivity', 'enrollment', 'cost', 'views')))
		{
			$orderby 	=	'selectivity';
		}

		if(!in_array($order, array('desc', 'asc')))
		{
			$order 		=	'desc';
		}

		if($order == NULL)
		{
			$order 		=	'desc';
		}

		$orderByset 	=	0;

		if($orderby == 'enrollment')
		{
			$orderByset 	=	1;
			$this->db->order_by('schools.TOTALSTUDENTS', $order);
		}

		if($orderby == 'cost')
		{
			$orderByset 	=	1;
			$this->db->order_by('schoolStudentChargesYear.TUITION7', $order);
		}

		if($orderby == 'views')
		{
			$orderByset 	=	1;
			$this->db->order_by('schoolsViewCount.count', $order);
		}

		if($orderby == 'selectivity' || $orderByset == 0)
		{
			$this->db->order_by('schools.SELECTIVITY', $order);
		}
		
		if(count($savedSchools)>0)
		{
			$this->db->where_in('schools.UNITID', $savedSchools);
			$limit 	=	20;
		}
		else
		{
			if(!in_array($onlyGraduate, array(0,1)))
			{
				$onlyGraduate 	=	1;
			}

			if($onlyGraduate == 1)
			{
				$this->db->where('schools.GROFFER', '1');
			}
		}

		if($state)
		{

			$state 	=	explode('_', $state);

			$selectivityCondition 		=	array();

			foreach($state as $frag)
			{

				if($frag -1 >= 0 && in_array($frag, array_values($stateList)))
				{

					$value 	=	$this->db->escape_str($frag -1);
					
					if($frag >= 1000)
					{
						$value 	=	$this->db->escape_str($frag);
					}

					$selectivityCondition[] 	=	'(schools.OBEREG = '. $value .')';
				}
			}
			
			if(count($selectivityCondition) > 0)
			{
				$selectivityCondition 		=	'('. implode(' OR ', $selectivityCondition) .')';
				
				$this->db->where($selectivityCondition);
			}
			else
			{
				unset($_REQUEST['state']);
			}
			
		}

		//adding tuition filter
		/*if($tuition)
		{

			$frags = explode('|', $tuition);

			foreach($frags as $frag)
			{
				if(substr($frag, 0, 1) == 'L')
				{
					$this->db->where('schoolStudentChargesYear.TUITION7 <', substr($frag, 1));
				}
				else
				{
					$this->db->where('schoolStudentChargesYear.TUITION7 >', substr($frag, 1));
				}
			}

		}*/
		
		if($tuition)
		{
			$frags 			=	array();

			$tuition 	=	explode('_', $tuition);

			if(in_array('5', $tuition))
			{
				$frags[] 	=	array(40000);
			}
			
			if(in_array('4', $tuition))
			{
				$frags[] 	=	array(30000, 40000);
			}
			
			if(in_array('3', $tuition))
			{
				$frags[] 	=	array(20000, 30000);
			}
			
			if(in_array('2', $tuition))
			{
				$frags[] 	=	array(10000, 20000);
			}

			if(in_array('1', $tuition))
			{
				$frags[] 	=	array(0, 10000);
			}

			if(count($frags) > 0)
			{
				$selectivityCondition 		=	array();

				foreach($frags as $frag)
				{
					if(count($frag) == 1)
					{
						$selectivityCondition[] 	=	'(schoolStudentChargesYear.TUITION7 > '. $this->db->escape_str($frag[0]) .')';
					}
					else if(count($frag) == 2)
					{
						$selectivityCondition[] 	=	'(schoolStudentChargesYear.TUITION7 BETWEEN '. $this->db->escape_str($frag[0]) .' AND '. $this->db->escape_str($frag[1]).')';
					}
				}

				if(count($selectivityCondition) > 0)
				{
					$selectivityCondition 		=	'('. implode(' OR ', $selectivityCondition) .')';
					
					$this->db->where($selectivityCondition);
				}
			}
			else
			{
				unset($_REQUEST['tuition']);
			}
			
		}

		if($selectivity)
		{
			$frags 			=	array();

			$selectivity 	=	explode('_', $selectivity);

			if(in_array('5', $selectivity))
			{
				$frags[] 	=	array(700, 800);
			}
			
			if(in_array('4', $selectivity))
			{
				$frags[] 	=	array(600, 699);
			}
			
			if(in_array('3', $selectivity))
			{
				$frags[] 	=	array(550, 599);
			}
			
			if(in_array('2', $selectivity))
			{
				$frags[] 	=	array(500, 549);
			}

			if(in_array('1', $selectivity))
			{
				$frags[] 	=	array(0, 499);
			}

			if(count($frags) > 0)
			{
				$selectivityCondition 		=	array();

				foreach($frags as $frag)
				{
					$selectivityCondition[] 	=	'(schools.SELECTIVITY BETWEEN '. $this->db->escape_str($frag[0]) .' AND '. $this->db->escape_str($frag[1]).')';
				}
				
				if(count($selectivityCondition) > 0)
				{
					$selectivityCondition 		=	'('. implode(' OR ', $selectivityCondition) .')';
					
					$this->db->where($selectivityCondition);
				}
			}
			else
			{
				unset($_REQUEST['selectivity']);
			}
		}

		if(isset($instsize))
		{

			$instValues = 	explode('_',$instsize);

			$frags 		=	array();

			if(in_array('5', $instValues))
			{
				$frags[] 	=	array(40001);
			}
			
			if(in_array('4', $instValues))
			{
				$frags[] 	=	array(25001, 40000);
			}
			
			if(in_array('3', $instValues))
			{
				$frags[] 	=	array(10001, 25000);
			}
			
			if(in_array('2', $instValues))
			{
				$frags[] 	=	array(5001, 10000);
			}

			if(in_array('1', $instValues))
			{
				$frags[] 	=	array(0, 5000);
			}

			if(count($frags) > 0)
			{
				$selectivityCondition 		=	array();

				foreach($frags as $frag)
				{
					if(count($frag) == 1)
					{
						$selectivityCondition[] 	=	'(schools.TOTALSTUDENTS > '. $this->db->escape_str($frag[0]) .')';
					}
					else if(count($frag) == 2)
					{
						$selectivityCondition[] 	=	'(schools.TOTALSTUDENTS BETWEEN '. $this->db->escape_str($frag[0]) .' AND '. $this->db->escape_str($frag[1]).')';
					}
				}
				
				if(count($selectivityCondition) > 0)
				{
					$selectivityCondition 		=	'('. implode(' OR ', $selectivityCondition) .')';
					
					$this->db->where($selectivityCondition);
				}
			}
			else
			{
				unset($_REQUEST['instsize']);
			}
		}

		if(isset($city))
		{
			if($city != '')
			{
				$this->db->where('`schools.CITY` LIKE "%'. $this->db->escape_str(urldecode($city)) .'%"');
			}
		}

		if(isset($major) && isset($majorList[$major]))
		{
			$this->db->where_in('schools.UNITID', $majorSchoolIds);
		}

		$this->db->stop_cache();

		//final section
		$totalCount = $this->db->count_all_results();
		
		$query = $this->db->limit($limit, $offset)->get()->result();
		
		$this->db->flush_cache();

		$schoolIds = array();

		foreach($query as $result)
		{
			//modify acceptance
			$result->ACCEPTANCE 	= 	$result->ACCEPTANCE;
			$result->SELECTIVITY 	= 	parseSelectivity($result->SELECTIVITY);
			//$result->SELECTIVITY 	= 	$result->SELECTIVITY;
			$result->URLTITLE    	= 	url_title($result->INSTNM);
			$result->GROFFER        =   $result->GROFFER;


			if(isset($stateList[$result->STABBR]))
			{	
				$result->STATENAME   	= $stateList[$result->STABBR];			
			}
			else
			{
				$result->STATENAME   	= $result->STABBR;
			}

			
			
			//$returnArr[$result->UNITID] = $result;
			
			/*if(!$result->photo)
			{
				$result->photo = getSchoolLocationPics($result);

				if($result->photo != '')
				{
					$result->photo = implode($this->config->item('schoolPhotoGlue'), $result->photo);
				}
			}*/
			
			$result->TUITIONLABEL 		= 	$this->tuitionRanges($result->TUITION);
			$result->TUITION 			= 	number_format($result->TUITION);
			$result->TOTALSTUDENTSLABEL = 	$this->instRanges($result->TOTALSTUDENTS);
			$result->TOTALSTUDENTS 		= 	number_format($result->TOTALSTUDENTS);
			$schoolIds[]				=	$result->UNITID;
			$result->LOCALE 			= 	parseLocale($result->LOCALE);

			$result->COUNTRY 			=	parseCountry($result->OBEREG);


			$returnArr[] = $result;
		}

		$schoolIds = array_unique($schoolIds);

		if(count($schoolIds) > 0)
		{
			$schoolsImages	=	$this->db->select('ID,photo,isGoogleLocationPicsProcessed,UNITID')
											->where_in('UNITID',$schoolIds)
												->limit(count($schoolIds))
													->get('schoolsImages')
														->result_array();
		}
		else
		{
			$schoolsImages 	=	array();
		}

		if(count($schoolsImages) > 0)
		{
			foreach ($schoolsImages as $key => $value) 
			{
				$tempSchoolImages[$value['UNITID']]	=	$value;
			}
		}
		else
		{
			$tempSchoolImages	=	array();
		}

		$extraData = getSchoolExtraData($schoolIds);
		
		foreach ($returnArr as $key => $value) {
		
			if(isset($extraData['savedSchools'][$value->UNITID])){
				$value->SAVEDID = $extraData['savedSchools'][$value->UNITID];
			}	
			else
			{
				$value->SAVEDID = null;
			}

			if(isset($extraData['votedSchools'][$value->UNITID])){
				$value->VOTEDID = $extraData['votedSchools'][$value->UNITID];
			}
			else
			{
				$value->VOTEDID = null;	
			}
			// change for views count[2/12/2015 HAri]
			if(isset($extraData['viewsCount'][$value->UNITID])){

			
				$value->VIEWSCOUNT = $extraData['viewsCount'][$value->UNITID];
			}
			else
			{
				$value->VIEWSCOUNT = 0;	
			}

			if(isset($tempSchoolImages[$value->UNITID]))
			{
				$value->photo 	=	$tempSchoolImages[$value->UNITID]['photo'];
			}
			else
			{
				$value->photo 	=	null;
			}


		}

			//dump($returnArr);		

		if($this->tank_auth->is_logged_in())
		{
			$showGraduateFlag 	=	$this->session->userdata('onlyGradSchools');
			$userId 			=	$this->session->userdata('user_id');

			if($onlyGraduate != $showGraduateFlag)
			{
				$this->session->set_userdata('onlyGradSchools', $onlyGraduate);

				$this->db->set('onlyGradSchools', $onlyGraduate)
							->where('id', $userId)
								->update('users');
			}
		}
		
		return 	array(
						'totalCount' => $totalCount,
						'count' => count($returnArr),
						'data' 	=> $returnArr
					);
	}






	function getInstRanges($instsize){

		if($instsize == 4)
		{
			return	array(25000, 40000);
		}
		else if($instsize == 3)
		{
			return 	array(10000, 25000);
		}
		else if($instsize == 2)
		{
			return 	array(5000, 10000);
		}
		else
		{
			return 	array(0, 5000);
		}
		
	}

	function instRanges($instsize){

		if($instsize > 40001)
		{
			return 5;
		}
		else if($instsize >= 25001 && $instsize <= 40000)
		{
			return 4;
		}
		else if($instsize >= 10001 && $instsize <= 25000)
		{
			return 3;
		}
		else if($instsize >= 5001 && $instsize <= 10000)
		{
			return 2;
		}
		else
		{
			return 1;
		}
		
	}

	function tuitionRanges($cost)
	{
		if($cost > 40000)
		{
			return 5;
		}
		else if($cost >= 30000 && $cost <= 40000)
		{
			return 4;
		}
		else if($cost >= 20000 && $cost <= 30000)
		{
			return 3;
		}
		else if($cost >= 10000 && $cost <= 20000)
		{
			return 2;
		}
		else
		{
			return 1;
		}
	}


}