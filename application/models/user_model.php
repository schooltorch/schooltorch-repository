<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model 
{

	function __construct()
    {
    	parent::__construct();
    }

    /*
		NAME: Neeraj
		DESC: to update the user email settings.29/1/15.
	*/

	function updateEmailSettings($data)
	{
		if($data['key']	==	'all' && $data['value']	==	1)
		{
			$updateData	=	array(
									'blogUpdatesEmail'	=>	1,
									'reviewUpdatesEmail'=>	1
								);

			$this->db->where('id',$this->session->userdata('user_id'))
					 	->update('users',$updateData);

			return array('status'	=>	'success');
		}
		else if(($data['key'] ==	'blogUpdatesEmail' || $data['key'] == 'reviewUpdatesEmail') && ($data['value'] ==	'1' || $data['value'] == '0'))
		{
			if($data['value']	==	0)
			{
				$data['value']	=	NULL;
			}

			$updateData	=	array(
									$data['key']	=>	$data['value']
								);

			$this->db->where('id',$this->session->userdata('user_id'))
					 	->update('users',$updateData);

			return array('status'	=>	'success');
		}
		else
		{
			$updateData	=	array(
									'blogUpdatesEmail'	=>	NULL,
									'reviewUpdatesEmail'=>	NULL
								);

			$this->db->where('id',$this->session->userdata('user_id'))
					 	->update('users',$updateData);

			return array('status'	=>	'success');
		}

	}

    /*
    	NAME: Neeraj
    	DESC: to get permissions for loggedIn user.29/1/15.
    */

    function getCurrentUserPermissions()
    {
    	$permissions 	=	$this->db->select('id,blogUpdatesEmail,reviewUpdatesEmail')
    								 	->where('id',$this->session->userdata('user_id'))
    								 		->get('users')
    								 			->result_array();

    	return $permissions;
    }

    function read($params)
    {
    	$this->db->start_cache();

		if(isset($params['filterBy']) &&  isset($params['filter']))
        {
            if($params['filterBy'] == 'name') 
            {
               $query_array    =   array();

                $query_array[]  =   "LCASE( CONCAT(  '',  firstName ,  ' ',  lastName) ) LIKE '". $this->db->escape_str(strtolower($params['filter'])) ."%'";
                $query_array[]  =   "LCASE(firstName) LIKE '". $this->db->escape_str(strtolower($params['filter'])) ."%'";

                $query_array[]  =   "LCASE(lastName) LIKE '". $this->db->escape_str(strtolower($params['filter'])) ."%'";

                $searchCondition    =   '('. implode(' OR ', $query_array) .')';
                $this->db->where($searchCondition);
            }
            else if($params['filterBy'] == 'email') 
            {
                $this->db->where("email LIKE '%". $this->db->escape_str($params['filter']) ."%'"); 
            }
            else if($params['filterBy'] == 'role') 
            {
                if($params['filter'] == 1)
                {
                    $this->db->where("role",1);
                }
                else
                {
                    $this->db->where("role",2);
                }
            }
        }

    	$this->db->stop_cache();

    	$total_users 	= 	$this->db->from('users')->where('isArchived IS NULL')
										->count_all_results();

    	$users_data = $this->db->select('id , username, firstName, lastName, role , email , activated, banned, ban_reason, created, modified')
											->from('users')
												->where('isArchived IS NULL')
													->limit($params['limit'], $params['offset'])
														->order_by('id', 'desc')
															->get()
																->result_array();

		foreach ($users_data as $key => $value) 
		{
			//$users_data[$key]['name']	=	$value['firstName'].' '.$value['lastName'];

			//Updated by Jayant Wankhede.
			if(strlen(trim($value['firstName'])) == 0 && strlen(trim($value['lastName'])) == 0) 
			{
				$users_data[$key]['name']	= 'Anonymous';
			}
			else
			{
				$users_data[$key]['name']	=	$value['firstName'].' '.$value['lastName'];
			}
		}
		
		$this->db->flush_cache();
		$return_array 	= 	$params;

		$return_array['site_name'] 			= 	'Users';	
		$return_array['count']				=	count($users_data);
		$return_array['status']				=	'success';
		$return_array['total_count']		=	$total_users;
		$return_array['current_page_no']	=	$params['page_no'];
		$return_array['data'] 				=	$users_data;

		return $return_array;

    }

    function update($params)
    {

    	if($params['role'] == 1)
    	{
    		$users_data 	=	$this->db->from('users')
											->where('role', 2)
												->count_all_results();

			if($users_data <= 1)
			{
				$return_array['status']		=	'failure';
				$return_array['message'] 	=	'There has to be one administrator in the system.';
				$admini['code'] 			=	1103;

				return $return_array;
			}

    	}

    	$users_data 	=	$this->db->from('users')
										->where('id', $params['id'])
											->limit(1)
												->count_all_results();

		if($users_data == 0)
		{
			$return_array['status']		=	'failure';
			$return_array['message'] 	=	'User does not exists.';
			$return_array['code'] 		=	1103;

			return $return_array;
		}
		else
		{
			$this->db->set('role', $params['role'])
						->where('id', $params['id'])
							->update('users');

			$flag 	=	$this->db->affected_rows();

			if($flag == 0)
			{
				$return_array['status']		=	'failure';
				$return_array['message'] 	=	'User does not exists.';
				$return_array['code'] 		=	1103;
			}
			else
			{
				$return_array['status']		=	'success';
			}

			return $return_array;

		}

    }

    // get user by their social media id
	function get_user_by_sm($data, $sm_id)
	{
		$this->db->select("u.*, up.facebook_id, up.twitter_id, up.google_open_id, up.facebook_data, up.twitter_data, up.google_data");
		$this->db->from("users AS u");
		$this->db->join("user_profiles AS up", "u.id=up.user_id");
		$this->db->where($data);
		$query = $this->db->get();
		return $query->result();
	}

	// Returns user by its email
	function get_user_by_email($email)
	{
		$query = $this->db->query("SELECT * FROM users u, user_profiles up WHERE u.email='$email' and u.id = up.user_id");
		return $query->result();
	}
	
	function get_user_by_username($username)
	{
		$query = $this->db->query("SELECT * FROM users u, user_profiles up WHERE u.username='$username' and u.id = up.user_id");
		return $query->result();
	}

	// a generic update method for user profile
	function update_user_profile($user_id, $data)
	{
		$count = $this->db->select('user_id')
					->from('user_profiles')
						->where('user_id',$user_id)
							->count_all_results();

		if($count == 0){
			$data['user_id'] = $user_id;
			$this->db->insert('user_profiles', $data); 
		}
		else
		{
			$this->db->where('user_id', $user_id);
			$this->db->update('user_profiles', $data); 	
		}						
		
	}

	// return the user given the id
	function get_user($user_id)
	{
		$query = $this->db->query("SELECT users.*, user_profiles.* FROM users, user_profiles WHERE " .
								  "users.id='$user_id' AND user_profiles.user_id='$user_id'");
		return $query->result();
	}

    /*// for finding user via facebook id
	function get_user_by_facebook_id($facebook_id)
	{
		$query = $this->db->query("SELECT users.*, user_profiles.facebook_id FROM users " .
								  "JOIN user_profiles ON users.id=user_profiles.user_id " .
								  "WHERE facebook_id='$facebook_id'");		
		return $query->result();
	}
	
	// for finding user via twitter id
	function get_user_by_twitter_id($twitter_id)
	{
		$query = $this->db->query("SELECT users.*, user_profiles.twitter_id FROM users " .
								  "JOIN user_profiles ON users.id=user_profiles.user_id " .
								  "WHERE twitter_id='$twitter_id'");		
		return $query->result();
	}	
	
	// for finding user via gfc id
	function get_user_by_gfc_id($gfc_id)
	{
		$query = $this->db->query("SELECT users.*, user_profiles.gfc_id FROM users " .
								  "JOIN user_profiles ON users.id=user_profiles.user_id " .
								  "WHERE gfc_id='$gfc_id'");		
		return $query->result();
	}	

	// Returns user by its email
	function get_user_by_email($email)
	{
		$query = $this->db->query("SELECT * FROM users u, user_profiles up WHERE u.email='$email' and u.id = up.user_id");
		return $query->result();
	}
	
	// update user profile with facebook id
	function update_facebook_user_profile($user_id, $facebook_id)
	{
		$query = $this->db->query("UPDATE user_profiles SET facebook_id='$facebook_id' WHERE user_id='$user_id'");
	}
	
	// update user profile with twitter id
	function update_twitter_user_profile($user_id, $twitter_id)
	{
		$query = $this->db->query("UPDATE user_profiles SET twitter_id='$twitter_id' WHERE user_id='$user_id'");
	}	
	
	// update user profile with gfc id
	function update_gfc_user_profile($user_id, $gfc_id)
	{
		$query = $this->db->query("UPDATE user_profiles SET gfc_id='$gfc_id' WHERE user_id='$user_id'");
	}		

	// return the user given the id
	function get_user($user_id)
	{
		$query = $this->db->query("SELECT users.*, user_profiles.* FROM users, user_profiles WHERE " .
								  "users.id='$user_id' AND user_profiles.user_id='$user_id'");
		return $query->result();
	}*/

	function get_user_details($user_id){

		$data 			=	array();

		$row 			=	$this->db->select('email, firstName, lastName, picture, profilePicFlag')
										->where('id',$user_id)	
											->limit(1)
												->get('users')
													->row();

		$socialInfo 	=	$this->db->select('facebook_id, twitter_id, google_open_id, facebook_data, twitter_data, google_data')
										->where('user_id',$user_id)	
											->limit(1)
												->get('user_profiles')
													->row();

		if(isset($row->email)) 
		{
			$data['email'] 		= 	$row->email;
			$data['firstName'] 	= 	$row->firstName;
			$data['lastName'] 	= 	$row->lastName;

			if($row->picture != NULL)
			{
				$data['picture'] 	= 	base_url('profilePics/'.$row->picture);
			}
			else
			{
				$data['picture'] 	= 	$row->picture;
			}
			
			$data['profilePicFlag'] 	= 	$row->profilePicFlag;
		}

		if(isset($socialInfo->facebook_id) || isset($socialInfo->twitter_id) || isset($socialInfo->google_open_id))
		{

			$data['facebook_id'] 		= 	$socialInfo->facebook_id;
			$data['twitter_id'] 		= 	$socialInfo->twitter_id;
			$data['google_open_id'] 	= 	$socialInfo->google_open_id;

			$data['facebook_data'] 		= 	json_decode($socialInfo->facebook_data, true);
			$data['twitter_data'] 		= 	json_decode($socialInfo->twitter_data, true);
			$data['google_data'] 		= 	json_decode($socialInfo->google_data, true);

		}

		return $data;

	}


	function update_basic_info(){

		$data = array();

		if(isset($_FILES['userfile']))
		{
			if(count($_FILES['userfile']) > 0)
			{
				if(isset($_FILES['userfile']['name']))
				{
					if(trim($_FILES['userfile']['name']) != '')
					{
						$this->load->model('school_model');
						
						$result 	=	$this->school_model->uploadFile('userfile', './profilePics', 80, 80);

						if($result['status'] == "success")
						{
							$this->db->set('picture' , $result['data']['file_name']);
						}
					}
				}
			}
		}

		//Updated By Jayant Wankhede.
		$firstName 	=	$this->input->post('firstName');
		$lastName 	=	$this->input->post('lastName');
		
		if(is_null($firstName) || $firstName == '') 
		{
			$firstName 	=	NULL;
		}

		if(is_null($lastName) || $lastName == '') 
		{
			$lastName 	=	NULL;
		}


		$this->db->set('firstName',$firstName)
						->set('lastName',$lastName)
							->where('id',$this->session->userdata('user_id'))
								->update('users');

		$data['status'] = 'success';
		$data['message'] = 'Basic Info Updated';

		return $data;			

	}

	//GET background section
	function getBackGroundSection()
	{

		$returnArray  = array();

		$userInfo = $this->db->select('location,gmat,gre,gpa,undergradInstitute,budget,yearsOfExp')
							->where('id',$this->session->userdata('user_id'))
								->from('users')
									->limit(1)
										->get()
											->result_array();
		

		if(count($userInfo) > 0)
		{	
			$returnArray['status'] 	= "success";
			$returnArray['data']	= $userInfo[0];
		}	
		else
		{
			$returnArray['failure'] 	= "success";
			$returnArray['errors']   	= "You are not authorized to update others info";

		}

		return $returnArray;

	}

	// Update background section
	function updateBackGroundSection(){

		$data = array();

		$count = $this->db->select('email')
							->where('id',$this->session->userdata('user_id'))
								->from('users')
									->count_all_results();

		if($count > 0)
		{

			$updateData = array();

			if($this->input->post('location') != '')
			{
				$updateData['location'] = 	$this->input->post('location');
			}
			else{
				$updateData['location'] = NULL;
			}
			if($this->input->post('gmat') != '')
			{
				$updateData['gmat'] = 	$this->input->post('gmat');
			}
			else{
				$updateData['gmat'] = NULL;
			}

			if($this->input->post('gre') != '')
			{
				$updateData['gre'] = 	$this->input->post('gre');
			}
			if($this->input->post('gpa') != '')
			{
				$updateData['gpa'] = 	$this->input->post('gpa');
			}
			else{
				$updateData['gpa'] = NULL;
			}

			if($this->input->post('undergradInstitute') != '')
			{
				$updateData['undergradInstitute'] = 	$this->input->post('undergradInstitute');
			}
			else{
				$updateData['undergradInstitute'] = NULL;
			}
			if($this->input->post('budget') != '')
			{
				$updateData['budget'] = 	$this->input->post('budget');
			}
			else{
				$updateData['budget'] = NULL;
			}
			if($this->input->post('yearsOfExp') != '')
			{
				$updateData['yearsOfExp'] = 	$this->input->post('yearsOfExp');
			}
			else{
				$updateData['yearsOfExp'] = NULL;
			}

			

			if(count($updateData) > 0)
			{
				$this->db->where('id',$this->session->userdata('user_id'))
							->update('users',$updateData);
			}

			//dump($this->db->last_query());


			$data['status'] = 'success';
			$data['message'] = 'Backgroud  section updated';								

		}	
		else
		{
			$data['status'] = 'failure';	
			$data['errors']['email'] = 'You cannot udate others information';

		}	

		return $data;			

	}


	/*
		NAME: Ranjit Randive
		DESC: 
	*/

	function profilePhotoSettings($flag=0)
	{

		if($flag == 0)
		{
			$flag 	=	NULL;
		}

		$this->db->set('profilePicFlag', $flag)
					->where('id', $this->session->userdata('user_id'))
						->update('users');

		return updateSessionData();


	}


	/*
		NAME: Ranjit Randive
		DESC: 
	*/
	function disconnectFacebook()
	{

		$this->session->set_userdata(array('facebook_id' => ''));

		$this->db->set('facebook_id', NULL)
					->set('facebook_data', NULL)
						->where('user_id',$this->session->userdata('user_id'))
							->update('user_profiles');

		updateSessionData();

		return array('status'=>'success');			

	}

	/*
		NAME: Ranjit Randive
		DESC: 
	*/
	function disconnectTwitter()
	{

		$this->session->set_userdata(array('twitter_id' => ''));
		
		$this->db->set('twitter_id', NULL)
					->set('twitter_data', NULL)
						->where('user_id',$this->session->userdata('user_id'))
							->update('user_profiles');

		updateSessionData();

		return array('status'=>'success');			

	}


	/*
		NAME: Ranjit Randive
		DESC: 
	*/
	function disconnectGoogle()
	{

		$this->session->set_userdata(array('google_open_id' => ''));
		
		$this->db->set('google_open_id', NULL)
					->set('google_data', NULL)
						->where('user_id',$this->session->userdata('user_id'))
							->update('user_profiles');

		updateSessionData();

		return array('status'=>'success');			

	}


}