<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class School_model extends CI_Model{

	function edit($id)
	{
		echo "in the edit function";
	}

	function delete($id)
	{
		echo "in the delete function";
	}


	function getSchool($sid=""){

		if(isset($sid))
		{
			
			$data['schools'] 	=	$this->db->select('schools.UNITID as UNITID,schools.INSTNM as INSTNM,schools.ADDR as ADDR,schools.CITY as CITY,schools.STABBR as STABBR,schools.ZIP as ZIP, schools.SELECTIVITY as SELECTIVITY,schools.GENTELE as GENTELE,schools.WEBADDR as WEBADDR,schools.LOCALE as LOCALE,schools.LONGITUD as LONGITUD,schools.LATITUDE as LATITUDE,schools.ACCEPTANCE as ACCEPTANCE,schools.TOTALSTUDENTS as TOTALSTUDENTS,schools.CHFNM AS CHFNM,schools.CHFTITLE AS CHFTITLE,schools.GENTELE AS GENTELE,schools.FAXTELE AS FAXTELE,schools.EIN AS EIN,schools.ADMINURL AS ADMINURL,schools.FAIDURL AS FAIDURL,schools.APPLURL AS APPLURL,schools.GROFFER as GROFFER,
				schools.NPRICURL AS NPRICURL,schoolStudentChargesYear.TUITION7 as TUITION1,schools.OPENPUBL AS OPENPUBL,schoolStudentChargesYear.CHG5AY3 as CHG5AY3,schoolStudentChargesYear.CHG6AY3 as CHG6AY3,schoolStudentChargesYear.CHG4AY3 as CHG4AY3,schoolCharacteristics.APPLFEEU as APPLFEEU')
												->where('schools.UNITID',$sid)
													->join('schoolStudentChargesYear','schoolStudentChargesYear.UNITID = schools.UNITID','left outer')
														->join('schoolCharacteristics','schoolCharacteristics.UNITID = schools.UNITID','left outer')
															->from('schools')
																->limit(1)
																	->get()
																		->result_array();

			if(count($data['schools']) != 1 )
			{	
				return false;
			}
			else
			{

				$schoolPics	=	$this->db->select('id,schoolId,fileName')
											->where('schoolId',$sid)
												->where('isApproved',1)
													->where('isArchived',NULL)
														->where('isDeleted',NULL)
															->get('schoolsUserImages')
																->result_array();

				if(count($schoolPics) > 0)
				{
					foreach($schoolPics as $key => $schoolPic)
					{
						$schoolPics[$key]['fileName']	=	'schoolsImages/'.$schoolPic['fileName'];
					}
				}

				//dump($schoolPics);

				$schoolsImages	=	$this->db->select('ID,photo,isGoogleLocationPicsProcessed,UNITID')
												->where('UNITID',$sid)
													->limit(1)
														->get('schoolsImages')
															->result_array();

				if(count($schoolsImages)	==	1)
				{
					$data['schools'][0]['photo']	=	$schoolsImages[0]['photo'];
				}
				else
				{
					$data['schools'][0]['photo']	=	'';
				}
				
				
				if(count($data['schools']) > 0)
				{
					if($data['schools'][0]['GROFFER'] == 1)
					{
						$selectStr 	=	"SELECT schoolFrequencies.valuelabel, schoolCompletions.CIPCODE, MAX( schoolCompletions.CTOTALT ) AS SumOfCTOTALT
										FROM (schools INNER JOIN schoolCompletions ON schools.UNITID = schoolCompletions.UNITID) INNER JOIN schoolFrequencies ON schoolCompletions.CIPCODE = schoolFrequencies.codevalue
										WHERE (((schoolCompletions.AWLEVEL)=7 Or (schoolCompletions.AWLEVEL)=17 Or (schoolCompletions.AWLEVEL)=18 Or (schoolCompletions.AWLEVEL)=19)) AND schoolCompletions.CIPCODE != 99 AND schools.UNITID=". $sid ."
										GROUP BY schools.UNITID, schoolCompletions.CIPCODE
										ORDER BY SumOfCTOTALT DESC
										LIMIT 5";

						$majorData 	=	$this->db->query($selectStr)->result_array();
						// echo "<pre>";
						// print_r($majorData);
						// die;
						$data['schools'][0]['topMajors'] 	=	array(
																		'status'	=>	'success',
																		'data'		=>	$majorData
																	);
					}
					else
					{
						$data['schools'][0]['topMajors'] 	=	array(
																		'status'	=>	'failure',
																		'data'		=>	array()
																	);
					}

					
					if(isset($data['schools'][0]['TUITION1']) || isset($data['schools'][0]['CHG4AY3']) || isset($data['schools'][0]['CHG5AY3']) || isset($data['schools'][0]['CHG5AY3']))
					{
						$data['schools'][0]['TOTALONCAMPUS'] = number_format($data['schools'][0]['TUITION1']+$data['schools'][0]['CHG4AY3']+$data['schools'][0]['CHG5AY3']);
					}

					$checkArray = array('TOTALSTUDENTS', 'TUITION1', 'CHG5AY3', 'CHG6AY3', 'CHG4AY3');

					$this->load->model('search_model');

					$data['schools'][0]['SELECTIVITY'] 			= 	parseSelectivity($data['schools'][0]['SELECTIVITY']);

					$data['schools'][0]['TUITIONLABEL'] 		=	$this->search_model->tuitionRanges($data['schools'][0]['TUITION1']);
					$data['schools'][0]['TUITION'] 				=	number_format($data['schools'][0]['TUITION1']);

					$data['schools'][0]['TOTALSTUDENTSLABEL'] 	=	$this->search_model->instRanges($data['schools'][0]['TOTALSTUDENTS']);
					//$data['schools'][0]['TOTALSTUDENTS'] 		=	number_format($data['schools'][0]['TOTALSTUDENTS']);
					
					foreach($checkArray as $checkItem)
					{
						if(isset($data['schools'][0][$checkItem]))
						{
							$data['schools'][0][$checkItem] = number_format($data['schools'][0][$checkItem]);
						}
					}

					//echo 'Before IF';
					if($data['schools'][0]['photo'] && $data['schools'][0]['photo'] != '' && $data['schools'][0]['photo'] != NULL)
					{
							//echo 'IN IF';
							//dump($schoolPics);

						if(count($schoolPics) > 0)
						{
							foreach($schoolPics as $key => $schoolPic)
							{
								if($data['schools'][0]['UNITID'] == $schoolPic['schoolId'])
								{
									$data['schools'][0]['photo']	=	$data['schools'][0]['photo'].'[|]'.$schoolPic['fileName'];
									
								}
							}
						}

						$data['schools'][0]['photo'] = explode($this->config->item('schoolPhotoGlue'), $data['schools'][0]['photo']);
					}
					else
					{
						//echo 'IN ELSE';
						
						if(count($schoolPics) > 0)
						{
							$tempPics	=	array();
							foreach($schoolPics as $key => $schoolPic)
							{

								if($data['schools'][0]['UNITID'] == $schoolPic['schoolId'])
								{
									
									$tempPics[]	=	$schoolPic['fileName'];
									
								}
							}	

							//dump($tempPics);

							$data['schools'][0]['photo'] 	= 	array_filter(array_unique($tempPics));

						}
						else
						{
							$data['schools'][0]['photo'] = array();
						}
						
					}
					
				}

					//dump($data['schools'][0]['photo']);
				//get liked photos
				$data['likedPhotos'] = array();
				if($this->tank_auth->is_logged_in())
				{
					$query = $this->db->select('imgUrl')->from('likedPhotos')->where('user_id', $this->session->userdata('user_id'))->where('UNITID', $sid)->where('is_archived is NULL')->get()->result();
					
					foreach($query as $item)
					{
						$data['likedPhotos'][] = $item->imgUrl;
					}
				}

				
				$extraData = getSchoolExtraData(array($sid));

				if(isset($extraData['viewsCount']) && isset($extraData['viewsCount'][$sid]))
				{
					$data['schools'][0]['VIEWSCOUNT'] =  $extraData['viewsCount'][$sid];
				}
				else
				{
					$data['schools'][0]['VIEWSCOUNT'] =  NULL;
				}
				

				if(isset($extraData['savedSchools']) && isset($extraData['savedSchools'][$sid]))
				{
					$data['schools'][0]['SAVEDID'] =  $extraData['savedSchools'][$sid];
				}
				else
				{
					$data['schools'][0]['SAVEDID'] =  NULL;
				}

				if(isset($extraData['votedSchools']) && isset($extraData['votedSchools'][$sid]))
				{
					$data['schools'][0]['VOTEDID'] =  $extraData['votedSchools'][$sid];
				}	
				else
				{	
					$data['schools'][0]['VOTEDID'] =  NULL;
				}

				if(isset($data['schools']))
				{
					if(isset($data['schools'][0]))
					{
						if(isset($data['schools'][0]['photo']))
						{
							$data['schools'][0]['photo'] 	=	array_unique($data['schools'][0]['photo']);
						}
					}
				}
				
				return $data;		
		
			
			}


		}
		else
		{
			return array();
		}									

	}

	// Update the schools view count
	function updateSchoolCount($id)
	{
		$count  = $this->db->where('ISDELETED',NULL)
									->where('UNITID',$id)
										->count_all_results('schools');
	


		if(count($count) == 1)	
		{	
			// check if the data is present 
			$data   =   $this->db->select('count')
											->where('isArchived IS NULL')
												->where('isDeleted IS NULL')
													->where('schoolId',$id)
	        											->from('schoolsViewCount')
	                                        				->limit(1)
	                                        					->get()
	                                        						->result_array();
	        if(count($data) == 0)
	        {
	        	$insertArray = array(
	        							'count' 	=> 1,
	        							'schoolId' 	=> $id
	        						);

	        	$this->db->insert('schoolsViewCount',$insertArray);
	        }
	        else
	        {
	        	$insertArray = array(
	        							'count' 	=> $data[0]['count'] +1, 
	        							
	        						);

	        	$this->db->where('schoolId',$id)
	        				->update('schoolsViewCount',$insertArray);

	        }


		}


	}

	function getAverageReviewRating($id)
	{

		$data   =   $this->db->select('AVG(rating) as average')
									->where('isArchived IS NULL')
										->where('isDeleted IS NULL')
											->where('schoolId',$id)
    											->from('reviews')
            	                        			->get()
                                    					->result_array();

		
		                                					
		if(count($data) > 0)
		{
			return $data[0]['average'];
		}  
		else{
			return false;
		}                             					

	}

	function getPopularSchools()
	{
		$popularSchools  = $this->db->select('UNITID')
										->from('popularSchools')
											->where('isArchived',NULL)
												->get()
													->result_array();
		


		if(count($popularSchools) > 0)	
		{	

			$schoolIds  = array();
			foreach ($popularSchools as $key => $value) {
				$schoolIds[]  =  $value['UNITID'];
			}

			$this->db->where_in("UNITID",$schoolIds);
		}
		else
		{
			//$this->db->order_by('VIEWCOUNT', 'DESC');
		}

		$schools 	=	$this->db->select('UNITID, INSTNM, CITY, STABBR')
									->from('schools')
										->where('ISDELETED IS NULL')
											->limit(3)
												->get()
													->result_array();


		//dump($schools);
		$schoolIds = array_unique(__::pluck($schools,'UNITID'));

		$schoolsImages	=	$this->db->select('ID,photo,isGoogleLocationPicsProcessed,UNITID')
										->where_in('UNITID',$schoolIds)
											->limit(count($schoolIds))
												->get('schoolsImages')
													->result_array();

		if(count($schoolsImages) > 0)
		{
			foreach ($schoolsImages as $key => $value) 
			{
				$tempSchoolImages[$value['UNITID']]	=	$value['photo'];
			}
		}
		else
		{
			$tempSchoolImages	=	array();
		}

		$schoolsImages	=	$this->db->select('id, schoolId, fileName')
										->where_in('schoolId',$schoolIds)
											->where('isApproved', 1)
												->where('isArchived IS NULL')
													->get('schoolsUserImages')
														->result_array();

		if(count($schoolsImages) > 0)
		{

			$userImages 	=	array();

			foreach ($schoolsImages as $key => $value) 
			{
				$userImages[$value['schoolId']][]	=	'schoolsImages/'.$value['fileName'];
			}

			foreach ($userImages as $key => $value) 
			{
				$imgs 	=	'';

				$imgs 	=	implode('[|]', $value);

				if($tempSchoolImages[$key] != NULL)
				{
					$tempSchoolImages[$key] 	=	$tempSchoolImages[$key] .'[|]'. $imgs;
				}
				else
				{
					$tempSchoolImages[$key] 	=	$imgs;
				}
			}

		}

		if(count($schools) > 0)
		{

			foreach($schools as $key=>$value)
			{
				if(isset($tempSchoolImages[$value['UNITID']]))
				{
					$value['photo'] 			=	explode('[|]', $tempSchoolImages[$value['UNITID']]);
					$schools[$key]['photo'] 	=	$value['photo'][0];
				}
				else
				{
					$schools[$key]['photo'] 	=	null;
				}
			}

		}


		return $schools;

	}

	function updatePopularSchools($data)
	{
		$this->db->set("isArchived",1)
					->update("popularSchools");

		$schoolIds  	= array();

		$schoolIds[] 	= $data['school1'] ;
		$schoolIds[] 	= $data['school2'] ;
		$schoolIds[]    = $data['school3'] ;


		$schoolsInfo  = $this->db->select('UNITID, INSTNM, CITY, STABBR')
									->from('schools')
										->where('ISDELETED IS NULL')
											->where_in('UNITID',$schoolIds)
												//->limit(3)
													->get()
														->result_array();


		$insertArray   = array();	

		if(count($schoolsInfo) > 0)
		{

			$schoolsImages	=	$this->db->select('ID,photo,isGoogleLocationPicsProcessed,UNITID')
											->where_in('UNITID',$schoolIds)
												->limit(count($schoolIds))
													->get('schoolsImages')
														->result_array();

			if(count($schoolsImages) > 0)
			{
				foreach ($schoolsImages as $key => $value) 
				{
					$tempSchoolImages[$value['UNITID']]	=	$value['photo'];
				}
			}
			else
			{
				$tempSchoolImages	=	array();
			}

			$schoolsImages	=	$this->db->select('id, schoolId, fileName')
											->where_in('schoolId',$schoolIds)
												->where('isApproved', 1)
													->where('isArchived IS NULL')
														->get('schoolsUserImages')
															->result_array();

			if(count($schoolsImages) > 0)
			{

				$userImages 	=	array();

				foreach ($schoolsImages as $key => $value) 
				{
					$userImages[$value['schoolId']][]	=	'schoolsImages/'.$value['fileName'];
				}

				foreach ($userImages as $key => $value) 
				{
					$imgs 	=	'';

					$imgs 	=	implode('[|]', $value);

					if($tempSchoolImages[$key] != NULL)
					{
						$tempSchoolImages[$key] 	=	$tempSchoolImages[$key] .'[|]'. $imgs;
					}
					else
					{
						$tempSchoolImages[$key] 	=	$imgs;
					}
				}

			}

			foreach($schoolsInfo as $key=>$value)
			{

				$photo = "";
				
				$temp = array();

				if($tempSchoolImages[$value['UNITID']] != NULL)
				{
					$value['photo'] 			=	explode('[|]', $tempSchoolImages[$value['UNITID']]);
					$photo 						=	$value['photo'][0];
				}

				$temp['UNITID']  = $value['UNITID'];
				$temp['image']   = $photo;

				$insertArray[]  = $temp;



			}

			$this->db->insert_batch('popularSchools',$insertArray);


		}
	}




	function compareSchools($sids=""){

		if(isset($sids))
		{
			
			$data['schools'] 	=	$this->db->select('schools.UNITID as UNITID,schools.INSTNM as INSTNM,schools.ADDR as ADDR,schools.CITY as CITY,schools.STABBR as STABBR,schools.ZIP as ZIP,schools.GENTELE as GENTELE,schools.WEBADDR as WEBADDR,schools.LOCALE as LOCALE,schools.LONGITUD as LONGITUD,schools.LATITUDE as LATITUDE,schools.ACCEPTANCE as ACCEPTANCE,schools.TOTALSTUDENTS as TOTALSTUDENTS,schools.CHFNM AS CHFNM,schools.CHFTITLE AS CHFTITLE,schools.GENTELE AS GENTELE,schools.FAXTELE AS FAXTELE,schools.EIN AS EIN,schools.ADMINURL AS ADMINURL,schools.FAIDURL AS FAIDURL,schools.APPLURL AS APPLURL,schools.VOTECOUNT AS VOTECOUNT,
				schools.NPRICURL AS NPRICURL,schoolStudentChargesYear.TUITION1 as TUITION1,schools.OPENPUBL AS OPENPUBL,schoolStudentChargesYear.CHG5AY3 as CHG5AY3,schoolStudentChargesYear.CHG6AY3 as CHG6AY3,schoolStudentChargesYear.CHG4AY3 as CHG4AY3,schoolCharacteristics.APPLFEEU as APPLFEEU')
							->where_in('schools.UNITID',$sids)
								->join('schoolStudentChargesYear','schoolStudentChargesYear.UNITID = schools.UNITID','left outer')
									->join('schoolCharacteristics','schoolCharacteristics.UNITID = schools.UNITID','left outer')
										->from('schools')
											->limit(count($sids))
												->get()
													->result_array();

			
			if(count($data['schools']) > 0)
			{
				//dump($data['schools'][0]);
				if(isset($data['schools'][0]['TUITION1']) || isset($data['schools'][0]['CHG4AY3']) || isset($data['schools'][0]['CHG5AY3']) || isset($data['schools'][0]['CHG5AY3']))
				{
					$data['schools'][0]['TOTALONCAMPUS'] = number_format($data['schools'][0]['TUITION1']+$data['schools'][0]['CHG4AY3']+$data['schools'][0]['CHG5AY3']);
				}

				$checkArray = array('TOTALSTUDENTS', 'TUITION1', 'CHG5AY3', 'CHG6AY3', 'CHG4AY3');
				foreach($checkArray as $checkItem)
				{
					if(isset($data['schools'][0][$checkItem]))
					{
						$data['schools'][0][$checkItem] = number_format($data['schools'][0][$checkItem]);
					}
				}

				
			}
			
			return $data;		

		}
		else
		{
			return array();
		}									

	}

	function likePhoto($unitid, $imgUrl)
	{
		if($this->tank_auth->is_logged_in())
		{
			$existingPhotos = $this->db->select('photo')->from('schools')->where('UNITID', $unitid)->get()->result();

			if(count($existingPhotos) == 0)
			{
				return FALSE;
			}
			else
			{
				$existingPhotos = explode($this->config->item('schoolPhotoGlue'), $existingPhotos[0]->photo);
				if(in_array($imgUrl, $existingPhotos))
				{
					$this->unlikePhoto($unitid, $imgUrl);
					$this->db->insert(
						'likedPhotos',
						array(
							"user_id" 	=> $this->session->userdata('user_id'),
							"UNITID" 	=> $unitid,
							"imgUrl" 	=> $imgUrl
						)
					);

					if(count($existingPhotos) > 1)
					{
						$this->db->set('photo', implode($this->config->item('schoolPhotoGlue'), array_unique(array_merge(array($imgUrl), $existingPhotos))))
									->where('UNITID', $unitid)
										->update('schools');
					}
					
					return TRUE;
				}
				else
				{
					return FALSE;
				}
				
			}
			
		}
		else
		{
			return FALSE;
		}
	}

	function unlikePhoto($unitid, $imgUrl)
	{
		if($this->tank_auth->is_logged_in())
		{
			$this->db->set('is_archived', 1)
						->where('user_id',$this->session->userdata('user_id'))
							->where('UNITID', $unitid)
								->where('imgUrl', $imgUrl)
									->where('is_archived is NULL')
										->update('likedPhotos');

			if($this->db->affected_rows() > 0)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
		
	}


	// user uploads image for the School
	public function addImage()
	{	

		$returnArray = array("status" => "success");

		$insertArray  = array();
		

		if(count($_FILES) > 0)
		{
			$errorCount = 0;
			$temp = 0;
			foreach ($_FILES as $key => $value) 
			{

				if($value['name'] != '' || $value['name'] != null)
				{
					$result = $this->uploadFile($key);
                                        
                                      
					if($result['status'] == "success")
					{
						$insertArray[] = array(
							'schoolId' => $this->input->post('schoolId'),
							'userId'   => $this->session->userdata('user_id'),
							'fileName' => $result['data']['file_name'],
						   );
					}
					else
					{
						
						if(strpos($result['error'],"permitted")){
							$temp = 1;
						}
						++$errorCount;
					}
				}
			} 

    		if($errorCount > 0)
			{	
				$returnArray['status'] = "failure";
				if($temp == 1){
					$returnArray['errors'] = "Upload failed for ".$errorCount."file(S) because your image size is greater than the permitted size";
				}else{
					$returnArray['errors'] = "Upload failed for ".$errorCount."file(S)";	
				}
			}

			if(count($insertArray) > 0)
			{
				$this->db->insert_batch('schoolsUserImages', $insertArray);
			}
		}
	

		return $returnArray;
	}


	function uploadFile($fieldName="", $uploadPath = './schoolsImages', $thumbWidth = 500, $thumbHeight = 350)
	{
		$fileTemp   = explode(".",$_FILES[$fieldName]['name']);

		$config['file_name']  		= 	$fileTemp[0]."_".strtotime("now").".".$fileTemp[1];
		$config['upload_path'] 		= 	$uploadPath;
		$config['allowed_types'] 	= 	'gif|jpg|png|jpeg';
		$config['max_size']			= 	1024;

		$this->load->library('upload');

		$this->upload->initialize($config);

		if(! $this->upload->do_upload($fieldName))
		{
			$error = array('error' => $this->upload->display_errors());
			return  array('status' => "failure",'error' => $error['error']);
		}
		else
		{

			$fileData 	=	$this->upload->data();

			$config 			=	array();
			$config['image_library'] 	= 	'gd2';
			$config['source_image']		= 	$fileData['full_path'];
			$config['create_thumb']		= 	TRUE;
			$config['maintain_ratio'] 	= 	TRUE;
			$config['width']		= 	$thumbWidth;
			$config['height']		= 	$thumbHeight;

			$this->load->library('image_lib');

			$this->image_lib->initialize($config);

			if (!$this->image_lib->resize())
			{
				$error = array('error' => $this->image_lib->display_errors());
				$CI->image_lib->clear();
				return  array('status' => "failure",'error' => $error);
			}
			else
			{
				return  array('status' => "success", 'data' => $fileData);
			}

		}

	}

	//Read users reviews[Functionis called only when the user is logged in]
	function readProfileReviews($params= array())
	{

		if(isset($params['limit']) && isset($params['offset']) )
		{
			$this->db->limit($params['limit'],$params['offset']);
		}


		$usersReviews =  $this->db->from('reviews')
										->where('isArchived IS NULL')
										->where('isDeleted IS NULL')
											->where('schoolId !=',0)
												->where('userId',$this->session->userdata('user_id'))
													->order_by('createdDate', 'DESC')
														->get()
															->result_array();

		$totalCount   =   $this->db->where('isArchived IS NULL')
										->where('isDeleted IS NULL')
											->where('schoolId !=',0)
												->where('userId',$this->session->userdata('user_id'))
													->order_by('createdDate', 'DESC')
			        									->from('reviews')
			                                        		->count_all_results();
		
		//dump($this->db->last_query());

		$schoolIds = array();														
		foreach ($usersReviews as $key => $value) 
        {	
        	$schoolIds[] = $value['schoolId'];
        }



        $schoolsName = array();
        if(count($schoolIds) > 0)
        {
        	$schoolsData 	=	$this->db->select('UNITID,INSTNM')
        									->where_in("UNITID",$schoolIds)
        										->where('ISDELETED',NULL)
        											->get('schools')
        												->result_array();
			
			if(count($schoolsData) > 0)
			{	

				foreach ($schoolsData as $key => $value) {
					$schoolsName[$value['UNITID']] = $value['INSTNM'];
				}

			}        												

        }

        // Format for the schools
        foreach ($usersReviews as $key => $value) 
        {	
            if($value['flagCount'] ==  NULL)
            {
                $usersReviews[$key]['flagCount']  =   0;
            }
            if($value['helpfulFlagCount'] ==  NULL)
            {
                $usersReviews[$key]['helpfulFlagCount']  =   0;
            }

            if(isset($schoolsName[$value['schoolId']]))
            {
            	//dump($value['schoolId']);
            	 $usersReviews[$key]['schoolName'] = $schoolsName[$value['schoolId']];
            }
            else{
            	 $usersReviews[$key]['schoolName'] ="";
            }
           
        }

        

		$returnData = array();														
		if(count($usersReviews) > 0)
		{

			$returnData['status'] 			= 	"success";
			$returnData['count']  			= 	count($usersReviews);
			$returnData['total_count'] 		= 	$totalCount;
			$returnData['current_page_no']	=  	$params['page_no'];
			$returnData['data']   			= 	$usersReviews;

		}	
		else
		{
			$returnData['status'] 			= 	"success";
			$returnData['count'] 			= 	0;
			$returnData['total_count'] 		= 	$totalCount;
			$returnData['current_page_no']	=  	$params['page_no'];
			$returnData['data']   			= 	array();

		}		

		return $returnData;					


	}

	

	function readSchools($showSavedSchools = 0){

		$limit 	= 10;
		$offset = 0;

		if($this->uri->segment(3))
		{
			$offset = $this->uri->segment(3);
		}

		if($this->input->get('sortby')){
			$sortby = strtoupper($this->input->get('sortby'));
		}
		else{
			$sortby = 'desc';
		}

		if($this->input->get('orderby')){
			$orderby = strtoupper($this->input->get('orderby'));
		}
		else{
			$orderby = 'ACCEPTANCE';
		}

		$savedSchool = array();

		if($showSavedSchools == 1){

			$result = $this->db->select('schoolId')
									->from('schoolSavedList')
										->where('isDeleted IS NULL')
											->where('userId',$this->session->userdata('user_id'))
												->order_by('createdDate', 'DESC')
													->get()
														->result_array();

			if(count($result) > 0)
			{

				foreach ($result as $key => $value) 
				{
					
					$savedSchool[] = $value['schoolId'];

				}

			}	

			if(count($savedSchool) == 0){

				return 	array(
					'totalCount' => 0,
					'count' => 0,
					'data' 	=> array()
				);

			}

			$offset 	=	0;

		}

		$this->load->model("search_model");
		$data = $this->search_model->advanced(
					$this->input->get('instsize'),
					$this->input->get('locale'),
					$this->input->get('state'),
					$this->input->get('tuition'),
					$this->input->get('selectivity'),
					$this->input->get('city'),
					$this->input->get('major'),
					$limit,
					$offset,
					$sortby,
					$orderby,
					$savedSchool
				);

	

		$this->load->library('pagination');
		
		$config['base_url'] 	= site_url().'home/schools';
		$config['total_rows'] 	= $data['count'];
		$config['per_page'] 	= 20; 

		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = "<li class='active'><a href='#'>";
		$config['cur_tag_close'] = '</a></li>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$this->pagination->initialize($config); 											
		
		$data['sortby'] = strtolower($sortby);
		$data['orderby'] = strtolower($orderby);

		$data['pagination'] = $this->pagination->create_links();
		
		return $data;								

	}

	function readAdminSchools($params)
	{

		

		if(isset($params['filterBy']) &&  isset($params['filter']))
        {
            if($params['filterBy']	==	'name')
            {
            	$this->db->start_cache();
                $this->db->where("schools.INSTNM LIKE '%". $this->db->escape_str($params['filter']) ."%'");
                $this->db->stop_cache();
            }
        }
        
        

		$totalSchools  	=  	$this->db->count_all_results('schools');

		$schoolsData 	=	$this->db->select('schools.UNITID as UNITID,schools.INSTNM as INSTNM,schools.ADDR as ADDR,schools.CITY as CITY,schools.STABBR as STABBR,schools.ZIP as ZIP,schools.GENTELE as GENTELE,schools.WEBADDR as WEBADDR,schools.LOCALE as LOCALE,schools.LONGITUD as LONGITUD,schools.LATITUDE as LATITUDE,schools.ACCEPTANCE as SELECTIVITY,schools.ACCEPTANCE as ACCEPTANCE, schools.TOTALSTUDENTS AS TOTALSTUDENTS,schoolStudentChargesYear.TUITION1 as TUITION,schools.OPENPUBL AS OPENPUBL,schools.LASTUPDATED as LASTUPDATED')
							->join('schoolStudentChargesYear','schoolStudentChargesYear.UNITID = schools.UNITID','left outer')
								->from('schools')
									->limit($params['limit'], $params['offset'])
										->order_by('INSTNM', 'asc')
											->get()
												->result_array();

		$this->db->flush_cache();

		$returnArray 	= 	$params;

		$returnArray['site_name'] 			= 	'Users';	
		$returnArray['count']				=	count($schoolsData);
		$returnArray['status']				=	'success';
		$returnArray['total_count']			=	$totalSchools;
		$returnArray['current_page_no']		=	$params['page_no'];
		$returnArray['data'] 				=	$schoolsData;

		return $returnArray;

	}

	function getSurvey($id)
	{

		return array();

	}

	function getHeadCountChartsNew($id){

		$row =  $this->db->select('EFYTOTLM,EFYTOTLW,EFYAIANT,EFYASIAT,EFYBKAAT,EFYHISPT,EFYWHITT,EFY2MORT,EFYUNKNT,EFYNRALT,EFYNHPIT')
							->from('schoolHeadCount')
								->where('UNITID',$id)
									->limit(1)
										->get()
											->row();

		if(count($row) > 0)
		{



			$peopleChart 						= 	array();
			$ethinicityChart 					= 	array();

			$recommendationChart[]  			= 	array('name'=>'Yes','y'=> 64.4);
			$recommendationChart[]  			= 	array('name'=>'No','y' => 35.6);

			$data 								=	$this->dummyData();

			$chart['aspectsChart']				= 	json_encode($data['aspects']);
			$chart['likesChart']				= 	json_encode($data['likes']);
			$chart['jobProspectsChart']			= 	json_encode($data['jobProspects']);
			$chart['fundOpportunitiesChart']	= 	json_encode($data['fundOpportunities']);
			
			$chart['recommendation'] 			= 	$this->generatePieChart($recommendationChart, 'Recommendation');

			$chart['peopleChart']				= 	json_encode(array());
			$chart['ethinicityChart']			= 	json_encode(array());

			return $chart;

		}
		else
		{
			return array();
		}	



	}

	function getHeadCountCharts($id){

		$schoolsInfo =  $this->db->select('EFFYLEV,LSTUDY,EFYTOTLT,EFYTOTLM,EFYTOTLW,EFYAIANT,EFYASIAT,EFYBKAAT,EFYHISPT,EFYWHITT,EFY2MORT,EFYUNKNT,EFYNRALT,EFYNHPIT,EFYNRALM,EFYNRALW')
							->from('schoolHeadCount')
								->where('UNITID',$id)
									->get()
										->result_array();

		if(count($schoolsInfo) > 0)
		{

			$peopleChart 				= 	array();
			
			$peopleChart['total'] 						=	0;
			$peopleChart['totalMenCount'] 				=	0;
			$peopleChart['totalWomenCount'] 			=	0;
			$peopleChart['totalGraduationStud'] 		=	0;
			$peopleChart['totalGraduationMale'] 		=	0;
			$peopleChart['totalGraduationFemale'] 		=	0;
			$peopleChart['totalNonResident'] 			=	0;
			$peopleChart['totalNonResidentMale'] 		=	0;
			$peopleChart['totalNonResidentFemale'] 		=	0;
			$peopleChart['totalGradNonResident'] 		=	0;
			$peopleChart['totalGradNonResidentMale'] 	=	0;
			$peopleChart['totalGradNonResidentFemale'] 	=	0;

			foreach($schoolsInfo as $key=>$value)
			{
				if($value['EFFYLEV'] == 1)
				{
					$peopleChart['total'] 						=	$value['EFYTOTLT'];
					$peopleChart['totalNonResident'] 			=	$value['EFYNRALT'];
					$peopleChart['totalMenCount'] 				=	$value['EFYTOTLM'];
					$peopleChart['totalWomenCount'] 			=	$value['EFYTOTLW'];
					$peopleChart['totalNonResidentMale'] 		=	$value['EFYTOTLM'];
					$peopleChart['totalNonResidentFemale'] 		=	$value['EFYTOTLW'];
				}

				if($value['EFFYLEV'] == 4)
				{
					$peopleChart['totalGraduationStud'] 		=	$value['EFYTOTLT'];
					$peopleChart['totalGraduationMale'] 		=	$value['EFYTOTLM'];
					$peopleChart['totalGraduationFemale'] 		=	$value['EFYTOTLW'];
					$peopleChart['totalGradNonResident'] 		=	$value['EFYNRALT'];
					$peopleChart['totalGradNonResidentMale'] 	=	$value['EFYNRALM'];
					$peopleChart['totalGradNonResidentFemale'] 	=	$value['EFYNRALW'];
				}
			}

			//$data 								=	$this->getSurveyData($id);
			
			//$chart['aspectsChart']				= 	json_encode($data['aspects']);
			//$chart['likesChart']				= 	json_encode($data['likes']);
			//$chart['jobProspectsChart']			= 	json_encode($data['jobProspects']);
			//$chart['fundOpportunitiesChart']	= 	json_encode($data['fundOpportunities']);
			
			//$chart['recommendation'] 			= 	json_encode($data['recomandations']);
			//$chart['ratings']					= 	$data['ratings'];
			$chart['peopleChart']				= 	json_encode($peopleChart);
			//$chart['ethinicityChart']			= 	json_encode(array());
			//$chart['reviews']					= 	$data['reviews'];
			
			return $chart;

		}
		else
		{
			return array();
		}	

	}

	function getSurveyData($id)
	{

		$this->load->config('survey');

		$data 		=	array(
								'aspects' 				=>	array(),
								'likes' 				=>	array(),
								'jobProspects' 			=>	array(),
								'fundOpportunities' 	=>	array(),
								'ratings' 				=>	0,
								'recomandations' 		=>	array(),
								'reviews'				=>	array()
							);

		$answers 	=	$this->db->select('userId, schoolId, questionId, answer,createdDate')
									->from('surveyAnswers')
										->where_in('questionId', array(3,4,5,6,7,8,10))
											->where('schoolId', $id)
												->order_by('createdDate', 'DESC')
													->get()
														->result_array();

		if(count($answers) == 0)
		{
			return $data;
		}
		else
		{

			$globalOptions 		=	$this->config->item('surveyOptions');

			$recomandations 	=	array(
											1	=>	0,
											2	=>	0,
										);

			$ratings 			=	array();

			$aspects 			=	array(
											1 	=>	0,
											2 	=>	0,
											3 	=>	0,
											4 	=>	0,
											5 	=>	0,
											6 	=>	0,
											7 	=>	0
										);

			$opportunities 		=	array(
											1 	=>	0,
											2 	=>	0,
											3 	=>	0,
											4 	=>	0,
											5 	=>	0,
											6 	=>	0,
											7 	=>	0
										);

			$likes 				=	array(
											1 	=>	0,
											2 	=>	0,
											3 	=>	0,
											4 	=>	0,
											5 	=>	0,
											6 	=>	0,
											7 	=>	0,
											8 	=>	0
										);

			$jobProspects 		=	array(
											1 	=>	0,
											2 	=>	0,
											3 	=>	0
										);

			$userIds 			=	array();
			$userInfo 			=	array();

			foreach($answers as $value)
			{
				if($value['questionId'] == 10)
				{
					$userIds[]	=	$value['userId'];
				}
			}

			if(count($userIds) > 0)
			{
				$userIds 	=	array_unique($userIds);

				$userData 	=	$this->db->select('id,firstName, lastName, email')
											->from('users')
												->where_in('id', $userIds)
													->limit(count($userIds))
														->get()
															->result_array();

				if(count($userData) > 0)
				{
					foreach($userData as $value)
					{
						if($value['firstName'] && $value['lastName'])
						{
							$value['displayName']	=	$value['firstName'] .' '. $value['lastName'];
						}
						else
						{
							$value['displayName']	=	$value['email'];
						}

						$userInfo[$value['id']]	=	$value;
					}
				}

			}

			foreach($answers as $value)
			{
				if($value['questionId'] == 3)
				{
					if(isset($recomandations[$value['answer']]))
					{
						$recomandations[$value['answer']]++;
					}
				}
				else if($value['questionId'] == 4)
				{
					$ratings[] 	=	$value['answer'];
				}
				else if($value['questionId'] == 5)
				{
					if(isset($aspects[$value['answer']]))
					{
						$aspects[$value['answer']]++;
					}
				}
				else if($value['questionId'] == 6)
				{
					if(isset($likes[$value['answer']]))
					{
						$likes[$value['answer']]++;
					}
				}
				else if($value['questionId'] == 7)
				{
					if(isset($opportunities[$value['answer']]))
					{
						$opportunities[$value['answer']]++;
					}
				}
				else if($value['questionId'] == 8)
				{
					if(isset($jobProspects[$value['answer']]))
					{
						$jobProspects[$value['answer']]++;
					}
				}
				else if($value['questionId'] == 10)
				{
					if(isset($userInfo[$value['userId']]))
					{
						$data['reviews'][$value['userId']]		=	array(
																			'userInfo'	=>	$userInfo[$value['userId']],
																			'review'	=>	$value['answer']
																		);
					}
				}
			}

			$data['recomandations'][]		=	array('name'=>'Yes','y'=> $recomandations[1]);
			$data['recomandations'][]		=	array('name'=>'No','y'=> $recomandations[2]);

			if(count($ratings) > 0)
			{
				$data['ratings'] 			=	array_sum($ratings)/count($ratings);
			}
			else
			{
				$data['ratings'] 			=	0;
			}

			foreach($globalOptions[5] as $key=>$value)
			{
				$data['aspects'][]		=	array($value, $aspects[$key]);
			}

			foreach($globalOptions[6] as $key=>$value)
			{
				$data['likes'][]		=	array($value, $likes[$key]);
			}

			foreach($globalOptions[8] as $key=>$value)
			{
				$data['jobProspects'][]	=	array($value, $jobProspects[$key]);
			}

			foreach($globalOptions[7] as $key=>$value)
			{
				$data['fundOpportunities'][]	=	array($value, $opportunities[$key]);
			}

			return $data;
		}

	}

	function dummyData()
	{
		$data 				=	array();

		$data['aspects']	=	array(
		                         		array('Location/campus life', 5),
		                         		array('Good infrastructure & facilities', 8),
		                         		array('High calibre of professors', 3),
		                         		array('Strong quality of students', 18),
		                         		array('Great learning environment', 19),
		                         		array('Excellent courses', 6),
		                         		array('Strong alumni network', 2)
									);

		$data['likes']	=	array(
	                         		array(
	                         		      'Knowledge seeker',
	                         		      21
	                         		),
	                         		array(
	                         		      'Research geeks',
	                         		      19
	                         		),
	                         		array(
	                         		      'Art & culture fiends',
	                         		      18
	                         		)
		                        );

		$data['jobProspects']	=	array(
			                         		array(
			                         		      'Will get a job but not my top preference',
			                         		      5
			                         		),
			                         		array(
			                         		      'Concerned about job prospects at graduation',
			                         		      8
			                         		),
			                         		array(
			                         		      'Will get the job I want',
			                         		      3
			                         		)
			                        );

		$data['fundOpportunities']	=	array(
				                         		array(
				                         		      'Self-funded',
				                         		      26
				                         		),
				                         		array(
				                         		      'Employer',
				                         		      21
				                         		),
				                         		array(
				                         		      'On campus job',
				                         		      14
				                         		),
				                         		array(
				                         		      'School assistantships/scholarships',
				                         		      17
				                         		),
				                         		array(
				                         		      'External loans',
				                         		      3
				                         		)
				                        );

		return $data;
	}


	function generatePieChart($data,$title){

		$chart = array();

		$chart['chart']['plotBackgroundColor'] 	= null;
		$chart['chart']['plotBorderWidth'] 		= 0;
		$chart['chart']['plotShadow'] 			= false;
		$chart['credits']['enabled']   			= false;
		$chart['title']['text'] 				= "";
		$chart['tooltip']['pointFormat'] 		= '<b>{point.percentage:.1f}%</b>';
		$chart['plotOptions']['pie']['allowPointSelect'] 		= true;
		$chart['plotOptions']['pie']['cursor'] 					= 'pointer';
		$chart['plotOptions']['pie']['dataLabels']['enabled'] 	= false;
		
		/*
		$chart['plotOptions']['pie']['dataLabels']['format'] 	= '<b>{point.name}</b>: {point.percentage:.1f} %';	
		$chart['plotOptions']['pie']['dataLabels']['style']['color'] = '#404041';
		$chart['plotOptions']['pie']['dataLabels']['style']['fontFamily'] = 'Open Sans,Helvetica,Arial,sans-serif';
		$chart['plotOptions']['pie']['dataLabels']['style']['fontSize'] = '14px';		
		*/

		$chart['plotOptions']['pie']['showInLegend'] = true;

		$series = array();

		$series1['type'] = 'pie';
		$series1['name'] = $title;
		$series1['data'] = $data;

		$series[] = $series1;

		$chart['series'] = $series;

		return json_encode($chart);

	}


	function generateColumnChart(){

		$data =  "";



	    return $data;



	}


	function getRequiredData()
	{

		$query_data 	=	$this->db->from('states')
										->get()
											->result_array();

		$returnArray 				=	array();
		$returnArray['states'] 		=	array();
		$returnArray['programs'] 	=	array();
		$returnArray['survey'] 		=	array();

		if(count($query_data) > 0)
		{
			$returnArray['states'] 	=	array();

			foreach($query_data as $key=>$value)
			{

				$returnArray['states'][] 	=	array(
													'code'	=>	$value['state_code'],
													'name'	=>	$value['state']
												);

			}
		}
		
		/*$query_data = $this->db->select('id , name , createDate')
									->from('schoolPrograms')
										->where('isDeleted IS NULL')
											->get()
												->result_array();

		$returnArray['programs'] 	=	$query_data;*/

		$query_data = $this->db->select('id , name , createDate')
									->from('survey')
										->where('isDeleted IS NULL')
											->get()
												->result_array();

		$returnArray['survey'] 	=	$query_data;

		return $returnArray;
	}

	function getCities($params)
	{

		$cities 	=	$this->db->from('cities_extended')
									->where('state_code', $params['stateCode'])
										->get()
											->result_array();

		$returnArray 	=	array();

		if(count($cities) > 0)
		{
			$returnArray['status'] 	=	'success';
			$returnArray['data'] 	=	$cities;
		}
		else
		{
			$returnArray['status'] 		=	'failure';
			$returnArray['message'] 	=	'Sorry. No cities are there for this state.';
		}

		return $returnArray;
	}

	function addPrograms($params)
	{

		$insertArray 	=	array(
									'name'			=>	$params['name'],
									'createDate'	=>	date("Y-m-d H:i:s", time())
								);

		$this->db->insert('schoolPrograms', $insertArray);

		return array(
						'status' => 'success'
					);

	}

	function getAllPrograms($params)
	{
		$total_count 	= 	$this->db->from('schoolPrograms')
										->where('isDeleted IS NULL')
											->count_all_results();

		$query_data = $this->db->select('id , name , createDate')
											->from('schoolPrograms')
												->limit($params['limit'], $params['offset'])
													->where('isDeleted IS NULL')
														//->order_by('id', 'desc')
															->get();

		$query_data 	=	$query_data->result_array();

		$return_array 	= 	$params;

		$return_array['site_name'] 			= 	'Programs';	
		$return_array['count']				=	count($query_data);
		$return_array['status']				=	'success';
		$return_array['total_count']		=	$total_count;
		$return_array['data'] 				=	$query_data;

		return $return_array;
	}
	function saveMySchool($user_id,$school_id){
		$data['userId'] = $user_id;
		$data['schoolId'] = $school_id;
		$this->db->insert('schoolSavedList',$data);
		return;		
	}

	function saveSchool(){

		if($this->input->post('userId') && $this->input->post('schoolId')){

			$data['userId'] = $this->input->post('userId');
			$data['schoolId'] = $this->input->post('schoolId');

			if($this->input->post('isDelete')){

				$this->db->set('isDeleted',1)
							->where('userId',$data['userId'])
								->where('schoolId',$data['schoolId'])
									->update('schoolSavedList');

			}
			else
			{
				$this->db->insert('schoolSavedList',$data);	
			}

			return array('status'=>'success');

		}
		else
		{
			return array('status'=>'failure');
		}

	}


	function voteSchool(){

		if($this->input->post('userId') && $this->input->post('schoolId')){

			$data['userId'] = $this->input->post('userId');
			$data['schoolId'] = $this->input->post('schoolId');

			$count 	=	$this->db->from('schoolVotedList')
										->where('schoolId', $data['schoolId'])
											->where('userId', $data['userId'])
												->where('isDeleted IS NULL')
													->count_all_results();

			if($count > 0)
			{
				return array('status'=>'failure', 'message' => 'You already upvoted this school.');
			}
			else
			{
				$this->db->insert('schoolVotedList',$data);

				$result = $this->db->select('VOTECOUNT')
							->from('schools')
								->where('UNITID',$data['schoolId'])
									->where('VOTECOUNT IS NOT NULL')
										->limit(1)
											->get()
												->row();

				$vote = 0;							
											
				if(isset($result->VOTECOUNT)){

					$vote = $result->VOTECOUNT;

				}							

				$vote = $vote + 1;

				$this->db->set('VOTECOUNT',$vote)
								->where('UNITID',$data['schoolId'])
									->update('schools');

				return array('status'=>'success');
			}

		}
		else
		{
			return array('status'=>'failure');
		}

	}

	function downVoteSchool()
	{
		/*if($this->input->post('userId') && $this->input->post('schoolId')){

			$data['userId'] = $this->input->post('userId');
			$data['schoolId'] = $this->input->post('schoolId');

			$count 	=	$this->db->from('schoolVotedList')
										->where('schoolId', $data['schoolId'])
											->where('userId', $data['userId'])
												->where('isDeleted IS NULL')
													->count_all_results();

			if($count == 0)
			{
				return array('status'=>'failure', 'message' => 'You can not downvote this school.');
			}
			else
			{
				$updateArray 	=	array(
				                      		'isDeleted'	=>	1
				                      	);

				$this->db->where('userId', $data['userId']);

				$this->db->where('schoolId', $data['schoolId']);

				$this->db->update('schoolVotedList',$updateArray);

				$result = $this->db->select('VOTECOUNT')
										->from('schools')
											->where('UNITID',$data['schoolId'])
												->where('VOTECOUNT IS NOT NULL')
													->limit(1)
														->get()
															->row();

				$vote = 0;							
											
				if(isset($result->VOTECOUNT)){

					$vote = $result->VOTECOUNT;

				}							

				$vote = $vote - 1;

				if($vote < 0)
				{
					$vote 	=	0;
				}

				$this->db->set('VOTECOUNT',$vote)
								->where('UNITID',$data['schoolId'])
									->update('schools');

				return array('status'=>'success');
			}

		}
		else
		{
			return array('status'=>'failure');
		}*/

		return array('status'=>'failure');

	}
 
	function getSchoolData($unitid = 0,$imageFlag = 0)
	{

		$returnData 	= 	array();
		$returnArray 	=	$this->db->select("UNITID,INSTNM,ADDR,CITY,LATITUDE,LONGITUD")
										->from('schools')
											->where("ISDELETED",NULL)
												->where('UNITID',$unitid)
													->limit(1)
														->get()
															->row();
		

		//$returnData['name']  = $returnArray->INSTNM;												 
		if($imageFlag == 1)
		{
			$schoolsImages	=	$this->db->select('ID,photo,isGoogleLocationPicsProcessed,UNITID')
												->where('UNITID',$unitid)
													->limit(1)
														->get('schoolsImages')
															->result_array();


			$returnData = array();

			$returnData['name']  = $returnArray->INSTNM;

			if(count($schoolsImages) == 1)
			{
				$returnData['imageArr'] = explode($this->config->item('schoolPhotoGlue'), $schoolsImages[0]['photo']);
			}
			else
			{
				$returnData['imageArr'] = NULL;	
			}

			$schoolsImages	=	$this->db->select('id, schoolId, fileName')
											->where('schoolId',$unitid)
												->where('isApproved', 1)
													->where('isArchived IS NULL')
														->get('schoolsUserImages')
															->result_array();

			if(count($schoolsImages) > 0)
			{

				$userImages 	=	array();

				foreach ($schoolsImages as $key => $value) 
				{
					$userImages[]	=	'schoolsImages/'.$value['fileName'];
				}

				//$userImages 	=	implode($this->config->item('schoolPhotoGlue'), $userImages);

				/*if($returnData['imageArr'] != NULL)
				{

					$returnData['imageArr'] 	=	implode($this->config->item('schoolPhotoGlue'), $returnData['imageArr']);

					$returnData['imageArr'] 	=	$returnData['imageArr'].$this->config->item('schoolPhotoGlue').$userImages;
				}
				else
				{
					$returnData['imageArr'] 	=	$userImages;
				}*/

				$returnData['userImageArr'] 		=	$userImages;

				$returnData['userImageArr'] 		=	array_unique(array_filter($userImages));

				//locationPics/110404-14229469631531.jpg
			}

			return $returnData;
			
		}
		else{

			return $returnArray;
		}



		

	}


	public function getReviews($param)
    {
    	$result = array();

    	$checkSchool = $this->db->select('UNITID')
    								->from('schools')
    									->where('UNITID',$param['schoolId'])
    										->limit(1)
    											->get()
    												->result_array();

    	//dump(count($checkSchool));
		if(count($checkSchool) > 0)
		{


	    	

			//dump(count($checkSchool));
			if(count($checkSchool) > 0)
			{
				$this->db->start_cache();
				if($param['reviewId'] > 0)
				{
					$this->db->where('id >',$param['reviewId']);
				}

				$order      =   'desc';

		        if(isset($param['order']))
		        {
		            $order  =   $param['order'];
		        }

		        $order_by   =   'helpfulFlagCount';

		        if(isset($param['order_by']))
		        {
		            if($param['order_by'] == '2')
		            {
		                $this->db->order_by('rating', $order);
		                $this->db->order_by('createdDate', 'desc');
		            }
		            else if($param['order_by'] == '1')
		            {
		                $this->db->order_by('helpfulFlagCount', $order);
		                $this->db->order_by('createdDate', 'desc');
		            }
		            else
		            {
		                $this->db->order_by('createdDate', $order);
		            }
		        }
		        else
		        {
		        	$param['order_by']	=	'1';
		        	$param['order']		=	'desc';

		            $this->db->order_by('helpfulFlagCount', $order);
		            $this->db->order_by('createdDate', 'desc');
		        }
				$this->db->where('isApproved IS NOT NULL')
							->where('isArchived',NULL)
								->where('isDeleted',NULL)
									->where('schoolId',$param['schoolId']);
		        $this->db->stop_cache();

				$answers = array();
				$answers = $this->db->select('id,schoolId,userId,title,gradType, good,bad,isApproved,rating,flagCount,helpfulFlagCount,createdDate,features,culture,major,userType')
										->limit($param['limit'],$param['offset'])
									 		->get('reviews')
									 			->result_array();
				//echo $this->db->last_query();
				$totalCount             =   $this->db->from('reviews')
                                                	 	->count_all_results();   


				

        		$this->db->flush_cache();
        		// echo $this->db->last_query();
				//dump($answers);exit();




				if(count($answers) > 0)
				{
	    				$userId = array();
	    				$reviewIds = array();
				    	foreach ($answers as $key => $value) 
				    	{
				    		
					    	$userId[] = $value['userId'];
					    	$reviewIds[] = $value['id'];

				    	}

				    	// for the review ids get the FlagConnect data
				        $reviewLoggedInUserFlag = array();
				        $reviewsFlagData    = array();


				        if(count($reviewIds) > 0)
				        {

				        	//dump($reviewIds);

				        	$reviewsFlagData =$this->db->select('schoolId,reviewId,userId,flag,helpfulFlag') 
					    								->from('reviewFlagConnect')
					    									->where('isArchived',NULL)
					    										->where('isDeleted',NULL)
					    											->where_in('reviewId',$reviewIds)
					    													->get()
					    														->result_array();
							
							//dump($this->db->last_query());										
							
							//dump($reviewsFlagData );					    														
							if(count($reviewsFlagData) > 0)
							{

						
								foreach ($reviewsFlagData as $key => $value) 
								{ 
									if($this->tank_auth->is_logged_in())
									{
										if($this->session->userdata('user_id') == $value['userId'])
										{
											$reviewLoggedInUserFlag[$value['reviewId']][$value['userId']]['flag'] = $value['flag'];
											$reviewLoggedInUserFlag[$value['reviewId']][$value['userId']]['helpfulFlag'] = $value['helpfulFlag'];
										}
									}
									
								}
								
							}


				        }

					   if(array_unique($userId))
					   {
					    	$users = array();
					    	$userdata =  $this->db->select('id,username,email,firstName,lastName,role,banned') 
					    								->from('users')
					    									->where_in('id',$userId)
					    										->limit(count($userId))
					    											->get()
					    											  ->result_array();
				    	}		

				    	
				    	$reviewUserTypeArr   = $this->config->item('reviewUserType');	
				    	foreach ($answers as $key => $value)
				    	{	


				    		$temp = array();

				    		$temp2= NULL;

				    		$value['loggedUserFlag'] 		= NULL;
				    		$value['loggedUserHelpfulFlag'] = NULL;
				    		$value['featureStr']  = '';

				    		
				    		// Feature String 
				    		if($value['features'] != NULL || $value['features'] != '')
				    		{
				    			$features = $this->config->item('reviewFeatures');
				    			//dump($features);
				    			$tempFeatures = array();
				    			$value['features'] = array_filter(explode("|",$value['features']));

				    			foreach ($value['features'] as $key3 => $feature)
						        {
						          	$tempFeatures[] = $features[$feature];
						        }



						        //$value['features'] = implode(", ",$tempFeatures);
						        $value['featureStr'] =  implode(", ",$tempFeatures);

				    		}
				    		else
				    		{
				    			$value['featureStr']="";
				    		}

				    		//Culture String
				    		$value['cultureStr']  = '';

				    		if($value['culture'] != NULL || $value['culture'] != '')
				    		{
				    			$cultures = $this->config->item('reviewCulture');
				    			
				    			
				    			$value['culture'] = array_filter(explode("|",$value['culture']));

				    			

				    			$tempCulture = array();
				    			foreach ($value['culture'] as $key4 => $val4)
						        {
						        	if(isset($cultures[$val4]))
						        	{
						          		$tempCulture[] = $cultures[$val4];
						          	}
						        }
						      	
						      	//dump($tempCulture);
						        $value['cultureStr'] =  implode(", ",$tempCulture);

				    		}
				    		else
				    		{
				    			$value['cultureStr']="";
				    		}




				    		if(isset($reviewLoggedInUserFlag[$value['id']]))
				    		{	


				    			if($this->tank_auth->is_logged_in())
								{	

									if(isset($reviewLoggedInUserFlag[$value['id']][$this->session->userdata('user_id')]))
									{
										$value['loggedUserFlag']  			= $reviewLoggedInUserFlag[$value['id']][$this->session->userdata('user_id')]['flag'];

										$value['loggedUserHelpfulFlag'] 	= $reviewLoggedInUserFlag[$value['id']][$this->session->userdata('user_id')]['helpfulFlag'];
									}
									//dump($value);
									
								}
				    			
				    		}

				    		foreach ($userdata as $key2 => $value2)
				    		{
				    			if($value['userId'] == $value2['id'])
				    			{
				    				$temp= $value2;
				    				/*if($value2['firstName'] != NULL && $value2['lastName'] != NULL)
				    				{
				    					$temp2 = $value2['firstName'].' '.$value2['lastName'];
				    				}
				    				else
				    				{
				    					//$temp2 = $value2['email'];
				    					//$temp2 = $value2['lastName'];
				    					$temp2 = 'Anonymous User';				    						
				    				}*/

									if (strlen(trim($value2['firstName'])) == 0 && strlen(trim($value2['lastName'])) == 0) 
									{
										$temp2 = 'Anonymous';
									}
									else
									{
										$temp2 = $value2['firstName'].' '.$value2['lastName'];
									}

				    			}
				    		}
				    		$value['userTypeName'] = $reviewUserTypeArr[$value['userType']];

				    		$value['createdDate'] = date('M d, Y',strtotime($value['createdDate'])); //formatDate($value['createdDate']);

			    			$value['userdata'] = array();
			    			$value['userdata'] = $temp;
			    			$value['userdata']['displayName'] = $temp2;
			    			$value['review']  = $value['title'];



			    			$answers[$key] = $value;
			    			
				    	}	

				    	//dump($answers);


				    	
				 }

				
				 $result = array('status' =>'success','count'=>count($answers),'data'=>$answers,'orderBy' => $param['order_by'],'order' => $param['order'],'total_count' => $totalCount);

			}
			else
			{
				$result = array('status' =>'failed','data'=>'no school found');
			}  				
		
    		return $result;
			
		}
    }

	

}
?>