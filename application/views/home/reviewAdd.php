<?php

	if(!isset($schoolId))
	{
		$schoolId = '';
	}
	
	if(isset($schoolName))
	{
		$schoolName = $schoolName;
	}
	else
	{
		$schoolName = '';
	}	


	$rating  	=  	set_value('rating');
	$gradYear   = 	set_value('gradYear');
	$userType  	= 	set_value('userType');
	$title  	= 	set_value('title');
	$major  	= 	set_value('major');
	$otherMajor = 	set_value('otherMajor');
	$good  		= 	set_value('good');
	$bad  		= 	set_value('bad');

	if(!$major)
	{
		$major 	=	1;
	}

	$features   =  	$this->input->post('features');

	$culture    = 	$this->input->post('culture');

	$majorList 	=	$this->config->item('major1');

	array_unshift($majorList, 'Other');
	// echo "<pre>";
	// print_r($majorList);
	// die;
?>

<div id="school-details" class="main_container">
	<div class="container">
		
		

		<div class="row">
			

			<div class="col-md-12">

				<h1 class="main-header">
					Write a Review 
				</h1>

					<div class="row">
						<div class="col-sm-8">
							<div class="wrapper bg_white main-wrapper">
							<form class="form" id="reviewForm" role="form"  action="<?= site_url('reviews/createReview')?>" method="POST">
								<div class="form-group form-medium">
								    <label for="schoolId">School</label>

									<?php
								    	if(isset($schoolId))
								    	{
								    		
											$this->load->view('common/schoolAutocomplete', array('onlySchools' => 1, 'schoolId' => $schoolId, 'schoolName'	=> $schoolName ));
								    	}
								    	else
								    	{
								    		
								    		$this->load->view('common/schoolAutocomplete', array('onlySchools' => 1));
								    	}
								    ?>
								    <span class="help-block error-block"><?php echo form_error('schoolId'); ?><?php echo isset($errors['schoolId'])?$errors['schoolId']:''; ?></span>
								</div>
								<div class="form-group form-medium">
								
									    <label for="rating">Click to rate</label>
									   	<?php
								    	if($rating == "" || $rating ==  null)
								    	{	
								    		$rating = 1;
								    	?>	
										<?php
								    	}
								    	?>
										
										<div class="rating-stars">
											<input type="hidden" class="rating" id="rating" name="rating" value="<?= $rating?>"/>

										</div> 


										<span class="help-block error-block"><?php echo form_error('rating'); ?></span>
									
									
								</div>
								 
								<div class="form-group row">
									<div class="col-md-6">
										<div class="btn-group" data-toggle="buttons">
										<label for="userType" style="display:block">Please pick one</label>
										<?php

											if(!$userType)
											{
												$userType = 1;
											}
										
											foreach ($reviewUserType as $key => $value) 
											{

												
												if($userType == $key)
												{	
													?>
													
									  					<label class="btn btn-default active  ">
									  						<input type="radio" checked="checked" name="userType"  value="<?= $key ?>"><?= $value ?>
									  					</label>


												<?php
												}
												else{
												?>
												
									  				<label class="btn btn-default  ">
									  					<input type="radio"  name="userType" value="<?= $key ?>"><?= $value ?>
									  				</label>
												
												
												<?php
												}
											}

										?>
									</div>
									<span class="help-block error-block"><?php echo form_error('userType'); ?></span>
									</div>

									<div class="col-md-6">

										<label for="gradYear">Graduation Year</label>
										<select class="form-control " name="gradYear" id="gradYear">

											<option value="1">In School</option>
											<option value="2">N/A</option>
											<?php
											$yearCount  = 1;
											$currentYear = date("Y") ;

											for ($i=0; $i <= 30; $i++) 
											{	
												$currentYear = $currentYear -1;
												?> 
													<option value="<?= $currentYear ?>"><?= $currentYear?></option>
												<?php
											}
											?>
											
										</select>
										<span class="help-block error-block"><?php echo form_error('gradYear'); ?></span>
										<div id="wait" style="display:none;width:69px;border:1px solid black;position:absolute;top:209%;left:-25%;padding:2px;"><img src='http://www.techuz.info/schooltorch/assets/images/ajax-loading-bar-19.gif' width="64" height="64" /></div>
									</div>
									
									
								</div>
								
								<div class="form-group row">
									<div class="col-md-6">
										<div class="btn-group" data-toggle="buttons">
										<label for="gradType" style="display:block;">Graduate Type</label>
											

											<?php

											if(!$gradYear)
											{
												$gradYear = 1;
											}
									
											 if($gradYear == 1)
											 {?>
												<label class="btn btn-default active">
													<input type="radio" checked name="gradType" value="1">
													Grad
												</label>

											<?php
											 }
											 else
											 {?>
												<label class="btn btn-default">
													<input type="radio"  name="gradType" value="1">
													Grad
												</label>

											<?php

											 }
											?>
							  				
									

										

											<?php
											 if($gradYear == 2)
											 {?>
												
											<label class="btn btn-default active"><input type="radio" checked="" name="gradType" value="2">Undergrad</label>

											<?php
											 }
											 else
											 {?>
												<label class="btn btn-default"><input type="radio"  name="gradType" value="2">Undergrad</label>

											<?php

											 }
											?>


							  				
										
									</div>
									<span class="help-block error-block"><?php echo form_error('gradType'); ?></span>

									</div>
									
									<div class="col-md-6">
										<label class="control-label" for="major"> Major</label>
											<?php asort($majorList);?>
											<select id="major1" name="major1" class="form-control">
												<option value="" selected="selected">Select Option</option>
											<?php
												$count 	=	0;
												foreach($majorList as $innerKey => $innerVal)
												{
													$selectStr 	=	'';
													if($count == 0)
													{
														$selectStr 	=	'selected="selected"';
													}
													$arr = explode("-",$innerVal);
														if($arr[1]!= ""){
															
											?>
													<option value="<?=$arr[0]?>" <?=$selectStr?>>
														<?//=$innerVal?>
														<?php 
															echo $arr[1];
														?>
													</option>
														<?php } ?>
											<?php
													$count++;
												}
											?>
											</select>
											
											<input type="hidden" name="txtMajorType" id="txtMajorType"/>
											<input type="text" style="margin-top: 10px;" class="form-control hide" name="otherMajor"  id="otherMajor" placeholder="Major" value="<?= $otherMajor?>">
											<span class="help-block error-block"><?php echo form_error('otherMajor'); ?></span>
											<!--<input type="text" class="form-control" name="major"  id="major" placeholder="Major" value="<?//= $major?>">-->
											
									</div>
									
								</div>
								<div class="form-group row">
									<div class="col-md-6 algnRight">
										<label for="" style="display:block;">&nbsp;</label>
										<select id="major" name="major" class="form-control">
											<?php
												$count1 	=	0;
												//$_COOKIE['majorName'] = "";
												if(!isset($_COOKIE['majorName']) && $_COOKIE['majorName'] == ""){
													//$selmajorList 	=	$this->config->item('art-major');
												}else{
													//$selmajorList 	=	$this->config->item($_COOKIE['majorName'].'-major');
												}
												foreach($selmajorList as $innerKey => $innerVal)
												{
													$selectStr 	=	'';
													if($count1 == 1)
													{
														$selectStr 	=	'selected="selected"';
													}
											?>
													<option value="<?=$arr[0]?>" <?=$selectStr?>>
														<?=$innerVal?>
													</option>
											<?php
													$count1++;
												}
											?>
											<option value="">Select Option</option>
											</select>
									</div>
									<div class="col-md-6">
										<div class="btn-group" data-toggle="buttons">
										<label for="studentType" style="display:block;">Student Type</label>
										<?php

										if(!$gradYear)
										{
											$gradYear = 1;
										}
								
										 if($gradYear == 1)
										 {?>
											<label class="btn btn-default active">
												<input type="radio" checked name="studentType" value="1">
												Local
											</label>

										<?php
										 }
										 else
										 {?>
											<label class="btn btn-default">
												<input type="radio"  name="studentType" value="1">
												Local
											</label>

										<?php

										 }
										?>
										<?php
										 if($gradYear == 2)
										 {?>
											
										<label class="btn btn-default active"><input type="radio" checked="" name="studentType" value="2">International</label>

										<?php
										 }
										 else
										 {?>
											<label class="btn btn-default"><input type="radio"  name="studentType" value="2">International</label>

										<?php

										 }
										?>
									</div>
									<span class="help-block error-block"><?php echo form_error('studentType'); ?></span>
									</div>
									
								</div>
								
								<div class="form-group form-medium">
									
										
											<label class="control-label" for="title"> Title of your review</label>
											<input type="text" class="form-control" name="title"  id="title" placeholder=" Summarize your opinion or highlight an interesting aspect" value="<?= $title?>">
											<span class="help-block error-block"><?php echo form_error('title'); ?></span>
									
										<?php
											if(isset($errors['good'])){
											?>
											<div class="help-block error-block">3 words minimum</div>
												
											<?php
												}else{
												?>

													<div class="help-block error-block" id="error-reviewTitle">3 words minimum</div>
											<?php
												}
											?>


								</div>
								<div class="form-group form-medium">
									
										
										<label class="control-label" for="good"> Good


										</label>
									
										<textarea class="form-control" rows="3" id="good" name="good" placeholder=""><?= $good ?></textarea>




										<?php
												if(isset($errors['good'])){
												?>

													<span class="help-block error-block">20 words minimum <span class="pull-right"> 
												<?php
												}else{
												?>

													<span class="help-block error-block" id="error-reviewGood">20 words minimum <span class="pull-right"> 
											<?php
												}
											?>

											<span id="counter2"></span> words max.</span>
								</div>
								<div class="form-group form-medium">
									
											<label class="control-label" for="bad"> Bad</label>
											<textarea class="form-control" rows="3" id="bad" name="bad" placeholder=""><?= $bad ?></textarea>

											<?php
												if(isset($errors['bad'])){
												?>

													<span class="help-block error-block">20 words minimum <span class="pull-right"> 
												<?php
												}else{
												?>

													<span class="help-block error-block" id="error-reviewBad">20 words minimum <span class="pull-right"> 
											<?php
												}
											?>

											<span id="counter3"></span> words max.</span>


										
								</div>
							

								<div class="form-group">
									<div class="btn-group" data-toggle="buttons">

										<label class="control-label" for="features" style="display:block;">Best Features</label>

										<?php

											
											if(!$features)
											{
												$features = array();
											}


											foreach ($reviewFeatures as $key => $value) 
											{
												if(in_array($key, $features))
												{
													?>
														
															<label class="btn btn-default active "><input type="checkbox" checked name="features[]" value="<?= $key?>" data-bv-field="features"> 
																<?= $value ?>																	
															</label>
														
												<?php

												}
												else{


												?>
												
													<label class="btn btn-default "><input type="checkbox" name="features[]" value="<?= $key?>" data-bv-field="features"> 
														<?= $value ?>																	
													</label>
											
												<?php
												}
											}
										?>
										
									</div>
									<div class="help-block">Pick 3 best features</div>
									<span class="help-block error-block"><?php echo form_error('features'); ?><?php echo isset($errors['features'])?$errors['features']:''; ?></span>
								</div>

								<div class="form-group">
									
									<div class="btn-group" data-toggle="buttons">

										<label class="control-label" for="culture" style="display:block;">Culture</label>

										<?php

											if(!$culture)
											{
												$culture = array();
											}

											

											foreach ($reviewCulture as $key => $value) 
											{

												if(in_array($key, $culture))
												{
												?>
													<label class="btn btn-default active "><input type="checkbox"  checked name="culture[]" value="<?= $key?>" data-bv-field="culture"> 
															<?= $value ?>																	
														</label>
													
												<?php

												}
												else{


												?>
												<label class="btn btn-default "><input type="checkbox" name="culture[]" value="<?= $key?>" data-bv-field="culture"> 
														<?= $value ?>																	
													</label>
											
												<?php
												}
											}
										?>
										
									</div>
									<div class="help-block">Pick at least one option</div>
									<span class="help-block error-block"><?php echo form_error('culture'); ?><?php echo isset($errors['culture'])?$errors['culture']:''; ?></span>
								</div>

								<div class="form-group">
								
									<input type="submit" id="form_submit" class="btn btn-success" value="Submit Review"/>
									<a href="javascript: history.go(-1)" class="btn btn-link">Cancel</a>
									
								</div>

							

							</form>
						</div>
						</div>
						 <div class="col-sm-4  blog-sidebar">
				           
				            
				                <div class="sidebar-module">
				                  <div class="col-heading">Help Us Help You! </div>
				                 <div class="help-text">
				                	<?php 

				                		$this->load->view('home/reviewGuidelines');

				                	?>
				                 </div>
				             </div>



				          </div>




					</div>
				
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">

	var globalSiteUrl = "<?= site_url() ?>";

	var isLoggedIn = '<?= $this->tank_auth->is_logged_in();?>'; 

	$(window).load(function() {
	    if(!isLoggedIn)
	    {
	       $('#loginPopup').trigger('click');
	    }
		$("#major").val($("#major option:first").val());
	});
	$("#major1").change(function(event){
		selValue = $('#major1').val();
		$("#wait").css("display", "block");
		$.ajax({
			url: globalSiteUrl + 'reviews/getMappings/',
			type:'POST',
			data:{'mappingValue': selValue},
			success: function(data){
				$("#wait").css("display", "none");
				var arr = data.split(',');
				$('#major').empty();
				$.each( arr, function( index, value ){
					$('#major').append('<option value="'+value+'">'+value+'</option>');
				});
			},
			error: function( jqXhr, textStatus, errorThrown ){
				console.log( errorThrown );
			}
		});
		/*$("#txtMajorType").val(selValue);
		document.cookie="majorName="+selValue;
		location.reload();*/
		
	});
	$(document).ready(function(){

		try{
			if($('#major option[value="<?=$major?>"]').length > 0)
			{
				$('#major').val("<?=$major?>");
			}
		}
		catch(e){

		}



		$('#major').unbind('change')
		$('#major').change(function(event){

			console.log($('#major').val());

			if($('#major').val() == "Other")
			{
				$('#otherMajor').removeClass('hide');
			}
			else
			{
				$('#otherMajor').addClass('hide');
			}

		});

		$('#major').trigger('click');


		$('#good').unbind('keyup')
		$('#good').keyup(function(event){

			if(+($('#counter2').html()) >= 80){

				$('#error-reviewGood').addClass('error-block');

			}else{

				$('#error-reviewGood').removeClass('error-block');
			}

			

		});


		$('#title').unbind('keyup')
		$('#title').keyup(function(event){

			var value = $('#title').val();

			var regex = /\s+/gi;
    		var wordCount = value.trim().replace(regex, ' ').split(' ').length;

    		if(wordCount >= 3){

    			$('#error-reviewTitle').removeClass('error-block');

    		}else{

    			$('#error-reviewTitle').addClass('error-block');
    		}

			

		});



		$('#bad').unbind('keyup')
		$('#bad').keyup(function(event){

			if(+($('#counter3').html()) >= 80){

				$('#error-reviewBad').addClass('error-block');
				
			}else{

				$('#error-reviewBad').removeClass('error-block');
			}

			

		});

		


		$('#good').simplyCountable({
           counter: '#counter2',
           countType: 'words',
           maxCount: 100,
           countDirection: 'down',
           onOverCount: function(count, countable, counter){


           					$('#counter2').removeClass("text-green");
           					$('#counter2').addClass("text-danger");

           				},
            onSafeCount:function(count, countable, counter){


             				$('#counter2').removeClass("text-danger");
             				$('#counter2').addClass("text-green");
             			},
        
       	});

		$('#bad').simplyCountable({
         counter: '#counter3',
           countType: 'words',
           maxCount: 100,
           countDirection: 'down',
            onOverCount: function(count, countable, counter){
           					$('#counter3').removeClass("text-green");
           					$('#counter3').addClass("text-danger");

           				},
            onSafeCount:function(count, countable, counter){
             				$('#counter3').removeClass("text-danger");
             				$('#counter3').addClass("text-green");
             			},
       	});


		var schoolName 	=	'';

		$('#reviewForm').unbind('submit');
		$('#reviewForm').submit(function(event){

			schoolName 	=	$("#searchSchool").val();

			$("#searchSchool").val($("#searchSchool").attr('value'));

		});

		$('#reviewForm').bootstrapValidator({

			message: 'This value is not valid',

			excluded: [':disabled', ':hidden', ':not(:visible)'],

			feedbackIcons: {
				valid: '',
				invalid: '',
				validating: ''
			},

			fields: {

            	'schoolId' :  	{
					                validators: {
					                    notEmpty: {
					                        message: 'The school id is required'
					                    }
					                }
					            },
			    
				'gradYear'	: 	{
									validators: {
					                    notEmpty: {
					                        message: 'The Graduation Year is required'
					                    }
					                }
								},

            	'rating' : {
				                validators: {

				                	 notEmpty: {
					                        message: 'The rating field is required'
					                    },
				                    between: {
				                        min: 1,
				                        max: 5,
				                        message: 'The rating value is between 1 to 5'
				                    }
				                }
				            },
	            'major' :  {
					                validators: {
					                    notEmpty: {
					                        message: 'The major field is required'
					                    }
					                }
					            },
				'otherMajor' :  {
					                validators: {
					                    notEmpty: {
					                        message: 'The major field is required'
					                    }
					                }
					            },
	           /*  'title' :  {
					                validators: {
					                    notEmpty: {
					                        message: 'The title field is required'
					                    }
					                }
					            },
	           'good' :  {
					                validators: {
					                    notEmpty: {
					                        message: 'The good field is required'
					                    }
					                }
					            },
	            'bad' :  {
					                validators: {
					                    notEmpty: {
					                        message: 'The bad field is required'
					                    }
					                }
					            },*/
              
                'features[]': {
				           validators: {
				                    choice: {
				                    	min: 3,
				                       // max: 3,
				                        message: ' '
				                    }
				                }
				 },
				'culture[]': {
				          validators: {
				                    choice: {
				                        min: 1,
				                       // max: 1,
				                      
				                        message: ' '
				                    }
				                }
				 }
			},
			callback: function(value, validator){
				alert(validator);
			}
		}).on('error.form.bv', function(e){

			$("#searchSchool").val(schoolName);

		});

	});

</script>
