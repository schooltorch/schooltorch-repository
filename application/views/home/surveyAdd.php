<?php
	
	//QuestionType
	/*1.radio
	2.checkbox
	3.dropdown
	4.textArea*/
	
	if(!isset($schoolName))
	{
		$schoolName = '';
	}
?>



<div id="school-details" class="main_container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-header">
					<?= $schoolName ?> - Survey 
				</h1>
			</div>
			<div class="col-md-12">

				<div class="wrapper bg_white">
					
					<div class="row">
						<div class="col-md-7">
							<form class="form surveyForm" role="form"  action="<?= site_url('/survey/add')?>/<?= $id?>" method="POST">



								<?php

								//echo '<pre>';

								//print_r($questions);

								//echo '</pre>';

								if(isset($questions) && count($questions) > 0)
								{
									

									foreach ($questions as $key => $value) 
									{
											if($value['questionType'] == 1)
											{
												// for radio
												?>
													<div class="form-group">
														<div class="count">
															<?=$value['id'] ?>. 
														</div>
														<div class="survey-data">
														 <label for="q<?=$value['id'] ?>"><?= $value['question'] ?></label>


														 <?php
															 if(is_array($value['option']) && count($value['option']))
															 {
															 	$count = 1;
															 	foreach ($value['option'] as $key2 => $value2) 
															 	{
															 		if($count == 1)
															 		{
															 			$count++
															 			?>

																	 		<div class="radio">
																  					<label><input type="radio" checked name="q<?=$value['id'] ?>" value="<?= $key2 ?>"><?= $value2  ?></label>
																			</div>

															 			<?php

															 		}
															 		else{
															 			?>

																	 		<div class="radio">
																  					<label><input type="radio" name="q<?=$value['id'] ?>" value="<?= $key2 ?>"><?= $value2  ?></label>
																			</div>

															 			<?php


															 		}
															 		
															 	}
															 }	
															?>
														<span class="help-block error-block"> <?php echo form_error('q'.$value["id"]); ?> </span>
													</div>
													</div>
												<?php
											}
											else if($value['questionType'] == 3)
											{?>
												<div class="form-group">
													<div class="count">
														<?=$value['id'] ?>. 
													</div>
													<div class="survey-data">
													<label for="q1"><?= $value['question'] ?></label>

													
														<select name="q<?= $value['id']?>" class="form-control" style="display:inline-block;width:auto;">

															<?php
																for ($x=2014; $x>=1950; $x--) {
																	?>
																		<option value="<?= $x?>"><?= $x?> </option> 
																	<?php
																  
																} 

															?>

														</select>
													</div>
												</div>

											<?php
											}else if($value['questionType'] == 2 )
											{
												// checkbox
												?>
													
													<div class="form-group">
														<div class="count">
															<?=$value['id'] ?>. 
														</div>
														<div class="survey-data">
														<label class="control-label" for="q<?=$value['id'] ?>"> <?= $value['question'] ?></label>

															 <?php
																 if(is_array($value['option']) && count($value['option']))
																 {

																 	foreach ($value['option'] as $key2 => $value2) 
																 	{
																 		?>
																 		<div class="checkbox">
																	 		<label>
																			  <input type="checkbox" name="q<?= $value['id']?>[]"  value="<?= $key2 ?>"> <?= $value2 ?>
																			</label>
																		</div>
																 		<?php
																 	}
																 }	
																?>
														</div>
													</div>
												<?php


											}
											else if ($value['questionType'] == 4)
											{?>
												<div class="form-group">
													<div class="count">
														<?=$value['id'] ?>. 
													</div>
													<div class="survey-data">
														<label for="q1"><?= $value['question'] ?></label>
														<textarea class="form-control" rows="3" name="q<?= $value['id'] ?>"></textarea>
													</div>
												</div>
											<?
											}
											else{

												dump($value);
											}
											
									}

								}
								?>

								<div class="form-group">
									<div class="count">
									</div>

									<div class="survey-data">
										<input type="submit" id="form_submit" class="btn btn-success" value="Submit"/>
										
										<a href="<?= site_url('home/school/') ?>/<?= $id?>/PutNameHere" class="btn btn-default"> Cancel </a>
									</div>
								</div>

							</form>
						</div>
					</div>


				</div><!--/wrapper-->


			</div>
		</div><!--/row-->
	</div><!--/container-->
</div>


<script type="text/javascript">
		
    $(document).ready(function() {

        $('.surveyForm').bootstrapValidator({

            message: 'This value is not valid',

            excluded: [':disabled', ':hidden', ':not(:visible)'],

            feedbackIcons: {
                valid: '',
                invalid: '',
                validating: ''
            },

            fields: {
                
              
                q2: {
                    validators: {
                        notEmpty: {
                            message: 'The Question2 field is required and cannot be empty'
                        },
                       
                    }
                },
                'q5[]': {
				           validators: {
				                    choice: {
				                        min: 3,
				                        message: 'Please select minimum 3 options to proceed'
				                    }
				                }
				 },
				'q6[]': {
				          validators: {
				                    choice: {
				                        min: 1,
				                      
				                        message: 'Please select minimum 1 options to proceed'
				                    }
				                }
				 },
				 'q7[]': {
				          validators: {
				                    choice: {
				                        min: 1,
				                      
				                        message: 'Please select minimum 1 options to proceed'
				                    }
				                }
				 },
           }
        });
    });

</script>