<div class="main_container" id="blog">
	<div class="container">
        <div class="row">
			<div class="col-md-12">
				<h1 class="main-header">Write a Review </h1>
			</div>
        </div>

        <div class="row">

			<div class="col-sm-12">
				<div class="wrapper bg_white">
					<h4> Thank you for submitting your review.
						<a href="<?= site_url('/reviews')?>" class="btn btn-default pull-right">Click here to write another review </a>
					</h4>
				</div>
			</div>
		</div>
	</div>
</div>
 