


			<!-- This is the community View -->


	<p>	The following ground rules will help ensure an enjoyable and safe experience for our community. Our
		members are expected to participate with common sense, courtesy, and respect. Your use of this site is
		subject to these guidelines and our Terms of Service. User content on SchoolTorch reflects the opinions
		of those users and not of SchoolTorch.
	</p>
	<h2><strong>General Guidelines</strong></h2>
	<h3><strong>Your SchoolTorch Account</strong></h3>	
	<p>SchoolTorch accounts are intended for your personal use, to contribute content that you have created
		yourself. Since the best travel advice comes from the people you trust, you will get the most out of your
		SchoolTorch experience by providing accurate information about yourself. Use of SchoolTorch accounts
		for any other purpose may be considered an infringement of our Terms of Service.
	</p>
	<h3><strong>Courtesy & Respect</strong></h3>
	<p>Treat others with courtesy and respect. Remember that your tone can easily be misinterpreted in print, so
		take care in your choice of language. SchoolTorch is a global travel community so you are likely to
		encounter people of various opinions, experiences, budgets, and preferences. Show respect for these
		differences, and don't take things personally.
	</p>
	<h3><strong>Lead by Example</strong></h3>
	<p>Make newcomers feel welcome, be helpful to others, and keep it constructive. The best way to deal with
		flame-baiters and trolls is to ignore them and report them to SchoolTorch. By being a positive member,
		you encourage others to do the same.
	</p>
	<h3><strong>Integrity</strong></h3>
	<p>Community members are looking to you for advice, so be sincere and truthful. Refrain from defaming and
		harassing people, impersonating others, spamming, violating the privacy of others, and engaging in illegal
		activities
	</p>
	<h3><strong>Common Sense</strong></h3>
	<p>Use your common sense when seeking advice. Be safe and smart when arranging meetups, and don't
		post personal information about yourself in public spaces. SchoolTorch is not responsible for the actions
		of our community members.
	</p>
	<h3><strong>Reviews and Recommendations</strong></h3>
	<p>The best reviews and recommendations are insightful, personal, informative, and readable. SchoolTorch
		offers a rich community experience because our members take the time to share their personal stories.
		Here are some tips for writing great reviews and recommendations.
	</p>
	<ul>
        <li>
         	<h3><strong>Share your experience:</strong></h3>
         	The community benefits when you share your unique, personal
			experiences. Describe what you liked and disliked, and what it meant to you.
		</li>
        <li>
         	<h3><strong>Be informative:</strong></h3>
         	Try to be as accurate as possible, and include relevant and useful details. Share
			something novel or surprising that isn't usually covered in travel guidebooks.
		</li>
        <li>
        	<h3><strong>Be constructive:</strong></h3>
        	It's ok to share negative feedback, but be constructive and include relevant facts
			and details. Do not post false information as this may have legal consequences.
		</li>
        <li>
        	<h3><strong>Use good grammar and spelling:</strong></h3>
        	A good recommendation is a readable one. Use complete
			sentences and write clearly. Go easy on capitalization and resist the urge to write entire novels.
        </li>
    </ul>
    <p>SchoolTorch rarely removes reviews, but when we do below are some possible reasons why. We reserve
		the right to remove any content that we feel violates our Terms of Service or these guidelines, or violates
		the spirit of these guidelines. Read more in our Terms of Service.
    </p>
    <ul>
        <li>
         	<h3><strong>Inappropriate content:</strong></h3>
         	Bad language, adult content, or otherwise objectionable content.
		</li>
        <li>
         	<h3><strong>Plagiarism:</strong></h3>
         	Copying a review from another person or website and posting as your own.
		</li>
        <li>
        	<h3><strong>Conflict of interest:</strong></h3>
        	Posting favorable reviews for your own business or posting reviews in exchange
			for money or freebies. You should also refrain from reviewing your employer or any business in which
			you have a commercial interest.
		</li>
        <li>
        	<h3><strong>Fake or malicious recommendations:</strong></h3>
        	Posting bad reviews about your competitors.
        </li>
        <li>
        	<h3><strong>Off-topic:</strong></h3>
        	Reviews that are unrelated to the topic page it is listed under.
        </li>
    </ul>

	<h3><strong>Reporting Abuse</strong></h3>
	<p>If you see something that may be in violation of these guidelines or our Terms of Service, please flag it by
		clicking on the "Flag this" button next to the relevant content.
		We do not arbitrate disputes among business owners, competitors, customers, ex-employees, or other
		entities. We remove only the reviews that we consider to be in violation of our guidelines and Terms of
		Service.
	</p>