<?php

	$name  		=	set_value('name');
	$email 	    = 	set_value('email');
	$subject 		= 	set_value('subject');
	$message 	= 	set_value('message');


	$captcha = array(
		'name'	=> 'captcha',
		'id'	=> 'captcha',
		'placeholder' =>'Confirmation Code',
		'class'  => 'form-control input-lg',
		'maxlength'	=> 8,
	);

?>


<div class="main_container">
	<div class="container">
		<div class="row">

				<div class="col-md-12">

					<h1 class="main-header">Contact Us</h1>

					<div class="wrapper bg_white">

						<div class="profile-content">

							
							<div class="row">

								<div class="col-md-6">
									<form class="form" role="form" id="contactForm"  action="<?php echo site_url('home/contact')?>" method="POST">

										<div class="form-group">
											
											<!--[if lte IE 9]>
											<label for="name">Name</label>
											<![endif]-->
											<input class="form-control input-lg" type="text" required name="name"  id="name" placeholder="Name" value="<?=$name?>"/>
											
											<span class="help-block error-block"> <?php echo form_error('name'); ?> </span>
											
										 </div>

										 <div class="form-group">
											<!--[if lte IE 9]>
											<label for="email">Email address</label>
											<![endif]-->
											<input class="form-control input-lg" type="email" required name="email"  id="email" placeholder="Email address" value="<?=$email?>"/>
											
											<span class="help-block error-block"> <?php echo form_error('email'); ?> </span>
											
										 </div>

										 <div class="form-group">
											<!--[if lte IE 9]>
											<label for="subject">Subject</label>
											<![endif]-->
											<input class="form-control input-lg " type="text" name="subject"  id="subject" placeholder="Sum it up with a short title" value="<?=$subject?>"/>
											
											<span class="help-block error-block"> <?php echo form_error('subject'); ?> </span>
											
										 </div>

										 <div class="form-group">
											<!--[if lte IE 9]>
											<label for="message">Message</label>
											<![endif]-->
											<textarea class="form-control input-lg" rows="4" cols="50"  required name="message" id="message" placeholder="Message"><?=$message?></textarea>
											<span class="help-block error-block"> <?php echo form_error('message'); ?> </span>
											
										 </div>	

				<?php if ($show_captcha) {
				if ($use_recaptcha) { ?>
			<div class="form-group">
				
					<div id="recaptcha_image"></div>
					<a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
					<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
					<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
				
			</div>
			<div class="form-group">

				<div class="recaptcha_only_if_image">Enter the words above</div>
				<div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
				
				<input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
				<span class="help-block error-block"><?php echo form_error('recaptcha_response_field'); ?></span>
				<?php echo $recaptcha_html; ?>
			</div>
			<?php } else { ?>
			<div class="form-group">
					<p>Enter the code exactly as it appears:</p>
					<?php echo $captcha_html; ?>
			</div>

			<div class="form-group">
				
				<!--[if lte IE 9]>
				<label for="captcha">Confirmation Code</label>
				<![endif]-->
				<?php echo form_input($captcha); ?>
				<span class="help-block error-block"> <?php echo form_error($captcha['name']); ?></span>
			</div>
			<?php }
			} ?>

										
										<div class="form-group">

											<input type="submit" id="form_submit" class="btn btn-success" value="Submit"/>
											
											<a href="<?php echo site_url('/home') ?>" class="btn btn-link"> Cancel </a>
										</div>	


									</form>
								</div>
							</div>
							
						</div>
					</div>
				</div>

			
		</div>
	</div>
</div>