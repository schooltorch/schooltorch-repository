
<div class="sidebar-module">
	<div class="col-heading"> Upload Guidelines</div>

	<ul class="list-unstyled">
		<li><span class="help-text">JPG, GIF, PNG, or BMP file formats only.</span></li>
		<li><span class="help-text">No more than 1MB per photo.</span></li>
		<li><span class="help-text">Only original, non-copyrighted images.</span></li>
		<li><span class="help-text">No logos, websitescreenshots or marketing materials.(Logos pictured on walls, buildings, or automobiles are OK)</span></li>
		<li><span class="help-text">No portraits of an individual, Incriminating photos of workplace violations, patients in a healthcare setting, or photos that are irrelevant to the employment setting(cartoons, etc.)</span></li>
		<li><span class="help-text">Photo captions must be considered appropriate in accordance with our guidelines.</span></li>
	</ul>
</div>