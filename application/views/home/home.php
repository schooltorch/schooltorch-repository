

<?php

	if(!isset($popularSchools) || count($popularSchools) == 0)
	{
		$popularSchools 	=	array(
		                         		array(
		                         		      	'UNITID' 	=>	190150,
		                         		      	'photo'		=>	'assets/images/popular/school-1-thumb.jpg',
		                         		      	'INSTNM'	=>	'Columbia University',
		                         		      	'CITY'		=>	'New York',
		                         		      	'STABBR'	=>	'NY'
		                         		    ),
		                         		array(
		                         		      	'UNITID' 	=>	198385,
		                         		      	'photo'		=>	'assets/images/popular/school-2-thumb.jpg',
		                         		      	'INSTNM'	=>	'Columbia University',
		                         		      	'CITY'		=>	'Davidson',
		                         		      	'STABBR'	=>	'NC'
		                         		    ),
		                         		array(
		                         		      	'UNITID' 	=>	217156,
		                         		      	'photo'		=>	'assets/images/popular/school-3-thumb.jpg',
		                         		      	'INSTNM'	=>	'Brown University',
		                         		      	'CITY'		=>	'Providence',
		                         		      	'STABBR'	=>	'RI'
		                         		    )
		                         	);
	}

?>

<div class="home" id="home">

	<div class="home-wrapper"></div>

	<?php
		if(isset($errorMessage))
		{
			if($errorMessage != '')
			{
	?>
				<div class="alert alert-danger" style="margin: auto;display: table;">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<div>
						<strong>Error!</strong>
						<div><?=$errorMessage?></div>
					</div>
				</div>
	<?php
			}
		}
	?>
	
	<table width="100%" cellpadding="100" cellspacing="100">
		
		<tr>
			<td valign="middle"> 
				<?php

					$this->load->view("home/homeSearch");
				?>

			</td>
		</tr>

		<tr>

			<td valign="middle">

				<section class="popular-schools">
					<div class="container">
						<div class="row">
							<div id="popular">

								<div class="lead">
									<span>Popular</span> Schools
								</div>

					   			<!-- HARD CODED DATA START -->

									<!-- <div class="col-md-4">
										<div class="content" style="">
											<div class="row">

												<a href="<?=site_url()?>home/school/190150">	

													<div class="col-md-4 col-xs-4 pull-left img">
														<img src="<?=base_url()?>assets/images/popular/school-1-thumb.jpg" class="img-responsive"/>
													</div>

													<div class="col-md-8 img-content">
														<h4>Columbia University</h4>
														<span>New York, NY</span>

													</div>

												</a>	

											</div>
										</div>
									</div>

									<div class="col-md-4">
										<div class="content" style="">
											<div class="row">

												<a href="<?=site_url()?>home/school/198385">

													<div class="col-md-4 col-xs-4 pull-left img">
														<img src="<?=base_url()?>assets/images/popular/school-2-thumb.jpg" class="img-responsive "/>
													</div>

													<div class="col-md-8 img-content">
														<h4>Davidson University</h4>
														<span>Davidson, NC</span>
													</div>

												</a>	

											</div>
										</div>
									</div>


									<div class="col-md-4">
										<div class="content" style="">
											<div class="row">

												<a href="<?=site_url()?>home/school/217156">

													<div class="col-md-4  col-xs-4 pull-left img">
														<img src="<?=base_url()?>assets/images/popular/school-3-thumb.jpg" class="img-responsive "/>
													</div>

													<div class="col-md-8 img-content">
														<h4>Brown University</h4>
														<span>Providence, RI</span>
													</div>

												</a>	
															
											</div>
										</div>
									</div> -->

								<!-- HARD CODED DATA END -->
								
								<?php
									foreach($popularSchools as $key=>$value)
									{
								?>

										<div class="col-md-4">
											<div class="content" style="">
												<div class="row">
													<a href="<?=site_url()?>home/school/<?=$value['UNITID']?>/<?=url_title($value['INSTNM'])?>">

														<div class="col-md-4  col-xs-4 pull-left img">
															<?php
																if($value['photo'])
																{
															?>
																	<img src="<?=base_url()?>/<?=$value['photo']?>" class="img-responsive " style="height: 100px;width:100px;"/>
															<?php
																}
																else
																{

																}
															?>
																
														</div>

														<div class="col-md-8 img-content">
															<h4><?=$value['INSTNM']?></h4>
															<span><?=$value['CITY']?>, <?=$value['STABBR']?></span>
														</div>

													</a>															
												</div>
											</div>
										</div>

								<?php
									}
								?>
								

							</div>
						</div>

					</div>
				</section>

			</td>
		</tr>
	</table>


</div>
