


   
     
      <p>Candela Labs LLC, dba SchoolTorch ("we" or "us") receives information about you through your use of
         our websites, emails and mobile applications ("SchoolTorch"). When you use SchoolTorch, you consent
         to our collection, use and disclosure of your information as described in this Privacy and Cookie Policy.
      </p>
      <p>This Privacy and Cookie Policy explains what kind of information we collect, how we use that information
         and how we may share it. We collect both personal and non-personal information (as those terms are
         defined in this policy) and this Privacy and Cookie Policy explains what steps we take to secure your
         personal information and how you can manage your choices about our collection and use of your
         personal information. 
      </p>
      <h2>Information We Collect and How We Use It</h2>
      <h3>Personal Information We Collect</h3>
      <p>Personal information is data that can be used to uniquely identify or contact a single person. You may
         choose to provide personal information to us when you use SchoolTorch. Because we change our
         offerings and features from time to time, the options you have to provide us with personal information also
         may change, but here are some examples of situations in which you may decide to provide personal
         information to us:
      </p>
      <ul>
         <li>Creating a SchoolTorch account;</li>
         <li>Signing up for email alerts;</li>
         <li>Completing a survey;</li>
         <li>Completing a form to subscribe to receive information;</li>
         <li>When you upload content; </li>
         <li>Contacting us for any reason, such as by email, including for technical support or customer service.</li>
      </ul>
      <p>Depending on how you interact with SchoolTorch, the personal information we collect from you may vary.
         For example, to create an account we ask only for an email address and password. In other
         circumstances, such as when you complete a form, we may ask you to provide your name, email
         address, phone number, and, in some cases, postal address. Because we request this information
         directly, it will be clear what types of personal information we are collecting.
      </p>
      <p>If you connect to SchoolTorch using Facebook, Google+, or another social networking site (each a "Social
         Networking Site"), we will receive information that you authorize the Social Networking Site to share with
         us, including your information about your friends and connections on that Social Networking Site. Any
         information that we collect from your Social Networking Site account may depend on the privacy settings
         you have set with the Social Networking Site, so please consult the Social Networking Site's privacy and
         data practices.    
      </p>
      <h3>Non Personal Information We Collect</h3>
      <p>We also collect non-personal information – data in a form that does not permit direct association with any
         specific individual. We may collect, use, transfer, sell, and disclose non-personal information for any
         purpose. Aggregated data is considered non-personal information for the purposes of this Privacy and
         Cookie Policy. If we combine non-personal information with your personal information, the combined
         information will be treated as personal information for as long as it remains combined.
      </p>
      <h3>Information We Collect by Automated Means</h3>
      <p>We obtain certain personal and non-personal information by automated means when you visit
         SchoolTorch. The type of information we collect by automated means may vary, but generally includes
         technical information about your computer, such as IP address or other device identifier. The information
         we collect also may include usage information and statistics about your interaction with SchoolTorch.
         That information may include the URLs of our web pages that users visited, URLs of referring and exiting
         pages, page views, time spent on a page, number of clicks, platform type, location data, and other
         information about how you used SchoolTorch. 
      </p>
      <p> Automated means of data collection include the following:</p>
      <p> <strong>Web Browser.</strong> When you visit a SchoolTorch website, your browser automatically sends us your Internet
         protocol ("IP") address so that the web pages you request can be sent to your computer or device. We
         use your IP address to determine additional information, such as whether the computer or device has
         ever been used to visit SchoolTorch before, which SchoolTorch features were used the most often, and
         how much time was spent on a page.
      </p>
      <p><strong>Web Beacons and Log Files.</strong> We may use "web beacons" (also known as Internet tags, pixel tags,
         tracking pixels, and clear GIFs) on pages of our websites. These web beacons allow third parties to
         collect information such as the IP address of the device, the URL of the web page, the time the page was
         viewed, the type of browser used, and any additional information in cookies that may have been set by
         the third party. The information is typically collected in log files, which generally track traffic on our
         websites. Purposes for this information collection may include managing our online advertising, including
         by determining which ads brought users to our websites. The information also is used for analytical
         purposes and to manage technical issues that may arise. We do not attempt to identify individual users
         through this information. 
      </p>
      <p><strong> Location Data.</strong> If you access SchoolTorch using a mobile device, we will collect your physical location if
         you have enabled this feature and given your consent. In some cases, even if you are not using a mobile
         device, information about your general location may be discernable from your device's IP address or the
         URLs we receive.
      </p>
      <p><strong>Cookies.</strong> We also collect information using cookies to help us identify you when you visit SchoolTorch
         and we may share cookies with third-party advertisers or advertising networks to serve you relevant,
         targeted ads. 		
      </p>
      <p>For more information on cookies and similar tracking mechanisms, how we use them, and how you can
         control them, please see the section entitled <strong>Cookie Policy and Ad Choices.</strong>
      </p>
      <h3>How We Use Information</h3>
      <p>We may use the information we receive about you: </p>
      <ul>
         <li>to provide you with personalized content; </li>
         <li>to fill out your profile;</li>
         <li>to generate content for the site in the form of school or college reviews, and other content to provide
            users with the services;
         </li>
         <li>to better tailor the features, performance, and support of the site;</li>
         <li>for internal operations, including troubleshooting, data analysis, testing, research, and service
            improvement; 			
         </li>
         <li>to provide relevant advertising, including interest-based advertising from us and third parties, which may
            mean that we share non-personally identifiable information to third-party advertisers;
         </li>
         <li>to respond to requests you make;</li>
         <li>to show you relevant information, if you connect to SchoolTorch through a Social Networking Site, from
            your Social Networking Site friends and connections, and to allow you to share relevant information with
            them; and
         </li>
         <li>to contact you regarding use of our services.</li>
      </ul>
      <h2>How We Share Information</h2>
      <p>We only disclose or share your information in the way that you authorize us to. By using SchoolTorch,
         you authorize us to share your information as described here.
      </p>
      <p> We may share personal information we collect with our trusted business partners. We also will share
         personal information with service providers that perform services on our behalf. These partners and
         service providers are not authorized by us to use or disclose the information except as necessary to
         perform services on our behalf or comply with legal requirements.
      </p>
      <p>We may access, preserve, and share information we have collected from and about you if we believe in
         good faith that such disclosure is necessary (1) to comply with relevant laws or to respond to subpoenas
         or warrants or legal process served on us; (2) to enforce our SchoolTorch Terms of Use and Privacy
         Policy; (3) when we believe disclosure is necessary or appropriate to prevent physical harm or financial
         loss or in connection with an investigation of suspected or actual illegal activity; or (4) as we otherwise
         deem necessary to protect and defend the rights or property of us, the users of our services, or third
         parties. Our general procedure with respect to civil subpoenas requesting user data is to require a court
         order, binding on SchoolTorch, before we release such information.
      </p>
      <p> If the ownership of SchoolTorch changes, we may transfer your information to the new owner so they can
         continue to operate the service. The new owner will still have to honor the commitments made in this
         Privacy and Cookie Policy.
      </p>
      <p>As described in Information We Collect and How We Use It, we may collect, use, transfer, and disclose
         non-personal information for any purpose. The section entitled Cookie Policy and Ad Choices explains
         how you can manage the collection, use and sharing of certain non-personally identifiable information we
         collect via cookies. 
      </p>
      <h2>Cookie Policy and Ad Choices</h2>
      <h3>Cookies</h3>
      <p> Cookies are small pieces of data that are stored on your computer, mobile phone, or other device when
         you first visit a page. We use cookies, web beacons, locally shared objects (sometimes called "flash
         cookies"), mobile identifiers and similar technologies ("Cookies") to help us recognize you on
         SchoolTorch, enhance your user experience, understand SchoolTorch usage, and show you relevant
         advertising. By visiting SchoolTorch, you consent to their placement of in your browser in accordance with
         this Privacy and Cookie Policy.
      </p>
      <h3>When do we use Cookies?</h3>
      <p>We may set Cookies may when you visit SchoolTorch. They may also be set by other websites or
         services that run content on the page you're visiting. After you register on SchoolTorch, we may connect
         information stored in Cookies with other information received from you.
      </p>
      <h3>What types of Cookies do we use?</h3>
      <p>We use two types of Cookies on SchoolTorch: "session cookies" and "persistent cookies." Session
         Cookies are temporary Cookies that remain on your device until you leave SchoolTorch. A persistent
         Cookie remains on your device for much longer until you manually delete it (how long the Cookie remains
         will depend on the duration or "lifetime" of the specific Cookie and your browser settings).
      </p>
      <h3>What are Cookies used for?</h3>
      <p>Cookies may transmit information about you and your use of SchoolTorch, such as your browser type,
         search preferences, data relating to advertisements that have been displayed to you or that you have
         clicked on, and the date and time of your use. The data stored in Cookies is anonymous and is not linked
         to your personally identifiable information without your permission. Cookies may link to certain unique,
         non-personally identifiable information such as the information that you entered at time of registration or
         on your profile, but this is not linked to your name or any personally identifiable information.
      </p>
      <p>We use Cookies for things like:</p>
      <ul>
         <li><strong>Authentication: </strong>to help us authenticate you to deliver personalized content. </li>
         <li><strong>Security: </strong> to protect you, us and others, and help us detect fraud and other violations of our Terms of
            Use. 					
         </li>
         <li><strong>Performance: </strong>to make SchoolTorch easier and faster to use.</li>
         <li><strong>Features: </strong>to enable features and store information about you (including on your device or in your browser
            cache) and your use of SchoolTorch.
         </li>
         <li><strong>Advertising: </strong> to deliver, evaluate, and improve advertising, such as by using non-personally identifiable
            information about you to provide you with relevant, targeted advertising (but using unique identifiers, that
            do not identify you personally, such as the information that you entered at time of registration or on your
            profile).				 
         </li>
         <li><strong>Analytics and Research: </strong> to monitor and evaluate the use of SchoolTorch.</li>
      </ul>
      <h3>Ad Choices and Managing Cookies</h3>
      <p>SchoolTorch works with several third parties to provide you with personalized, interest-based advertising.
         We may target ads to you on and off SchoolTorch using:
      </p>
      <ul>
         <li>Cookies (both on and off SchoolTorch);</li>
         <li>Location information, to the extent you have enabled location tracking on your mobile device;
         </li>
         <li>Member-provided profile information;</li>
         <li>Your use of SchoolTorch (for example, your SchoolTorch search history); and</li>
         <li>Information from advertising partners which we use to help deliver relevant ads to you.</li>
      </ul>
      <p>We do not share personally identifiable information with any third-party advertiser or ad network, although
         we do share certain information that does not identify you personally, but which is unique to your use of
         SchoolTorch, such as the information that you have entered at the time of registration or on your profile.
         We do not link your name when we provide this information to advertisers.
      </p>
      <p>We also work with third parties to provide analytics services that may use the Cookies set on your device
         to measure the performance of advertising and track traffic to SchoolTorch generally.
      </p>
      <p>You may be able to opt out of third-party advertiser and ad network placement of Cookies for targeted
         advertising by visiting the following links:<a href="http://www.networkadvertising.org/choices/">  Network Advertising Initiative</a>,  <a href="http://www.aboutads.info/choices/">Digital Advertising Alliance</a>,
         and <a href="https://www.google.com/settings/u/0/ads?sig=ACi0TCgfl2ZcxGHRFbAj3TdWffEdXKNhw8Fqs6M_g3fo7GSjaR3a_Qspyx08a__nbuLoHcXLZpK2yB8mLl0YeenLM9iAgpQoWZ0FW4CNhDbfKhUwkMax2Sp8OojvC0Tx8l2iIxr1SbF0lTowrV6EzDEe548_2eD8dMOT7yDKkdqcLF_d2FX15aA&hl=en">Google Ads Settings</a>. You may continue to receive generic ads by companies not listed with these
         opt-out tools.
      </p>
      <p>You may also be able to disable placement of some (but not all) Cookies by setting your browser to
         decline cookies, though this may worsen your user experience. If you delete your browser cookies, your
         opt-out cookie will also be deleted. Additionally, if you change computers or web browsers, you will need
         to opt out again. A useful resource for information about deleting and controlling cookies can be found
         at <a href="http://www.aboutcookies.org/">AboutCookies.org</a>.
      </p>
      <p>If you enable location data for the mobile version of SchoolTorch if developed (including any version
         installed as a native application), you are expressly agreeing that we may use your location data (after
         de-identifying it) to serve you geo-targeted ads and offers for businesses that are local to you. In such
         instances, we do not share your location with the advertiser or advertising network, rather, we provide the
         advertiser or advertising network with a means to push ads through to users located in certain areas or
         zip codes. You may disable location services at any time in your "settings."
      </p>
      <p><em>Please note: </em> if ads or other features on SchoolTorch are provided by third parties, those parties also may set and use their own Cookies that are subject to those third parties' privacy notices and policies.
         SchoolTorch does not have access to, or control over, these Cookies.
      <p>
      <h2>Other Important Privacy Information</h2>
      <h3>Choices Regarding Your Personal Information</h3>
      <p>You may change your personal information (e.g. email address) by logging into the service and editing
         your account information. We will provide you with the opportunity to 'opt-out' of having your personally
         identifiable information used for certain purposes, when we ask for this information.
      </p>
      <p>We may send you notifications, promotional communications, or other messages using the contact
         information (i.e. your e-mail address) you provided to us when you registered or if you requested
         information from us. You may opt-out of continuing to receive most messages by following the
         instructions included in each message. Also, you can control most of the e-mails you receive from us by
         editing your e-mail preferences within the user profile section on the site. 
      </p>
      <p>We may send you service-related announcements when we believe it is necessary to do so. Generally,
         you may not opt-out of these announcements, which are not primarily promotional in nature. If you do not
         wish to receive these announcements, you have the option to deactivate your account.
      </p>
      <p>In some cases, you can stop or limit the information we collect by automated means. To learn more about
         how you may be able to stop or limit our receipt of that information please review the section
         entitled Cookie Policy and Ad Choices.
      </p>
      <h3>Deleting Your Account</h3>
      <p>If you'd like to delete your SchoolTorch account entirely, simply contact <a href="mailto:team@schooltorch.com">team@schooltorch.com</a> and
         let us know that you would like to permanently delete your account. Upon account deletion you will no
         longer have full access to reviews, and all of your content will be pulled from the site. Any personal
         information collected will be deleted from our active databases but may remain in our archives.
      </p>
      <h3>Security Safeguards</h3>
      <p><strong>Security.</strong> We employ physical, electronic, and managerial measures to safeguard the information we
         collect online. However, no company can fully eliminate security risks, so we cannot make guarantees
         about any part of our services. You are responsible for keeping your username and password secret.
         Once you have registered with us, we will never ask you for your password.
      </p>
      <p>We do not collect sensitive information, such as credit card information or social security numbers. </p>
      <h2>YOU SHOULD NEVER INCLUDE SENSITIVE INFORMATION SUCH AS CREDIT OR DEBIT CARD
         NUMBERS, DRIVERS LICENSE NUMBERS, OR SOCIAL SECURITY NUMBERS IN CONTENT THAT
         YOU SUBMIT. WE CANNOT BE RESPONSIBLE FOR ANY INFORMATION CONTAINED IN
         UPLOADED CONTENT.  
      </h2>
      <p>If you see a suspicious posting, please email: <a href="mailto:team@schooltorch.com">team@schooltorch.com</a>. If you think you have been the
         victim of fraud, immediately report the fraud to your local police and contact SchoolTorch. We also
         recommend that you file an online report with <a href="http://www.ic3.gov/default.aspx"> The Internet Crime Complaint Center (IC3)						</a>.
      </p>
      <p><strong>Privacy of Minors</strong>. SchoolTorch is not targeted or directed to persons less than 13 years of age and we
         expressly prohibit any such minor person from using SchoolTorch or providing any personal information.
         If you become aware that a child has provided us with personal information without parental consent,
         please contact us at <a href="mailto:team@schooltorch.com">team@schooltorch.com</a>. If we become aware that a child under 13 has provided
         us with personal information without parental consent, we take steps to remove such information and
         terminate the child's account.
      </p>
      <h3>Updates to Our Privacy Policy</h3>
      <p>If we make changes to our Privacy and Cookie Policy, we will post the changes here and update the
         effective date at the top of our policy. If the changes we make are material, we will provide you additional,
         prominent notice as appropriate under the circumstances. Your continued use of SchoolTorch after we
         publish or provide notice of the changes to this Privacy and Cookie Policy means that you consent to the
         changes
      </p>
      <h3>Contact Us</h3>
      <p>If you have any questions or suggestions regarding our Privacy and Cookie Policy please contact us by
         sending us an email to <a href="mailto:team@schooltorch.com">team@schooltorch.com</a>.
      </p>


