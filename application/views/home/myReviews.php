<div class="profile-content">
	<div class="row">
			
		<div style="clear:both;" class="custom-responsive-table col-md-12" id="mySavedSchoolstable" >
			<div class="col-heading">My Reviews</div>

			<?php

			if(isset($myReviews) && count($myReviews) > 0)
			{

			?>
			
				<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">
						<tr>
							<th>Details</th> 
							<th>Submitted on</th>
							<th>Review status</th>
							<th>Action</th>
							
						</tr>
						<?php

						foreach ($myReviews as $key => $value) 
						{
							$approveTxt = "No";
							if($value['isApproved'] == 1)
							{
								$approveTxt = "Yes";
							}

							$schoolUrl = site_url('home/school')."/".$value['schoolId'].'/'.url_title($value['schoolName']);;
							?>
							<tr> 
								<td>
									<div class="responsive-table-th"><label>Details</label></div> 
									<div class="responsive-table-td">

										<div><a href="<?= $schoolUrl ?>"> <?= $value['schoolName'] ?></a></div>
										<div class="rating-star">

											<?php
												for ($i=1; $i<= $value['rating'];$i++) 
												{?>
													<span class="glyphicon glyphicon-star rating-star"></span>
												<?
												}
											?>
										</div>
										<div><?= $value['title']?> </div>
									</div>
								</td> 
								
								<td>
									<div class="responsive-table-th"><label>Submitted on</label></div> 
									<div class="responsive-table-td">
										<?=$value['createdDate']?>
									</div>
								</td>
								
								<td>
									<div class="responsive-table-th"><label>Review status</label></div> 
									<div class="responsive-table-td">
										<?=$approveTxt?>
									</div>
								</td>

								<td>
									<div class="responsive-table-th"><label>Action</label></div> 
									<div class="responsive-table-td">
										<a href="" class="btn-link">Delete</a>
									</div>
								</td>
								
								
							</tr>
							<?php
						}
						?>
				</table>
			<?php

				echo $create_link;
			}
			else
			{

			?>
					<h4>No Reviews Present </h4>

			<?php		
				
			}

			?>
		</div>
	</div>

</div>