<div class="main_container">
	<div class="container">
		<div class="row">


			<div class="col-md-12">

				<h1 class="main-header">
					SchoolTorch 
					<?php
						if(isset($isPrivacy)){
							
							echo 'Privacy Policy';
						}
						else if(isset($isTerms)){


						echo 'Terms';

						}else if(isset($isCommunity)){

							echo 'Community Guidelines';

						}else{

							echo 'School Torch';

						}
					?>





				</h1>


			
					<!-- Nav tabs -->
					<ul  class="nav nav-tabs">
						<?php
							if(isset($isPrivacy))
							{?>
								 <li role="presentation" class="active">
								 	<a href="<?=site_url()?>privacy">
								 		Privacy Policy
								 	</a>
								 </li>
							<?
							}
							else{
								?>
								 <li role="presentation"><a href="<?=site_url()?>privacy">Privacy Policy</a></li>
								<?

							}
						
							if(isset($isTerms))
							{?>
								 <li role="presentation" class="active" ><a href="<?=site_url()?>terms">Terms of Use</a></li>
							<?
							}
							else{
								?>
								 <li role="presentation" ><a href="<?=site_url()?>terms">Terms of Use</a></li>
								<?

							}
						
							if(isset($isCommunity))
							{?>
								 <li role="presentation" class="active"><a href="<?=site_url()?>guidelines">Community Guidelines</a></li>
							<?
							}
							else{
								?>
								 <li role="presentation"><a href="<?=site_url()?>guidelines">Community Guidelines</a></li>
								<?

							}
						?>
					   
					   
					   
					 </ul>

				  		<div class="row">
				  			<div class="col-md-12 tab-content">

				  		<?php

							if(isset($isPrivacy))
							{
								$this->load->view('home/privacy');
							
							}
							
						
							if(isset($isTerms))
							{
								$this->load->view('home/terms');
							
							}
							
						
							if(isset($isCommunity))
							{
							
								 $this->load->view('home/communityGuidelines');
							
							}
							
						
					   ?>
					
						</div>
					</div>


				
			</div>
		</div>
	</div>
</div>