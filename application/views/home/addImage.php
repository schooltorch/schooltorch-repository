<?php

	if(!isset($schoolId))
	{
		$schoolId = '';
	}
	

	if(isset($schoolName))
	{
		$schoolName = $schoolName;
	}
	else
	{
		$schoolName = '';
	}

?>


<div class="main_container">
  <div class="container">
    
        <div class="row">

        	<div class="col-md-12">
				<h1 class="main-header">School Photos</h1>
			</div>

			  <?php
		    	if(isset($status) && $status == "success")
		    	{?>
					<!-- 
					<div class="col-sm-12">
						<div class="wrapper bg_white">
							<h4> 
								Your image has been uploaded successfully
								
							</h4>
						</div>
					</div> -->
				<?php
		    	}
		    ?>
		    	


			<div class="col-sm-8">
				<div class="wrapper bg_white">
					<form  id="addImageForm"   role="form"  action="<?= site_url('home/addImage')?>" method="POST" class="form"  enctype="multipart/form-data">
						  <?php
					    	if(isset($status) && $status == "success")
					    	{?>
								<div class="alert alert-success">
									Your image has been uploaded successfully
								</div> 
							<?php
					    	}
					    ?>
						<div class="form-group">
							You may upload upto 5 photos at a time.
						</div>
						
						<div class="form-group">
						    <label for="schoolId">School</label>

						    <?php
						    	if(isset($schoolId))
						    	{
						    		$this->load->view('common/schoolAutocomplete', array('onlySchools' => 1, 'name' => 'id', 'schoolId' => $schoolId, 'schoolName'	=> $schoolName ));
						    	}
						    	else
						    	{
						    		$this->load->view('common/schoolAutocomplete', array('onlySchools' => 1, 'name' => 'id'));
						    	}
						    ?>
						    
						    <span class="help-block error-block"> <?php echo form_error('schoolId'); ?> </span>

						   
						</div>


						<div class="form-group">
								 <label for="photo1">Photo 1</label>
								<input type="file" name="photo1" size="20" />
						</div>
						<div class="form-group">
							 <label for="photo2">Photo 2</label>
							<input type="file" name="photo2" size="20" />
						</div>
						<div class="form-group">
							 <label for="photo3">Photo 3</label>
							<input type="file" name="photo3" size="20" />
						</div>
						<div class="form-group">
								 <label for="photo4">Photo 4</label>
								<input type="file" name="photo4" size="20" />
						</div>
						<div class="form-group">
								 <label for="photo5">Photo 5</label>
								<input type="file" name="photo5" size="20" />
								<?php
								if(isset($errors['photoSelect']))
								{?>
									
									<span class="help-block error-block"> <?php echo $errors['photoSelect'] ?> </span>

								<?
								}
								?>
						</div>
						<div class="form-group">
							<input type="submit" id="form_submit" class="btn btn-success submit" value="Upload Photos"/>
							
							<a href="javascript: history.go(-1)" class="btn btn-link">Cancel</a>
						</div>


						<!--<div class="form-group">
						<span class="help-block"><b>Note:</b> Minimum dimension for an image should be 600px x 400px (width x height)</span>
						</div>-->
					</form>
				</div>

			</div>

         	<div class="col-sm-4  blog-sidebar">
            	

				<?php

				 $this->load->view("/home/photo-guidelines");
				?>

				
	        </div>


	    </div>
	</div>
</div>

<script>

	var isLoggedIn = '<?= $this->tank_auth->is_logged_in();?>'; 

	$(window).load(function() {
	    if(!isLoggedIn)
	    {
	       $('#loginPopup').trigger('click');
	    }
	});

	$(document).ready(function(){

		$("#addImageForm").submit( function(eventObj){

			var schoolId = $("#searchSchool").attr('value');

			console.log(schoolId);

			$(this).append('<input type="hidden" name="schoolId" value="'+schoolId+'" /> ');               
			
		});
	});

</script>