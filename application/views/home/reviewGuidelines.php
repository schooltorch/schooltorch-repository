<p>We are committed to protecting our community from fraudulent submissions. We review and approve every post before it appears on our site. Let us know if you see something suspicious - we will look into the issue and take appropriate action.</p>

<br/>


<p><b>We will not post reviews containing:</b><p>
<br/>
<p>- Personal insults or defamation</p>
<p>- Rants, venting, obscenities</p>
<p>- Aggressive or discriminatory language</p>
<p>- Irrelevant content</p>
<p>- ALL CAPS text or poor grammar</p>
<br/>
<p>Please refer to our <a href="<?=site_url()?>guidelines"><b>Community Guidelines</b></a> for more information. </p>