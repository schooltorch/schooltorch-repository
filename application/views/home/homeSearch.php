<?php  
	
	$this->load->config('major');
	
	$major 	=	$this->config->item('major');

	$major 	=	htmlspecialchars(json_encode($major));

?>
<div class="container">
	<div class="lead">
		Discover <span>your</span> education. Find <span>your</span> grad school.
	</div>

	<form role="form" class="search_form has-feedback form-inline" action="<?=site_url()?>home/schools" method="post">

		<div class="form-group">
	  		<input type="text" class="input-lg" id="search" style="width:100%" placeholder="Search for School">
		</div><div class="form-group">
  			<input type="text" class="input-lg" id="searchCity" style="width:100%" placeholder="Location">
		</div><div class="form-group">
	  		<div class="input-group">
	  			<input type="text" class="input-lg" id="searchMajor" style="width:100%" placeholder="Search for Major">
	  			<div class="input-group-addon" id="submitSearch"><i class="glyphicon glyphicon-search icon"></i></div>
	  		</div>
	  	</div>

	</form>

</div><!--/container -->


<script type="text/javascript">
	
	$(document).ready(function(event){

		$('#submitSearch').unbind('click');
		$('#submitSearch').click(function(event){

			window.location.href 	=	schoolApp.site_url+'home/schools';

		});

		var majors 	=	JSON.parse(schoolApp.htmlspecialchars_decode("<?=$major?>"));

		var object 	=	[];

		$.each(majors, function(key, value){

			object.push({
							label 	:  	value,
							value 	:  	value,
							id 		: 	key
						});

		});

		$("#searchMajor").autocomplete({
	        source: object,
	        minLength: 1,
	        select: function( event, ui ) {
	        	
	        	event.preventDefault();
	        	event.stopPropagation();
	        	event.stopImmediatePropagation();

	        	callIsGoing     =   1;
	        	
	            window.location = schoolApp.site_url+'home/schools?major='+ui.item.id;
	        }
	    });

	})

</script>