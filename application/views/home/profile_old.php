<?php

$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'class' =>  'form-control',
	'value'	=> set_value('email')?set_value('email'):$user['email'],
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'email',
	
);

$fname = array(
	'name'	=> 'firstName',
	'id'	=> 'firstName',
	'class' =>  'form-control',
	'value'	=> set_value('firstName')?set_value('firstName'):$user['firstName'],
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'text',
	
);

$lname = array(
	'name'	=> 'lastName',
	'id'	=> 'lastName',
	'class' =>  'form-control',
	'value'	=> set_value('lastName')?set_value('lastName'):$user['lastName'],
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'text',
	
);

$oldPassword = array(
	'name'	=> 'old_password',
	'id'	=> 'old_password',
	'class' =>  'form-control',
	'value'	=> set_value('old_password'),
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'password',
	
);

$newPassword = array(
	'name'	=> 'new_password',
	'id'	=> 'new_password ',
	'class' =>  'form-control',
	'value'	=> set_value('new_password'),
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'password',
	
);

$confirmPassword = array(
	'name'	=> 'confirm_new_password',
	'id'	=> 'confirm_new_password',
	'class' =>  'form-control',
	'value'	=> set_value('confirm_new_password'),
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'password',
	
);

?>

<div class="main_container">
	<div class="container">
	
	<div class="row">

		<div class="col-md-3">
			<div class="wrapper">
				<ul class="nav nav-pills nav-stacked">
			        <li class="<?=$activeTab=='basicInfoTab'?'active':''?>"><a data-toggle="tab" href="#basicInfoTab">Basic Info</a></li>
			        <li class="<?=$activeTab=='passwordTab'?'active':''?>"><a data-toggle="tab" href="#passwordTab">Password</a></li>
			        <li class="<?=$activeTab=='mySchoolsTab'?'active':''?>"><a data-toggle="tab" href="#mySchoolsTab">My Schools</a></li>
			        <!-- <li class="<?=$activeTab=='socialConnect'?'active':''?>"><a data-toggle="tab" href="#socialConnect">Facebook Connect</a></li> -->
			    </ul>
			 </div>

		</div>

		<div class="col-md-9">

			<div class="wrapper bg_white">

				<div class="tab-content">
			        
					<?php
			         		
		         		if(isset($status) && $status == "success"){
		         			?>
		         				<div class="alert alert-success alert-dismissible" role="alert">
		         					
		         					<button type="button" class="close" data-dismiss="alert">
		         						<span aria-hidden="true">&times;</span>
		         						<span class="sr-only">Close</span>
		         					</button>

		         					<?=$message;?>

		         				</div>
		         			<?php		
		         		}

		         	?>	

			        <div id="basicInfoTab" class="tab-pane fade <?=$activeTab=='basicInfoTab'?'in active':''?>">
			            
						<div class="col-heading">Basic Info</div>

						<div class="row tabContent">
							<div class="col-md-6">
								 <form role="form" class="" method="post" action="<?=site_url();?>home/profile">

						            	<div class="form-group">

						            		<label for="fname" class="control-label">Email Address</label>

						            		<div class="">
												<?php echo form_input($email); ?>
												<span class="help-block error-block"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></span>	
											</div>	

						            	</div>

						            	<div class="form-group">

						            		<label for="fname" class=" control-label">First Name</label>

						            		<div class="">
						            			<?php echo form_input($fname); ?>
												<span class="help-block error-block"><?php echo form_error($fname['name']); ?><?php echo isset($errors[$fname['name']])?$errors[$fname['name']]:''; ?></span>	
						            		</div>	

						            	</div>

						            	<div class="form-group">

						            		<label for="lname" class=" control-label">Last Name</label>

						            		<div class="">
						            			<?php echo form_input($lname); ?>
												<span class="help-block error-block"><?php echo form_error($lname['name']); ?><?php echo isset($errors[$lname['name']])?$errors[$lname['name']]:''; ?></span>	
						            		</div>	

						            	</div>	

						            	<div class="form-group">

						            		<button type="submit" class="btn btn-success ">Save</button>

						            	</div>

						            </form>

							</div>
						</div>
			         	
			           	<div class="col-heading">Facebook</div>

			           	<div class="row tabContent">
							<div class="col-md-6">

								<?php
								if(!$this->session->userdata('facebook_id'))
								{
								?>
									<div class="fb-login-button" data-max-rows="1" data-size="large" data-show-faces="false" data-auto-logout-link="false" onlogin='window.location="https://graph.facebook.com/oauth/authorize?client_id=<?=$this->config->item('appId'); ?>&redirect_uri=<?=site_url('auth_other/fb_signin'); ?>&amp;r="+window.location.href;'>Connect Facebook</div>
								<?php
								}
								else
								{
								?>	
									<div class="fb-disconnect-button">
										<button class="btn btn-danger" id="disconnectFacebook">Disconnect Facebook</button>
									</div>
								<?php
								}
								?>	
							</div>

						</div>

			        </div>

			        <div id="passwordTab" class="tab-pane fade <?=$activeTab=='passwordTab'?'in active':''?>">
			            
			           	<div class="col-heading">Password</div>
			           	
			           	<div class="row tabContent">
							
							<div class="col-md-6">
								 	
								<form role="form" class="" method="post" action="<?=site_url();?>home/profile">

					            	<div class="form-group">

					            		<label for="old_password" class=" control-label">Current Password</label>

										<a href="<?=site_url();?>auth/forgot_password" class="pull-right" style="font-size: 12px; color: #BDBDBD;">Forgot Password</a>

					            		<div class="">
					            			<?php echo form_input($oldPassword); ?>
											<span class="help-block error-block"><?php echo form_error($oldPassword['name']); ?><?php echo isset($errors[$oldPassword['name']])?$errors[$oldPassword['name']]:''; ?></span>	
					            		</div>	

					            	</div>	

					            	<div class="form-group">

					            		<label for="new_password" class=" control-label">New Password</label>

					            		<div class="">
					            			<?php echo form_input($newPassword); ?>
											<span class="help-block error-block"><?php echo form_error($newPassword['name']); ?><?php echo isset($errors[$newPassword['name']])?$errors[$newPassword['name']]:''; ?></span>	
					            		</div>	

					            	</div>	

					            	<div class="form-group">

					            		<label for="confirm_new_password" class="control-label">Confirm Password</label>

					            		<div class="">
					            			<?php echo form_input($confirmPassword); ?>
											<span class="help-block error-block"><?php echo form_error($confirmPassword['name']); ?><?php echo isset($errors[$confirmPassword['name']])?$errors[$confirmPassword['name']]:''; ?></span>	
					            		</div>	

					            	</div>

					            	<div class="form-group">
					            		
					            		<button type="submit" class="btn btn-success">Save</button>
					            		
					            	</div>

					            </form>	

							</div>

						</div>

			           

			        </div>

			        <div id="mySchoolsTab" class="tab-pane fade <?=$activeTab=='mySchoolsTab'?'in active':''?>">

			            	<div class="col-heading">My Saved Schools</div>
			           	
			           	<div class="tabContent">	

			           		<ul class="school_list">
							
			           		<?php

			           			if($mySchools['count']>0)
								{

									foreach($mySchools['data'] as $row){
										
										$photos = getSchoolImage($row->UNITID);
										$imgUrl = '';

										if(count($photos) > 0){
											
											if(isset($photos[0]))
											{	
												$imgUrl = locationPicsBaseUrl().$photos[0];
											}
											else
											{
												$imgUrl = base_url().'assets/images/sample.jpg';
											}

										}
										else{
											$imgUrl =  base_url().'assets/images/sample.jpg';
										}

										?>

										<li>
											<div class="row">

											<div class="col-md-4 col-xs-4" >

												<div class="img img pull-left" style="background: url('<?=$imgUrl?>') no-repeat  0 0 transparent; background-size: cover;width:100%;">


												</div>

												
			 								</div>

											<div class="img-content  col-md-8 col-xs-8">

												<h4>

													<?php
														
														$isSaved 	= 0;
														
														$savedText 	= 'Save school';

														if($row->SAVEDID && $row->SAVEDID!=""){
															$isSaved 	= 1;
															$savedText 	= 'Remove';
														}
														
													?>

													<a href="#" class="btn btn-default pull-right saveSchool btn-link-success" isAct="1" isSaved="<?=$isSaved;?>" dataSchoolId="<?=$row->UNITID;?>" dataUserId="<?=$user_id;?>"><?=$savedText;?></a>

													<?php

													$inst_url = site_url().'home/school/'.$row->UNITID.'/'.url_title($row->INSTNM);

													?>

													<a href="<?=$inst_url;?>" class="btn-link"><?=$row->INSTNM;?></a>


												</h4>

												<div class="location">
													<?=$row->CITY;?>, 
													<?=$row->STATENAME;?>
												</div>

												<div class="row school-details">
													<div class="col-xs-6">
														<label>Tuition:</label> <?=$row->TUITION==""?"N/A":"$".$row->TUITION;?>

													</div>

													<div class="col-xs-6">
														<label>Setting:</label> <?=$row->LOCALE;?>
													</div>

												</div>


												<div class="row school-details">
													<div class="col-xs-6">
														<label>Acceptance:</label> <?=$row->ACCEPTANCE;?>%

													</div>

													<div class="col-xs-6">
														<label>Selectivity:</label> <?=parseAcceptance($row->SELECTIVITY);?>
													</div>

												</div>


												<div class="row school-details">
													<div class="col-xs-6">
														<label>Total Students:</label> 

														<?php 
														if($row->TOTALSTUDENTS == 0){
															echo 'N/A';
														}
														else
														{
															echo $row->TOTALSTUDENTS;
														}	
														?>
													
													</div>

												</div>
											
											</div>
											
										</li>

										<?php

									}

								}
								else
								{
									?>

										<li><div>You do not have any Saved Schools.</div></li>

									<?php
								}

							?>

							</ul>

			           	</div>

			        </div>
			        
			       
			    </div>

			</div>

		</div>


	</div>
	
</div>		

</div>














