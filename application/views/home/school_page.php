 <?php

	$noGraphMsg = 'We don\'t have enough data to generate this Report';
 ?>

 <style>
 .active{
 	color: red;
 }

 .carousel-indicators{
 	display: none;
 }
 </style>

 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

 <style>
      	#map-canvas {

	        height: 280px;
	        margin: 0px;
	        padding: 0px
      	}

      	
</style>

<?php

	if(!isset($user_id))
	{
		$user_id 	=	0;
	}

	$ratings 	=	0;

	if(isset($chart))
	{
		

		if(isset($chart['ratings']))
		{
			$ratings 	=	$chart['ratings'];
		}

	}
	else
	{
		$chart 	=	array(
							'aspectsChart' 				=>	array(),
							'likesChart' 				=>	array(),
							'jobProspectsChart' 		=>	array(),
							'fundOpportunitiesChart' 	=>	array(),
							'ratings' 					=>	0,
							'recommendation' 			=>	array(),
							'reviews'					=>	array()
						);
	}

	$className 			=	'';
	$insideClassName 	=	'';

?>

<div id="school-details" class="main_container">
	<?php
		$this->load->view("home/homeSearch");
	?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
			
				<h1 class="main-header">
					<?=$school['INSTNM']?>


					<div class="survey pull-right">
						<?php
							$reviewUrl = site_url('/reviews/createReview').'/'.$school['UNITID'].'/'.url_title($school['INSTNM']);
							$firstReviewFlag = 1;
							if($averageReviewRating)
							{
								$firstReviewFlag = 0; // i,e Already reviewed
								
								$ratingSrt = '';
								for ($i=1; $i <= 5 ; $i++) 
								{ 
									if($i <= floor($averageReviewRating))
									{
										$ratingSrt.='<span class="glyphicon glyphicon glyphicon-star rating-star" aria-hidden="true"></span>';
									}
									else
									{
										$ratingSrt.='<span class="glyphicon glyphicon glyphicon glyphicon-star-empty rating-star" aria-hidden="true"></span>';
									}
									
								}
								echo $ratingSrt;
							}
							else
							{
							?>
								
								<a href="<?= $reviewUrl?>" class="btn-link loginCheck" >Be the first to review.</a>
							<?
							}
							

						?>
						
					</div>


					<div class="info">

						<?php
								if(isset($school['WEBADDR']) && $school['WEBADDR']!=""){
									?>
									<a href="<?=prep_url($school['WEBADDR']);?>" class="" target="_blank">
										<?= strtolower($school['WEBADDR']);?>
									</a>
									<?php
								}
						?>
						<div class="survey pull-right">

							<a href="<?= site_url('/home/addImage')?>/<?=$school['UNITID'];?>/<?= url_title($school['INSTNM'])?>" class="btn btn-warning loginCheck" ><i class="icon icon-plus"></i> Photos</a>
							<?php
								if($firstReviewFlag != 1)
								{?>
									
								<a href="<?= $reviewUrl ?>" class="btn btn-danger loginCheck" ><i class="icon icon-plus"></i> Review</a>
								<?php
								}
							?>
							

							
						</div>	
					</div>



				</h1>
			</div>
		</div>

		


	
		<div class="row">
			<div class="item col-md-6">
				<div class="item-content">
					<div class="heading">
						Overview
					</div>
					<div class="well">
						
						
						<div class="row">
							<div class="col-md-3 col-xs-6">
								<label>Selectivity: </label>
							</div>

							<div class="col-md-9 col-xs-6">
								<span class="circle-<?=$school['SELECTIVITY'];?> indicator indicator-circle" title="<?=$school['SELECTIVITY'];?>"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 col-xs-6">
								<label>Program Cost: </label>
							</div>

							<div class="col-md-9 col-xs-6">
								<span class="dollar-<?=$school['TUITIONLABEL'];?> indicator indicator-dollar" title="<?=$school['TUITION'];?>"></span>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 col-xs-6">
								<label>Enrollment: </label>
							</div>

							<div class="col-md-9 col-xs-6">
								<span class="circle-<?=$school['TOTALSTUDENTSLABEL'];?> indicator indicator-circle" title="<?=$school['TOTALSTUDENTS'];?>"></span>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-3 col-xs-6">
								<label>Public/Private: </label>
							</div>

							<div class="col-md-9 col-xs-6">
								<?php
									if($school['OPENPUBL'] == 0){
										echo 'Private';
									}
									else
									{
										echo 'Public';
									}
								?>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<div class="overview-options">
									<span class="count">
									<?php
									if($school['VIEWSCOUNT'] == NULL || $school['VIEWSCOUNT'] == 0)
									{
										echo 0;
									}
									else{
										echo $school['VIEWSCOUNT']; 
									}
									?>			
									</span>
									<span class="voteText">
										View(s)
									</span>
								</div>
							</div>

						</div>



						
					</div>
				</div>
			</div>

			<div class="item col-md-6">
				<div class="item-content">
					<div class="heading">
						<?=$school['CITY'];?>, <?=$school['STABBR'];?> <span class="pull-right">Setting: <?=$locale[$school['LOCALE']];?></span>
					</div>
					<div class="well">
						<div id="map-canvas" latitude="<?=$school['LATITUDE'];?>" longitude="<?=$school['LONGITUD'];?>"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
		
			<div class="item col-md-6">
				<div class="item-content">
					<div class="heading">
						Demographics
					</div>
					<div class="well">
						<div class="row">
							<div class="col-md-6">
								<div class="highchart-container">
						       		<div id="demographics1" style="min-height:200px;"></div>
						       	</div>
							</div>
							<div class="col-md-6">
								<div class="highchart-container">
						       		<div id="demographics2" style="min-height:200px;"></div>
						       	</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="item col-md-6">
			<div class="item-content">
						
					<div class="heading">
						Top 5 Graduate Courses
					</div>
					<div class="well">
						<ul class="grad-courses">
								<?php
									if(isset($school['topMajors']))
									{
										if($school['topMajors']['status'] == 'success' && count($school['topMajors']['data']) > 0)
										{
											
											$courseCount = 1;
											foreach($school['topMajors']['data'] as $row)
											{
												$code 	=	floor($row['CIPCODE']);
								?>
												
													<li>
														<div class="pull-left count">#<?=$courseCount;?></div>
														<div class="name"><a href="<?=site_url('home/schools?major='. $code);?>" class="btn-link"><?=$row['valuelabel']?></a></div>
													</li>


													<?php
														$courseCount++ ;

													?>
												
								<?php
											}
										}
										else
										{
								?>
											<li class="error" style="margin: 30px auto;text-align: center;font-size: 16px;color: #ccc;">
												Not application for Undergraduate schools
											</li>
								<?php
										}
									}
								?>
						</ul>
					</div>
				</div>
				</div>

		</div>

		<div class="row">

			<div class="item col-md-6">

				<?php
						if(count($school['photo']) > 0)
							{ 

								//dump($school['photo']);
							?>
							
								<div class="item-content">
								<div class="heading">Gallery</div>
									<div class="well">
										<div id="school-photos" class="carousel slide" data-ride="carousel">

										<?php
										//dump($school['photo']);
										?>
										  <!-- Indicators -->
										  <ol class="carousel-indicators">
										  	<?php
										  		$activeClass = "active";
										  		foreach($school['photo'] as $photo)
										  		{	
										  			echo '<li data-target="#school-photos" data-slide-to="0" class="'. $activeClass.'"></li>';

										  			$activeClass = '';
										  		}
										  	?>
										  </ol>

										  <!-- Wrapper for slides -->
										  <div class="carousel-inner">
											  	<?php

											  		$activeClass = "active";
											  		foreach($school['photo'] as $photo)
											  		{
											  			$optionStr = '';

											  			if($this->tank_auth->is_logged_in())
														{
															if(in_array($photo, $likedPhotos))
															{
																$optionStr = '<button type="button" class="btn btn-primary" disabled="disabled">Liked</button>';
															}
															else if(strpos($photo, 'locationPics') === 0)
															{
																$optionStr = '<button type="button" class="btn btn-info likePhoto" data-unitid="'. $school['UNITID'] .'" data-imgUrl="'. $photo .'">Like</button>';
															}
														}?>	
											  	
											  				<div class="item <?=$activeClass;?>" style="background-image:url('<?=locationPicsBaseUrl().$photo;?>')">
														    
														      


														      <div class="carousel-caption">
														        	<?php //commented by NEIL$optionStr;?>
														      </div>
														   </div>

											  			<?php	
											  			$activeClass = "";	
											  		}
											  	?>
										  
										  </div>

										  <!-- Controls -->
											  <a class="left carousel-control" href="#school-photos" role="button" data-slide="prev">
											    <span class="glyphicon glyphicon-chevron-left"></span>
											  </a>
											  <a class="right carousel-control" href="#school-photos" role="button" data-slide="next">
											    <span class="glyphicon glyphicon-chevron-right"></span>
											  </a>
										</div>
									</div>

								</div>

						
								
						

								<script>
									//$('.carousel').carousel();
									$('.likePhoto').click(function(){
										console.log("UNITID: "+ $(this).attr('data-unitid'));
										console.log("imgUrl: "+ $(this).attr('data-imgUrl'));
										$(this).attr('disabled', 'disabled');
										var target = $(this);
										$.ajax({
											url: 		'<?=site_url();?>home/likePhoto',
											type: 		"POST",
											dataType: 	"json",
											data: 		{
															"UNITID": $(this).attr('data-unitid'),
															"imgUrl": $(this).attr('data-imgUrl')
														},
											success: 	function(response){
															if(response.status == 'success')
															{
																target.removeClass('btn-info').addClass('btn-primary').html('Liked');
															}
															else
															{
																alert(response.message);
																location.reload();
															}														
														},
											error: 		function(){
															alert("Something went wrong!");
															location.reload();
														}
										});
									});
								</script>

							<?
							}
							?>



				<div class="item-content">
						
					<div class="heading">
						Similar Schools
					</div>
					<div class="well">
						<div id="similarSchools">
						</div>
					</div>

				</div>

				


			</div>

			<div class="item col-md-6">
				
				<div class="item-content">
								<div class="heading">
									Reviews
								</div>
								<div class="well" id="reviews">
									
										<div class="sort-bar">
											<div id="review-order" class="pull-right">
												 
													<a class="order asc glyphicon glyphicon-chevron-up" data-order="asc"></a> 
													<a class="order desc glyphicon glyphicon-chevron-down active" data-order="desc"></a> 
												
											</div>

											<div id="review-order-by">
												<label>Sort</label> 
													<a data-order-by="helpfulFlagCount" id="sort-popular" class="orderby btn-link active">POPULAR</a> | 
													<a data-order-by="rating" id="sort-rating" class="orderby btn-link"> RATING </a> | 
													<a data-order-by="createdDate" id="sort-created-date" class="orderby btn-link"> Created</a>
											</div>
											
										</div>
									
									
									
									
								</div>
				</div>
			</div>

		</div>


		


		
	
		</div>
	</div>
</div>
<script type="text/javascript">

	var isLoggedIn = '<?= $this->tank_auth->is_logged_in(); ?>';

	if(isLoggedIn)
	{
		var loggedUserId = "<?=  $this->session->userdata('user_id'); ?>";
	}

	$("#input-ratings").rating({
		disabled: true, 
		showClear: false, 
		showCaption: false,
		starCaptions: [],
		size: 'xs'
	});

	function commonErr(str)
	{

	}

	function bindViewMore()
	{

		$('#reviewsMore').unbind('click');

		$('#reviewsMore').click(function(event){

			var lastId 	=	$('#reviewsMore').attr('last-id');
			$(this).html('Loading...');
			if(lastId)
			{
			  postReview(lastId);
			}

		})

	}

	function processReviews(data)
	{

		var str 			=	'';

		var viewMoreFlag	=	0;

		if(data.count <= +data.limit)
		{
			
		
			if(+data.page_no*data.limit == +data.total_count || +data.page_no*data.limit > +data.total_count)
			{
				viewMoreFlag 	=	0;
			}
			else{
				viewMoreFlag 	=	1;
			}
			//data.data.pop();
		}
		else
		{
			viewMoreFlag 	=	1;
		}

		var lastId 	=	0;

		

		$.each(data.data, function(key, value){

			lastId 	=	value.id;

			var helpfulStr = '<span id="review-helpflag-count-top-'+value.id+'">'+0+'</span> people found this helpful';

			if(value.helpfulFlagCount == null)
			{
				value.helpfulFlagCount= 0;

			}
			if(value.flagCount == null)
			{
				value.flagCount= 0;
				
			}
			if(value.helpfulFlagCount != null && value.helpfulFlagCount != 0 && value.helpfulFlagCount != '')
			{
				helpfulStr = '<span id="review-helpflag-count-top-'+value.id+'">'+value.helpfulFlagCount+'</span> people found this helpful';
			}
			var ratingSrt = '';

			for (var i = 1; i <= 5; i++) 
			{
				if(i <= value.rating)
				{
					ratingSrt+='<span class="glyphicon glyphicon glyphicon-star rating-star" aria-hidden="true"></span>';
				}
				else{
					ratingSrt+='<span class="glyphicon glyphicon glyphicon glyphicon-star-empty rating-star" aria-hidden="true"></span>';
				}
				
			};

			var flagAcitveClass  ="";
			var flaghelpfulClass ="";

			if(value.flagCount > 0 )
			{
				flagAcitveClass = "btn-warning";
			}

			// for adding class active to the flagCount buttons
			if(isLoggedIn)
			{

				if(typeof value.loggedUserFlag != "undefined")
				{
					if(value.loggedUserFlag == 1)
					{
						flagAcitveClass = "btn-danger";

					}
				}

			

				if(typeof value.loggedUserHelpfulFlag != "undefined")
				{	
					//console.log("here in helpfule flag count");
					//console.log(value.loggedUserHelpfulFlag);
					if(value.loggedUserHelpfulFlag == 1)
					{
						
						flaghelpfulClass = "btn-success";

					}
					
				}
			}
			var displayName ="Anonymous";

			if(typeof typeof value.userdata != "undefined")
			{
				console.log(value);

				if(typeof value.userdata.displayName == "undefined" || value.userdata.displayName == "")
				{

				}
				else
				{
					displayName  = value.userdata.displayName;
				}
			}





			var gradType = "Grad";
			
				if(+value.gradType == 2)
				{
					gradType ="Undergrad";
				}
			


				
				console.log(value.userType);


			
			str 	+=	'<div class="review-item">'+

								

										'<div class="row">'+

											'<div class="col-md-12 date">'+
											 value.createdDate +
											'<span class="pull-right">'+helpfulStr+ '</span>'+
								

											'</div>'+

											'<div class="col-md-2 col-sm-2 col-xs-3">'+

												'<img src="<?=base_url()?>/assets/images/profile.png" class="img-responsive"/>'+
											'</div>'+


											'<div class="col-md-10 col-sm-10 col-xs-9">'+
												'<div class="title">'+
														value.title +
												'</div>'+
												'<div class="info">'+
													'<cite>- '+
														displayName+' - '+ value.userTypeName +" - "+gradType+" - "+value.major+
													'</cite>'+
												'</div>'+
												'<div class="rating">'+
													'<!--<b>'+ value.userdata.displayName +'</b><br/>-->'+
													ratingSrt+
												'</div>'+
												'<div class="content row">'+
														'<div class="col-md-3 col-sm-3"><label>Good</label></div>'+
														'<div class="col-md-9 col-sm-9">'+value.good +'</div>'+
														 

												'</div>'+
												'<div class="content row">'+
														'<div class="col-md-3 col-sm-3"><label>Bad</label></div>'+
														'<div class="col-md-9 col-sm-9">'+value.bad+'</div>'+
														
												'</div>'+
												'<div class="content row">'+
														'<div class="col-md-3 col-sm-3"><label>Best Features</label></div>'+
														'<div class="col-md-9 col-sm-9">'+value.featureStr+'</div>'+
														
												'</div>'+

												'<div class="content row">'+
														'<div class="col-md-3 col-sm-3"><label>Culture</label></div>'+
														'<div class="col-md-9 col-sm-9">'+value.cultureStr+'</div>'+
														
												'</div>'+
												'<div class="action">'+
													'<a id="markFlag-'+value.id+'"class="markFlag  btn btn-default pull-right '+flagAcitveClass+'" data-id="'+value.id+'" data-count="'+value.flagCount+'"><i class="icon icon-flag"></i> (<span id="review-flag-count-'+value.id+'">'+value.flagCount+'</span>)</a>'+

													'<a id="markHelpFlag-'+value.id+'"class="markHelpFlag  btn btn-default '+flaghelpfulClass+'" data-id="'+value.id+'" data-count="'+value.helpfulFlagCount+'">Helpful (<span id="review-helpflag-count-'+value.id+'">'+value.helpfulFlagCount+'</span>)</a>'+
												'</div>'+

											'</div>'+


										'</div>'+

								


						'</div>';

		});

		if(viewMoreFlag == 1)
		{
			str 	+=	'<div class="viewMore btn btn-link" id="reviewsMore" last-id="'+ (+data.page_no +1) +'">View More</div>';
		}

		$('#reviewsMore').remove();

		$('#reviews').append(str);


		//schoolApp.loginCheck();

		bindViewMore();

	}

	function reviewCall(params,lastId)
	{
		
		
		var postObj 		= {};

		postObj['schoolId'] = "<?=$school['UNITID'];?>";

		if(typeof lastId != 'undefined')
		{
			lastId 	=	'/'+ lastId;

		}
		else
		{
			lastId 	=	'';
		}

		if(typeof params != "undefined")
		{
			console.log("if");
			postObj['order_by'] = params['order_by'];
			postObj['order']    = params['order'];
		}
		else{
			console.log("else");
			console.log(typeof postObj);
		}

		$.ajax({
				//url: 		"<?=site_url();?>home/reviews/"+ "<?=$school['UNITID'];?>" + lastId,
				url: 		"<?=site_url();?>home/reviews"+ lastId,
				type: 		"GET",
				dataType: 	"JSON",
				data: 		postObj,
				success: 	function(response){
								
								try{
									
									console.log(response);
									if(+response.count > 0)
									{
										console.log("getting inside the function");
										console.log(response);
										if(lastId == '')
										{
											//$('#reviews').empty();
										}
										processReviews(response);
									}
									else
									{
										if(lastId == '')
										{

											$('#reviews').empty();

											$('#totalResults').html('Total Results: '+ response.totalCount);

											$('#reviews').append('<div id="error-404"><br/><a href="<?= $reviewUrl?>" class="btn-link loginCheck" >Be the first to review.</a></div>')
										}
										else
										{
											$('#loadmoreBar').parent().remove();
										}
									}
								}
								catch(e){
									console.log(e);
									commonErr('Something went wrong..');
								}
							},
				error: 		function(){
								console.log('Reviews wrong');
								commonErr('Something went wrong..');
							}

			});
	}

	// function to get data based or sorting
	function postReview(lastId)
	{	

		var orderby  = $("#review-order-by a.active").attr("data-order-by");
		var order    = $("#review-order a.active").attr("data-order");

		var params = {
						'order_by': orderby,
						'order'	  : order,
					 }

		reviewCall(params,lastId);

	}

	postReview();

	
	var currentDefaultPos = 0;

	function formatSchools(data)
	{

		var str =	'';

		var count 	=	1;

		$.each(data.data, function(key, value){

			if("<?=$school['UNITID'];?>" != ""+value.UNITID)
			{
				if(count <= 5)
				{
					var url 	=	"<?=site_url()?>home/school/"+ value.UNITID +"/"+ value.URLTITLE;
					
					var imgStr 	= 	'';

					if(value.photo)
					{
						if(value.photo != '')
						{

							value.photo 	=	value.photo.split('[|]');

							value.photo 	=	value.photo[0];

							imgStr 			=	 '<?=base_url();?>'+value.photo 
						}
					}

					if(imgStr == '')
					{
						imgStr 				= 	'<?=base_url();?>assets/images/defaults/default-'+ currentDefaultPos +'.jpg';

						currentDefaultPos++;
						
						if(currentDefaultPos == 11)
						{
							currentDefaultPos = 0;
						}
					}

					str +=	
									'<div class="row item">'+
										'<a href="'+ url +'">'+
											'<div class="item col-md-3 col-sm-3 img col-xs-4">'+
												'<div class="school-photo" style="background-image: url('+imgStr+')">'+

												'</div>'+
											'</div>'+
											'<div class="item col-md-9 col-sm-9 img-content col-xs-8">'+
												'<div class="name">'+ value.INSTNM +'</div>'+
												'<div class="location">'+ value.CITY +', '+ value.STABBR +'</div>'+
											'</div>'+
										'</a>'+
									'</div>';
							

				

				}
				count++;
			}

		})



	

		$('#similarSchools').html(str);

	}

	

	function similarSchools()
	{

		var filterOpts =	{
								instsize		: 	"<?=$school['TOTALSTUDENTSLABEL'];?>",
								selectivity		: 	"<?=$school['SELECTIVITY'];?>",
								tuition			: 	"<?=$school['TUITIONLABEL'];?>",
								orderby 		: 	'SELECTIVITY',
								order 			: 	'DESC',
								limit 			: 	10,
								offset			: 	0,
								similarSchools 	: 	1
							};

		$.ajax({
				url: 		'<?=site_url();?>search/advanced',
				type: 		"GET",
				dataType: 	"json",
				data: 		filterOpts,
				success: 	function(response){
								try{
									if(+response.count > 1)
									{
										//lg('process!');
										formatSchools(response);
									}
									else
									{
										$('#similarSchools').parent().hide();
									}
								}
								catch(e){
									//lg(e);
									commonErr('Something went wrong..');
								}
							},
				error: 		function(){
								commonErr('Something went wrong..');
							}

			});
	}

	similarSchools();























$(document).ready(function(){

		var schoolId  = "<?=$school['UNITID'];?>";

		var globalSiteUrl = "<?= site_url()?>";

		console.log(schoolId);

		$("#reviews").on("click",'.markFlag',function(e){

			e.preventDefault();


			if(!isLoggedIn)
            {
               
                $('#loginPopup').trigger('click');
            }else{
            	var target = $(this);

				var reviewId = target.attr("data-id");
				var flagCount = target.attr('data-count');
				var flag     = 1;


				var postData = {
								reviewId:reviewId,
			            		schoolId:schoolId,
			            		flag :'flag',

							 }


				//pre response increment
				



				if($(this).hasClass('btn-danger')){

					target.attr('data-count', +(flagCount)-1);
					$('#review-flag-count-'+reviewId).html(target.attr('data-count'));

					$('#markFlag-'+reviewId).removeClass('btn-danger');
				
				}else{

					target.attr('data-count', +(flagCount)+1);
					$('#review-flag-count-'+reviewId).html(+(flagCount)+1);

					$('#markHelpFlag-'+reviewId).removeClass('btn-success');
					$('#markFlag-'+reviewId).addClass('btn-danger');

				}


				reviewCount(postData,target);

	          }


			
		});


		$("#reviews").on("click",'.markHelpFlag',function(e){

			  e.preventDefault();


			if(!isLoggedIn)
            {
              
                $('#loginPopup').trigger('click');

            }else{


            	var target = $(this);

			var reviewId = target.attr("data-id");
			var flagCount = target.attr('data-count');
			var helpfulFlag     = 1;

			var postData = {
							reviewId:reviewId,
		            		schoolId:schoolId,
		            		flag :'helpfulFlag',
						 }



			



			if($(this).hasClass('btn-success')){

				target.attr('data-count', +(flagCount)-1);
				$('#review-helpflag-count-'+reviewId).html(target.attr('data-count'));	

				$('#markHelpFlag-'+reviewId).removeClass('btn-success');
				
			}else{

				target.attr('data-count', +(flagCount)+1);
				$('#review-helpflag-count-'+reviewId).html(target.attr('data-count'));	

				$('#markFlag-'+reviewId).removeClass('btn-danger');
				$('#markHelpFlag-'+reviewId).addClass('btn-success');

			}
					
			reviewCount(postData,target);

           }

			
    
	        
		});

		function reviewCount(postData,target)
		{
			if(typeof postData != "undefined")
			{
				$.ajax({
			            url:globalSiteUrl+'reviews/countFlags',
			            method:'POST',
			            data: postData,
			            success:function(response)
			            {
			            	var result = JSON.parse(response);

			            
			            	if(result.status != "success")
			            	{
			            		alert(response);
			            	}
			            	else
			            	{

			            		

			            		$('#review-helpflag-count-'+postData.reviewId).html(result.helpfulFlagCount);
			            		$('#markHelpFlag'+postData.reviewId).attr('data-count',result.helpfulFlagCount);


			            		$('#markFlag-'+postData.reviewId).attr('data-count',result.flagCount);
			            		$('#review-flag-count-'+postData.reviewId).html(result.flagCount);

			            	
			            		



			            	}
			            	
			            },
			            error:function()
			            {
			            	console.log("The ajax request failed");
			            }
		           
		        });
		    }
		}





















		//for the SORTING

		$("#review-order-by .orderby").on("click",function(e){

			e.preventDefault();

			var target = $(this);
			$("#review-order-by a").removeClass("active");
			target.addClass("active");

			console.log("When order by is clicked");

			console.log("Before the post review call");

			postReview();


		});


		$("#review-order .order").on("click",function(e){

			e.preventDefault();
			var target = $(this);
			$("#review-order a").removeClass("active");
			target.addClass("active");

			console.log("When order is clicked");
			postReview();
			
		})

		schoolApp.loginCheck();

	})

	



</script>