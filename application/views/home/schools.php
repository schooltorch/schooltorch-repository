<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style type="text/css">
	.selected{
		background-color: #FF6B45;
		color: #FFF;
		padding: 0 4px;
	}

	#sorting .orderBy.active, #sorting .order.active{
		font-weight: 600;
		color: #000;
		cursor: context-menu;
	}

	#sorting .orderBy.active:hover, #sorting .order.active:hover{
		text-decoration: blink;
	}

	#sorting{
		font-size: 13px;
	}
</style>

<?php
	if(!isset($user_id))
	{
		$user_id = 0;
	}

	$showGraduateFlag 	=	1;

	/*$showGraduateFlag 	=	$this->input->cookie('showGraduateOnly', true);
	
	if($showGraduateFlag == NULL || $showGraduateFlag == '')
	{
		$showGraduateFlag 	=	1;

		$cookie = 	array(
						    'name'   => 'showGraduateOnly',
						    'value'  => 1,
    						'expire' => '86500'
					    );

		$this->input->set_cookie($cookie);
	}*/

	$majorFilters 	=	$this->config->item('major');

?>

<div class="main_container">

	<?php

		$subUrl = '';
		$clearSearch = array();

		function clearSearchURL($filter){
			
			$csURL = '';
			
			if(isset($_REQUEST['instsize']) && $filter != 'instsize'){
				$csURL.='&instsize='.$_REQUEST['instsize'];
			}
			
			if(isset($_REQUEST['selectivity']) && $filter != 'selectivity'){
				$csURL.='&selectivity='.$_REQUEST['selectivity'];	
			}

			if(isset($_REQUEST['locale']) && $filter != 'locale'){
				$csURL.='&locale='.$_REQUEST['locale'];
			}

			if(isset($_REQUEST['state']) && $filter != 'state'){
				$csURL.='&state='.$_REQUEST['state'];
			}

			if(isset($_REQUEST['tuition']) && $filter != 'tuition'){
				$csURL.='&tuition='.$_REQUEST['tuition'];
			}

			if(isset($_REQUEST['city']) && $filter != 'city'){
				$csURL.='&city='.$_REQUEST['city'];
			}

			if(isset($_REQUEST['major']) && $filter != 'major'){
				$csURL.='&major='.$_REQUEST['major'];
			}

			return prep_url(site_url().'home/schools?'.$csURL);

		}

		function selectedFilter($filters_array,$filterType,$filter){

			if($filterType == 'instsize'){
				if(isset($filters_array[$filter]))
				{
					return $filters_array[$filter];
				}
			}

			if($filterType == 'selectivity'){
				if(isset($filters_array[$filter]))
				{
					return $filters_array[$filter];
				}
			}

			if($filterType == 'locale'){
				if(isset($filters_array[$filter]))
				{
					return $filters_array[$filter];
				}	
			}

			if($filterType == 'tuition'){
				if(isset($filters_array[$filter]))
				{
					return $filters_array[$filter];
				}	
			}

			if($filterType == 'state'){
				//$filters_array =  array_flip($filters_array);
				if(isset($filters_array[$filter]))
				{
					return $filters_array[$filter];
				}
			}

			if($filterType == 'city'){
				//$filters_array =  array_flip($filters_array);
				return $_REQUEST['city'];
			}

			if($filterType == 'major'){
				//$filters_array =  array_flip($filters_array);
				if(isset($filters_array[$filter]))
				{
					return $filters_array[$filter];
				}

			}
	
		}

		$isFilterSet = 0;

		if(isset($_REQUEST['instsize'])){
			$isFilterSet++;
			$subUrl.='&instsize='.$_REQUEST['instsize'];
			$clearSearch['instsize'] = clearSearchURL('instsize');
		}

		if(isset($_REQUEST['selectivity'])){
			$isFilterSet++;
			$subUrl.='&selectivity='.$_REQUEST['selectivity'];
			$clearSearch['selectivity'] = clearSearchURL('selectivity');
		}

		/*if(isset($_REQUEST['locale'])){
			$isFilterSet++;
			$subUrl.='&locale='.$_REQUEST['locale'];
			$clearSearch['locale'] = clearSearchURL('locale');
		}*/

		if(isset($_REQUEST['state'])){
			$isFilterSet++;
			$subUrl.='&state='.$_REQUEST['state'];
			$clearSearch['state'] = clearSearchURL('state');
		}

		if(isset($_REQUEST['tuition'])){
			$isFilterSet++;
			$subUrl.='&tuition='.$_REQUEST['tuition'];
			$clearSearch['tuition'] = clearSearchURL('tuition');
		}

		if(isset($_REQUEST['city'])){
			$isFilterSet++;
			$subUrl.='&city='.$_REQUEST['city'];
			$clearSearch['city'] = clearSearchURL('city');
		}

		if(isset($_REQUEST['major'])){
			$isFilterSet++;
			$subUrl.='&major='.$_REQUEST['major'];
			$clearSearch['major'] = clearSearchURL('major');
		}

		$sortbyUrl = site_url().'home/schools?'.$subUrl;

		if(isset($_REQUEST['sortby'])){
			$subUrl.='&sortby='.$_REQUEST['sortby'];
		}

		if(isset($_REQUEST['orderby'])){
			$subUrl.='&orderby='.$_REQUEST['orderby'];
		}

		$filterUrl = prep_url(site_url().'home/schools?'.$subUrl);

	?>

	<div class="container">
		
		<div class="row">
		
			<div class="col-md-3">

				

				<div class="wrapper">

				<div class="col-heading"> Search </div>

				<form role="form" class="search_form has-feedback" action="<?=site_url()?>home/schools" method="post">

					  <div class="form-group">
					  	
					  	<input type="text" class="form-control input-lg" id="search" placeholder="Search for School">

					  </div>

					  <div class="form-group">
					  	
					  	<input type="text" class="form-control input-lg" id="searchCity" placeholder="Search for Location">

					  </div>
				
				</form>


				    <ul class="currentFilters">



				    	<?php

						if($subUrl != "" && $isFilterSet > 0){
							?>
							
								<div class="col-heading"> Current Filters </div>
							
							<?php



							if(isset($clearSearch['instsize'])){
								?>
								<li>
									<label style="display:block;">
										Enrollment:  
									</label>

									<ul>
									<?php

									$selectedParams = explode('_', $_REQUEST['instsize']);

									if(count($selectedParams) > 1)
									{

										foreach($filters_students as $key => $value)
										{
											if(in_array($key, $selectedParams))
											{
												$tempArray 	=	array();
												$tempArray 	=	$selectedParams;
												$tempKey 	=	array_search($key, $tempArray);
												unset($tempArray[$tempKey]);
												$tempArray 	=	array_filter(array_unique($tempArray));

												$url 		=	$clearSearch['instsize'];

												if(count($tempArray) > 0)
												{
													$url 	.=	'&instsize='. implode('_', $tempArray);
												}

									?>

												<li>
													<a href="<?=$url?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>

													<span class="circle-<?=$key?> indicator indicator-circle" title="<?=$value?>" ></span>

													
												</li>

											<?php		
											}
										}
									}
									else
									{
									?>
										<li>
											<a href="<?=$clearSearch['instsize']?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>
											<span class="circle-<?=$_REQUEST['instsize']?> indicator indicator-circle" title="<?=selectedFilter($filters_students,'instsize',$_REQUEST['instsize']);?>"></span>
											
											
										</li>
									<?php
									}
									?>
								 </ul>
								</li>
								<?php
							}

							if(isset($clearSearch['selectivity']))
							{
							?>
								<li>
									<label>
										Selectivity:  
									</label>

									<ul>
							<?php
									$selectedParams 	=	array();
									$selectedParams 	= 	explode('_', $_REQUEST['selectivity']);

									if(count($selectedParams) > 1)
									{
										foreach($filters_selectivity as $key => $value)
										{
											if(in_array($key, $selectedParams))
											{
												$tempArray 	=	array();
												$tempArray 	=	$selectedParams;
												$tempKey 	=	array_search($key, $tempArray);
												unset($tempArray[$tempKey]);
												$tempArray 	=	array_filter(array_unique($tempArray));

												$url 		=	$clearSearch['selectivity'];

												if(count($tempArray) > 0)
												{
													$url 	.=	'&selectivity='. implode('_', $tempArray);
												}

							?>
														<li>
															<a href="<?=$url?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>
															<span class="circle-<?=$key?> indicator indicator-circle" title="<?=$value?>"></span>
															

														</li>
							<?php
												}
											}
										}
										else
										{
							?>	
											<li>
												<a href="<?=$clearSearch['selectivity']?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>
												<span class="circle-<?=$_REQUEST['selectivity']?> indicator indicator-circle" title="<?=selectedFilter($filters_selectivity,'selectivity',$_REQUEST['selectivity']);?>"></span>
												<!--<?=selectedFilter($filters_selectivity,'selectivity',$_REQUEST['selectivity']);?>-->
												
											</li>
							<?php
										}

							?>
									</ul>
								</li>
							<?php
							}

							if(isset($clearSearch['locale'])){
							?>
								<li>
									<label>
										Settings:  
									</label>
									<ul>
							<?php
									$selectedParams 	=	array();
									$selectedParams 	= 	explode('_', $_REQUEST['locale']);

									if(count($selectedParams) > 1)
									{
										foreach($filters_setting as $key => $value)
										{
											if(in_array($key, $selectedParams))
											{
												$tempArray 	=	array();
												$tempArray 	=	$selectedParams;
												$tempKey 	=	array_search($key, $tempArray);
												unset($tempArray[$tempKey]);
												$tempArray 	=	array_filter(array_unique($tempArray));

												$url 		=	$clearSearch['locale'];

												if(count($tempArray) > 0)
												{
													$url 	.=	'&locale='. implode('_', $tempArray);
												}

							?>
														<li>
															<a href="<?=$url?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>
															<?=$value;?>
															

														</li>
							<?php
												}
											}
										}
										else
										{
							?>	
											<li>
												<a href="<?=$clearSearch['locale']?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>
												<?=selectedFilter($filters_setting,'locale',$_REQUEST['locale']);?> 
											</li>
							<?php
										}

							?>
									</ul>
								</li>
							<?php
							}

							if(isset($clearSearch['tuition'])){
								?>
								<li>
									<label>
										Program Cost:  
									</label>
									<ul>
							<?php
									$selectedParams 	=	array();
									$selectedParams 	= 	explode('_', $_REQUEST['tuition']);

									if(count($selectedParams) > 1)
									{
										foreach($filters_tuition as $key => $value)
										{
											if(in_array($key, $selectedParams))
											{
												$tempArray 	=	array();
												$tempArray 	=	$selectedParams;
												$tempKey 	=	array_search($key, $tempArray);
												unset($tempArray[$tempKey]);
												$tempArray 	=	array_filter(array_unique($tempArray));

												$url 		=	$clearSearch['tuition'];

												if(count($tempArray) > 0)
												{
													$url 	.=	'&tuition='. implode('_', $tempArray);
												}

							?>
														<li>
															<a href="<?=$url?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>
															<span class="dollar-<?=$key?> indicator indicator-dollar" title="<?=$value?>"></span>
															

														</li>
							<?php
												}
											}
										}
										else
										{
							?>	
											<li>
												<a href="<?=$clearSearch['tuition']?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right" ></span></a>
												<span class="dollar-<?=$_REQUEST['tuition']?> indicator indicator-dollar" title="<?=selectedFilter($filters_tuition,'tuition',$_REQUEST['tuition']);?>"></span>
												
											</li>
							<?php
										}

							?>
									</ul>
								</li>
								<?php
							}

							if(isset($clearSearch['state'])){
								?>
								<li>
									<label>
										Location:  
									</label>
									<ul>
							<?php
									$selectedParams 	=	array();
									$selectedParams 	= 	explode('_', $_REQUEST['state']);

									if(count($selectedParams) > 1)
									{
										foreach($filters_state as $key => $value)
										{
											if(in_array($key, $selectedParams))
											{
												$tempArray 	=	array();
												$tempArray 	=	$selectedParams;
												$tempKey 	=	array_search($key, $tempArray);
												unset($tempArray[$tempKey]);
												$tempArray 	=	array_filter(array_unique($tempArray));

												$url 		=	$clearSearch['state'];

												if(count($tempArray) > 0)
												{
													$url 	.=	'&state='. implode('_', $tempArray);
												}

							?>
														<li>
											<a href="<?=$url?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>
															<?=$value;?>
															

														</li>
							<?php
												}
											}
										}
										else
										{
							?>	
											<li>
												<a href="<?=$clearSearch['state']?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>
												<?=selectedFilter($filters_state,'state',$_REQUEST['state']);?> 
											</li>
							<?php
										}

							?>
									</ul>
								</li>
							<?php
							}

							if(isset($clearSearch['city'])){
							?>
								<li>
									<label style="display:block;">
										City:  
									</label>

									<ul>
									<?php

									$selectedParams = explode('_', $_REQUEST['city']);

									if(count($selectedParams) > 1)
									{

										foreach($filters_students as $key => $value)
										{
											if(in_array($key, $selectedParams))
											{
												$tempArray 	=	array();
												$tempArray 	=	$selectedParams;
												$tempKey 	=	array_search($key, $tempArray);
												unset($tempArray[$tempKey]);
												$tempArray 	=	array_filter(array_unique($tempArray));

												$url 		=	$clearSearch['city'];

												if(count($tempArray) > 0)
												{
													$url 	.=	'&city='. implode('_', $tempArray);
												}

									?>

												<li>
													<a href="<?=$url?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>

													<span class="circle-<?=$key?> indicator indicator-circle" title="<?=$value?>" ></span>

													
												</li>

											<?php		
											}
										}
									}
									else
									{
									?>
										<li>
											<a href="<?=$clearSearch['city']?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>
											<span class="" title="<?=selectedFilter($filters_students,'city',$_REQUEST['city']);?>"><?=$_REQUEST['city']?></span>
											
											
										</li>
									<?php
									}
									?>
								 </ul>
								</li>
								<?php
							}

							if(isset($clearSearch['major']) && isset($majorFilters[$_REQUEST['major']])){
							?>
								<li>
									<label style="display:block;">
										Major:  
									</label>

									<ul>
									<?php

									$selectedParams = explode('_', $_REQUEST['major']);

									if(count($selectedParams) > 1)
									{

										foreach($filters_students as $key => $value)
										{
											if(in_array($key, $selectedParams))
											{
												$tempArray 	=	array();
												$tempArray 	=	$selectedParams;
												$tempKey 	=	array_search($key, $tempArray);
												unset($tempArray[$tempKey]);
												$tempArray 	=	array_filter(array_unique($tempArray));

												$url 		=	$clearSearch['major'];

												if(count($tempArray) > 0)
												{
													$url 	.=	'&major='. implode('_', $tempArray);
												}

									?>

												<li>
													<a href="<?=$url?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>

													<span class="circle-<?=$key?> indicator indicator-circle" title="<?=$value?>" ></span>

													
												</li>

											<?php		
											}
										}
									}
									else
									{
									?>
										<li>
											<a href="<?=$clearSearch['major']?>" class="pull-right"><span class="remove icon glyphicon glyphicon-remove-circle pull-right"></span></a>
											<span class="" title="<?=selectedFilter($majorFilters,'major',$_REQUEST['major']);?>"><?=selectedFilter($majorFilters,'major',$_REQUEST['major']);?></span>
											
											
										</li>
									<?php
									}
									?>
								 </ul>
								</li>
								<?php
							}

							?>


						<li>
							<a href="<?=site_url();?>home/schools" class="btn btn-default"> Clear Search </a>
						</li>
					
					<?php	
						}

						?>
				    </ul>








				
					<div class="col-heading">

						Search Filters

					</div>
				
					<div class="sidebar-nav filters">
						<div class="checkbox checkbox-inline" >
						<label style="font-weight: 600;">
							<?php
								$checked 	=	'';
								if($showGraduateFlag == 1)
								{
									$checked 	=	'checked="checked"';
								}
							?>
							<input id="onlyGraduate" type="checkbox" <?=$checked?>>
							Graduate Schools Only
						</label>
					</div>	
						<div id="accordion">
							
							<?php

								$selectedClass 	= 	'';

								if(isset($_REQUEST['instsize']))
								{
									$selectedClass 	= 	'filterActive';
								}
								/*
								<!--title="Enrollment represents the size of the student body.

Number of students:

Upto 5,000        :  0
5,000 - 10,000   : 00
10,000 - 25,000 : 000
25,000 - 40,000 : 0000
>40,000             : 00000"-->
								*/
							?>
									<h3 id="ddStudents"  href="#" role="button" class="dropdown-toggle <?=$selectedClass?>" data-toggle="dropdown">
										<span class="glyphicon glyphicon-chevron-down icon pull-right"></span>
										<span class="glyphicon glyphicon-chevron-up icon pull-right"></span>
										<!-- Students --> 
										Enrollment
									</h3>
					            <div>

					              	<ul>
					                	<?php
					                	$param 	=	'';
					                	$url 	=	$filterUrl;
					                	foreach ($filters_students as $key => $value) {

					                		$selectedFilterClass = '';
											$checked= "";
					                		if(isset($_REQUEST['instsize']))
											{
												$url 	=	$clearSearch['instsize'];
												if(!in_array($key, explode('_',$_REQUEST['instsize']))){

													$param = $key.'_'.$_REQUEST['instsize'];

													//$selectedFilterClass = 'active';

												}else{

													$selectedFilterClass = 'active';
													$checked			 = 'checked="checked"';
													$param = $_REQUEST['instsize'];
												}
											
											}else{

												$param = $key;
											}

					                		?>
					                		<li>
						                			<a href="<?=$url?>&instsize=<?=$param;?>" class="with-indicator <?=$selectedFilterClass?>" title="<?=$value;?>">
						                			<!--<input type="checkbox" id="enroll-<?//=$key;?>" <?//=$checked; ?>/>--><span class="circle-<?=$key;?> indicator indicator-circle"></span>
						                		</a>
						                	</li>	
					                		<?php
					                	}
					                	?>
					               	</ul>

					            </div>  

					        <?php

								$selectedClass 	= 	'';

								if(isset($_REQUEST['selectivity']))
								{
									$selectedClass 	= 	'filterActive';
								}
							?>  
					            <h3 id="ddSelectivity" href="#" role="button" class="dropdown-toggle <?=$selectedClass?>" data-toggle="dropdown">
					            	<span class="glyphicon glyphicon-chevron-down icon pull-right"></span>
					            	<span class="glyphicon glyphicon-chevron-up icon pull-right"></span>

					            	Selectivity </h3>

					            <div>
					              	
					              	<ul>
					                	<?php
						                	$param 	=	'';
						                	$url 	=	$filterUrl;
						                	foreach ($filters_selectivity as $key => $value)
						                	{

						                		$selectedFilterClass = '';
												$checked			 = '';
						                		if(isset($_REQUEST['selectivity']))
												{
													$url 	=	$clearSearch['selectivity'];
													if(!in_array($key, explode('_',$_REQUEST['selectivity']))){

														$param = $key.'_'.$_REQUEST['selectivity'];

														//$selectedFilterClass = 'active';

													}else{
														$selectedFilterClass = 'active';
														$param = $_REQUEST['selectivity'];
														$checked			 = 'checked="checked"';

													}
												
												}else{

													$param = $key;
												}

					                	?>
						                		<li>
							                		<a href="<?=$url?>&amp;selectivity=<?=$param;?>" title="<?=$value;?>" class="with-indicator <?=$selectedFilterClass?>">
							                			<!--<input type="checkbox" id="selectivity-<?//=$value;?>" <?//=$checked; ?>/>--><span class="circle-<?=$key;?> indicator indicator-circle"></span>
							                		</a>
							                	</li>
				                		<?php
					                		}
					                	?>
					              	</ul>

					            </div>

				          	<?php
				          	//}

				          	/*if(!isset($_REQUEST['locale']))
				          	{		
				          	?>
					            <h3 id="ddSettings" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
					            	<span class="glyphicon glyphicon-chevron-down icon pull-right"></span>
					            	<span class="glyphicon glyphicon-chevron-up icon pull-right"></span>
					            	Settings </h3>

					            <div>
					              	
					              	<ul>
					                	<?php
					                	foreach ($filters_setting as $key => $value) {
					                		?>
					                		<li>
						                		<a href="<?=$filterUrl?>&amp;locale=<?=$key;?>">
						                			<?=$value;?>
						                		</a>
						                	</li>	
					                		<?php
					                	}
					                	?>
					              	</ul>

					            </div>

					        <?php
					        }*/


					        //if(!isset($_REQUEST['tuition']))
					        //{	
					        ?>

					        <?php

								$selectedClass 	= 	'';

								if(isset($_REQUEST['tuition']))
								{
									$selectedClass 	= 	'filterActive';
								}
							?>

					        	<h3 id="ddTuition" href="#" role="button" class="dropdown-toggle <?=$selectedClass?>" data-toggle="dropdown">
					        		<span class="glyphicon glyphicon-chevron-down icon pull-right"></span>
					            	<span class="glyphicon glyphicon-chevron-up icon pull-right"></span>

					        		<!-- Tuition --> Program Cost </h3>

					        	<div>

					            	<ul>
					                	<?php
					                	$tempCount 	= 	1;
					                	$param 		=	'';
					                	$url 		=	$filterUrl;
					                	foreach ($filters_tuition as $key => $value)
					                	{
											$checked			 = '';
					                		$selectedFilterClass = '';
					                		if(isset($_REQUEST['tuition']))
											{
												$url 	=	$clearSearch['tuition'];

												if(!in_array($key, explode('_',$_REQUEST['tuition']))){

													$param = $key.'_'.$_REQUEST['tuition'];

													//$selectedFilterClass = 'active';

												}else{

													$selectedFilterClass = 'active';
													$checked			 = 'checked="checked"';
													$param = $_REQUEST['tuition'];

												}
											
											}else{

												$param = $key;
											}

					                		?>
					                		<li>
						                		<a href="<?=$url?>&amp;tuition=<?=$param;?>" title="<?=$value?>" class="with-indicator <?=$selectedFilterClass?>">
						                			<!--<input type="checkbox" id="progcost-<?//=$value;?>" <?//=$checked; ?>/>--><span class="dollar-<?=$tempCount;?> indicator indicator-dollar"></span>
						                		</a>
						                	</li>	
					                		<?php

					                		$tempCount++;
					                	}
					                	?>
					              	
					              	</ul>

					            </div>  

					        <?php

								$selectedClass 	= 	'';

								if(isset($_REQUEST['state']))
								{
									$selectedClass 	= 	'filterActive';
								}
							?>  	
				          
					            <h3 id="ddState" href="#" role="button" class="dropdown-toggle <?=$selectedClass?>" data-toggle="dropdown">

					            	<span class="glyphicon glyphicon-chevron-down icon pull-right"></span>
					            	<span class="glyphicon glyphicon-chevron-up icon pull-right"></span>
					            	<!-- State -->Location</h3>

					            <div>

					            	<ul>
					                	<?php
					                	$param 	=	'';
					                	$url 	=	$filterUrl;
					                	foreach ($filters_state as $key => $value)
					                	{
					                		$selectedFilterClass = '';
											$checked			 = '';
					                		if(isset($_REQUEST['state']))
											{
												$url 	=	$clearSearch['state'];
												if(!in_array($key, explode('_',$_REQUEST['state']))){

													$param = $key.'_'.$_REQUEST['state'];

													//$selectedFilterClass = 'active';

												}else{
													$selectedFilterClass = 'active';
													$param = $_REQUEST['state'];
													$checked			 = 'checked="checked"';

												}
											
											}else{

												$param = $key;
											}
				                		?>
					                		<li>
						                		<a href="<?=$url?>&amp;state=<?=$param;?>" class="<?=$selectedFilterClass?>">
						                			<!--<input type="checkbox" id="location-<?//=$value;?>" <?//=$checked; ?>/>--><?=$value?>
						                		</a>
						                	</li>	
					                		<?php
					                	}
					                	?>
					              	</ul>
					          	
					          	</div>

					        <?php
					        //}	
					        ?>  	

				        </div>

				    </div> 
					<!--<div class="checkbox checkbox-inline" >
						<label style="font-weight: 600;">
							<?php
								/*$checked 	=	'';
								if($showGraduateFlag == 1)
								{
									$checked 	=	'checked="checked"';
								}*/
							?>
							<input id="onlyGraduate" type="checkbox" <?=$checked?>>
							Graduate Schools Only
						</label>
					</div>-->
			    </div>      

			</div>

			<div id="schoolFilterResultsContainer" class="col-md-9">
				
				<div class="wrapper bg_white">

				<div class="list-heading">
					<div class="row">

						<div class="col-md-12">

							<!--<div class="checkbox checkbox-inline pull-right" >
							    <label style="font-weight: 600;">
							    	<?php
							    		/*$checked 	=	'';
							    		if($showGraduateFlag == 1)
							    		{
							    			$checked 	=	'checked="checked"';
							    		}*/
							    	?>
							      	<input id="onlyGraduate" type="checkbox" <?=$checked?>>
							      	Graduate Schools Only
							    </label>
							</div>-->

							<div class="pull-right">
								<label>Total Results:</label>
								<span id="totalResults"></span>
								<span id="totalResults"></span>
							</div>
							
							<div id="sorting">
								<label>Sort:</label>
								<a class="btn-link orderBy active" data-sortkey="selectivity">Selectivity</a><span class="separator">|</span>
								<a class="btn-link orderBy" data-sortkey="enrollment">Enrollment</a><span class="separator">|</span>
								<a class="btn-link orderBy" data-sortkey="cost">Program Cost</a><span class="separator">|</span>
								<a class="btn-link orderBy" data-sortkey="views">Views</a> 
								<label>:</label>
									<a class="btn-link order active" data-sortkey="desc">Desc</a><span class="separator">|</span>
									<a class="btn-link order" data-sortkey="asc">Asc</a>
																
							</div>
		               	</div>

					</div>
				
				</div>
				

				<ul id="schoolFilterResults" class="school_list">
						
					<li><div id="loading"><img class="img-responsive" src="<?=base_url()?>assets/images/loading_lg.gif" alt="Loading"/></div></li>

				</ul>


				</div>

			</div>


			





















		</div><!-- /row -->
	</div> <!-- /container -->

</div><!--/main-container ends-->




<script>
	var userID = '<?=$user_id;?>';

	var containerTarget = $('#schoolFilterResults');
	
	$('#search').val('');

	function getUrlParameter(sParam)
	{
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) 
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) 
			{
				return sParameterName[1];
			}
		}
	}

	function cleanArray(actual)
	{
		var newArray = new Array();

		for(var i = 0; i<actual.length; i++)
		{
			if(actual[i])
			{
				newArray.push(actual[i]);
			}
		}

		return newArray;

	}

	function getAllParameters()
	{
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		if(sURLVariables.length > 0)
		{
			return cleanArray(sURLVariables);
		}
		else
		{
			return sURLVariables;
		}
	}

	function lg(str)
	{
		console.log(str);
	}

	var orderBy 	=	'selectivity';
	var order 		=	'desc';

	if($('#orderBy').val())
	{
		orderBy 	=	$('#orderBy').val();
	}

	if($('#order').val())
	{
		if($('#order').val() == 'asc' || $('#order').val() == 'desc')
		{
			orderBy 	=	$('#orderBy').val();
		}
	}

	var filterOpts =	{
							instsize	: getUrlParameter('instsize'),
							selectivity	: getUrlParameter('selectivity'),
							locale		: getUrlParameter('locale'),
							tuition		: getUrlParameter('tuition'),
							state		: getUrlParameter('state'),
							city		: getUrlParameter('city'),
							major		: getUrlParameter('major'),
							onlyGraduate: <?=$showGraduateFlag?>,
							orderby 	: orderBy,
							order 		: order,
							limit 		: 30,
							offset		: 0
						};

	function commonErr(str)
	{

	}

	function triggerCall()
	{

		lg(filterOpts);

		if(+filterOpts.offset == 0)
		{
			$('#schoolFilterResults').html(
			'<li><div id="loading"><img class="img-responsive" src="<?=base_url()?>assets/images/loading_lg.gif" alt="Loading"/></div></li>');
		}

		$.ajax({
				url: 		'<?=site_url();?>search/advanced',
				type: 		"GET",
				dataType: 	"json",
				data: 		filterOpts,
				success: 	function(response){
								try{
									if(+response.count > 0)
									{
										lg('process!');
										processSchools(response);
									}
									else
									{
										if(filterOpts.offset == 0)
										{
											containerTarget.empty();
											$('#totalResults').html(response.totalCount);

											containerTarget.append('<li><div id="error-404"><br/>0 results found</div></li>')
										}
										else
										{
											$('#loadmoreBar').parent().remove();
										}
									}
								}
								catch(e){
									lg(e);
									commonErr('Something went wrong..');
								}
							},
				error: 		function(){
								commonErr('Something went wrong..');
							}

			});
	}

	triggerCall();

	var currentDefaultPos = 0;

	function processSchools(info)
	{
		if(filterOpts.offset == 0)
		{
			containerTarget.empty();
			$('#totalResults').html(info.totalCount);
		}
		else
		{
			$('#loadmoreBar').parent().remove();
		}

		var savedOptStr = '';
		var isSaved = 0;
		var savedText = 'Save School';
		var isVoted = 0;
		var votedText = 'UpVote';
		var savedClass = '';
		var saveSchoolAction = '';

		console.log(info.data);

		$.each(info.data, function(key, schoolDetails){
			
			//console.log(schoolDetails);
			//prepare photo
			if(schoolDetails.photo && schoolDetails.photo != '' && schoolDetails.photo.length != 0)
			{
				schoolDetails.photo = "<?=locationPicsBaseUrl();?>"+ (schoolDetails.photo.split('<?=$this->config->item("schoolPhotoGlue");?>')[0]);
			}
			else
			{
				schoolDetails.photo = '<?=base_url();?>assets/images/defaults/default-'+ currentDefaultPos +'.jpg';

				currentDefaultPos++;
				if(currentDefaultPos == 11)
				{
					currentDefaultPos = 0;
				}
			}

			savedOptStr = '';
			
			var url 	=	'<?=site_url();?>home/school/'+ schoolDetails.UNITID +'/'+ schoolDetails.URLTITLE;

			//if(+userID != 0)
			{

				isSaved = 0;
				//savedText = 'Save School';
				//savedClass  = '';
				saveSchoolAction = '<a href="#" class="btn btn-info btn-block saveSchool " isSaved="'+ isSaved +'" dataSchoolId="'+ schoolDetails.UNITID +'" dataUserId="'+ userID +'" loginHref="'+ url +'" onClick="return saveNewSchool('+schoolDetails.UNITID+');">Save School</a>';

				if(schoolDetails.SAVEDID && schoolDetails.SAVEDID != '')
				{
					isSaved 	= 1;
					//savedText 	= 'Saved';
					//savedClass  = 'btn-link-success';

					saveSchoolAction = '<span class="glyphicon glyphicon-ok savedSchool" title="School Saved"></span><span class="following">Saved</span>';
				}

				isVoted 	= 	0;
				votedText 	= 	'UpVote';

				if(schoolDetails.VOTEDID && schoolDetails.VOTEDID != '')
				{
					isVoted 	= 1;
					votedText 	= 'Voted';
				}

				if(!schoolDetails.VOTECOUNT)
				{
					schoolDetails.VOTECOUNT = 0;
				}

				savedOptStr = 	'<div class="save-bar">'+saveSchoolAction+'</div>'+
								'<div class="rating"><span class="count">'+ schoolDetails.VIEWSCOUNT+'</span><span class="voteText">View(s)</span> </div>';

								/*'<div class="rating"><a href="#" class="btn btn-link voteSchool '+ votedText+'" isVoted="'+ isVoted +'" dataSchoolId="'+ schoolDetails.UNITID +'" dataUserId="'+ userID +'" title="'+votedText+'" ><span class="glyphicon glyphicon-thumbs-up"></span>'+'</a>'+
								'<span class="count '+ votedText+'" loginHref="'+ url +'">'+
									 schoolDetails.VOTECOUNT +
								'</span></div>'*/
			}

			var locationName 	=	schoolDetails.CITY;

			if(schoolDetails.STATENAME != '')
			{
				locationName 	+=	', '+ schoolDetails.STATENAME;
			}

			locationName 		+=	', '+ schoolDetails.COUNTRY;

			//savedOptStr = '';
			containerTarget.append(

				'<li>'+
					'<div class="row">'+

					'<div class="col-md-4 col-sm-4 text-center">'+

						'<div class="img" style="background: url('+ schoolDetails.photo +') no-repeat  0 0 transparent; background-size: cover;width:100%;">'+

							


						'</div>'+

							
							'<div class="actions">'+
											'<a class="btn btn-warning" href="<?=site_url("/home/addImage")?>/'+schoolDetails.UNITID+'/'+schoolDetails.URLTITLE+'"><i class="icon icon-plus"></i> Photos</a>'+

											'<a class="btn btn-danger" href="<?=site_url("/reviews/createReview")?>/'+schoolDetails.UNITID+'/'+schoolDetails.URLTITLE+'"><i class="icon icon-plus"></i> Review</a>'+
									'</div>'+
						
						'</div>'+

					'<div class="img-content  col-md-8 col-sm-8">'+

						'<div class="row">'+
							'<div class="col-md-9 col-sm-8">'+
								'<h4>'+ 

										'<a href="<?=site_url();?>home/school/'+ schoolDetails.UNITID +'/'+ schoolDetails.URLTITLE +'" class="btn-link">'+ schoolDetails.INSTNM +'</a>'+


								'</h4>'+

									'<div class="location">'+
										
										locationName +
									'</div>'+

									'<div class="row school-details">'+
										'<div class="col-xs-6">'+
											'<label>Program Cost: </label> <span class="dollar-'+ schoolDetails.TUITIONLABEL +' indicator indicator-dollar" title="$'+ schoolDetails.TUITION +'"></span>'+

										'</div>'+

										'<div class="col-xs-6">'+
											'<label>Setting: </label> '+ schoolDetails.LOCALE +
										'</div>'+

									'</div>'+


									'<div class="row school-details">'+
										/*'<div class="col-xs-6">'+
											'<label>Acceptance: </label> '+ schoolDetails.ACCEPTANCE +'%'+

										'</div>'+*/

										'<div class="col-xs-6">'+
											'<label>Selectivity: </label> <span class="circle-'+ schoolDetails.SELECTIVITY +' indicator indicator-circle" title="'+ schoolDetails.SELECTIVITY +'"></span>'+
										'</div>'+

										'<div class="col-xs-6">'+
											'<label>Enrollment: </label> <span class="circle-'+ schoolDetails.TOTALSTUDENTSLABEL +' indicator indicator-circle" title="'+ schoolDetails.TOTALSTUDENTS +'"></span>'+
										'</div>'+

									'</div>'+
									
							'</div>'+
							'<div class="col-md-3 col-sm-4">'+
								savedOptStr +
							'</div>'+
						'</div>'+
								
					'</div>'+






								

							
								
							'</li>'

			);

			

		});

		schoolApp.schoolAction();

		schoolApp.loginCheck();

		if(+info.count == +filterOpts.limit)
		{	
			filterOpts.offset = +filterOpts.offset + +info.count;

			containerTarget.append('<li><div id="loadmoreBar">View More</div></li>');

			$('#loadmoreBar').click(function(){

				if($(this).attr('data-isLoading') != 'yes')
				{
					$(this).attr('data-isLoading', 'yes');
					$(this).html('Loading...');

					triggerCall();
				}
				
			});
		}

		viewMoreFlag = 0;
	}

	var toleranceLevel = 800;
	var viewMoreFlag = 0;
	var $win = $(window);

	$('#onlyGraduate').unbind('change');
	$('#onlyGraduate').change(function(event){

		if($(this).is(':checked'))
		{
			filterOpts.onlyGraduate 	=	1;
		}
		else
		{
			filterOpts.onlyGraduate 	=	0;
		}
		filterOpts.offset 	=	0;
		triggerCall();
	});

	$('#sorting .orderBy').unbind('click');
	$('body').on('click', "#sorting .orderBy", function(event){

		var selector 	=	$(this);

		if(!$(selector).hasClass('active'))
		{
			$('#sorting .orderBy').removeClass('active');
			$(selector).addClass('active');

			filterOpts.orderby 	=	$(selector).attr('data-sortkey');
			filterOpts.offset 	=	0;
			triggerCall();
		}

	});

	$('#sorting .order').unbind('click');
	$('body').on('click', "#sorting .order", function(event){

		var selector 	=	$(this);

		if(!$(selector).hasClass('active'))
		{
			$('#sorting .order').removeClass('active');
			$(selector).addClass('active');

			filterOpts.order 	=	$(selector).attr('data-sortkey');
			filterOpts.offset 	=	0;
			triggerCall();
		}
		
	});

     $win.scroll(function () {
        	if ($win.height() + $win.scrollTop()
                        > ($(document).height() - toleranceLevel) && viewMoreFlag == 0) {
          	
          	viewMoreFlag = 1;
          	lg("FIRIN ALL CYLINDERS!!!");
          	$('#loadmoreBar').trigger('click');
         }
     });
	 function saveNewSchool(school_id){
		 document.cookie = "school_save_id="+school_id;
		 return true;
	 }
</script>