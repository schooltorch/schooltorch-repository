

<p>These Terms of Use (the "<strong>Terms </strong>") govern your access to and use of our websites, emails and
   mobile applications (Candela Labs, LLC dba "SchoolTorch"). These Terms also include our Privacy
   and Cookie Policy. By accessing and using SchoolTorch, you agree to comply with these Terms. If
   you are using SchoolTorch on behalf of a company or other legal entity, you agree to be bound by
   these Terms even if we have separate agreement with you. You may not use SchoolTorch if you do
   not agree to the version of the Terms posted on SchoolTorch at the time you access SchoolTorch.
   (The terms "we" and "us" refer to SchoolTorch)
</p>
<p><em>Please note: </em> These Terms require the use of arbitration on an individual basis to resolve disputes,
   rather than jury trials or class actions, and also limit the remedies available to you in the event of a
   dispute.			
</p>
<h2>1. Eligibility to Use SchoolTorch</h2>
<p>To access or use SchoolTorch, you must be 18 years of age or older and have the power and
   authority to enter into these Terms. Except as approved by us, SchoolTorch is for your personal,
   non-commercial use. If you wish to make commercial use of SchoolTorch, you must enter into an
   agreement with us to do so. You may not use SchoolTorch if we have terminated your account or
   banned you.			
</p>
<h2>2. Your SchoolTorch Account</h2>
<ol>
   <li>
      <p><strong>SchoolTorch Account</strong>. In order to become a member of SchoolTorch (a "<strong>Member</strong>"), we require
         you to register an account by setting up a visitor name and password. Other registration
         requirements (such as the requirement for users to contribute one college or school review) may
         also apply. You are entirely responsible for maintaining the confidentiality of your password. You
         agree not to use the account or password of another Member at any time. You agree to notify us
         immediately if you suspect any unauthorized use of your account or access to your password. You
         are solely responsible for any and all use of your account. Passwords are subject to cancellation or
         suspension by SchoolTorch at any time.
      </p>
   </li>
   <li>
      <p><strong>Social Sign In</strong>. If you access SchoolTorch through a social networking site, such as Facebook or
         Google+ ("Social Networking Site"), you agree that we may access, make available, and store (if
         applicable) any information, data, text, messages, tags, and/or other materials accessible through
         SchoolTorch that you have provided to and stored in your Social Networking Site account so that it is
         available on and through SchoolTorch via your account and your profile page. Subject to the privacy
         settings that you have set with the Social Networking Site account you use to access SchoolTorch,
         personally identifiable information that you post to that Social Networking Site may be displayed on
         SchoolTorch. Please note: your relationship with your Social Networking Sites is governed solely by
         your agreement with those Social Networking Sites and we disclaim any liability for personally
         identifiable information that may be provided to us by a Social Networking Site in violation of the
         privacy settings that you have set with that Social Networking Site account.
      </p>
   </li>
</ol>
<h2>3. Your Use of SchoolTorch</h2>
<ol>
   <li>
      <p><strong>Rules of Conduct</strong>.
         You will use SchoolTorch solely for lawful purposes in a manner consistent with
         these Terms and any and all applicable laws, regulations, or other binding obligations (including
         contractual obligations) you may have towards third parties.
      </p>
      <p>You will not:</p>
      <ul>
         <li>Use any information obtained from SchoolTorch in order to harass, abuse, or harm another person,
            or in order to contact, advertise to, solicit, or sell to any visitor without their prior explicit consent;
         </li>
         <li>Introduce software or automated agents to SchoolTorch, or access the Service so as to produce
            multiple accounts, generate automated messages, or to strip or mine data from SchoolTorch;
         </li>
         <li>Interfere with, disrupt, or create an undue burden on SchoolTorch or the networks or services
            connected to SchoolTorch;
         </li>
         <li>Interfere with, disrupt, modify, reverse engineer, or decompile any data or functionality of
            SchoolTorch.					
         </li>
      </ul>
   </li>
   <li>
      <p><strong>Links to Third-Party Websites</strong>. SchoolTorch may contain links to third-party websites placed by us
         as a service to those interested in this information, or posted by other Members. Your use of all such
         links to third-party websites is at your own risk. We do not monitor or have any control over, and
         make no claim or representation regarding third-party websites. To the extent such links are
         provided by us, they are provided only as a convenience, and a link to a third-party website does not
         imply our endorsement, adoption or sponsorship of, or affiliation with, such third-party website. When
         you leave SchoolTorch, our terms and policies no longer govern.
      </p>
   </li>
   <li>
      <p><strong>Third-Party Content on SchoolTorch</strong>. Content from other Members, visitors, advertisers, and
         other third parties is made available to you through SchoolTorch. ("<strong>Content</strong>") means any work of
         authorship or information, including college or school reviews, school campus or associated photos,
         advertisements, comments, opinions, postings, resumes, messages, text, files, images, photos,
         works of authorship, e-mail, or other materials you find on SchoolTorch. Because we do not control
         such Content, you understand and agree that: (1) we are not responsible for, and do not endorse,
         any such Content, including advertising and information about third-party products and services, or
         the college-related information provided anonymously by other Members; (2) we make no
         guarantees about the accuracy, currency, suitability, or quality of the information in such Content;
         and (3) we assume no responsibility for unintended, objectionable, inaccurate, misleading, or
         unlawful Content made available by other visitors, advertisers, and third parties.
      </p>
      <p>In accordance with Section 230 of the U.S. Communications Decency Act, we generally cannot be
         held liable for claims arising from the Content provided by third parties on SchoolTorch. 
      </p>
   </li>
</ol>
<h2>4. Sharing Your Content on SchoolTorch</h2>
<ol>
   <li>
      <p><strong>Your Responsibility for Your Content</strong>. You are solely responsible for any and all Content that is
         posted through your account on SchoolTorch ("Your Content"), as well as the representations you
         make to us about Your Content. You agree that by submitting Your Content to SchoolTorch, have
         reviewed and agree to abide by our Community Guidelines.
      </p>
   </li>
   <li>
      <p><strong>Representations Regarding Your Content</strong>. You represent and warrant that:</p>
      <ul>
         <li>You own Your Content or otherwise have the right to grant the license set forth in these Terms;</li>
         <li>Your Content does not violate the privacy rights, publicity rights, copyright rights, or other rights of
            any person;
         </li>
         <li>By providing or posting Your Content, you do not violate any binding confidentiality, non-disclosure,
            or contractual obligations you might have towards a third party, including your current or former
            employer or any potential employer;
         </li>
         <li>Any information you provide in a review, photo or profile is correct;</li>
         <li>Any information you provide about your current, past or potential status as a student of a certain
            school or college is correct and complete.
         </li>
         <li>Any material you upload is accurate and submitted on your own behalf</li>
      </ul>
      <p>Please do not provide any information that you are not allowed to share with others, including by
         binding contractual obligation or by law, because any information you provide will be accessible by
         every visitor of SchoolTorch.
      </p>
   </li>
   <li>
      <p><strong>Prohibited Content.</strong> You agree that you will not post any Content that:</p>
      <ul>
         <li>Is offensive or promotes racism, bigotry, hatred or physical harm of any kind against any group or
            individual, or is pornographic or sexually explicit in nature; bullies, harasses or advocates stalking,
            bullying, or harassment of another person;
         </li>
         <li>Involves the transmission of "junk mail", "chain letters", or unsolicited mass mailing, or "spamming";</li>
         <li>Is false or misleading or promotes, endorses or furthers illegal activities or conduct that is abusive,
            threatening, obscene, defamatory or libelous;
         </li>
         <li>Promotes, copies, performs or distributes an illegal or unauthorized copy of another person's work
            that is protected by copyright or trade secret law, such as providing pirated computer programs or
            links to them, providing information to circumvent manufacturer-installed copy-protection devices, or
            providing pirated music, videos, or movies, or links to such pirated music, videos, or movies;
         </li>
         <li>Is involved in the exploitation of persons under the age of eighteen (18) in a sexual or violent
            manner, or solicits personal information from anyone under eighteen (18);
         </li>
         <li>Provides instructional information about illegal activities such as making or buying illegal weapons,
            violating someone's privacy, or providing or creating computer viruses and other harmful code;
         </li>
         <li>Contains identification information such as social security number, passport number, national
            identification number, insurance number, driver's license number, immigration number, or any other
            similar number, code, or identifier;
         </li>
         <li>Solicits passwords or personally identifying information for commercial or unlawful purposes from
            other visitors;
         </li>
         <li>Except as expressly approved by us, involves commercial activities and/or promotions such as
            contests, sweepstakes, barter, pyramid schemes, advertising, affiliate links, and other forms of
            solicitation;
         </li>
         <li>Contains viruses, Trojan horses, worms, time bombs, cancelbots, corrupted files, or similar software;</li>
         <li>Posts or distributes information which would violate any binding confidentiality, non-disclosure or
            other contractual restrictions or rights of any third party, including any current or former employers or
            potential employers;
         </li>
         <li>Implies a SchoolTorch endorsement or partnership of any kind; or</li>
         <li>Otherwise violates these Terms, the terms of your agreements with us, or creates liability for us.</li>
      </ul>
   </li>
</ol>
<h2>5. Special Provisions Applicable to Advertisers</h2>
<p>This provision applies to all advertisers who may purchase ads. Unless we agree otherwise, you
   may not use data collected or derived from ads ("<strong>Ad Data</strong>") for any purpose (including retargeting,
   building or augmenting visitor profiles, allowing piggybacking or redirecting with tags, or combining
   with data across multiple advertisers' campaigns) other than to assess the performance and
   effectiveness of your campaigns on an aggregate and anonymous basis. You may not, and you may
   not permit a third-party to, transfer or sell any Ad Data to, or use Ad Data in connection with, any ad
   network, ad exchange, data broker, or other party not acting on behalf of you and your campaigns.
   You may use information provided directly to you from visitors if you provide clear notice to and
   obtain consent from those visitors and comply with all applicable laws and industry guidelines.
</p>
<h2>6. Enforcement by SchoolTorch</h2>
<ol>
   <li>
      <p><strong>Removal of Content. </strong> While SchoolTorch has no obligation to do so, SchoolTorch reserves the right
         to review and delete (or modify) any Content that we believe, in our sole discretion, violates these
         Terms or other applicable policies posted on SchoolTorch (including our Community Guidelines), or
         that we deem, in our sole discretion, inappropriate. If you see any Content on SchoolTorch that you
         believe violates our policies, you may report that Content by clicking on an applicable link adjacent to
         that Content (e.g. links titled: "Inappropriate" or "Flag Review") or bycontacting us here. Once
         notified, we will review the Content and consider whether to remove or modify it. Please note: Our
         interpretation of our policies and the decision whether or not to edit or remove Content is within our
         sole discretion. You understand and agree that if we choose not to remove or edit Content that you
         find objectionable, that decision will not constitute a violation of these Terms or any agreement we
         have with you. For more information please see our Legal FAQs.
      </p>
   </li>
   <li>
      <p><strong>Copyright Policy. </strong>SchoolTorch has adopted the following policy toward copyright infringement on
         SchoolTorch in accordance with the Digital Millennium Copyright Act (the "DMCA"). It is our policy to
         terminate membership privileges of any Member who repeatedly infringes copyright upon prompt
         notification to us by the copyright owner or the copyright owner's legal agent. Without limiting the
         foregoing, if you believe that your work has been copied in violation of DMCA and used on
         SchoolTorch in a way that constitutes copyright infringement, please provide us with the following
         information:
      </p>
      <ul>
         <li>An electronic or physical signature of the person authorized to act on behalf of the owner of the
            copyright interest;				
         </li>
         <li>An identification of the copyrighted work that you claim has been infringed;</li>
         <li>A description of where the material that you claim is infringing is located on SchoolTorch;
         </li>
         <li>Your address, telephone number, and e-mail address;</li>
         <li>A written statement by you that you have a good faith belief that the disputed use is not authorized
            by the copyright owner, its agent, or the law; and
         </li>
         <li>A statement by you, made under penalty of perjury, that the above information in your notice is
            accurate and that you are the copyright owner or authorized to act on the copyright owner's behalf
         </li>
      </ul>
      </br>
      <p>If you feel that content has been taken down inappropriately please contact us.</br>
         Contact information for claims of copyright infringement is as follows:
      </p>
      </br>
      <p>
         Northwest Registered Agent, LLC.</br>
         90 State Street STE 700 Office 40</br>
         Albany, New York 12207</br>
         Albany County</br>
         e-mail:<a href="mailto:team@schooltorch.com">team@schooltorch.com</a> </br>
      </p>
   </li>
   <li>
      <p><strong>Other Enforcement Actions. </strong> While we have no obligation to do so, we reserve the right to
         investigate and take appropriate action in our sole discretion against you if you violate these Terms,
         including without limitation: removing Content from SchoolTorch (or modifying it); suspending your
         rights to use SchoolTorch; terminating your membership and account; reporting you to law
         enforcement, regulatory authorities, or administrative bodies; and taking legal action against you.
      </p>
   </li>
   <li>
      <p><strong>Defending Our Members. </strong> While we have no obligation to do so, we reserve the right to take
         appropriate action to protect the anonymity of our Members against the enforcement of subpoenas
         or other information requests that seek a Member's electronic address or identifying information.
      </p>
   </li>
</ol>
<h2>7. Rights to Your Content</h2>
<p>We do not claim ownership in any Content that you submit to SchoolTorch, but to be able to legally
   provide SchoolTorch to our visitors, we have to have certain rights to use such Content in
   connection with SchoolTorch, as set forth below. By submitting any Content to SchoolTorch, you
   hereby grant to us an unrestricted, irrevocable, perpetual, non-exclusive, fully-paid and royalty-free,
   license (with the right to sublicense through unlimited levels of sublicensees) to use, copy, perform,
   display, create derivative works of, and distribute such Content in any and all media (now known or
   later developed) throughout the world. No compensation will be paid with respect to the Content that
   you post through SchoolTorch. You should only submit Content to SchoolTorch that you are
   comfortable sharing with others under the terms and conditions of these Terms.
</p>
<h2>8. Rights to SchoolTorch Content</h2>
<p>SchoolTorch contains Content provided by us and our licensors. We and our licensors (including
   other visitors) own and retain all proprietary (and intellectual property) rights in the Content we each
   provide and SchoolTorch owns and retains all property rights in SchoolTorch. If you are a visitor, we
   hereby grant you a limited, revocable, non-sublicensable license under the intellectual property
   rights licensable by us to download, view, copy and print Content from SchoolTorch solely for your
   personal use in connection with using SchoolTorch. Except as provided in the foregoing, you agree
   not to: (1) reproduce, modify, publish, transmit, distribute, publicly perform or display, sell, or create
   derivative works based on SchoolTorch or the Content (excluding Your Content); or (2) rent, lease,
   loan, or sell access to SchoolTorch. The trademarks, logos and service marks ("Marks") displayed
   on SchoolTorch are our property or the property of other third parties. You are not permitted to use
   these Marks without our prior written consent or the consent of the third party that owns the Mark.
</p>
<h2>9. Indemnity</h2>
<p>You agree to defend, indemnify, and hold us, our subsidiaries, affiliates, officers, agents, and other
   partners and employees, harmless from any loss, liability, claim, or demand, including reasonable
   attorney's fees, made by any third party due to or arising from your use of SchoolTorch and due to or
   arising from your breach of any provision of these Terms.
</p>
<h2>10 Disclaimers and Limitation on Liability</h2>
<p>The disclaimers and limitations on liability in this section apply to the maximum extent allowable
   under applicable law. Nothing in this section is intended to limit any rights you have which may not
   be lawfully limited.
</p>
<p>You are solely responsible for your interactions with advertisers and other visitors and we are not
   responsible for the activities, omissions, or other conduct, whether online or offline, of any advertiser
   or visitor of SchoolTorch. We are not responsible for any incorrect or inaccurate Content (including
   any information in profiles) posted on SchoolTorch, whether caused by visitors or by any of the
   equipment or programming associated with or utilized in SchoolTorch. We assume no responsibility
   for any error, omission, interruption, deletion, defect, delay in operation or transmission,
   communications line failure, theft or destruction or unauthorized access to, or alteration of, any
   communication with other visitors. We are not responsible for any problems or technical malfunction
   of any hardware and software due to technical problems on the Internet or on SchoolTorch or
   combination thereof, including any injury or damage to visitors or to any person's computer related to
   or resulting from participation or downloading materials in connection with SchoolTorch. Under no
   circumstances shall we be responsible for any loss or damage resulting from use of SchoolTorch or
   from any Content posted on SchoolTorch or transmitted to visitors, or any interactions between
   visitors of SchoolTorch, whether online or offline.
</p>
<p>SchoolTorch is provided "as-is" and as available. We expressly disclaim any warranties and
   conditions of any kind, whether express or implied, including the warranties or conditions of
   merchantability, fitness for a particular purpose, title, quiet enjoyment, accuracy, or non-infringement.
   We make no warranty that: (1) SchoolTorch will meet your requirements; (2) SchoolTorch will be
   available on an uninterrupted, timely, secure, or error-free basis; or (3) the results that may be
   obtained from the use of SchoolTorch will be accurate or reliable.
</p>
<p>You hereby release us, our officers, employees, agents and successors from any and all claims,
   demands, and losses, damages, rights, claims, and actions of any kind that are either directly or
   indirectly related to or arises from: (1) any interactions with other visitors to or users of SchoolTorch,
   or (2) your participation in any of our offline events.
</p>
<div class="indent">
   <h2>TO THE EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL THE
      COMPANY, ITS AFFILIATES, DIRECTORS, OR EMPLOYEES, OR ITS
      LICENSORS OR PARTNERS, BE LIABLE TO YOU FOR ANY LOSS OF PROFITS,
      USE, OR DATA, OR FOR ANY INCIDENTAL, INDIRECT, SPECIAL,
      CONSEQUENTIAL OR EXEMPLARY DAMAGES, HOWEVER ARISING, THAT
      RESULT FROM (A) THE USE, DISCLOSURE, OR DISPLAY OF YOUR USER
      CONTENT; (B) YOUR USE OR INABILITY TO USE THE SERVICE; (C) THE
      SERVICE GENERALLY OR THE SOFTWARE OR SYSTEMS THAT MAKE THE
      SERVICE AVAILABLE; OR (D) ANY OTHER INTERACTIONS WITH THE
      COMPANY OR ANY OTHER USER OF THE SERVICE, WHETHER BASED ON
      WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE) OR ANY OTHER
      LEGAL THEORY, AND WHETHER OR NOT THE COMPANY HAS BEEN
      INFORMED OF THE POSSIBILITY OF SUCH DAMAGE, AND EVEN IF A REMEDY
      SET FORTH HEREIN IS FOUND TO HAVE FAILED OF ITS ESSENTIAL
      PURPOSE. SOME JURISDICTIONS LIMIT OR DO NOT PERMIT DISCLAIMERS
      OF LIABILITY, SO THIS PROVISION MAY NOT APPLY TO YOU.
   </h2>
   <p>If you have a dispute with one or more users, a school or college or a merchant of a product or
      service that you review using the Service, you release us (and our officers, directors, agents,
      subsidiaries, joint ventures and employees) from claims, demands and damages (actual and
      consequential) of every kind and nature, known and unknown, arising out of or in any way
      connected with such disputes. If you are a California resident, you waive California Civil Code
      §1542, which says: “A general release does not extend to claims which the creditor does not
      know or suspect to exist in his favor at the time of executing the release, which if known by him
      must have materially affected his settlement with the debtor.”
   </p>
</div>
<h2>11. Termination</h2>
<p>These Terms remain in effect while you use SchoolTorch and, for Members, as long as your account
   remains open. You may delete your account at any time. We may suspend or terminate your
   account or your access to parts of SchoolTorch, without notice to you, if we believe that you have
   violated these Terms. All provisions of these Terms shall survive termination or expiration of these
   Terms except those granting access to or use of SchoolTorch. We will have no liability whatsoever
   to you for any termination of your account or related deletion of your information.
</p>
<h2>12. Changes to Terms</h2>
<p>We may revise these Terms from time by posting an updated version on SchoolTorch and you agree
   that the revised Terms will be effective thirty (30) days after the change is posted. Your continued
   use of SchoolTorch is subject to the most current effective version of these Terms.
</p>
<h2>13. Other.</h2>
<p>Except as specifically stated in another agreement we have with you, these Terms constitute the
   entire agreement between you and us regarding the use of SchoolTorch and these Terms
   supersede all prior proposals, negotiations, agreements, and understandings concerning the subject
   matter of these Terms. You represent and warrant that no person has made any promise,
   representation, or warranty, whether express or implied, not contained herein to induce you to enter
   into this agreement. Our failure to exercise or enforce any right or provision of the Terms shall not
   operate as a waiver of such right or provision. If any provision of the Terms is found to be
   unenforceable or invalid, then only that provision shall be modified to reflect the parties' intention or
   eliminated to the minimum extent necessary so that the Terms shall otherwise remain in full force
   and effect and enforceable. The Terms, and any rights or obligations hereunder, are not assignable,
   transferable or sublicensable by you except with SchoolTorch's prior written consent, but may be
   assigned or transferred by us without restriction. Any attempted assignment by you shall violate
   these Terms and be void. The section titles in the Terms are for convenience only and have no legal
   or contractual effect; as used in the Terms, the word "including" means "including but not limited to."
   Please contact us with any questions regarding these Terms. Please contact us with any questions
   regarding these Terms by contacting us at <a href="mailto:team@schooltorch.com">team@schooltorch.com</a>.
</p>

