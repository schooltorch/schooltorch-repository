<?php

					if(count($data)>0)
					{

						foreach($data as $row){

							$photos = getSchoolImage($row->UNITID);
							$imgUrl = '';

							if(count($photos) > 0){
								
								if(isset($photos[0]))
								{	
									$imgUrl = $photos[0];
								}
								else
								{
									$imgUrl = base_url().'assets/images/sample.jpg';
								}

							}
							else{
								$imgUrl =  base_url().'assets/images/sample.jpg';
							}

							?>

							<li>
								<div class="row">

								<div class="col-md-4 col-xs-4" >

									<div class="img img pull-left" style="background: url('<?=$imgUrl?>') no-repeat  0 0 transparent; background-size: cover;width:100%;">


									</div>

									
 								</div>

								<div class="img-content  col-md-8 col-xs-8">

									<h4>

										<?php
										if ($this->tank_auth->is_logged_in())
										{
											$isSaved 	= 0;
											$savedText 	= 'Save school';

											if($row->SAVEDID && $row->SAVEDID!=""){
												$isSaved 	= 1;
												$savedText 	= 'Saved';
											}

											$isVoted 	= 0;
											$votedText 	= 'UpVote';

											if(isset($row->VOTEDID) && $row->VOTEDID!=""){
												$isVoted 	= 1;
												$votedText 	= 'Voted';
											}

											$VOTECOUNT = 0;

											if($row->VOTECOUNT){
												$VOTECOUNT = $row->VOTECOUNT;
											}
											
										?>

											<a href="#" class="btn btn-default pull-right saveSchool" isSaved="<?=$isSaved;?>" dataSchoolId="<?=$row->UNITID;?>" dataUserId="<?=$user_id;?>"><?=$savedText;?></a>


											<a href="#" class="btn btn-link pull-right voteSchool" isVoted="<?=$isVoted;?>" dataSchoolId="<?=$row->UNITID;?>" dataUserId="<?=$user_id;?>"><?=$votedText;?></a>

											<span class="pull-right">
												<?=$VOTECOUNT;?> upvotes
											</span>


										<?php
										}	
										?>



										

										<a href="<?=site_url();?>home/school/<?=$row->UNITID;?>" class="btn-link"><?=$row->INSTNM;?></a>


									</h4>

									<div class="location">
										<?=$row->CITY;?>, 
										<?=$row->STATENAME;?>
									</div>

									<div class="row school-details">
										<div class="col-xs-6">
											<label>Tuition:</label> <?=$row->TUITION==""?"N/A":"$".$row->TUITION;?>

										</div>

										<div class="col-xs-6">
											<label>Setting:</label> <?=$locale[$row->LOCALE];?>
										</div>

									</div>


									<div class="row school-details">
										<div class="col-xs-6">
											<label>Acceptance:</label> <?=$row->ACCEPTANCE;?>%

										</div>

										<div class="col-xs-6">
											<label>Selectivity:</label> <?=parseAcceptance($row->SELECTIVITY);?>
										</div>

									</div>


									<div class="row school-details">
										<div class="col-xs-6">
											<label>Total Students:</label> 

											<?php 
											if($row->TOTALSTUDENTS == 0){
												echo 'N/A';
											}
											else
											{
												echo $row->TOTALSTUDENTS;
											}	
											?>
										
										</div>

									</div>

								
								</div>






								

							
								
							</li>

							<?php

						}

					}	

					?>