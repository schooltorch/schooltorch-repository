<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>


<div id="container2" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

<div id="container3" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>


<script>
$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: 'Ethnicity share'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Etnicity Share',
            data: [
                ['Hispanic/Latino',  13],
                ['Black or African-American', 8],
                {
                    name: 'Chrome',
                    y: 12.8,
                    sliced: true,
                    selected: true
                },
                ['White or Caucasian',42],
                ['Asian', 15],
            ]
        }]
    });
});

	
$(function () {
    $('#container2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Student Citizenship'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['North America', 'Asia', 'South America'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'citizenship',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' millions'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'student CitizenShip',
            data: [635,173,70]
        }, ]
    });
});

	
$(function () {
    $('#container3').highcharts({
        chart: {
            type: 'column'
        },
        colors: ['#434348'],
        title: {
            text: 'Selectivity'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['The Wharton School', 'Average Across All school'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'citizenship',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' millions'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: "Selectivity",
            data: [635,173]
        }, ]
    });
});


</script>