<?php

if(!isset($dataFlag))
{
	$location 		= set_value('location');
	$gmat 			= set_value('gmat');

	$gre			= set_value('gre'); 
	$gpa 			= set_value('gpa');
	$undergradInstitute = set_value('undergradInstitute');

	$budget 		= set_value('budget');
 	$yearsOfExp	= set_value('yearsOfExp');


}



$locationArr = array(
	'name'	=> 'location',
	'id'	=> 'location',
	'class' =>  'form-control',
	'value'	=> $location,
	'placeholder' =>'e.g San Francisco',
	'type'  => 'text',
	
);

$gmatArr = array(
	'name'	=> 'gmat',
	'id'	=> 'gmat',
	'class' =>  'form-control',
	'value'	=> $gmat,
	'placeholder' =>'e.g. 700',
	
	'type'  => 'number',
	//'min'	=> '200',
	//'max'	=> '800',
	'step'	=> '1'
	
);
$greArr = array(
	'name'	=> 'gre',
	'id'	=> 'gre',
	'class' =>  'form-control',
	'value'	=> $gre,
	'placeholder' =>'e.g. 165',
	
	'type'  => '',
	
);
//
$gpaArr = array(
	'name'	=> 'gpa',
	'id'	=> 'gpa',
	'class' =>  'form-control',
	'value'	=> $gpa,
	'placeholder' =>'e.g. 3.5',
	
	'type'  => 'number',
	'min'	=> '0',
	'max'	=> '4',
	'step'	=> '0.1'
	
);
$undergradInstituteArr = array(
	'name'	=> 'undergradInstitute',
	'id'	=> 'undergradInstitute',
	'class' =>  'form-control',
	'value'	=> $undergradInstitute,
	'placeholder' =>'',
	
	'type'  => 'text',
	
);
$budgetArr = array(
	'name'	=> 'budget',
	'id'	=> 'budget',
	'class' =>  'form-control',
	'value'	=> $budget,
	'placeholder' =>'e.g. 3000',
	
	'type'  => 'text',
	
);
$yearsOfExpArr = array(
	'name'	=> 'yearsOfExp',
	'id'	=> 'yearsOfExp',
	'class' =>  'form-control',
	'value'	=> $yearsOfExp,
	'placeholder' =>'e.g. 3',
	
	'type'  => 'text',
	
);

?>

<div class="profile-content section">
	<div class="col-heading">Background section</div>
	<div class="row">
		
		<div class="col-md-6">
			
			 <form role="form" class="form" method="post" action="<?=site_url();?>profile/background">

				<div class="form-group">

					<label for="location" class="control-label">Location</label>

					<div class="">
						<?php echo form_input($locationArr); ?>
						<span class="help-block error-block"><?php echo form_error($locationArr['name']); ?><?php echo isset($errors[$locationArr['name']])?$errors[$locationArr['name']]:''; ?></span>	
					</div>	

				</div>	

				<div class="form-group">

					<label for="gmat" class=" control-label">GMAT score</label>

					<div class="">
						<?php echo form_input($gmatArr); ?>
						<span class="help-block error-block"><?php echo form_error($gmatArr['name']); ?><?php echo isset($errors[$gmatArr['name']])?$errors[$gmatArr['name']]:''; ?></span>	
					</div>	

				</div>	

				<div class="form-group">

					<label for="gre" class="control-label">GRE score</label>

					<div class="">
						<?php echo form_input($greArr); ?>
						<span class="help-block error-block"><?php echo form_error($greArr['name']); ?><?php echo isset($errors[$greArr['name']])?$errors[$greArr['name']]:''; ?></span>	
					</div>	

				</div>
				
				<div class="form-group">

					<label for="gpa" class="control-label">Undergrad GPA on a 4.0 scale</label>

					<div class="">
						<?php echo form_input($gpaArr); ?>
						<span class="help-block error-block"><?php echo form_error($gpaArr['name']); ?><?php echo isset($errors[$gpaArr['name']])?$errors[$gpaArr['name']]:''; ?></span>	
					</div>	

				</div>
				<div class="form-group">

					<label for="undergradInstitute" class="control-label">Undergrad Instituition</label>

					<div class="">
						<?php echo form_input($undergradInstituteArr); ?>
						<span class="help-block error-block"><?php echo form_error($undergradInstituteArr['name']); ?><?php echo isset($errors[$undergradInstituteArr['name']])?$errors[$undergradInstituteArr['name']]:''; ?></span>	
					</div>	

				</div>
				<div class="form-group">

					<label for="budget" class="control-label">Budget (USD $)</label>

					<div class="">
						<?php echo form_input($budgetArr); ?>
						<span class="help-block error-block"><?php echo form_error($budgetArr['name']); ?><?php echo isset($errors[$budgetArr['name']])?$errors[$budgetArr['name']]:''; ?></span>	
					</div>	

				</div>
				<div class="form-group">

					<label for="yearsOfExp" class="control-label">Years of work experience</label>

					<div class="">
						<?php echo form_input($yearsOfExpArr); ?>
						<span class="help-block error-block"><?php echo form_error($yearsOfExpArr['name']); ?><?php echo isset($errors[$yearsOfExpArr['name']])?$errors[$yearsOfExpArr['name']]:''; ?></span>	
					</div>	

				</div>

				<div class="form-group">
					
					<button type="submit" class="btn btn-success">Save</button>
				</div>

			</form>	
		</div>
	</div>
</div>