

<?php
	
	if(!isset($blogUpdatesEmail))
	{
		$blogUpdatesEmail 	=	0;
	}

	if(!isset($reviewUpdatesEmail))
	{
		$reviewUpdatesEmail 	=	0;
	}
	
?>


<div class="profile-content section">
	<div class="col-heading">Customize your email settings</div>
	<div class="row">
		
		<div class="col-md-12 settings" id="emailSettings">

				<div class="item">
					<label>
					<input type="checkbox" class="pull-right emailPermission" id="allPermissions" data-id="all"> 
					<h4>Subscribe to all email notifications</h4> 
					<div class="info"></div>
					</label>
				</div>
				
					<div class="item">
						<label>
						<input type="checkbox" class="pull-right emailPermission" data-id="blogUpdatesEmail">  
						<h4>Blog emails</h4> 
						<div class="info">Send me an email whenever a new blog article is published on the website.</div>
						</label>
					</div>
				
					<div class="item">
						<label>
						<input type="checkbox" class="pull-right emailPermission" data-id="reviewUpdatesEmail">
						<h4>Review Emails</h4> 
						<div class="info">Send me an email whenever a new review is added to a saved school.</div>
						</label>
					</div>
			
		</div>
	</div>
</div>

<script type="text/javascript">
	
	$(document).ready(function(){

	

		function updateCheckboxes(flag)
		{

			var checkFlag 	=	1;

			if(+flag == 1)
			{
				if($('#allPermissions').is(':checked'))
				{
					$('.emailPermission').prop('checked', true);
				}
				else
				{
					$('.emailPermission').prop('checked', false);
				}
			}
			else
			{

				$('.emailPermission').each(function(){

					var selector 	=	$(this);

					if($(selector).attr('id') != "allPermissions")
					{
						if(!$(selector).is(':checked'))
						{
							checkFlag 	=	0;
						}
					}

				});

				if(checkFlag == 1)
				{
					$('#allPermissions').prop('checked', true);
				}
				else
				{
					$('#allPermissions').prop('checked', false);
				}
			}

		}

		if(+"<?=$blogUpdatesEmail?>" == 1)
		{
			$('.emailPermission[data-id="blogUpdatesEmail"]').attr('checked', 'checked');
		}

		if(+"<?=$reviewUpdatesEmail?>" == 1)
		{
			$('.emailPermission[data-id="reviewUpdatesEmail"]').attr('checked', 'checked');
		}

		$('.emailPermission').unbind('change');
		$('.emailPermission').change(function(event){

			var selector 	=	$(this);

			var key 		=	$(selector).attr('data-id');

			var value		=	0;

			var flag 		=	0;

			if(key == 'all')
			{
				flag 		=	1;
			}

			if($(selector).is(':checked'))
			{
				value		=	1;
			}

			updateCheckboxes(flag);

			$.ajax({
                url: schoolApp.site_url+'profile/updateEmailSettings',
                method:'POST',
                data: {key:key, value: value},
                success:function(response)
                {
                    var result = JSON.parse(response);

                    if(result.status != "success")
                    {
                        alert(response);
                    }
                    else
                    {

                    }
                    
                },
                error:function()
                {
                    console.log("The ajax request failed");
                }
           
        	});

		});

		updateCheckboxes();

	})

</script>