 <?php
$oldPassword = array(
	'name'	=> 'old_password',
	'id'	=> 'old_password',
	'class' =>  'form-control',
	'value'	=> set_value('old_password'),
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'password',
	
);

$newPassword = array(
	'name'	=> 'new_password',
	'id'	=> 'new_password ',
	'class' =>  'form-control',
	'value'	=> set_value('new_password'),
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'password',
	
);

$confirmPassword = array(
	'name'	=> 'confirm_new_password',
	'id'	=> 'confirm_new_password',
	'class' =>  'form-control',
	'value'	=> set_value('confirm_new_password'),
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'password',
	
);
?>

<div class="profile-content section">
	<div class="col-heading">Change Password</div>
	<div class="row">
		

		<div class="col-md-6">
			<form role="form" class="form" method="post" action="<?=site_url();?>profile/changePassword">

				<div class="form-group">

					<label for="old_password" class=" control-label">Current Password</label>

					

					<div class="">
						<?php echo form_input($oldPassword); ?>
						<span class="help-block error-block"><?php echo form_error($oldPassword['name']); ?><?php echo isset($errors[$oldPassword['name']])?$errors[$oldPassword['name']]:''; ?></span>	
					</div>	

				</div>	

				<div class="form-group">

					<label for="new_password" class=" control-label">New Password</label>

					<div class="">
						<?php echo form_input($newPassword); ?>
						<span class="help-block error-block"><?php echo form_error($newPassword['name']); ?><?php echo isset($errors[$newPassword['name']])?$errors[$newPassword['name']]:''; ?></span>	
					</div>	

				</div>	

				<div class="form-group">

					<label for="confirm_new_password" class="control-label">Confirm Password</label>

					<div class="">
						<?php echo form_input($confirmPassword); ?>
						<span class="help-block error-block"><?php echo form_error($confirmPassword['name']); ?><?php echo isset($errors[$confirmPassword['name']])?$errors[$confirmPassword['name']]:''; ?></span>	
					</div>	

				</div>

				<div class="form-group">
					
					<button type="submit" class="btn btn-success">Save</button>

					<a href="<?=site_url();?>auth/forgot_password" class="pull-right btn btn-link" style="">Forgot Password</a>
					
				</div>

			</form>	
		</div>

		
	</div>

</div>