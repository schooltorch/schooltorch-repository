<div class="profile-content">
	<div class="row">
			
		<div style="clear:both;" class="custom-responsive-table col-md-12" id="mySavedSchoolstable" >
			<div class="col-heading">My Reviews</div>

			<?php

			if(isset($myReviews) && count($myReviews) > 0)
			{

				//dump($myReviews);
			?>
			
				<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">
						<tr>
							<th>Details</th> 
							<th>Submitted on</th>
							<th>Approved</th>
							<th>Action</th>
							
						</tr>
						<?php

						foreach ($myReviews as $key => $value) 
						{
							$approveTxt = "No";
							if($value['isApproved'] == 1)
							{
								$approveTxt = "Yes";
							}

							$schoolUrl = site_url('home/school')."/".$value['schoolId'].'/'.url_title($value['schoolName']);;
							?>
							<tr id="review-<?=$value['id']?>" class="reviews"> 
								<td>
									<div class="responsive-table-th"><label>Details</label></div> 
									<div class="responsive-table-td">

										<div class="school"><a href="<?= $schoolUrl ?>"> <?= $value['schoolName'] ?></a></div>
										<div class="rating-star">

											<?php
												for ($i=1; $i<= $value['rating'];$i++) 
												{?>
													<span class="glyphicon glyphicon-star rating-star"></span>
												<?
												}
											?>
										</div>
										<div class="title">
											<a href="#" class="btn-link"   data-toggle="modal" data-target="#review-details-modal"
											 	data-schoolName="<?= $value['schoolName'] ?>" data-id="<?=$value['id']?>"><?= $value['title']?></a>
												 
										</div>
									</div>
								</td> 
								
								<td>
									<div class="responsive-table-th"><label>Submitted on</label></div> 
									<div class="responsive-table-td">
										<?=date('Y-m-d',strtotime($value['createdDate']))?>
									</div>
								</td>
								
								<td>
									<div class="responsive-table-th"><label>Approved</label></div> 
									<div class="responsive-table-td">
										<?=$approveTxt?>
									</div>
								</td>

								<td>
									<div class="responsive-table-th"><label>Action</label></div> 
									<div class="responsive-table-td">
										
										<a class="deleteReview btn-link" data-id="<?= $value['id'] ?>">Delete</a>
									</div>
								</td>
								
							</tr>
							<?php
						}
						?>
				</table>
			<?php

				echo $create_link;
			}else
			{

			?>
					<!-- <h4>No Reviews Present </h4> -->
					<div class="form-wrapper no-data">

						<div class="icon icon-bullhorn"></div>
					
						<div class="msg">No reviews found</div>

					</div>
			<?php		
				
			}

			?>
			
		</div>

		<!-- modal section  -->
		<div class="modal fade" id="review-details-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">

				<div class="modal-content">
					
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="headerName">Review Details</h4>
					</div>


					<div class="modal-html">
							

							
					</div>

				</div>
			</div>
		</div>
	</div>

</div>

<script>
var globalSiteUrl = "<?= site_url() ;?>";
	$(document).ready(function(){

	

	$('#review-details-modal').on('show.bs.modal', function (e) {

		mainSelector  	= 	$(e.relatedTarget);

		reviewId 		=	$(mainSelector).attr('data-id');

		console.log(reviewId);



		$.ajax({

	            url:globalSiteUrl+'reviews/view',
	            method:'GET',
	            data: {id:reviewId},
	            success:function(response)
	            {

	            	if(response)
	            	{
	            		$(".modal-html").html(response);
	            	}

	            	delteReview();
	            	approveReview();
	            	
	            },
	            error:function()
	            {
	            	console.log("The ajax request failed");
	            }
           
        });
	});


	function delteReview()
	{
	    $('.deleteReview').unbind('click');
	    $('.deleteReview').click(function(e){

	        e.preventDefault();
	        e.stopPropagation();

	       // alert("clicker delete");

	        

	        var target = $(this);
	        var reviewId = target.attr("data-id");

	            if(confirm('Are you sure you want to delete the review?'))
	            {
	    
	               $.ajax({
	                        url:globalSiteUrl+'reviews/delete',
	                        method:'POST',
	                        data: {reviewId:reviewId},
	                        success:function(response)
	                        {
	                            var result = JSON.parse(response);

	                            if(result.status != "success")
	                            {
	                                alert(response);
	                            }
	                            else
	                            {
	                                //$("#"+"approve-id-"+reviewId).html("Yes");

	                                console.log($("#"+"review-"+reviewId));

	                                $('#review-details-modal').modal('hide');

	                                $("#"+"review-"+reviewId).remove();
	                                if($(".reviews").length == 0)
	                                {
	                                    $("#reviewsContainer").html("<h4>No Reviews Present </h4>");
	                                }
	                            }
	                            
	                        },
	                        error:function()
	                        {
	                            console.log("The ajax request failed");
	                        }
	                   
	                });
	            }
	    });

	}

	delteReview();

})

			

</script>