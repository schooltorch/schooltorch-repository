<div class="profile-content section">

	<div class="col-heading">Connect Social Accounts</div>

	<div class="form-group social-signin" style="margin: 20px;">
		
		<?php
			if(isset($user['facebook_id']) && isset($user['facebook_data']))
			{
		?>
				<div class="btn btn-default btn-lg facebook btn-block" onclick="window.location='<?=site_url('facebook/disconnect')?>'"><i class="icon icon-facebook"></i> Disconnect your Facebook account</div>
		<?php
			}
			else
			{
		?>
				<div class="btn btn-default btn-lg facebook btn-block" onclick="window.location='<?=site_url('facebook/auth')?>'"><i class="icon icon-facebook"></i> Connect your Facebook account</div>
		<?php
			}
		?>

		<?php
			if(isset($user['twitter_id']) && isset($user['twitter_data']))
			{
		?>
				<div class="btn btn-default btn-lg twitter btn-block" onclick="window.location='<?=site_url('twitter/disconnect')?>'"><i class="icon icon-twitter"></i> Disconnect your Twitter account</div>
		<?php
			}
			else
			{
		?>
				<div class="btn btn-default btn-lg twitter btn-block" onclick="window.location='<?=site_url('twitter/auth')?>'"><i class="icon icon-twitter"></i> Connect your Twitter account</div>
		<?php
			}
		?>

		<?php
			if(isset($user['google_open_id']) && isset($user['google_data']))
			{
		?>
				<div class="btn btn-default btn-lg google btn-block" onclick="window.location='<?=site_url('google/disconnect')?>'"><i class="icon icon-google"></i> Disconnect your Google account</div>
		<?php
			}
			else
			{
		?>
				<div class="btn btn-default btn-lg google btn-block" onclick="window.location='<?=site_url('google/auth')?>'"><i class="icon icon-google"></i>Connect your Google account</div>
		<?php
			}
		?>
		
		

	</div>

</div>