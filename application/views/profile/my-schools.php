
<div class="profile-content">
		<div style="clear:both;" class="custom-responsive-table" id="mySavedSchoolstable" >
			<div class="col-heading">Saved Schools</div>

				<?php


				if($mySchools['count'] > 0)
				{?>
					<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped my-schools">
						<tr>
							<th>Name</th>
							<th>Cost</th>
							<th>Setting</th> 
							<th>Enrollment</th>
							<th>Selectivity</th>
							<th>Actions</th>
						</tr>

						<?php
							$currentDefaultPos 	=	0;
							foreach($mySchools['data'] as $row)
							{
								
								$photos = getSchoolImage($row->UNITID);
								$imgUrl = '';

								if(count($photos) > 0)
								{
									
									if(isset($photos[0]))
									{	
										$imgUrl = locationPicsBaseUrl().$photos[0];
									}
									else
									{
										$currentDefaultPos++;
										if($currentDefaultPos == 11)
										{
											$currentDefaultPos = 0;
										}

										$imgUrl = base_url().'assets/images/defaults/default-'.$currentDefaultPos.'.jpg';
									}

								}
								else{
									$currentDefaultPos++;
									if($currentDefaultPos == 11)
									{
										$currentDefaultPos = 0;
									}
									$imgUrl = base_url().'assets/images/defaults/default-'.$currentDefaultPos.'.jpg';
								}

									
									
								$isSaved 	= 0;
								
								$savedText 	= 'Save school';

								if($row->SAVEDID && $row->SAVEDID!=""){
									$isSaved 	= 1;
									$savedText 	= 'Unsave';
								}	
								
								$inst_url = site_url().'home/school/'.$row->UNITID.'/'.url_title($row->INSTNM);

								?>

								<tr id="remove-school-<?=$row->UNITID;?>" class="schools">
									<td style="width:40%"> 
										<div class="responsive-table-th"><label>Name</label></div> 
										<div class="responsive-table-td">
											<div class="row">
												<div class="col-md-4 col-sm-4 text-center">
													<div class="img" style="background:url('<?=$imgUrl?>')"></div>
												</div>
												<div class="col-md-8 col-sm-8">
													<div class="name"><a href="<?=$inst_url;?>" class="btn-link"><?=$row->INSTNM;?></a></div>
													<div class="location"><?=$row->CITY;?>, <?=$row->STATENAME;?></div>
												</div>
											</div>

										</div>
									</td>
									<td> 
										<div class="responsive-table-th"><label>Cost</label></div> 
										<div class="responsive-table-td">
											<span class="dollar-<?=$row->TUITIONLABEL?> indicator indicator-dollar" title="$<?=$row->TUITION?>"></span>
										</div>
									</td>
									<td> 
										<div class="responsive-table-th"><label>Setting</label></div> 
										<div class="responsive-table-td">
											<?=$row->LOCALE;?>
										</div>
									</td>
									<td> 
										<div class="responsive-table-th"><label>Enrollment</label></div> 
										<div class="responsive-table-td">
											<?php 
												if($row->TOTALSTUDENTS == 0){
													echo 'N/A';
												}
												else
												{
											?>
													<span class="circle-<?=$row->TOTALSTUDENTSLABEL;?> indicator indicator-circle" title="<?=$row->TOTALSTUDENTS?>"></span>
											<?php
												}
											?>
										</div>
									</td>
									<td> 
										<div class="responsive-table-th"><label>Selectivity</label></div> 
										<div class="responsive-table-td">
											<span class="circle-<?=$row->SELECTIVITY;?> indicator indicator-circle" title="<?=$row->SELECTIVITY?>"></span>
										</div>
									</td>
									
									<td> 
										<div class="responsive-table-th"><label>Actions</label></div> 
										<div class="responsive-table-td">
											<a href="#" class="btn btn-info saveSchool btn-link-success" data-id="remove-school-<?=$row->UNITID;?>" isAct="1" isSaved="<?=$isSaved;?>" dataSchoolId="<?=$row->UNITID;?>" dataUserId="<?=$user_id;?>"><?=$savedText;?></a>
										</div>
									</td>
								</tr>

							<?php
							}

						?>
					</table>


				<?php
				}
				else{?>
					<div class="form-wrapper no-data">
			
						<div class="icon-bullhorn icon"></div>
						
						<div class="msg"> You do not have any saved schools.</div>

					</div>


				<?php
				}
				?>	

		
				
		</div> 
	</div>
</div>

