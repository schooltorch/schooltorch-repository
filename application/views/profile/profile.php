<?php
	
	if($section == 2)
	{
		$section = 1;
	}

	$this->load->view('profile/unregister');

?>
<div class="main_container">
	<div class="container">
	
		<div class="row">
			
			<div class="col-md-3">
				<div class="wrapper">
					<ul class="stacked-options">
						
				        <li class="<?=($section == 1) ? 'active' : ''; ?>"><a  href="<?=site_url()?>profile/basicInfo">Login Settings</a></li>
				        <li class="<?=($section == 4) ? 'active' : ''; ?>"><a  href="<?=site_url()?>profile/background">Background Section</a></li>
				        <li class="<?=($section == 6) ? 'active' : ''; ?>"><a  href="<?=site_url()?>profile/emailSettings">Email Settings</a></li>
				        <li class="<?=($section == 3) ? 'active' : ''; ?>"><a  href="<?=site_url()?>profile/savedSchools">Saved Schools</a></li>
				        
				        <li class="<?=($section == 5) ? 'active' : ''; ?>"><a  href="<?=site_url()?>home/usersReviews">My Reviews</a></li>
				        
				       
				    </ul>
				 </div>
			</div>

			<div class="col-md-9">
				<div class="wrapper bg_white">
					<?php
						if($section == 1 || $section == 2)
						{
							$this->load->view('profile/login-info');
							$this->load->view('profile/change-password');
							$this->load->view('profile/social-connect');
							$this->load->view('profile/basic-info');
							$this->load->view('profile/profile-photo');

					?>
							<div class="profile-content section">
								<div class="col-heading">Delete Account</div>
								<div class="row">
									
									<div class="col-md-12">
										<p></p>
										<a class="btn btn-danger" data-toggle="modal" data-target="#unregister">Delete Account</a>
									</div>

								</div>		
								
							</div>
					<?php
					
						}else if($section == 3)
						{

							$this->load->view('profile/my-schools.php');

						}else if($section == 5)
						{
							$this->load->view('profile/myReviews.php');
						}
						else if($section == 6)
						{
							$this->load->view('profile/emailSettings.php');
						}
						else{

							$this->load->view('profile/backgroundSettings.php');

						}
					?>

				</div>
			</div>





		</div><!--/row-->
	</div><!--/container-->
</div><!--/main_container-->
