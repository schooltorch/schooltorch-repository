<div class="modal fade login-modal" id="unregister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <?php
               $this->load->view('auth/unregister_form');
            ?>
        </div>
    </div>
</div>