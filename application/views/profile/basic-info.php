 <?php

$fname = array(
	'name'	=> 'firstName',
	'id'	=> 'firstName',
	'class' =>  'form-control',
	'value'	=> set_value('firstName')?set_value('firstName'):$user['firstName'],
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'text',
	
);

$lname = array(
	'name'	=> 'lastName',
	'id'	=> 'lastName',
	'class' =>  'form-control',
	'value'	=> set_value('lastName')?set_value('lastName'):$user['lastName'],
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'text',
	
);

$picture = array(
	'name'		=> 	'userfile',
	'id'		=> 	'userfile',
	'class' 	=>  'form-control',
	'value'		=> 	set_value('userfile')?set_value('userfile'):$user['picture'],
	'accept'	=>	"image/gif, image/jpeg, image/jpg, image/png",
	 'style'       => 'border-color:transparent;height: auto; box-shadow:none;padding:0;',
);

?>

<div class="profile-content section">
	<div class="col-heading">Basic Information</div>
	<div class="row">
		
		<div class="col-md-6">

			<form role="form" enctype="multipart/form-data" class="form" method="post" action="<?=site_url();?>profile/basicInfo">

				<div class="form-group">

					<label for="fname" class=" control-label">First Name</label>

					<div class="">
						<?php echo form_input($fname); ?>
						<span class="help-block error-block"><?php echo form_error($fname['name']); ?><?php echo isset($errors[$fname['name']])?$errors[$fname['name']]:''; ?></span>	
					</div>	

				</div>

				<div class="form-group">

					<label for="lname" class=" control-label">Last Name</label>

					<div class="">
						<?php echo form_input($lname); ?>
						<span class="help-block error-block"><?php echo form_error($lname['name']); ?><?php echo isset($errors[$lname['name']])?$errors[$lname['name']]:''; ?></span>	
					</div>	

				</div>

				<div class="form-group">

					<label for="picture" class=" control-label">Profile Picture</label>

					<div class="">
						<?php echo form_upload($picture); ?>
						<span class="help-block error-block"><?php echo form_error($picture['name']); ?><?php echo isset($errors[$lname['name']])?$errors[$picture['name']]:''; ?></span>	
					</div>	

				</div>

				<div class="form-group">

					<div class="preview">

						<img id="imgPreview" style="" class="hide img-responsive" src="" alt="" width="100"/>

					</div>

				</div>

				<div class="form-group">

					<button type="submit" class="btn btn-success ">Save</button>

				</div>

			</form>

		</div>

	</div>		
	
</div>

<script type="text/javascript">

	$(document).ready(function(){

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();


				reader.onload = function (e) {
					$('#imgPreview').attr('src', e.target.result).removeClass('hide');
				}

				reader.readAsDataURL(input.files[0]);
			}
		}

		$('input[type="file"]').change(function(){
			$('.prev-image').remove();
			readURL(this);
		});

		if("<?=$user['picture']?>" != null && "<?=$user['picture']?>" != 'null' && "<?=$user['picture']?>" != "")
		{
			$('#imgPreview').attr('src', "<?=$user['picture']?>").removeClass('hide');
		}

	})

</script>