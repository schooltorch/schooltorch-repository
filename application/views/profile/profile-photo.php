

	<?php

		$showFlag 			=	0;

		$profilePicFlag 	=	0;

		if(isset($user['profilePicFlag']))
		{
			if(in_array($user['profilePicFlag'], array(1,2,3)))
			{
				$profilePicFlag 	=	$user['profilePicFlag'];
			}
		}

		if(isset($user['picture']))
		{
			$showFlag 		=	1;
			$uploadedPic 	=	$user['picture'];
		}
		else
		{
			$uploadedPic 	=	'';
		}

		$fbData 			=	array();
		$twitterData 		=	array();
		$googleData 		=	array();

		if(isset($user['facebook_data']))
		{
			try{
				$fbData 	=	$user['facebook_data'];
			}
			catch(Excaption $e)
			{

			}
		}

		if(isset($user['twitter_data']))
		{
			try{
				$twitterData 	=	$user['twitter_data'];
			}
			catch(Excaption $e)
			{
				
			}
		}

		if(isset($user['google_data']))
		{
			try{
				$googleData 	=	$user['google_data'];
			}
			catch(Excaption $e)
			{
				
			}
		}

		if(isset($fbData['picture']))
		{
			$showFlag 		=	1;
			$fbPic 			=	$fbData['picture'];
		}
		else
		{
			$fbPic 			=	'';
		}

		if(isset($twitterData['picture']))
		{
			$showFlag 		=	1;
			$twitterPic 	=	$twitterData['picture'];
		}
		else
		{
			$twitterPic 	=	'';
		}

		if(isset($googleData['picture']))
		{
			$showFlag 		=	1;
			$googlePic 		=	$googleData['picture'];
		}
		else
		{
			$googlePic 		=	'';
		}

	?>

	<?php

		if($showFlag == 1)
		{
	?>
			<div class="profile-content section profile-img-selector">

				<div class="col-heading">Select your Profile Photo</div>

	<?php

			if($uploadedPic != '')
			{
	?>
			<div class="row form-group">
				<div class="col-md-1 col-sm-1 col-xs-4 ">
					<img style="" class="img-responsive" src="<?=$uploadedPic?>" alt="">
				</div>

				<div class="col-md-11 col-sm-11 col-xs-8">

					<div class="radio">
						<label>
							<input type="radio" data-custom="all" class="changePic" name="same" value="0">
							Use my uploaded profile picture
							
						</label>
					</div>

				</div>

			</div>
				
	<?php
			}

	?>

	<?php

			if($fbPic != '')
			{
	?>

		<div class="row form-group">
					<div class="col-md-1 col-sm-1 col-xs-4 ">
					<img style="" class="img-responsive" src="<?=$fbPic?>" alt="">
				</div>

				<div class="col-md-11 col-sm-11 col-xs-8">

					<div class="radio">
						<label>
							<input type="radio" data-custom="all" class="changePic" name="same" value="1">
							Use my Facebook profile image
							
						</label>
					</div>

				</div>

			</div>

				
	<?php
			}

	?>

	<?php

			if($twitterPic != '')
			{



	?>

		<div class="row form-group">
					<div class="col-md-1 col-sm-1 col-xs-4 ">
					<img style="" class="img-responsive" src="<?=$twitterPic?>" alt="">
				</div>

			<div class="col-md-11 col-sm-11 col-xs-8">

					<div class="radio">
						<label>
							<input type="radio" data-custom="all" class="changePic" name="same" value="2">
							Use my Twitter profile image
							
						</label>
					</div>

				</div>

			</div>
				
	<?php
			}
	?>

	<?php

			if($googlePic != '')
			{
	?>

		<div class="row form-group">
					<div class="col-md-1 col-sm-1 col-xs-4 ">
					<img style="" class="img-responsive" src="<?=$googlePic?>" alt="">
				</div>

				<div class="col-md-11 col-sm-11 col-xs-8">

					<div class="radio">
						<label>
							<input type="radio" data-custom="all" class="changePic" name="same" value="3">
							Use my Google profile image
							
						</label>
					</div>

				</div>

			</div>
				

			
	<?php
			}

	?>

	</div>
<?php
	}
?>

<script type="text/javascript">
	
	$(document).ready(function(){

		$('.changePic').unbind('change');
		$('.changePic').change(function(event){

			var value =	$(this).val();

			window.location.href 	=	"<?=site_url('profile/profilePhotoSettings')?>/"+ value;

		})
		

	})

</script>