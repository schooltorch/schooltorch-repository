 <?php
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'class'  => 'form-control input-lg',
	'placeholder' =>'',
	'size'	=> 30,
);
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'class' =>  'form-control',
	'value'	=> set_value('email')?set_value('email'):$user['email'],
	'placeholder' =>'',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'email',
	
);

?>

<div class="profile-content section">
	<div class="col-heading">Login Information</div>
	<div class="row">
		
		<div class="col-md-6">

			<form role="form" class="form" method="post" action="<?=site_url();?>auth/change_email">

				<div class="form-group">
					<?php echo form_label('Email Address', $email['id']); ?>
					<?php echo form_input($email); ?>
					<span class="help-block error-block"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></span>
				</div>
			
				<div class="form-group">
					<?php echo form_label('Password', $password['id']); ?>
					<?php echo form_password($password); ?>
					<span class="help-block error-block"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></span>

				</div>

				<div class="form-group">

					<button type="submit" class="btn btn-success ">Save</button>

				</div>

			</form>











		</div>



	</div>




		
	
</div>