Welcome to <?php echo $site_name; ?>,

<?php echo $site_name; ?> SchoolTorch helps you discover, research and empower yourself with the best higher education options.

Let’s get you started:

- *Verify your email address within <?php echo $activation_period; ?> hours by following this link:*

<?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>




-*Read reviews: *Get the inside scoop on schools, locations, majors and
  more from people who studied at your schools of interest.

- *Write a review: *Studied at a school you find on SchoolTorch? Share
your thoughts and help the community! 

*We hope you enjoy <?php echo $site_name; ?> !*

<?php if (strlen($username) > 0) { ?>

Your username: <?php echo $username; ?>
<?php } ?>

Your email address: <?php echo $email; ?>
<?php if (isset($password)) {  ?>

Your password: <?php echo $password; ?>
<?php  } ?>



Have fun!
The <?php echo $site_name; ?> Team