<?php include 'email-header.php'; ?>

<!--email-body-->

	<table width="80%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			
			<td align="left">
				<h2 style=" margin: 0; padding: 0 0 18px; ">Your new email address on <?php echo $site_name; ?></h2>
				You have changed your email address for <?php echo $site_name; ?>.<br />
				Follow this link to confirm your new email address:<br />
				<br />
				<big style=""><b><a href="<?php echo site_url('/auth/reset_email/'.$user_id.'/'.$new_email_key); ?>" style="color: #555;;">Confirm your new email</a></b></big><br />
				
				<br />
				Link doesn't work? Copy the following link to your browser address bar:<br />
				
				<nobr><a href="<?php echo site_url('/auth/reset_email/'.$user_id.'/'.$new_email_key); ?>" style="color: #555;"><?php echo site_url('/auth/reset_email/'.$user_id.'/'.$new_email_key); ?></a></nobr><br />
				<br />
				<br />
				Your email address: <?php echo $new_email; ?><br />
				<br />
				<br />
				You received this email, because it was requested by a <a href="<?php echo site_url(''); ?>" style="color:  #555;;"><?php echo $site_name; ?></a> user. If you have received this by mistake, please DO NOT click the confirmation link, and simply delete this email. After a short time, the request will be removed from the system.<br />
				<br />
				<br />
				Thank you,<br />
				The <?php echo $site_name; ?> Team
			</td>
		</tr>
	</table>

<!--/email-body-->



<?php include 'email-footer.php' ;?>
