<?php include 'email-header.php'; ?>

<!--email-body-->
	<!-- <table width="600" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ddd;background: #fff; padding:30px;margin:10px 0 0">
		<tr>
			<td align="left">
				<h2 style="margin: 0; padding: 0 0 18px; ">Welcome to <?php echo $site_name; ?>!</h2>
				Thanks for joining <?php echo $site_name; ?>. We listed your sign in details below, make sure you keep them safe.<br />
				To verify your email address, please follow this link:<br />
				<br />
				<big style=""><b><a href="<?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>" style="color: #555;">Finish your registration...</a></b></big><br />
				<br />
				Link doesn't work? Copy the following link to your browser address bar:<br />
				<nobr><a href="<?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>" style="color: #555;	 "><?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?></a></nobr><br />
				<br />
				Please verify your email within <?php echo $activation_period; ?> hours, otherwise your registration will become invalid and you will have to register again.<br />
				
				<br />
				<br />
				<?php if (strlen($username) > 0) { ?>Your username: <?php echo $username; ?><br /><?php } ?>
				Your email address: <?php echo $email; ?><br />
				<?php if (isset($password)) {  ?>Your password: <?php echo $password; ?><br /><?php } ?>
				<br />
				<br />
				Have fun!<br />
				The <?php echo $site_name; ?> Team
			</td>
		</tr>
	</table> -->
<!--/email-body-->

	<table width="600" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ddd;background: #fff; padding:30px;margin:10px 0 0">
		<tr>
			<td align="left">
				<h2 style="margin: 0; padding: 0 0 18px; ">Welcome to <?php echo $site_name; ?>!</h2>
				<?php echo $site_name; ?> helps you discover, research and empower yourself with the best
				higher education options.<br />
				<br/>
				<br/>
				Let’s get you started:
				<br/>
				<br/>
				<br/>
				<strong>Verify your email address within <?php echo $activation_period; ?> hours by following this link:</strong><br />
				<br />
				<big style=""><b><a href="<?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>" style="color: #555;">Finish registration</a></b></big><br />
				<br />
				If you have issues, please copy the following into your browser
				address bar:<br />
				<nobr><a href="<?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?>" style="color: #555;	 "><?php echo site_url('/auth/activate/'.$user_id.'/'.$new_email_key); ?></a></nobr><br />
				<br />				
				<br />
				<br />
				<strong>Read reviews: </strong>Get the inside scoop on schools, locations, majors and
   				more from people who studied at your schools of interest.
   				<br />
				<br />
				<strong>Write a review: </strong>Studied at a school you find on SchoolTorch? Share
   				your thoughts and help the community!
   				<br />
				<br />
				<strong>We hope you enjoy <?php echo $site_name; ?> !</strong>
				<br/>
				<br/>
				<?php if (strlen($username) > 0) { ?>Your username: <?php echo $username; ?><br /><?php } ?>
				Your email address: <?php echo $email; ?><br />
				<?php if (isset($password)) {  ?>Your password: <?php echo $password; ?><br /><?php } ?>
				<br />
				<br />
				Have fun!<br />
				The <?php echo $site_name; ?> Team
			</td>
		</tr>
	</table>

<?php include 'email-footer.php' ;?>