<?php include 'email-header.php'; ?>

<!--email-body-->
	<table width="600" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ddd;background: #fff; padding:30px;margin:10px 0 0">
		<tr>
			
			<td align="left" width="100%">

				<h2 style="margin: 0; padding: 0 0 18px;">Create a new password</h2>
				Forgot your password, huh? No big deal.<br />
				To create a new password, just follow this link:<br />
				<br />
				<big style=""><b><a href="<?php echo site_url('/auth/reset_password/'.$user_id.'/'.$new_pass_key); ?>" style="color: #555;">Create a new password</a></b></big><br />
				<br />
				Link doesn't work? Copy the following link to your browser address bar:<br />
				<nobr><a href="<?php echo site_url('/auth/reset_password/'.$user_id.'/'.$new_pass_key); ?>" style="color: #555;"><?php echo site_url('/auth/reset_password/'.$user_id.'/'.$new_pass_key); ?></a></nobr><br />
				<br />
				<br />
				You received this email, because it was requested by a <a href="<?php echo site_url(''); ?>" style="color: #555;"><?php echo $site_name; ?></a> user. This is part of the procedure to create a new password on the system. If you DID NOT request a new password then please ignore this email and your password will remain the same.<br />
				<br />
				<br />
				Thank you,<br />
				The <?php echo $site_name; ?> Team
			</td>
		</tr>
	</table>
<!--/email-body-->


<?php include 'email-footer.php' ;?>

