<html>
	<?php include 'email-header.php'; ?>

	<!--email-body-->
		<table width="600" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ddd;background: #fff; padding:30px;margin:10px 0 0">
			<tr>
				<td align="left" width="95%" style="font: 13px/18px Arial, Helvetica, sans-serif;">
					
					
					Hello,</br>

					You received a new message from the <?= $site_name ?> website

					<ul style="list-style:none outside;">
						<li style="padding: 5px 0;">
							<label>Name: </label><?=$name?>	
						</li>

						<li style="padding: 5px 0;">
							<label>Email: </label><?=$email?>
						</li>

						<li style="padding: 5px 0;">
							<label>Subject: </label><?=$subject?>
						</li>
						<li style="padding: 5px 0;">
							<label>Message: </label><?=$message?>
						</li>


					</ul>
						
							
						
						
				</td>
			</tr>
		</table>

	<!--/email-body-->

	<?php include 'email-footer.php' ;?>
</html>