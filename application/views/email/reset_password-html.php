<?php include 'email-header.php'; ?>

<!--email-body-->
	<table width="600" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #ddd;background: #fff; padding:30px;margin:10px 0 0">
		<tr>
			<td align="left">
				
				<h2 style=" margin: 0; padding: 0 0 18px; color: black;">Your new password on <?php echo $site_name; ?></h2>
				You have changed your password.<br />
				Please, keep it in your records so you don't forget it.<br />
				<br />
				<?php if(isset($username)){ if (strlen($username) > 0) { ?>Your username: <?php echo $username; ?><br /><?php } }?>
				Your email address: <?php echo $email; ?><br />
				<?php /* Your new password: <?php echo $new_password; ?><br /> */ ?>
				<br />
				<br />
				Thank you,<br />
				The <?php echo $site_name; ?> Team
			</td>
		</tr>
	</table>

<!--/email-body-->



<?php include 'email-footer.php' ;?>


