
<?php

	if(!isset($onlySchools))
	{
		$onlySchools 	=	0;
	}

?>


	<input type="text" class="form-control" data-image="popular-image1" id="searchSchool" name="schoolId" placeholder="Enter school name to add" autocomplete="on" data-id="" value="">



<script>
		
	$(function(){

		var url 	=	"<?= site_url('search');?>";

		if("<?=$onlySchools?>" == 1)
		{
			var url 	=	"<?= site_url('search?onlySchools=1');?>";
		}

		$("#searchSchool").autocomplete({

			source: url,

			minLength: 1,

			select: function( event, ui ) {

				console.log(event);

				$(this).attr("value", ui.item.id);

				console.log(ui.item);

			}

		});

	});

</script>