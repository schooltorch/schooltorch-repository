<?php

	if(!isset( $filterAction))
	{
		$filterAction ="";
	}

	$isAdmin	=	0;

	if(!isset($until))
	{
		$to 	=	 '';
	}
	else
	{
		$to 	=	 $until;
	}

	if(!isset($since))
	{
	 	$from 	=	 '';
	}
	else
	{
		$from 	=	 $since;
	}

	if($to == '')
	{
		$to 	=	 date("m/d/Y", time());
	}
	else
	{
		$to 	=	 date("m/d/Y", strtotime($until));
	}

	$to_max 	=	date("m/d/Y", time());

	if($from == '')
	{
		$from 	=	 date("m/d/Y", time()- (86400 * 7));
	}
	else
	{
		$from 	=	 date("m/d/Y", strtotime($since));
	}

	$from_max 	=	date("m/d/Y", strtotime($to));

	if(!isset($jobId))
	{
		$jobId 	=	0;
	}

	$schoolName 	=	'';

	$schoolId 		=	0;

	if(isset($filterBy) && isset($filter))
	{
		if($filterBy ==	'schoolId' && $filter != 0)
		{
			$schoolId 		=	$filter;
			$schoolName 	=	getSchoolName($filter);
		}
	}

?>

<div class="filter">

	<div id="toggelSearch" class="btn btn-default">
		<span class="glyphicon glyphicon-search btn-icon "></span> Search
	</div>

	<form class="form-inline hide" id="form_filter" role="form" action="<?= $filterAction ?>" method="GET">
		<div class="form-group">
			<label class="sr-only" for="filter">Search</label>
			<input type="text" class="form-control" id="filter" name="filter" placeholder="Enter text for search">
		</div>

		<div class="form-group hide" id="schoolsList">
			<?php
				$this->load->view('common/schoolAutocomplete', array('onlySchools' => 1, 'schoolName' => $schoolName, 'schoolId' => $schoolId));
			?>
		</div>

		<div class="date-filter-wrapper form-group">
			<div class="form-group">
				<div class='input-group date' id="from">
                    <input type='text' class="form-control" id="fromVal" name="fromVal"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
          		  </div>
			</div>


			<div class="form-group">
				<div class='input-group date' id='to'>
                    <input type='text' class="form-control" id="toVal" name="toVal" />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar" ></span>
                    </span>
				</div>
			</div>


		</div>
		

		<div class="form-group" >
			<div class="select-filter-wrapper hide">
				<select class="form-control" id="filterStatus">
				</select>
			</div>
		</div>

		<div class="form-group">
			
			<select name="filterBy" class="form-control" id="filterBy">

				<?php

					if(isset($filterOption))
					{	

						foreach ($filterOption as $key => $value)
						{
							$suboption 	=	0;
							if(isset($value['suboptions']))
							{
								if(count($value['suboptions']) > 0)
								{
									$suboption 	=	1;
								}
							}

							if($suboption == 0)
							{
								$optionsStr 	=	'';

								if($value['type'] == 'select')
								{
									if(isset($value['options']))
									{
										if(is_array($value['options']))
										{
											$optionsStr 	=	json_encode($value['options']);
										}
									}
								}

				?>
								<option value="<?=  $key ?>" type="<?= $value['type']?>" options="<?= htmlspecialchars($optionsStr)?>"><?=  $value['value'] ?></option>
				<?php
							}
							else
							{



				?>
								
				<?php
							}

						}
					}
				?>
			</select>
		</div>

   		<div class="form-group">
			<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search btn-icon "></span> Submit</button>
			<button type="button" class="btn btn-link" onClick="clear_filter()">Clear</button>
		</div>

		
	</form>

	<?php
		if($isAdmin == 1)
		{
	?>
		<!-- <div class="filter pull-right">
			<a id="exportData" title="Export" href="#">Export</a>
		</div> -->
	<?php
		}
	?>
	
</div>

<script>

	var url 		=	"<?=$filterAction?>";

	if(url.indexOf('?') != -1)
	{
		url 	+=	'&';
	}
	else
	{
		url 	+=	'?';
	}
	
	var dateData 		=	['reservationDates', 'dates', 'deadline'];

	var autocomplete 	=	['schoolId'];

	var selectData 		=	['role', 'customers', 'suppliers', 'categories', 'roles', 'months', 'year', 'status', 'class', 'creator', 'method', 'priority', 'isPartTime','isApproved'];

	function filter(exportFlag, data){

		if(typeof exportFlag == 'undefined')
		{
			exportFlag 	=	0;
		}

		if(typeof data == 'undefined')
		{
			data 		=	0;
		}

		var order_by 	= 	getQueryVariable("order_by");
		
		var order 		= 	getQueryVariable("order");

		var isAll 		= 	getQueryVariable("isAll");

		var order_url 	= 	'';

		if(order_by != "" && order_by != 'order_by')
		{
			order_url 	= 	'order_by='+ order_by+'&order='+ order+'&';
		}

		if(isAll != 'isAll')
		{
			order_url 	+= 	'isAll='+ isAll +'&';
		}

		if(dateData.indexOf($('#filterBy').val()) != -1)
		{
			if($('#toVal').val() != '' && $('#fromVal').val() != '')
			{
				if(exportFlag == 1)
				{
					window.open(url+ order_url+'filterBy='+ $('#filterBy').val() +'&to='+ escape($('#toVal').val())+ '&from='+ escape($('#fromVal').val()), '_blank');
				}
				else
				{
					window.location 	= 	url+ order_url+'filterBy='+ $('#filterBy').val() +'&until='+ escape($('#toVal').val())+ '&since='+ escape($('#fromVal').val());
				}
			}
			else if(exportFlag == 1)
			{
				window.open(url+ order_url, '_blank');
			}
		}
		else if(selectData.indexOf($('#filterBy').val()) != -1)
		{
			if($('#filterStatus').val() != '' && $('#filterStatus').val() != null)
			{
				if(exportFlag == 1)
				{
					window.open(url+ order_url+'filterBy='+ $('#filterBy').val() +'&filter='+ escape($('#filterStatus').val()), '_blank');
				}
				else
				{
					window.location 	= 	url+ order_url+'filterBy='+ $('#filterBy').val() +'&filter='+ escape($('#filterStatus').val());
				}
				
			}
			else if(exportFlag == 1)
			{
				window.open(url+ order_url, '_blank');
			}
		}
		else if(autocomplete.indexOf($('#filterBy').val()) != -1)
		{
			if($('#schoolsList #searchSchool').attr('value') != '')
			{
				if(exportFlag == 1)
				{
					order_url += 'isExport=1&';
					window.open(url+ order_url+'filterBy='+ $('#filterBy').val() +'&filter='+ escape($('#schoolsList #searchSchool').attr('value')), '_blank')
				}
				else
				{
					window.location 	= 	url+ order_url+'filterBy='+ $('#filterBy').val() +'&filter='+ escape($('#schoolsList #searchSchool').attr('value'));
				}
			}
			else if(order_url != '')
			{
				window.location 	= 	url+ order_url;
			}
		}
		else
		{
			if($('#filter').val() != '')
			{
				if(exportFlag == 1)
				{
					order_url += 'isExport=1&';
					window.open(url+ order_url+'filterBy='+ $('#filterBy').val() +'&filter='+ escape($('#filter').val()), '_blank')
				}
				else
				{
					window.location 	= 	url+ order_url+'filterBy='+ $('#filterBy').val() +'&filter='+ escape($('#filter').val());
				}
			}
			else if(order_url != '')
			{
				window.location 	= 	url+ order_url;
			}
			
		}
		

	}

	function clear_filter(){

		var order_url	=	'';

		if($('.categoryFilter.active').length > 0)
		{
			var categoryId 	=	$('.categoryFilter.active').attr('data-category');
			
			order_url 		+=	'tabId='+ categoryId+'&';
		}

		var restaurantId 		= 	getQueryVariable("restaurantId");

		if(restaurantId != "" && restaurantId != 'restaurantId')
		{
			order_url 	+= 	'restaurantId='+ restaurantId;
		}

		window.location 	= 	url+order_url;

	}

	function updateSelectBox(){

		var selectedItem 		=	$('#filterBy').val();

		var selector 			=	$('#filterBy option[value='+ selectedItem +']');

		if($(selector).attr('options') != '')
		{
			var options 		=	$(selector).attr('options');
			
			options 			=	JSON.parse(options);

			var str 			=	'';

			$.each(options, function(key, value){

				str 		+=	'<option value="'+ value.id +'">'+ value.value +'</option>'

			})

			if(str 	!= '')
			{
				$('#filterStatus').html(str);
			}

			if(getQueryVariable('filter') != 'filter')
			{
				if($('#filterStatus option[value="'+ getQueryVariable('filter') +'"]').length > 0)
				{
					$('#filterStatus').val(getQueryVariable('filter'));
				}
			}

		}

	}

	function getQueryVariable(variable) {

		var query = window.location.search.substring(1);
	  
		var vars = query.split("&");

		for (var i=0;i<vars.length;i++) {

			var pair = vars[i].split("=");

			if (pair[0] == variable) 
			{
				return pair[1];
			}
		} 
		
		return variable;

	}

	function updateAutocomplete()
	{

	}
	
	$(document).ready(function(event){
		
		$(document).on('submit','#form_filter',function(e){

			e.preventDefault();
			filter(0);

		});

		$('#toggelSearch').unbind('click')
		$('#toggelSearch').click(function(event){

			if($('#form_filter').is(':visible'))
			{
				$('#form_filter').addClass('hide');
				$('#toggelSearch').html('<span class="glyphicon glyphicon-search btn-icon "></span>Search').removeClass('hide');
			}
			else
			{
				$('#form_filter').removeClass('hide');
				$('#filter').focus();
				$('#filterBy').trigger('change');
				$('#toggelSearch').html('Hide').addClass('hide');
			}

		});

		if(getQueryVariable('filterBy') != 'filterBy')
		{
			var filterBy 	=	getQueryVariable('filterBy');

			if($('#filterBy option[value='+ filterBy +']').length > 0)
			{
				$('#filterBy').val(filterBy);

				if(getQueryVariable('filter') != 'filter')
				{
					if(dateData.indexOf($('#filterBy').val()) == -1 && selectData.indexOf($('#filterBy').val()) == -1 && autocomplete.indexOf($('#filterBy').val()) == -1)
					{
						$('#filter').val(decodeURI(getQueryVariable('filter')));
					}
				}
			}

			if(dateData.indexOf(getQueryVariable('filterBy')) != -1)
			{
				$('.date-filter-wrapper').removeClass('hide');
				$('.select-filter-wrapper').addClass('hide');
				$('#schoolsList').addClass('hide');
				$('#filter').addClass('hide');
			}
			else if(selectData.indexOf(getQueryVariable('filterBy')) != -1)
			{
				updateSelectBox();
				$('.select-filter-wrapper').removeClass('hide').show();
				$('.date-filter-wrapper').addClass('hide');
				$('#schoolsList').addClass('hide');
				$('#filter').addClass('hide');
			}
			else if(autocomplete.indexOf($('#filterBy').val()) != -1)
			{
				$('.date-filter-wrapper').addClass('hide');
				$('.select-filter-wrapper').addClass('hide');
				$('#schoolsList').removeClass('hide');
				$('#filter').addClass('hide');
			}
			else
			{
				$('.date-filter-wrapper').addClass('hide');
				$('.select-filter-wrapper').addClass('hide');
				$('#schoolsList').addClass('hide');
				$('#filter').removeClass('hide');
			}

			$('#toggelSearch').trigger('click');

		}

		$('#filterBy').unbind('change')
		$('#filterBy').change(function(event){

			if(dateData.indexOf($('#filterBy').val()) != -1)
			{
				$('.date-filter-wrapper').removeClass('hide');
				$('.select-filter-wrapper').addClass('hide');
				$('#schoolsList').addClass('hide');
				$('#filter').addClass('hide');
			}
			else if(selectData.indexOf($('#filterBy').val()) != -1)
			{
				updateSelectBox();
				$('.select-filter-wrapper').removeClass('hide').show();
				$('.date-filter-wrapper').addClass('hide');
				$('#schoolsList').addClass('hide');
				$('#filter').addClass('hide');
			}
			else if(autocomplete.indexOf($('#filterBy').val()) != -1)
			{
				$('.date-filter-wrapper').addClass('hide');
				$('.select-filter-wrapper').addClass('hide');
				$('#schoolsList').removeClass('hide');
				$('#filter').addClass('hide');
			}
			else
			{
				$('.date-filter-wrapper').addClass('hide');
				$('.select-filter-wrapper').addClass('hide');
				$('#schoolsList').addClass('hide');
				$('#filter').removeClass('hide');
			}

		});
		

		$('#from').datetimepicker({
			pickTime: false,
			maxDate: new Date("<?=$to?>")
		});
		
		$('#to').datetimepicker({
			pickTime: false,
			minDate: new Date("<?=$from?>"),
			maxDate: new Date("<?=$to_max?>")
		});

		$('#from').data("DateTimePicker").setDate("<?=$from?>");
		$('#to').data("DateTimePicker").setDate("<?=$to?>");
		
		
		$('#to').on('dp.change',function(event){

			if($('#fromVal').val() != '')
			{
				$('#from').data("DateTimePicker").setMaxDate(new Date($('#toVal').val()));
			}

		});

		
		$('#from').on('dp.change',function(event){

			if($('#toVal').val() != '')
			{
				$('#to').data("DateTimePicker").setMinDate(new Date($('#fromVal').val()));
			}

		})

		$('#exportData').unbind('click')
		$('#exportData').click(function(event){

			event.preventDefault();

			filter(1);

		});

		

	});

</script>