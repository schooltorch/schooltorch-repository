<div class="container">
	
	<div class="row">

		<div class="col-md-12">
			
			<h1 class="page-header">

				<?=$page_header;?>

			</h1>

		</div>	

		<div class="col-md-7">
			
			<h3> Surveys </h3>
			
			<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">
				
				<tr>
					<th class="text-center">Id</th>
					<th>Name</th>
					<th class="text-center">Action</th>
				</tr>
				
				<?php
				foreach ($surveys as $key => $value) {
					
					?>
					<tr>
						<td class="text-center"><?=$value['id'];?></td>
						<td><?=$value['name'];?></td>
					    <td class="text-center"><a href="<?=site_url();?>admin/survey/questions/<?=$value['id'];?>">Manage Questions </a> | <a href="">Edit </a> | <a href="<?=site_url();?>admin/survey/deleteSurvey/<?=$value['id'];?>" onClick="return confirm('Are you sure you want to delete this Survey?')">Delete </a></td>
					</tr>	
					<?php

				}
				?>

			</table>

		</div>

		<div class="col-md-5">

			<h3>Add Survey</h3>
			
			<form role="form" action="<?=site_url("admin/survey/addSurvey");?>" method="post">
			  	
			  	<div class="form-group">
			  		<label>
			  			Title
			  		</label>
					<input type="text" name="name" class="form-control input-lg" id="name" placeholder="Enter name of Survey">
				</div>

				<div class="form-group">
					
					<label>
						Category
					</label>
					
					<select class="form-control input-lg" multiple name="category[]" id="catgeory">
						<option value="1">MBA</option>
						<option value="2">MTECH</option>	
						<option value="3">BTECH</option>	
						<option value="4">MS</option>	
					</select>	

				</div>

				<button type="submit" class="btn btn-success">Submit</button>
  				
			</form>

		</div>

	</div><!--/row-->

</div><!--/container-->		

