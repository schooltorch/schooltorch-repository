<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">
				User Management
			</h1>
		</div>
		<div class="col-md-12">
			<?php
				$this->load->view("/admin/filter2");
			?>
		</div>
		<div class="col-md-12">
			
			<?php
				if($status == 'success' && count($data) > 0)
				{?>

			   		<div style="clear:both;" class="custom-responsive-table">
						<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">
							<tr>
								<th>Name</th>
								<th>Role</th>
								<th>Email</th>
								<th>Action</th>
							</tr>

							<?php
								foreach ($data as $key => $value) {
										
									$role 				=	'Normal User';
									$action 			=	'Make Admin';
									$flag 				=	2;

									if($value['role'] == 2)
									{
										$role 			=	'Admin';
										$action 		=	'Make Normal User';
										$flag 			=	1;
									}
									?>

									<tr>
										<td><div class="responsive-table-th"><label>Name</label></div> 
											<div class="responsive-table-td">
											<?=$value['name']?>											
											</div>
										</td>
										<td><div class="responsive-table-th"><label>Role</label></div> 
											<div class="responsive-table-td">
											<?=$role?>
											</div>
										</td>
										<td><div class="responsive-table-th"><label>Email</label></div> 
											<div class="responsive-table-td">
											<?=$value['email']?>
											</div>
										</td>
										<td><div class="responsive-table-th"><label>Action</label></div> 
											<div class="responsive-table-td">
											<?= anchor('admin/user/update?id='. $value['id'] .'&role='. $flag, $action,array('class'=>"btn-link")) ?> | <?= anchor('admin/user/delete/'. $value['id'], "Delete User",array('class'=>"btn-link")) ?>
											</div>
										</td>
									</tr>

								<?php
								}
							?>

						</table>
					</div>
			
					<?php
						
						echo $pagination_str;
						
					?>


				<?php
				}
				else
				{
					if(isset($message))
					{
						echo $message;
						echo anchor('/admin/user', 'Go Back');
					}
				?>
					<div class="form-wrapper no-data">

						<div class="icon"></div>
					
						<div class="msg"> No Users Present.</div>

					</div>
			<?php		
				}

			?>

			</div>
		</div>
	</div>
</div>
