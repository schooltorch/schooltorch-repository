<div class="container">
	
	<div class="row">

		<div class="col-md-12">

			<h1 class="page-header">
				<?=$page_title;?>
			</h1>
			
		</div>	

		<div class="col-md-7">

			<h3> Categories </h3>

			<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">

				<tr>
					<th>Id</th>
					<th>Name</th>
					<th>Last Updated</th>
					<th>Action</th>
				</tr>

				<?php
				foreach ($categories as $key => $value) {
					?>
					<tr>
						<td><?=$value['id'];?></td>
						<td><?=$value['name'];?></td>
						<td><?=formatDate($value['updatedDate']);?></td>
						<td><a href="">Edit </a> | <a href="">Delete </a></td>
					</tr>	
					<?php
				}
				?>

			</table>

		</div>

		<div class="col-md-5">

			<h3>Add Categories</h3>
			
			<form role="form"  action="<?= site_url("admin/survey/addCategories");?>" method="post">
			  	
			  	<div class="form-group">
					<input type="text" name="name" class="form-control input-lg" id="name" placeholder="Enter name of Category">
				</div>

				<button type="submit" class="btn btn-success">Submit</button>
  				
  				<a href="<?= site_url("admin/survey") ?>" class="btn"> Cancel </a>
  	
			</form>

		</div>

	</div><!--/row-->

</div><!--/container-->

		

