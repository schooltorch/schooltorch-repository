<div class="container">
		
	<div class="row">

		<div class="col-md-12">
			
			<h1 class="page-header">
				
				<?=$page_title;?>

			</h1>

		</div>	

		<div class="col-md-4">

			<ul class="sidebar-nav">
					
				<li class="sidebar-brand">
                    <a href="#">Single Choice (Radio) </a>
                    <a href="#" class="pull-right addOption" data-type="1">+Add</a>
                </li>

                <li class="sidebar-brand">
                    <a href="#">Multiple Choice (Checkbox) </a>
                    <a href="#" class="pull-right addOption" data-type="2">+Add</a>
                </li>

            </ul>

		</div>	

		<div class="col-md-8">

			<form method="post" action="<?=site_url();?>admin/survey/addQuestion/<?=$surveyId;?>">

				<div class="form-group">

					<label>
						Question	
					</label>

					<input type="text" class="form-control" name="question" id="question">

				</div>
				
				<div class="form-group">
						
					<label>
						Answers	
					</label>

					<div class="row">

						<div class="col-md-6">

							<div id="answersContainer">



							</div>	
							
						</div>
						
					</div>
						
				</div>

				<div class="form-group">
					<button class="btn btn-default" id="addQuestions">
						Add
					</button>
				</div>

				<!-- <div class="form-group text-right">
					<button class="btn btn-primary">
						Next
					</button>
				</div>	
 -->
			</form>	

		</div>

	</div><!--/row-->

</div><!--/container-->

<script>
	
	$(document).ready(function(){

		var selectedType = 0;
		var optionsCount = 1;

		$('.addOption').click(function(){

			var optionBox 	= '';
			var optionType 	= '';

			if(selectedType == 0)
			{
				selectedType = $(this).attr('data-type');
			}

			if($(this).attr('data-type') == 1 && selectedType == $(this).attr('data-type'))
			{
				optionType = 'radio';
			}
			else if($(this).attr('data-type') == 2 && selectedType == $(this).attr('data-type'))
			{
				optionType = 'checkbox';
			}

			if(selectedType == $(this).attr('data-type'))
			{	

				optionBox = '<div class="form-group optionBox" rel="'+optionsCount+'">'+
								'<input type="'+optionType+'" checked disabled class="pull-left">'+
								'<a href="#" class="removeOption" rel="'+optionsCount+'">Remove</a>'+
								'<input type="text" name="options" class="form-control pull-right questionOption"/>'+
							'</div>';

				$('#answersContainer').append(optionBox);

				optionsCount++;
				
			}

			removeOption();
				
		})


		function removeOption(){

			$(".removeOption").unbind();
			$(".removeOption").on( "click", function() {
			  	
				$('.optionBox[rel='+$(this).attr('rel')+']').remove();

			});

		}

		$('#addQuestions').click(function(event){

			event.preventDefault();
				
			var questionOption = $('.questionOption');
			var questionOptionLabels = [];

			$.each(questionOption,function(key,value){

				questionOptionLabels.push($(this).val());

			})

			var data = {

				type:selectedType,
				question:$('#question').val(),
				questionOption:questionOptionLabels

			}

			$.ajax({

				url:'<?=site_url();?>admin/survey/addQuestion',
				data:data,
				type:'POST',
				success:function(){

					

				}

			})



		})
	
	})

</script>