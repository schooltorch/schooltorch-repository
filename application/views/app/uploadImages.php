<?php

	$order_parameters = array('isApproved','createdDate');

	$url 	=	site_url("admin/uploadImages");

	foreach($order_parameters as $val)
	{
	 	if($order_by == $val)
	 	{
	 		$order_by_active[$val] 	= 'active';
	 		$order_active[$val] 	= $order;
	 	}
	 	else
	 	{
	 		$order_by_active[$val] = '';
	 		$order_active[$val]    = '';
	 	}
	 	
	 	if($order == 'asc'){
			$order_inactive[$val] 	= 'desc'; 
		}
		else
		{
			$order_inactive[$val] 	= 'asc'; 
		}
	}

	$isDataPresent 	=	0;
	
	if(!isset($status))
	{
		$status 	=	'failure';
	}

	if(!isset($data))
	{
		$data 		=	array();
	}
	
	if($status == 'success' && count($data) > 0)
	{
		$isDataPresent 	=	1;
	}

	/*if($isDataPresent == 1)
	{
		if(isset($data))
		{
			foreach ($data as $key => $value)
			{
				$restaurantId = $value['restaurantId'];
			}
		}
	}*/

	$tabUrl	=	'?';

	/*if(isset($restaurantId))
	{
		$tabUrl	.=	'?restaurantId='.$restaurantId.'&';
	}*/
	/*else
	{
		$tabUrl	=	'?';
	}*/



	if(isset($urlSuffix))
	{

		if($urlSuffix != '')
		{
			$tabUrl 	=	$urlSuffix.'&';
		}
		
	}

	if(!isset($isAll))
	{
		$isAll 	=	0;
	}

//dump($data);
	/*$reviewFeatures	=	array();
	$reviewFeatures	=	$this->config->item('reviewFeatures');
	$reviewCulture	=	array();
	$reviewCulture	=	$this->config->item('reviewCulture');*/
	
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">
				Photos
			</h1>
		</div>
		<div class="row">
			<?php
				if(isset($errorMessage))
				{
					if($errorMessage != '')
					{
			?>
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<strong>Error!</strong>
							<div><?=$errorMessage?></div>
						</div>
			<?php
					}
				}
			?>
		</div>
		 <div class="col-md-9">
			<?php
				$this->load->view("/admin/filter2");
			?>
		</div>
		<div class="col-md-3">
			<div class="form-group pull-right">
				<div class="checkbox">
					<label>
				<?php
						if($isAll == 1)
						{
				?>

							<input type="checkbox" class="checkbox" name="showAll" id="showAll" checked="checked"> <span style="display: block;padding: 5px 0;font-weight: 600;">Show All</span>
				<?php
						}
						else
						{
				?>
							<input type="checkbox" class="checkbox" name="showAll" id="showAll"> <span style="display: block;padding: 5px 0;font-weight: 600;">Show All</span>
				<?php
						}

				?>
						</label>
				</div>
				<span class="alert-danger"></span>
			</div>
		</div>
		<div class="col-md-12">
			
			<?php 
				if($isDataPresent == 1)
				{?>

			   		<div style="clear:both;" class="custom-responsive-table">
						<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">
							<tr>
								<th>School</th>
								<th>User</th>
								<th>Image</th>
								<th><a href="<?=$url.$tabUrl?>order_by=isApproved&order=<?=$order_inactive['isApproved'];?>" class="<?=$order_by_active['isApproved'] .' '. $order_inactive['isApproved'];?>">
									<span class="order asc glyphicon glyphicon-chevron-up pull-right" ></span>
									<span class="order desc glyphicon glyphicon-chevron-down pull-right"></span> 
									Approved
								</th>
								<th><a href="<?=$url.$tabUrl?>order_by=createdDate&order=<?=$order_inactive['createdDate'];?>" class="<?=$order_by_active['createdDate'] .' '. $order_inactive['createdDate'];?>">
									<span class="order asc glyphicon glyphicon-chevron-up pull-right" ></span>
									<span class="order desc glyphicon glyphicon-chevron-down pull-right"></span> 
									Date
								</th>
								<th>Action</th>
							</tr>

							<?php
								foreach ($data as $key => $value) 
								{		
									
							?>

									<tr>

										<td>
											<div class="responsive-table-th"><label>School Name</label></div> 
											<div class="responsive-table-td">
												<?=$value['schoolName']?>
											</div>
										</td>

										<td>
											<div class="responsive-table-th"><label>User Name</label></div> 
											<div class="responsive-table-td">
												<?=$value['userName']?>
											</div>
										</td>

					<?php
							if($value['fileName'] != '')
							{
					?>	
								<td><div class="responsive-table-th"><label>Image</label></div> 
									<div class="responsive-table-td">

									<a class="fancybox" rel="gallery" href="<?=base_url()?>schoolsImages/<?=$value['fileName']?>" >
										<img src="<?=base_url()?>schoolsImages/<?=$value['fileName']?>" width="100">
									</a>


									</td>
								</div>
								</td>			
					<?php
							}
							else
							{
					?>	
								<td><div class="responsive-table-th"><label>Image</label></div> 
									<div class="responsive-table-td">
										--</td>
								</div>
								</td>
											
					<?php
							}
							
					?>				

										<?php
											if($value['isApproved']	==	NULL)
											{
												$isApproved		=	'No';
												$approveLink	=	'Approve';
											}
											else
											{
												$isApproved 	=	'Yes';
												$approveLink	=	'';
											}
										?>
										<td>
											<div class="responsive-table-th"><label>Approved</label></div> 
											<div class="responsive-table-td">
												<?=$isApproved?>
											</div>
										</td>

										<td>
											<div class="responsive-table-th"><label>Created Date</label></div> 
											<div class="responsive-table-td">
												<?=date('Y/m/d H:i a',strtotime($value['createdDate']))?>
											</div>
										</td>
										<td><div class="responsive-table-th"><label>Action</label></div> 
											<div class="responsive-table-td">
												<a id="approve-<?=$value['id']?>" data-id="<?=$value['id']?>" class="approvePhoto btn-link"><?php echo $approveLink?></a>
												<a id="delete-<?=$value['id']?>" data-id="<?=$value['id']?>" class="deletePhoto btn-link">Delete</a>
											</div>
										</td>
									</tr>

								<?php
								}
							?>

						</table>
					</div>
			
					<?php
						
						echo $create_link;
						
					?>


				<?php
				
				}
				else
				{

				?>
					<!-- <h4>No Photos Present </h4> -->
					<div class="form-wrapper no-data">

						<div class="icon"></div>
					
						<div class="msg"> No Photos Present.</div>

					</div>

			<?php		
				
				}

			?>

			</div>
		</div>
	</div>
</div>

<script> 
	var globalSiteUrl = "<?= site_url() ;?>";

	$(function(){


	    $( "#since" ).datepicker();
	    $( "#until" ).datepicker();

		$("input.checkbox").unbind('change');
		$("input.checkbox").change(function(event){

			var params 	=	window.location.search;

			if(params != '')
			{
				params 	=	params.split('&');

				if(params.indexOf('isAll=1') != -1 || params.indexOf('?isAll=1') != -1)
				{
					if(typeof params[params.indexOf('isAll=1')] != 'undefined')
					{
						params[params.indexOf('isAll=1')]	=	'';
						params[params.indexOf('isAll=1')]	=	null;
						delete params[params.indexOf('isAll=1')];
					}
					else if(typeof params[params.indexOf('?isAll=1')] != 'undefined')
					{
						params[params.indexOf('?isAll=1')]	=	'';
						params[params.indexOf('?isAll=1')]	=	null;
						delete params[params.indexOf('?isAll=1')];
					}
				}
			}
			else
			{
				params 	=	[];
			}

			if($(this).is(':checked')) 
			{
				if(params.length == 0)
				{
					params.push('?isAll=1');
				}
				else
				{
					params.push('isAll=1');
				}

				params 	=	params.join('&');

				if(params.indexOf('?') == -1)
				{
					params 	=	'?'+ params;
				}

		    	window.location = "<?php echo site_url('admin/uploadImages/index')?>"+ params;
			}
			else
			{
				params 	=	params.join('&');

				if(params.indexOf('?') == -1)
				{
					params 	=	'?'+ params;
				}
				
			    window.location = "<?php echo site_url('admin/uploadImages/index') ?>"+ params;
			}
		   
		});

		$('.approvePhoto').unbind('click');
		$('.approvePhoto').click(function(event){

			event.preventDefault();

			var id 	=	$(this).attr('data-id');

           	$.ajax({
                    url:globalSiteUrl+'admin/uploadImages/approveImage',
                    method:'POST',
                    data: {imageId:id},
                    success:function(response)
                    {
                        var result = JSON.parse(response);

                        if(result.status != "success")
                        {
                            alert(response);
                        }
                        else
                        {

                            window.location.reload();
                        }
                        
                    },
                    error:function()
                    {
                        console.log("The ajax request failed");
                    }
               
            });
           	

		})

		$('.deletePhoto').unbind('click');
		$('.deletePhoto').click(function(event){

			event.preventDefault();

			var id 	=	$(this).attr('data-id');

			if(confirm('Are you sure you want to delete the photo?'))
            {
    
               $.ajax({
                        url:globalSiteUrl+'admin/uploadImages/delete',
                        method:'POST',
                        data: {imageId:id},
                        success:function(response)
                        {
                            var result = JSON.parse(response);

                            if(result.status != "success")
                            {
                                alert(response);
                            }
                            else
                            {
                                window.location.reload();
                            }
                            
                        },
                        error:function()
                        {
                            console.log("The ajax request failed");
                        }
                   
                });
            }

		})

	});

	
    


</script>