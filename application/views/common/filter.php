<?php

	if(!isset( $filterAction))
	{
		$filterAction ="";
	}


?>

<div class="filter pull-right">
	<form class="form-inline " role="form" action="<?= $filterAction ?>" method="GET">
		<div class="form-group">
		   <label class="sr-only" for="filter">Search</label>
		   <input type="text" class="form-control" id="filter" name="filter" placeholder="Enter text for search" value="">
		</div>
		<div class="form-group">
			<label for="filterBy"></label>
			<select name="filterBy" class="form-control" id="filterBy">

				<?php
					if(isset($filterOption))
					{	
						foreach ($filterOption as $key => $value) {
						?>
							<option value="<?=  $key ?>"><?=  $value ?></option>
						<?php
						}
					}
				?>
			</select>
		</div>

		<div class="form-group">

				<div class="date-filter-wrapper">
					<label for="filterFrom" class="sr-only"  >From</label>
					<input type="date" id="filterFrom" name="filterFrom" class="form-control " placeholder="mm/dd/yyyy" value="start">
				
			 		<label for="filterTo" class="sr-only" >To</label>
					<input type="date" id="filterTo" name="filterTo" class="form-control"  placeholder="mm/dd/yyyy" value="end">
				</div>
		</div>
   		<div class="form-group">
			<button class="btn btn-info" type="submit">SEARCH</button>
			<button type="button" class="btn btn-link">Clear</button>
		</div>

		
	</form>
	
</div>