
<?php
    
    $showAdminLink  =   0;

    if($this->tank_auth->is_logged_in())
    {
        $role   =   $this->session->userdata('role');
        
        if($role == 2)
        {
            $showAdminLink  =   1;
        }
                    
    }

?>

<section class="footer">

    <div class="container">
        <div class="row">

            


           <!-- <div class="col-md-3">

                
                

               
            </div>-->

           

                <ul class="nav navbar-nav">    
                    <li><a href="<?=site_url()?>">Home</a></li>
                    <li><a href="<?=site_url()?>blog">Blog</a></li>
                    <li><a href="<?=site_url()?>privacy">Privacy</a></li>
                    <li><a href="<?=site_url()?>terms">Terms</a></li>
                    <li><a href="<?=site_url()?>guidelines">Community Guidelines</a></li>
                    <li><a href="<?=site_url()?>contact">Contact Us</a></li>
                </ul>

           


            

            <div class="pull-right">

                <ul class="pull-right social-wrapper">  
                     <li> 
                        <div class="copyright">
                        Copyright <?php echo date("Y"); ?> SchoolTorch </div>
                    </li>

                    <li> 
                    <a href="http://on.fb.me/1EOmUjV" class="social facebook" title="Facebook">
                        <i class="icon icon-facebook3"></i>
                    </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/getschooltorch" class="social twitter" title="Twitter">
                         <i class="icon icon-twitter3"></i>
                        </a>
                     </li>

                      <li>
                        <a href="http://bit.ly/1zIZisA" class="social google" title="Google+">
                         <i class="icon icon-googleplus4"></i>
                        </a>
                     </li>
                   
                   
                </ul>
               

               

            </div>



        </div> 
    </div>

    <?php
        if(!$this->tank_auth->is_logged_in())
        {
    ?>
            <div class="modal fade login-modal" id="loginContainer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <?php
                           $this->load->view('auth/login_form');
                        ?>
                    </div>
                </div>
            </div>

            <div class="modal fade login-modal" id="registerContainer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <button type="button"  class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <?php
                            $this->load->view('auth/register_form');
                        ?>
                    </div>
                </div>
            </div>

    <?php
        }
    ?>

</section>

<script>

    function lg(data)
    {
        console.log(data);
    }
		
    $(document).ready(function() {

        $('.formValidate').bootstrapValidator({

            message: 'This value is not valid',

            excluded: [':disabled', ':hidden', ':not(:visible)'],

            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            fields: {
                
                email: {
                    validators: {
                        notEmpty: {
                            message: 'The email field is required and cannot be empty'
                        },
                        emailAddress: {
                            message: 'This is not a valid email address'
                        }
                    }
                },
                confirm_email: {
                    validators: {
                        notEmpty: {
                            message: 'The email field is required and cannot be empty'
                        },
                         identical: {
                            field: 'email',
                            message: 'The email address and its confirm are not the same'
                        },
                        emailAddress: {
                            message: 'This is not a valid email address'
                        }
                    }
                },
              password: {
                    validators: {
                        identical: {
                            field: 'confirm_password',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                },
                confirm_password: {
                    validators: {
                        identical: {
                            field: 'password',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                },
                 login: {
                    validators: {
                         notEmpty: {
                            message: 'The login field is required and cannot be empty'
                        }
                    
                    }
                }
            }
        });

        $('#loginPopup').unbind('click');
        $('#loginPopup').click(function(event){

            event.preventDefault();
            event.stopPropagation();

            $('#errorContainer').addClass('hide');
            
            $('#registerContainer').modal('hide');
            $('#loginContainer').modal('show');

        });

        $('#registerUser').unbind('click');
        $('#registerUser').click(function(event){

            event.preventDefault();
            event.stopPropagation();

            $('#registerErrorContainer').addClass('hide');
            $('#registerErrorContainer').addClass('registerSuccess');
            
            $('#loginContainer').modal('hide');
            $('#registerContainer').modal('show');

        });

        

    });


    var $window = $(window);

    var  $stickyEl = $('#header .navbar');
    
    if(typeof $stickyEl != 'undefined')
    {
        if(typeof $stickyEl.offset() != 'undefined')
        {

            var elTop = $stickyEl.offset().top;

            $window.scroll(function() {
                $stickyEl.toggleClass('sticky', $window.scrollTop() > (elTop+ $stickyEl.height()));
            })
        }
    }
        
    
     

    //GLOBAL VARIABLES

    schoolApp.isLoggedIn = 0; 

    <?php

    if(isset($chart)){
        ?>
        
        schoolApp.peopleChartData               =   JSON.parse('<?=$chart["peopleChart"];?>');
        /*schoolApp.ethinicityChartData           =   JSON.parse('<?=$chart["ethinicityChart"];?>');
        schoolApp.recommendationChartData       =   JSON.parse('<?=$chart["recommendation"];?>');
        schoolApp.aspectsChartData              =   JSON.parse('<?=$chart["aspectsChart"];?>');
        schoolApp.likesChartData                =   JSON.parse('<?=$chart["likesChart"];?>');
        schoolApp.jobProspectsChartData         =   JSON.parse('<?=$chart["jobProspectsChart"];?>');
        schoolApp.fundOpportunitiesChartData    =   JSON.parse('<?=$chart["fundOpportunitiesChart"];?>');
        schoolApp.ratings                       =   '<?=$chart["ratings"];?>';*/
        
        <?php
    }

    if(isset($user_id)){
        ?>
        schoolApp.isLoggedIn = 1; 
        <?php
    }
    ?>

    schoolApp.site_url              = '<?=site_url();?>';

</script>



</body>

</html>
