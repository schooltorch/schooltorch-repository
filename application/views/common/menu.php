<style>
#header .navbar .nav > li > a{
  color: #777;
}

#header .navbar .nav > li > a:hover{
  color: #333;
}

</style>



<?php
  $first_uri          = $this->uri->segment(1);
  $second_uri         = $this->uri->segment(2);

  
  switch ($second_uri){

    case "school":
      $active_menu = 1;
      break;

    case "user":
      $active_menu = 2;  
      break;
      
    case "survey":
      $active_menu = 3;  
      break;   
    case "popularSchools":
      $active_menu = 4;  
      break;    
    case "blog":
      $active_menu = 5;  
      break;
    case "reviewManagement":
      $active_menu = 6;  
      break;
    case "uploadImages":
      $active_menu = 7;  
      break;

    default:
      $active_menu = 1;
      break;  
  }

  $menu               = array();

  if($second_uri == "school")
  {
    if($this->uri->segment(3))
    {
       $active_menu = 4;  
    }
  }

  

  $menu['home']               =   $active_menu==1?'active':'';
  $menu['users']              =   $active_menu==2?'active':'';
  $menu['survey']             =   $active_menu==3?'active':'';
  $menu['popularSchools']     =   $active_menu==4?'active':'';
  $menu['blog']               =   $active_menu==5?'active':'';
  $menu['reviewManagement']   =   $active_menu==6?'active':'';
  $menu['uploadImages']       =   $active_menu==7?'active':'';

  
    
?>
<div id="header">
<div class="container">

      <!-- Static navbar -->
      <div class="navbar navbar-default row" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=site_url()?>"></a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="<?=$menu['home'] ?>"><a href="<?= site_url("/admin/school"); ?>">Schools</a></li>
              <li class="<?=$menu['users'] ?>"><a href="<?= site_url("/admin/user/index"); ?>">User Management</a></li>
              <li class="<?=$menu['popularSchools'] ?>"><a href="<?= site_url("/admin/school/popularSchools"); ?>">Popular Schools</a></li>
              <li class="<?=$menu['blog'] ?>"><a href="<?= site_url("/admin/blog"); ?>">Blog</a></li>
              <li class="<?=$menu['reviewManagement'] ?>"><a href="<?= site_url("/admin/reviewManagement"); ?>">Reviews</a></li> 
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="<?= site_url("/auth/logout"); ?>">Logout</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </div>

     

    </div> <!-- /container -->
</div>