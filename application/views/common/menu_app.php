<?php
  if(!isset($page_id))
  {
    $page_id  = '';
  }

  if($this->tank_auth->is_logged_in())
  {
    $username   =   $this->session->userdata('username');
    $role       =   $this->session->userdata('role');
    $user_id    =   $this->session->userdata('user_id');
    $picture    =   $this->session->userdata('picture');

    if($picture == '')
    {
      $picture  =   base_url('assets/images/profile.png');
    }

  }
  else
  {
    $picture = '';
  }

?>

<div id="header">

  <div role="navigation" class="navbar navbar-fixed-top">
      
      <div class="container">
        
        <div class="navbar-header">
         
          
          <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

            <img src="<?=$picture?>" id="profilePictureUrl" class="pull-right show-mobile" width="36" height="36" alt="" style="margin-right: 8px; margin-top: 8px;"/>
          
          <a href="<?=site_url()?>" class="navbar-brand">
            
          </a>

        </div>
        
        <div class="navbar-collapse collapse">
          
          <ul class="nav navbar-nav navbar-right">
              <?php
                if($page_id != 'home_page'){ ?>

                   <li class="active"><a href="<?=site_url()?>">Home</a></li>
                <?php
                }


                ?>

             
              <li><a href="<?=site_url()?>home/schools">Schools</a></li>
              
                  <li><a class="loginCheck" href="<?=site_url()?>reviews/createReview" onclick="return setRedirect();">Write a review</a></li>

                
              <li><a href="<?=site_url()?>blog">Blog</a></li>

               <?php
              if(isset($user_id))
              {
                
                ?>
               
                  <li class="show-mobile">
                    <a  href="<?=site_url()?>profile">Profile</a>
                  </li>
                    
                  <li class="show-mobile">
                    <a  href="<?=site_url()?>auth/logout">Logout</a>
                  </li>
                   

              <?php

                }


              ?>




              
              <?php
              if(isset($user_id))
              {
                
                ?>
                <li class="hide-mobile">
                    <div class="dropdown profile-img">
                          
                      <a class="btn dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">
                        <span class="caret pull-right"></span>
                        <img src="<?=$picture?>" id="profilePictureUrl" width="36" height="36" alt="" />
                        
                      </a>

                      <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                          
                        <li role="presentation">
                          <a role="menuitem" tabindex="-1" href="<?=site_url()?>profile">Profile</a>
                        </li>
                          
                        <li role="presentation">
                          <a role="menuitem" tabindex="-1" href="<?=site_url()?>auth/logout">Logout</a>
                        </li>
                      </ul>

                  </div>
                </li>
               

                <?php

              }
              else
              {
                ?>
                <!--<li class=""><a href="" id="loginPopup">Login</a></li>-->
                <li class=""><a href="" id="loginPopup">Sign In</a></li>
                <?php
              }
              ?>
             
          </ul>

        </div><!--/.nav-collapse -->
      </div>
    </div>
</div>

<script type="text/javascript">
  function setRedirect(){
      document.cookie = "page_redirect=write_review";
      return true;
  }
  $(document).ready(function(){

    function imageExists(url, callback) {
      var img = new Image();
      img.onload = function() { callback(true); };
      img.onerror = function() { callback(false); };
      img.src = url;
    }    

    var newPic  = "<?=base_url('assets/images/profile.png')?>";

    imageExists("<?=$picture;?>", function(exists){

        if(exists)
        {
          $('#profilePictureUrl').attr('src', "<?=$picture;?>");
        }
        else
        {
          $('#profilePictureUrl').attr('src', newPic);
        }

    });

  });

</script>