<?php

	if(!isset($onlySchools))
	{
		$onlySchools 	=	0;
	}

	$inputName	=	'schoolId';

	if(isset($name))
	{
		$inputName	=	$name;
	}

	if(!isset($schoolId))
	{
		$schoolId	=	'';
	}

	if($schoolId == 0)
	{
		$schoolId	=	'';
	}

	if(!isset($schoolName))
	{
		$schoolName	=	'';
	}

?>

	<input type="text" class="form-control" data-image="popular-image1" id="searchSchool" name="<?=$inputName?>" placeholder="Enter school name to add" autocomplete="on"  data-id="" value="<?=$schoolId?>">

<script>
		
	$(function(){

		var url 	=	"<?= site_url('search');?>";

		if("<?=$schoolName?>" != '')
		{
			$('#searchSchool').val("<?=$schoolName?>");
		}

		if("<?=$onlySchools?>" == 1)
		{
			var url 	=	"<?=site_url('search?onlySchools=1');?>";
		}

		$("#searchSchool").autocomplete({

			source: url,

			minLength: 1,

			select: function( event, ui ) {

				$("#searchSchool").attr("value", ui.item.id);

			}

		});

	});

</script>