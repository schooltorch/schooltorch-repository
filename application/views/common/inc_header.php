<!DOCTYPE html>

<?php

if(!isset($page_id))
{
  $page_id = '';
}

if(!isset($page_class)){
  $page_class = "page_".$this->uri->segment(1);
}
?>

<html lang="en" class="<?=$page_class?>" id="<?=$page_id?>" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <link rel="shortcut icon" href="<?=base_url()?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=base_url()?>favicon.ico" type="image/x-icon">

    <title>
      <?php 
        if(isset($page_title))
        {
          echo $page_title;
        }
        else
        {
     
          echo $this->config->item('siteName'); 
     
        }
      ?>
    </title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/jquery-ui.css" />
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/bootstrapValidator.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/font.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/bootstrap-switch.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- <link href="<?=base_url()?>assets/css/style.less" rel="stylesheet/less">
    <link href="<?=base_url()?>assets/css/layout.less" rel="stylesheet/less">
    <script type="text/javascript" src="<?=base_url();?>assets/js/less.min.js"></script>-->

     <?php
    if(ENVIRONMENT == 'production')
    {
    ?>

       <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
      <link href="<?=base_url()?>assets/css/layout.css" rel="stylesheet">

    <?php
    }else{
      ?>

        <link href="<?=base_url()?>assets/css/style.less" rel="stylesheet/less">
        <link href="<?=base_url()?>assets/css/layout.less" rel="stylesheet/less">

        <script type="text/javascript" src="<?=base_url();?>assets/js/less.min.js"></script>

   
    <?php
    }

    ?>
  
    

    <script type="text/javascript" src="<?=base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/bootstrapValidator.min.js"></script>
    
    <script src="<?=base_url();?>assets/js/jquery.knob.min.js" type="text/javascript"></script>
  
 
    <script type="text/javascript" src="<?=base_url();?>assets/js/bootstrap-switch.min.js"></script>
  

    <script type="text/javascript" src="<?=base_url();?>assets/js/highcharts.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/highchartsTheme.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/highlight.js"></script>
  
    <script type="text/javascript" src="<?=base_url();?>assets/js/bootstrap-rating.js"></script>
   
    <script type="text/javascript" src="<?=base_url();?>assets/js/schools.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/summernote.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/js/jquery.simplyCountable.js"></script>
      
      <script>
        var schoolApp = {};
      </script>

    <style type="text/css">
    ._4z_c ._4z_f{

      padding: 10px 8px !important;

    }
    </style>

  </head>

 <body>
<script type="text/javascript">
  
  function isLoggedIn() 
  {
    return "<?=$this->tank_auth->is_logged_in()?>";
  }

</script>
 



 

