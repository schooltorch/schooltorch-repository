<?php

$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'class' =>  'form-control input-lg',
	'value' => set_value('password'),
	'placeholder' =>'',
	'maxlength'	=> $this->config->item('password_max_length'),
	'size'	=> 30,
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'class' =>  'form-control input-lg',
	'value' => set_value('confirm_password'),
	'placeholder' =>'',
	'maxlength'	=> $this->config->item('password_max_length'),
	'size'	=> 30,
);

?>

<div class="container">
	<div class="">
		<div class="form-wrapper">


<?php echo form_open($this->uri->uri_string(),array("class"=>"form","role"=>"form")); ?>
	
	<div class="form-signin-heading header-image">

		<img class="img-responsive" alt="" src="<?=base_url()?>assets/images/logo-black.png">

	</div>

	<div class="form-group">
		
			<label>Email</label>

			<input class="form-control input-lg" type="text" name="email" id="email" value="<?php echo set_value('email'); ?>"/>

			<span class="help-block error-block">
				<?php echo form_error('email'); ?><?php echo isset($errors['email'])?$errors['email']:''; ?>
			</span>	
	
	</div>

	<div class="form-group">
			<?php echo form_label('Password', $password['id']); ?>
			<?php echo form_password($password); ?>
			<span class="help-block error-block"><?php echo form_error($password['name']); ?></span>
	</div>

	<div class="form-group">
			<?php echo form_label('Confirm Password', $confirm_password['id']); ?>
			<?php echo form_password($confirm_password); ?>
			<span class="help-block error-block"><?php echo form_error($confirm_password['name']); ?></span>
	</div>

	<div class="form-group">

		<input type="submit" name="submit" value="Let me in" class="btn btn-success btn-lg btn-block" />

		<a href="<?=site_url()?>" class="btn btn-default btn-lg btn-block" role="button" />Cancel</a>

	</div>	

	<?php echo form_close(); ?>

	</div>
	</div><!--/row-->
</div><!--/container-->




				