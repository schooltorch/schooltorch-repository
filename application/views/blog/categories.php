<div class="container">
	<div class="row">
		<div class="col-md-12">

			<h1 class="page-header">
				Categories
				<?php  echo anchor(site_url()."admin/blog/add_category","Add Category",array('class'=>'btn btn-success pull-right main_action')) ?> 
				<?php  echo anchor(site_url()."admin/blog","Back to all articles",array('class'=>'btn btn-link pull-right')) ?> 
				

			</h1>
			
		</div>

		<div class="col-md-12">
			
			
			
			<?php
				if(sizeof($categories) > 0)
				{
					?>
					<div style="clear:both;" class="custom-responsive-table">	
						<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped custom-responsive-table">	
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Date</th> 

							<th>Action</th>
						</tr>
						<?php
							foreach($categories as $key=>$row)
							{
								
								?>
								<tr>
									<td><div class="responsive-table-th"><label>#</label></div> 
										<div class="responsive-table-td">
										<?php echo $row['id']; ?>
										</div> 
									</td>
									<td><div class="responsive-table-th"><label>Name</label></div> 
										<div class="responsive-table-td">
										<?php echo $row['name'] ;?>
										</div>  
									</td>
									<td><div class="responsive-table-th"><label>Date</label></div> 
										<div class="responsive-table-td">
										<?=  $row['createdDate'] ?>
										</div>  
									</td> 
									<td><div class="responsive-table-th"><label>Date</label></div> 
										<div class="responsive-table-td">
										<div class="action">
											<a href="<?=site_url('admin/blog/update_category') ;?>/<?= $row['id']?>" class="btn-link">Edit</a>
											<span class="separator">|</span>
											<a href="<?=site_url('admin/blog/delete_category') ;?>/<?= $row['id']?>" class="btn-link">Delete</a>
											
										</div>
										</div>
										
									</td>
									
								</tr>
							<?php
							}
						?>
						</table>
					</div>	

					<?php
				}
				else
				{?>
					
					<div class="form-wrapper no-data">

						<div class="icon"></div>
					
						<div class="msg"> No Categories Present.</div>

					</div>
					<!-- <tr>
						<td colspan="3"> </td>
					</tr> -->
				<?php
				}
			?>
			
		</div>
	</div><!--/row-->
</div><!--/container-->

		