<?php

//dump($articlesInfo);

//dump($categories['data']);

?>
<div class="main_container" id="blog">
	<div class="container">

	       

	        <div class="row">

	        	<div class="col-md-12">

	        		 <h1 class="main-header">The SchoolTorch Blog</h1>
	        	</div>


	        	 	<div class="col-sm-8">

	        	 		
	        	 			<?php
                				
                				$categoryStr = array();

				                if(isset($articlesInfo))
				                {
					               	if($articlesInfo['count'] > 0)
					                {
				                      	$articleId = 0;

				                      	foreach ($articlesInfo['data']  as $key => $value) 
				                      	{
			                      		   	$coverImageUrl = "";

			                      		   	$categoriesArray  =  array_filter(explode("|",$value['categoryId']));

			                      		   	foreach ($categoriesArray as $key2 => $value2) 
			                      		   	{
			                      		   		if(isset($categories['data'][$value2]))
			                      		   		{
				                      		   		$categoryUrl = site_url('blog/category')."/".$value2."/". $categories['data'][$value2]['safeTitle'];
				                      		   		$categoryStr[] =  '<a href="'.$categoryUrl.'">'. $categories['data'][$value2]['name'] .'</a>';
				                      		   	}
			                      		   		//$urlCat = site_url('blog/category');
			                      		   		//$urlSafe= 

			                      		   		/*$categoryStr.='<a href="<?= $urlCat ?>/<?= $value2?>/<?= $value['safeTitle']?>" class="btn-link"> <?= $value['name']  ?> </a>'*/
			                      		   	}

					                        if(isset($value['coverImage']))
					                        {
					                           $coverImageUrl = base_url()."/blogPics/".$value['coverImage'];
					                        }
				                      		?>
				                      			
				                      					
						                            

			                                	<div class="cover-image">

			                                		<img src="<?= $coverImageUrl?>" alt="<?= $value['title'] ?>" class="img img-responsive"/>
			                                			
			                                	</div>

			                                    <div class="blog-content">
				                                    <h4><a href="<?= site_url('blog/article') ?>/<?= $value['id']?>/<?= $value['safeTitle'] ?>"><?= $value['title'] ?></a></h4>

				                                     <?php
			                                          $fullName = "";
			                                          if(isset($value['firstName']) && isset($value['lastName']))
			                                          {
			                                            $fullName = $value['firstName'].' '.$value['lastName'];

			                                          }
			                                          else
			                                          {
			                                            $fullName=  "by SchoolTorch Team";
			                                          }
			                                        ?> 
				                                    
				                                    <div class="info"><?= date('F d, Y', strtotime($value['createdDate'])) ?> <cite class="pull-right">-  <?=$fullName ?></cite></div>
				                                   
				                                    <?= $value['content'] ?>

				                                    <div class="blog-footer">
				                                    	<label>Categories: </label>
				                                    <?php
				                                    if(count($categoryStr ) > 0)
								           			{
								           				echo implode(", ", $categoryStr);
								           			}
								           			?>

								           			</div>
			                                 	</div>
						                               
						                       

					                                
				                                


				                  	  	<?php
				                  		}
				                  	}
				                }
				           
				           			
				           			
				           		?>
				         
	        	 	<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'schooltorch';
    var disqus_identifier = '<?=$articleId?>';
    
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

				        



				      
				</div>










			<div class="col-sm-4  blog-sidebar">
           
              <div class="sidebar-module">
                <div class="col-heading"> Categories</div>

                <?php
                   if(isset($categories) && count($categories['data']) > 0 )
                   {?>  
                      
                      <ul class="list-unstyled">
                        <?php
                          foreach ($categories['data'] as $key => $value) 
                          {
                            if($value['count'] == 0)
                            {
                              ?>

                              <li><a href="<?= site_url('blog/category') ?>/<?= $value['id']?>/<?= $value['safeTitle']?>" class="btn-link"> <?= $value['name']  ?> </a></li>

                            <?php

                            }
                            else
                            {
                              ?>
                              <li><a href="<?= site_url('blog/category') ?>/<?= $value['id']?>/<?= $value['safeTitle']?>" class="btn-link"> <?= $value['name']  ?></a></li>
                            
                            <?php
                            }
                          }
                        ?>
                      </ul>
                      <?php
                   }
                   else
                   {
                    echo "No categories";
                   }
                ?>
              </div>
          </div><!-- /.blog-sidebar -->
		 </div>
	</div>
</div>    
