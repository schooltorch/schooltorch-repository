<?php
		
if(isset($is_edit))
{
	$catTitle       = "Update Category";
	$category_name  = $data['name'];
	$catTitle       = "Update Category";
	$catButton      = "Update Category";
 	/*$is_active 	 	= $data['is_active'];*/
 	$action         =  site_url()."admin/blog/update_category/".$id;

}
else
{
	$catTitle       = "Create Category";
	$catButton      = "Add category";
	$category_name  = $this->input->post('category_name');
 	/*$is_active 	 	= $this->input->post('is_active');*/
 	$action         =  site_url()."admin/blog/add_category";

}

 
	
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">

			<h1 class="page-header">
				<?= $catTitle?>
			</h1>
		</div>




	
	<div class="col-md-6">

		<form name="create_category" action="<?= $action  ?>" method="POST" class="form" role="form">

			
			<div class="form-group">
				<label for="name">Name </label>
				<input class="form-control" type="text" name="category_name" id="category_name" placeholder="Category Name" value="<?= $category_name; ?>">
				<?php echo form_error('category_name'); ?>
				
			</div>
			<!-- <div class="form-group">
					<label >Is Active</label>

					<?php
						if($is_active	== 2)
						{?>
							<input type="checkbox" name="is_active" value="1">
						<?php
						}
						else
						{?>
							<input type="checkbox" name="is_active" value="1" checked>
						<?php
						}
					?>
			</div> -->
			<div class="form-group submit_group">
				<input type="submit" id="add_category" class="btn btn-success main_action" value="<?= $catButton?>">
				<a href="<?php echo site_url() ?>admin/blog/categories" class="btn-link btb">Cancel</a>
			</div>
		</form>
	</div>

</div><!--/row-->
</div><!--/container-->
