<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">




<?php
	$category_ids = array();
  	
  if(isset($is_create))
  {
  	if($is_create == 1)
  	{
  		$artTitle		=	'Create Article';
  		$is_published = $this->input->post('isPublished');
  		$no  = "selected";
  		$is_favorite  	= $this->input->post('isFavorite');
  		$action    	  	= site_url()."admin/blog/add_article";
  		$title   	  	= $this->input->post('title');
  		$content 	  	= $this->input->post('content');
  		$user_id 	  	= $this->input->post('userId');

  		

  		//if($has_category == "yes")
  		{
  			//$category_id  =  $this->input->post('category_id');

  			$category_ids  = $this->input->post('categoryId');
  		}
  		$authorId  = $this->input->post('authorId');
  	}
  	
  }
  else if(isset($is_edit))
  {
  	if($is_edit == 1 && $is_post != 1)
  	{
  		$artTitle	  = 'Update Article';
  		$is_published = $data['isPublished'];
  		$is_favorite  = $data['isFavorite'];
  		$title   	  = $data['title'];
  		$content 	  = $data['content'];
  		$user_id	  = $data['userId'];
  		$coverImage   = $data['coverImage'];

  		//if($has_category == "yes")
  		{
  			$category_ids  =  array_filter(explode('|',$data['categoryId'])); 
  		}

  		$authorId		=	$data['userId'];

  		$action       	= 	site_url()."admin/blog/update_article/".$id."/1";


  	}
  	if($is_edit == 1 && $is_post == 1)
  	{
  		$artTitle	  = 'Update Article';
  		$is_published =  $this->input->post('isPublished');
  		$is_favorite  =  $this->input->post('isFavorite');
  		$title   	  =  $this->input->post('title');
  		$content 	  =  $this->input->post('content');
  		$user_id 	  =  $this->input->post('userId');
  		$coverImage   =  $coverImage;

  		//if($has_category == "yes")
  		{
  			$category_ids  =  $this->input->post('categoryId');
  		}
  		$authorId		=	$this->input->post('authorId');

  		$action       = site_url()."admin/blog/update_article/".$id."/1";


  	}
  }
?>



<div class="container">
	<div class="row">

	<div class="col-md-12">

			<h1 class="page-header">
				<?= $artTitle?>
				
			</h1>
	</div>

	<div class="col-md-8">

		
		<form id="article_form" action="<?php echo $action ?>" method="POST" class="form" role="form" enctype="multipart/form-data">

			<div class="form-group">
				    <label for="title">Title <span style="color:red;">*</span></label>
					<input class="form-control" type="text" name="title" id="title" placeholder="Article Title" value="<?php echo $title ?>"/>
					<?php echo form_error('title'); ?>
			</div>
	
	
			<div class="form-group">
				<label for="category">Category</label>

			    <?php

				if(count($categories) > 0)
				{
					
					?>
					<select name="categoryId[]" class="form-control" id="categoryId" style="max-width:300px;" multiple>

						<?php

							foreach($categories as $key=>$row)
							{
								//if(isset($is_edit) || )
								{
									//if($row['id'] == $category_id)

									if(in_array($row['id'],$category_ids))
									
									{?>
										<option value="<?= $row['id'] ?>" data="<?=$row['id'] ?>" selected><?= $row['name']?></option>

									<?php
									}
									else
									{?>
										<option value="<?= $row['id'] ?>"><?= $row['name']?></option>
									<?php
									}
								}
								
							}
						?>
			
					</select>
				<?php
				}
				else{?>
					<select name="categoryId" class="form-control" id="categoryId" style="max-width:300px;">
							<option value="no_category">No category to Select</option>
					</select>
				<?php
				}	
				?>
				<?php echo form_error('categoryId'); ?>
				
			</div>
			<div class="form-group">
				<label for="authors">Authors</label>

			    <?php

				if(count($authors) > 0)
				{
					
					?>
					<select name="authorId" class="form-control" id="authorId" style="max-width:300px;">
									<option value="">SchoolTorch Team</option>
						<?php

							foreach($authors as $key => $row)
							{
								if(($row['firstName'] == NULL || $row['firstName'] == '') && ($row['lastName'] == NULL || $row['lastName'] == ''))
								{
									$name 	=	$row['email'];
								}
								else
								{
									$name 	=	$row['firstName'].' '.$row['lastName'].'('.$row['email'].')';
								}
								
								if($row['id'] == $authorId)
								
								{
						?>
									<option value="<?= $row['id'] ?>" data="<?=$row['id'] ?>" selected><?= $name ?></option>

						<?php
								}
								else
								{
						?>
									
									<option value="<?= $row['id'] ?>"><?= $name ?></option>
						<?php
								}
							
							}
						?>
			
					</select>
				<?php
				}
				else{?>
					<select name="authorId" class="form-control" id="authorId" style="max-width:300px;">
							<option value="no_author">No Author to Select</option>
					</select>
				<?php
				}	
				?>
				<?php echo form_error('authorId'); ?>
				
			</div>
			
			

			<div class="form-group">

				<label for="content">Content <span style="color:red;">*</span> </label>
			
				<textarea  name="content" class="form-control pagedown" id="content" rows="10" ><?php echo $content ?></textarea>
				<?php echo form_error('content'); ?>
			</div>
	


			<div class="form-group">
				
				
				 <label for="isPublished">Published</label>
				 <div class="radio ">
				 	<?php
				 		if(isset($is_create))
				 		{?>
				 			<label class="radio-inline"><input type="radio" name="isPublished" value="yes">YES</label>
				 			<label class="radio-inline"><input type="radio" checked name="isPublished" value="no">NO</label>

				 		<?
				 		}
				 		else
				 		{
				 			if($is_published == 1)
				 			{?>
					 			<label class="radio-inline"><input type="radio" checked name="isPublished" value="yes">YES</label>
					 			<label class="radio-inline"><input type="radio" name="isPublished" value="no">NO</label>

				 			<?

				 			}
				 			else{
								?>
						 			<label><input type="radio" name="isPublished" value="yes">YES</label>
						 			<label><input type="radio" checked name="isPublished" value="no">NO</label>

								<?

				 			}

						}
				 	?>
  					
				</div>
			</div>
			<?php
				if(isset($coverImage) && $coverImage != "" && $coverImage != null)
				{

                    $coverImageUrl = base_url()."/blogPics/".$coverImage;
                   
				?>
					<div class="form-group">
						<div class="row"> 
							<div class="col-xs-6 col-md-3">
								<div class="imagepreview thumbnail">
									<img src="<?= $coverImageUrl ?>" class="img-responsive" >
								</div>  
							</div>
						</div>
				
					</div>
				
				<?php
				}
			?>
			<div class="form-group">
				<input type="file" name="coverImage" size="20" />
				<?php 
					if(isset($errors))
					{
				?>
						<span class="error" style="color:red;"><?=$errors?></span>
				<?php
					}
					else
					{
						echo form_error('coverImage'); 
					}
					
				?>
			</div>
			

			<div class="form-group">
				<input type="submit" id="form_submit" class="btn btn-success  main_action submit" value="submit"/>

				<?php
					
					if(isset($is_edit))
					{	
						$actionUrl = site_url()."admin/blog/update_article/".$id."/1?is_apply=1";
						?>
						<!-- <input type="submit" id="form_apply" class="btn btn-deafult  main_action submit" value="apply"/> -->
						<input type="submit" class="btn btn-default main_action" id="form_submit2" class="btn btn-success main_action"
						value="APPLY" 

						onclick='this.form.action="<?= $actionUrl ?>";'
						/>
					<?php
					}
				?>

				
				
				<a href="<?php echo site_url() ?>admin/blog" class="btn btn-link"> Cancel </a>
			</div>


		</form>
	</div>

	
	</div><!--/row-->
</div><!--/container-->



<script>

	$(document).ready(function(){

		$("#form_submit").val("SUBMIT");

 		$(".submit").click(function(){ 
 			editor.post();
 		});

 		$("#article_form").on("submit",function(){
 			$("#form_submit").val("Processing...");
 		})

 		
 		files.bindUploader();

 		// when apply is clicked then change the 
 	});



	/*var editor = new TINY.editor.edit('editor', {
		id: 'content',
		width: 584,
		height: 175,
		cssclass: 'tinyeditor',
		controlclass: 'tinyeditor-control',
		rowclass: 'tinyeditor-header',
		dividerclass: 'tinyeditor-divider',
		controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
			'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
			'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
			'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
		footer: true,
		fonts: ['Verdana','Arial','Georgia','Trebuchet MS'],
		xhtml: true,
		cssfile: 'custom.css',
		bodyid: 'editor',
		footerclass: 'tinyeditor-footer',
		toggle: {text: 'source', activetext: 'wysiwyg', cssclass: 'toggle'},
		resize: {cssclass: 'resize'}
	});*/

	function sendFile(file, editor, welEditable) {
	    data = new FormData();
	    data.append("file", file);
	    url = schoolApp.site_url+'admin/blog/upload';
	    $.ajax({
	        data: data,
	        type: "POST",
	        url: url,
	        cache: false,
	        contentType: false,
	        processData: false,
	        success: function (data) {

	        	try{
		        	data 	=	JSON.parse(data);

		        	if(data.status == 'success')
		        	{
		        		var url 	=	"<?=base_url();?>"+ 'blogPics/'+ data.data.file_name;
		        		
		            	editor.insertImage(welEditable, url);
		        	}
		        	else
		        	{
		        		
					}
				}
				catch(e){
					
				}
			}
		});
	}

	$('#content').summernote({
		height: 300,
		oninit: function() {
			
		},
	  	onImageUpload: function(files, editor, welEditable) {
			sendFile(files[0],editor,welEditable);
		}
	});


</script>