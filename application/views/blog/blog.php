<div class="container">
	<div class="row">
		<div class="col-md-12">

			<h1 class="page-header">
				Articles
				<?php  echo anchor(site_url()."admin/blog/add_article","Add Article",array('class'=>'btn btn-success pull-right main_action')) ?>
				<a href="<?=site_url('admin/blog/categories') ;?>" class="btn btn-link pull-right">Manage Categories</a>
				
			</h1>
		</div>

		
		<div class="col-md-12">

			
		

				<?php
					if(sizeof($articles) > 0)
					{


						?>
						<div style="clear:both;" class="custom-responsive-table">
							<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">
								<tr>
									<th>Sr.no</th>
									<th>Title</th>
									<!-- <th>User</th> -->
									<th>Published</th>
									<!-- <th>Favorite</th> -->
									
									<th>Category</th>
									<th>ID</th>
									<th>Action</th>
								</tr>


								<?php

									//$srNo = count($articles);
									 $srNo = $total_rows - $offset;

									foreach($articles as $key=>$row)
									{	
										//$srNo = $key;
										$is_published = "No";
										$is_favorite  = "No";
										if($row['isFavorite'] == 1)
										{
											$is_favorite = "Yes";
										}

										if($row['isPublished'] == 1)
										{
											$is_published = "Yes";
										}

										?>		
										<tr>
											<td><div class="responsive-table-th"><label>#</label></div> 
												<div class="responsive-table-td">
												<?=$srNo;?>
												</div>
											</td>
											<td><div class="responsive-table-th"><label>Title</label></div> 
												<div class="responsive-table-td">
												<?=$row['title'];?>
												</div>
											</td>
											<!-- <td>
												<?php
													if(count($users) > 0)
													{

														if(isset($users[$row['userId']]))
														{

															$user = $users[$row['userId']]->username;

															
														}
														else
														{
															$user = "--";	
														}
													}
													else
													{
														$user = "--";	
													}

												?>
												<?= $user  ?>
			 
											</td> -->
											<td><div class="responsive-table-th"><label>Published</label></div> 
												<div class="responsive-table-td">
												<?=$is_published;?>
												</div>
											</td>
											<!-- <td><?=$is_favorite;?></td> -->
											
											<?php 
												//if($has_category == "yes")
												{	

													//echo $row['category_id'];
													$temp_cat_id = array_filter(explode("|",$row['categoryId']));

													$category = "";

													if(count($temp_cat_id) > 0)
													{

														$tempArr = array();
														foreach ($temp_cat_id as  $value2) 
														{
															if(isset($categories[$value2]['name']))
															{
															
																$tempArr[] = $categories[$value2]['name'];
															}
															
														}

														if(count($tempArr) > 0)
														{
															$category = implode(", ",$tempArr);
														}
														else
														{
															$category = "--";
														}
														
													}
													else
													{
														$category  = "--";
													}
													?>

													<td><div class="responsive-table-th"><label>Category</label></div> 
														<div class="responsive-table-td">
														<?= $category;?>
														</div>
													</td>

												<?
												}
											?>
											<td><div class="responsive-table-th"><label>#</label></div> 
												<div class="responsive-table-td">
												<?=$key;?>
												</div>
											</td>
											<td><div class="responsive-table-th"><label>Action</label></div> 
												<div class="responsive-table-td">

												<a href="<?=site_url('admin/blog/update_article/') ;?>/<?= $key?>" class="btn-link">Edit</a>
												<span class="separator">|</span>
												<a href="<?=site_url('admin/blog/delete_article/') ;?>/<?= $key?>" class="btn-link">Delete</a>

												</div>									
											 </td>
										</tr>
										<?php

										$srNo--;
									}
								?>
							</table>
						</div>	
						<?php
					}
					else
					{?>
						
						<div class="form-wrapper no-data">

						<div class="icon"></div>
					
						<div class="msg"> No Articles Present.</div>

					</div>

						
					<?php
					}
				?>
			

			 <?php

              if(isset($paginationString))
              {
                echo $paginationString;
              }

          ?>

		</div>
	</div><!--/row-->
</div><!--/container-->