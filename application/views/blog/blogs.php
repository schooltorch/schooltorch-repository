<?php
  if(!isset($year))
  {
    $year   = 0;
  }

  if(!isset($month))
  {
    $month   = 0;
  }

  //dump($articlesInfo,1);

?>


<div class="main_container" id="blog">
  <div class="container">
         <div class="row">
              <div class="col-md-12">
                  <h1 class="main-header">The SchoolTorch Blog</h1>
              </div>
          </div>

        <div class="row">

          <div class="col-sm-8">

             
                <?php
                
                if(isset($articlesInfo))
                {
                  if($articlesInfo['count'] > 0)
                  {


                      $articleId = 0;

                      foreach ($articlesInfo['data']  as $key => $value) 
                      {
                        if(!isset($value["safeTitle"]))
                        {
                          $value["safeTitle"] = "";
                        }
                        
                        $articleId = $value['id'];

                    


                        if(isset($value['coverImage']) && $value['coverImage'] != "" && $value['coverImage'] != null)
                        {
                          
                           $coverImageUrl = base_url()."/blogPics/".$value['coverImage'];
                        }

                        ?>
                         
                         
                            <div class="blog-item">
                             
                              <div class="row"> 
                               

                                    <?php
                                      if($coverImageUrl != "")
                                      {?>
                                            <div class="col-md-3  col-sm-3 img">
                                              <img src="<?= $coverImageUrl ?>" class="img-responsive" >
                                            </div>

                                      <?php
                                      }
                                    ?>

                                  

                                    <div class="col-md-9 col-sm-9">
                                      <h4><a href="<?= site_url('blog/article') ?>/<?= $value['id']?>/<?= $value['safeTitle'] ?>"><?= $value['title'] ?></a>
                                        <?php
                                          $fullName = "";
                                          if(isset($value['firstName']) && isset($value['lastName']))
                                          {
                                            $fullName = $value['firstName'].' '.$value['lastName'];

                                          }
                                          else
                                          {
                                            $fullName=  "by SchoolTorch Team";
                                          }
                                        ?> 
                                      </h4>
                                      <div class="info"><?= date('F d, Y', strtotime($value['createdDate'])) ?> <cite class="pull-right">-  <?=$fullName ?></cite></div>
                                    
                                        <?php
                                       

                                        
                                          $readMoreFlag = 1;
                                         
                                        ?>
                                        
                                       

                                       
                                        <?php
                                        if($readMoreFlag == 1)
                                        {?>
                                           <div class="content"><?=substr( strip_tags($value['content']), 0,500)."..." ;?></div>
                                          <div class="action">
                                            <a href="<?= site_url('blog/article') ?>/<?= $value['id']?>/<?= $value['safeTitle'] ?>" class=" pull-right btn btn-info">Read More</a>
                                          </div>
                                        <?php
                                        }
                                        else{?>

                                           <div class="content"><?=substr( strip_tags($value['content']), 0,500)?></div>
                                        <?php
                                        }

                                        ?>
                                    </div>
                                  
                                                          
                               
                              </div>

                          
                          </div> 




                        <?php
                      }


                    
                   
                  }
                  else
                  {?>

                    <div class="blog-post">
                       <h3> No articles found </h3>
                    </div>
                  <?php
                  }
                
                }
                ?>

             


              <?php
                if(isset($paginationString))
                {
                  echo $paginationString;
                }
              ?>

          </div><!-- /.blog-main -->

         



          <div class="col-sm-4  blog-sidebar">
            <div class="row form-group">
              <div class="col-md-12">
                <div class="sidebar-module">
                  <div class="col-heading"> Categories</div>

                  <?php
                     if(isset($categories) && count($categories['data']) > 0 )
                     {?>  
                        
                        <ul class="list-unstyled">
                          <?php
                            foreach ($categories['data'] as $key => $value) 
                            {
                              if($value['count'] == 0)
                              {
                                ?>

                                <li><a href="<?= site_url('blog/category') ?>/<?= $value['id']?>/<?= $value['safeTitle']?>" class="btn-link"> <?= $value['name']  ?> </a></li>

                              <?php

                              }
                              else
                              {
                                ?>
                                <li><a href="<?= site_url('blog/category') ?>/<?= $value['id']?>/<?= $value['safeTitle']?>" class="btn-link"> <?= $value['name']  ?></a></li>
                              
                              <?php
                              }
                            }
                          ?>
                        </ul>
                        <?php
                     }
                     else
                     {
                      echo "No categories";
                     }
                  ?>
                </div>
              </div>
            </div>

            <div class="row form-group">
              <div class="col-md-12">
                <div class="sidebar-module">
                  <div class="col-heading"> Find Previous Posts</div>
                  <?php

                    $years   = $this->config->item('previousPostsYears');

                    if(!isset($years))
                    {
                      $years   = 1;
                    }

                    if($years < 1)
                    {
                      $years   = 1;
                    }

                    $first  = strtotime('first day this month');

                    $currentMonth   = date('F Y', time());
                    $key            = date('Y/m', time());

                    $totalMonths  = 12 * $years;

                    $months = array();

                    $months[$key] = $currentMonth;

                    for ($i = 1; $i <= ($totalMonths - 1); $i++) {

                        $key          = date('Y/m', strtotime( date( 'Y-m-01' )." -$i months"));
                        $months[$key] = date("F Y", strtotime( date( 'Y-m-01' )." -$i months"));

                    }

                    if(count($months) > 0)
                    {
                  ?>
                      <select id="checkMonth" class="form-control">
                        <option value="">Select Month</option>
                  <?php
                      foreach($months as $key=>$value)
                      {
                  ?>
                        <option value="<?=$key?>"><?=$value;?></option>
                  <?php
                      }
                  ?>
                      </select>
                  <?php
                    }

                    

                    

                  ?>
                </div>
              </div>
            </div>
          </div><!-- /.blog-sidebar -->

        </div><!-- /.row -->
  </div><!-- /.container -->
</div>

<script type="text/javascript">
  
  $(document).ready(function(){

    $('#checkMonth').unbind('change');
    $('#checkMonth').change(function(event){

      window.location  = "<?=site_url('blog/index/1')?>/"+ $('#checkMonth').val();

    });

    if(+"<?=$year?>" != 0 && +"<?=$month?>" != 0)
    {
      var value = "<?=$year?>/<?=$month?>";
      
      if($('#checkMonth option[value="'+ value +'"]').length > 0)
      {
        $('#checkMonth').val(value);
      }

    }

  })

</script>