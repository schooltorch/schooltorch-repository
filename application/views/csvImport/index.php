<div class="container">


	<?php
		if($errorMessage != '')
		{
			echo '<div class="alert alert-danger">'. $errorMessage .'</div>';
		}

		if($successMessage != '')
		{
			echo '<div class="alert alert-success">'. $successMessage .'</div>';
		}
	?>

	<?php echo form_open_multipart('admin/csvimporter/go');?>
		
		<div class="form-group">
			<label for="tableName">Table</label>
			<select id="tableName" name="tableName">
				<optgroup label="Data">
					<?php
						foreach($this->config->item('csvImportData') as $key => $value)
						{
							echo '<option value="'. $key .'">'. $value .'</option>';
						}
					?>
				</optgroup>
				<optgroup label="Frequencies">	
					<?php
						foreach($this->config->item('csvImportData') as $key => $value)
						{
							echo '<option value="schoolFrequencies:'. $key .'">Frequency: '. $value .'</option>';
						}
					?>
				</optgroup>
				<optgroup label="Var Lists">
					<?php
						foreach($this->config->item('csvImportData') as $key => $value)
						{
							echo '<option value="schoolVarlist:'. $key .'">Var List: '. $value .'</option>';
						}
					?>
				</optgroup>
				<optgroup label="Statistics">
					<?php
						foreach($this->config->item('csvImportData') as $key => $value)
						{
							echo '<option value="schoolStatistics:'. $key .'">Statistics: '. $value .'</option>';
						}
					?>
				</optgroup>
			</select>
		</div>

		<div class="form-group">
			<label for="tableName">CSV File</label>
			<input type="file" name="csvFile" id="csvFile" />
		</div>

		<button type="submit" class="btn btn-success">Submit</button>
	</form>

	<br /><br />
	<a href="<?=site_url();?>admin/csvimporter/updateAcceptance" class="btn btn-link">Update Acceptance</a>
	<a href="<?=site_url();?>admin/csvimporter/updateSelectivity" class="btn btn-link">Update Selectivity</a>

</div>