<style>	
	/*.glyphicon:empty {
		width: 1em;
	}

	table th a .order {
		display: none;
	}

	table th a.active.desc .order.desc {
	display: block;
	}

	table th a.active.asc .order.asc {
		display: block;
	}
*/
</style>

<?php

	$order_parameters = array('rating','flagCount','helpfulFlagCount','createdDate');

	$url 	=	site_url("admin/reviewManagement");

	foreach($order_parameters as $val)
	{
	 	if($order_by == $val)
	 	{
	 		$order_by_active[$val] 	= 'active';
	 		$order_active[$val] 	= $order;
	 	}
	 	else
	 	{
	 		$order_by_active[$val] = '';
	 		$order_active[$val]    = '';
	 	}
	 	
	 	if($order == 'asc'){
			$order_inactive[$val] 	= 'desc'; 
		}
		else
		{
			$order_inactive[$val] 	= 'asc'; 
		}
	}

	$isDataPresent 	=	0;
	
	if(!isset($status))
	{
		$status 	=	'failure';
	}

	if(!isset($data))
	{
		$data 		=	array();
	}
	
	if($status == 'success' && count($data) > 0)
	{
		$isDataPresent 	=	1;
	}

	if(!isset($isAll))
	{
		$isAll 	=	0;
	}

	/*if($isDataPresent == 1)
	{
		if(isset($data))
		{
			foreach ($data as $key => $value)
			{
				$restaurantId = $value['restaurantId'];
			}
		}
	}*/

	$tabUrl	=	'?';

	/*if(isset($restaurantId))
	{
		$tabUrl	.=	'?restaurantId='.$restaurantId.'&';
	}*/
	/*else
	{
		$tabUrl	=	'?';
	}*/



	if(isset($urlSuffix))
	{

		if($urlSuffix != '')
		{
			$tabUrl 	=	$urlSuffix.'&';
		}
		
	}
	$this->load->view('/reviews/reviewDetails');

	//dump($data);
	/*$reviewFeatures	=	array();
	$reviewFeatures	=	$this->config->item('reviewFeatures');
	$reviewCulture	=	array();
	$reviewCulture	=	$this->config->item('reviewCulture');*/
	
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">
				Reviews
			</h1>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-9">
					<?php
						$this->load->view("/admin/filter2");
					?>
				</div>
				<div class="col-md-3">
					<div class="form-group pull-right">
						<div class="checkbox">
							<label>
						<?php
								if($isAll == 1)
								{
						?>

									<input type="checkbox" class="checkbox" name="showAll" id="showAll" checked="checked"> <span style="display: block;padding: 5px 0;font-weight: 600;">Show All</span>
						<?php
								}
								else
								{
						?>
									<input type="checkbox" class="checkbox" name="showAll" id="showAll"> <span style="display: block;padding: 5px 0;font-weight: 600;">Show All</span>
						<?php
								}

						?>
								</label>
						</div>
						<span class="alert-danger"></span>
					</div>
				</div>
			</div>
		</div> 
		<div class="col-md-12">
			
			<?php 
				if($isDataPresent == 1)
				{?>

			   		<div style="clear:both;" class="custom-responsive-table" id="reviewsContainer">
						<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">
							<tr>
								<th width="20%">School</th>
								<th width="15%">User</th>
								<th width="10%"><a href="<?=$url.$tabUrl?>order_by=rating&order=<?=$order_inactive['rating'];?>" class="<?=$order_by_active['rating'] .' '. $order_inactive['rating'];?>">
									<span class="order asc glyphicon glyphicon-chevron-up pull-right" ></span>
									<span class="order desc glyphicon glyphicon-chevron-down pull-right"></span> 
									Rating
								</th>
								
								<th width="20%">Title</th>

								<th width="5%">Grad Type</th>
								<th width="5%">Student Type</th>
								<th width="5%"><a href="<?=$url.$tabUrl?>order_by=helpfulFlagCount&order=<?=$order_inactive['helpfulFlagCount'];?>" class="<?=$order_by_active['helpfulFlagCount'] .' '. $order_inactive['helpfulFlagCount'];?>">
									<span class="order asc glyphicon glyphicon-chevron-up pull-right" ></span>
									<span class="order desc glyphicon glyphicon-chevron-down pull-right"></span> 
									Helpful
								</th>

								<th width="5%"><a href="<?=$url.$tabUrl?>order_by=flagCount&order=<?=$order_inactive['flagCount'];?>" class="<?=$order_by_active['flagCount'] .' '. $order_inactive['flagCount'];?>">
									<span class="order asc glyphicon glyphicon-chevron-up pull-right" ></span>
									<span class="order desc glyphicon glyphicon-chevron-down pull-right"></span> 
									Flagged
								</th>

								<th width="5%">Approved</th>
															
								<th width="5%"><a href="<?=$url.$tabUrl?>order_by=createdDate&order=<?=$order_inactive['createdDate'];?>" class="<?=$order_by_active['createdDate'] .' '. $order_inactive['createdDate'];?>">
									<span class="order asc glyphicon glyphicon-chevron-up pull-right" ></span>
									<span class="order desc glyphicon glyphicon-chevron-down pull-right"></span> 
									Date
								</th>
								<th width="15%">Action</th>
							</tr>

							<?php
								foreach ($data as $key => $value) {
										
									
									?>

									<tr id="review-<?=$value['id']?>" class="reviews">

										<td>
											<div class="responsive-table-th"><label>School</label></div> 
											<div class="responsive-table-td">
												<?=$value['schoolName']?>
											</div>
										</td>

										<td>
											<div class="responsive-table-th"><label>User</label></div> 
											<div class="responsive-table-td">
												<?=$value['userName']?>
											</div>
										</td>

										<td>
											<div class="responsive-table-th"><label>Rating</label></div> 
											<div class="responsive-table-td">
										<?php

											for($i = 1;$i <= $value['rating'];$i++) 
											{
										?>
												<span class="glyphicon glyphicon-star rating-star"></span>
										<?php
											}

										?>
												
											</div>
										</td>

										<td>
											<div class="responsive-table-th"><label>Title</label></div> 
											<div class="responsive-table-td">
												<a href="#" class="btn-link"   data-toggle="modal" data-target="#review-details-modal"
											 	data-schoolName="<?= $value['schoolName'] ?>" data-id="<?=$value['id']?>"><?=$value['title']?></a>
												
											</div>
										</td>

										<td>
											<div class="responsive-table-th"><label>Grad Type</label></div> 
											<div class="responsive-table-td">
												<?php 
													if($value['gradType'] == 1)
													{
														$gradType	=	'Grad';
													}
													else
													{
														$gradType	=	'Undergrad';
													}
													echo $gradType;
												?>
											</div>
										</td>
										<td>
											<div class="responsive-table-th"><label>Grad Type</label></div> 
											<div class="responsive-table-td">
												<?php 
													if($value['studentType'] == 1)
													{
														$studenType	=	'Local';
													}
													else if($value['studentType'] == 2)
													{
														$studenType	=	'International';
													}else{
														$studenType	=	'-';
													}
													echo $studenType;
												?>
											</div>
										</td>
										
									<?php
										/*$tempFeatures	=	array();
										foreach (array_slice(explode('|',$value['features']),1,-1) as $key2 => $value2) 
										{
											$tempFeatures[]	=	$reviewFeatures[$value2];
										}*/
									?>
										<!-- <td><?=implode(',', $tempFeatures)?></td> -->
									<?php
										/*$tempCulture	=	array();
										foreach (array_slice(explode('|',$value['culture']),1,-1) as $key2 => $value2) 
										{
											$tempCulture[]	=	$reviewCulture[$value2];
										}*/
									?>
										<!-- <td><?=implode(',', $tempCulture)?></td> -->


										<td>
											<div class="responsive-table-th"><label>Helpful</label></div> 
											<div class="responsive-table-td">
												<?=$value['helpfulFlagCount']?>
											</div>
										</td>
										<td>
											<div class="responsive-table-th"><label>Flagged</label></div> 
											<div class="responsive-table-td">
												<?=$value['flagCount']?>
											</di>
										</td>
										<?php
											if($value['isApproved']	==	NULL)
											{
												$isApproved		=	'No';
												$approveLink	=	'Approve';
											}
											else
											{
												$isApproved 	=	'Yes';
												$approveLink	=	'';
											}
										?>
										<td>
											<div class="responsive-table-th"><label>Approved</label></div> 
											<div class="responsive-table-td" id="approve-id-<?= $value['id']?>" >
												<?=$isApproved?>
											</div>
										</td>								

										<td>
											<div class="responsive-table-th"><label>Date</label></div> 
											<div class="responsive-table-td">
												<?=date('Y/m/d H:i a',strtotime($value['createdDate']))?>
											</div>
										</td>
										<td>
											<!-- <a id="approve" href="<?=site_url();?>admin/reviewManagement/approveReview?reviewId=<?=$value['id']?>" class="btn-link">
												Approve
											</a> -->
											<div class="responsive-table-th"><label>Action</label></div> 
											<div class="responsive-table-td">
												<a href="#" class="btn-link"   data-toggle="modal" data-target="#review-details-modal"
											 	data-schoolName="<?= $value['schoolName'] ?>" data-id="<?=$value['id']?>">View</a>
												
											<a class="approveReview btn-link" id="approve-link-<?= $value['id']?>" data-id="<?= $value['id'] ?>"><?php echo $approveLink?></a>
											<a class="deleteReview btn-link" data-id="<?= $value['id'] ?>">Delete</a>
											</div>
											
										<!-- |<a id="delete" href="<?=site_url();?>admin/reviewManagement/delete?reviewId=<?=$value['id']?>" class="btn-link">Delete</a></td> -->
									</tr>

								<?php
								}
							?>

						</table>
					</div>
			
					<?php
						
						echo $create_link;
						
					?>


				<?php
				
				}
				else
				{

				?>
					<!-- <h4>No Reviews Present </h4> -->
					<div class="form-wrapper no-data">

						<div class="icon"></div>
					
						<div class="msg"> No Reviews Present.</div>

					</div>

			<?php		
				
				}

			?>

			</div>
		</div>
	</div>
</div>


<script> 
	var globalSiteUrl = "<?= site_url() ;?>";

	var reviewId = "<?= $reviewId ?>";

	console.log(reviewId);

	$(function(){


	    $( "#since" ).datepicker();
	    $( "#until" ).datepicker();

	    $("#reviewFilterForm").submit( function(eventObj){


	    	 var schoolId = $("#searchSchool").attr('data-id') ;
	    
		    if(typeof schoolId != "undefined"  && schoolId == "")
		    {
		    	var schoolId = $("#searchSchool").attr('value');

			 	console.log(schoolId);

			    $(this).append('<input type="hidden" name="schoolId" value="'+schoolId+'" /> ');               
			    return true;
		    }
		 	
		});

		$("input.checkbox").unbind('change');
		$("input.checkbox").change(function(event){

			var params 	=	window.location.search;

			if(params != '')
			{
				params 	=	params.split('&');

				if(params.indexOf('isAll=1') != -1 || params.indexOf('?isAll=1') != -1)
				{
					if(typeof params[params.indexOf('isAll=1')] != 'undefined')
					{
						params[params.indexOf('isAll=1')]	=	'';
						params[params.indexOf('isAll=1')]	=	null;
						delete params[params.indexOf('isAll=1')];
					}
					else if(typeof params[params.indexOf('?isAll=1')] != 'undefined')
					{
						params[params.indexOf('?isAll=1')]	=	'';
						params[params.indexOf('?isAll=1')]	=	null;
						delete params[params.indexOf('?isAll=1')];
					}
					
				}
			}
			else
			{
				params 	=	[];
			}

			if($(this).is(':checked')) 
			{
				if(params.length == 0)
				{
					params.push('?isAll=1');
				}
				else
				{
					params.push('isAll=1');
				}

				params 	=	params.join('&');

				if(params.indexOf('?') == -1)
				{
					params 	=	'?'+ params;
				}

		    	window.location = "<?php echo site_url('admin/reviewManagement/index')?>"+ params;
			}
			else
			{
				params 	=	params.join('&');
				
				if(params.indexOf('?') == -1)
				{
					params 	=	'?'+ params;
				}
				
			    window.location = "<?php echo site_url('admin/reviewManagement/index') ?>"+ params;
			}
		   
		});

	  	
	  	//binding event of approve[schools.js]
		approveReview();
	    // for delteing review[binding events of delete review]
	    delteReview();

	    if(typeof reviewId != "undefined")
	    {
	    	if(reviewId != "")
	    	{
	    		console.log("Here when the review id is there");
	    	
	    		
	    		console.log(reviewId);

	    		$.ajax({

			            url:globalSiteUrl+'admin/reviewManagement/view',
			            method:'GET',
			            data: {id:reviewId},
			            success:function(response)
			            {

			            	if(response)
			            	{
			            		$('#review-details-modal').modal('show');
			            		$(".modal-html").html(response);
			            	}

			            	delteReview();
			            	approveReview();
			            	
			            },
			            error:function()
			            {
			            	console.log("The ajax request failed");
			            }
		           
		        });

	    	}

	    }




	});

	
    


</script>