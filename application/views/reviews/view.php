<?php

/*
dump($review);
dump($school);
dump($user);
*/

if((is_null($user['firstName'] && is_null($user['lastName']))) || (strlen($user['firstName']) == 0 && strlen($user['lastName']) == 0)) 
{
	$user['firstName'] 	=	'Anonymous';
}
?>
<?php

	//dump($review);
	if($review['isApproved']	==	'Not Approved')
	{
		$isApproved		=	'No';
		$approveLink	=	'Approve';
	}
	else
	{
		$isApproved 	=	'Yes';
		$approveLink	=	'';
	}

	if($review['helpfulFlagCount'] == NULL)
	{
		$review['helpfulFlagCount'] = 0;
	}
	if($review['flagCount'] == NULL)
	{
		$review['flagCount'] = 0;
	}

	if($review['gradType'] == 1)
	{
		$gradType	=	'Grad';
	}
	else
	{
		$gradType	=	'Undergrad';
	}

	//dump($review);
	//exit();
?>

<div class="modal-body" id="reviews">

	<div class="review-item">	
		<div class="row">
			<div class="col-md-12">
				<h3>
					<?=$school['INSTNM']?>
				</h3>
			</div>
			<div class="col-md-12 date">
				<?= date('M d, Y',strtotime($review['createdDate']));  ?>
				<!-- <span class="pull-right">helpfulStr </span> -->
			</div>

			<div class="col-md-2 col-sm-2 col-xs-4">
				<!--<?=$user['firstName'].' '.$user['lastName'].'('.$user['email'].')'?>-->
				<img src="<?=base_url()?>/assets/images/profile.png" class="img-responsive"/>
			</div>

			<div class="col-md-10 col-sm-10 col-xs-8">
				<div class="title">
					<?=$review['title']?>
				</div>
				<div class="info">
					<cite>- <?=$user['firstName']." ". $user['lastName'] ?> - <?= $review['userTypeName']?> - <?= $gradType?> - <?= $review['major']?></cite>

				</div>
				<div class="rating">
					<?php
					if(isset($review['rating']))
					{
						for ($i=0; $i < $review['rating']; $i++) 
						{ 
						?>
							<span class="glyphicon glyphicon-star rating-star"></span>

						<?php
						}
					}
					?>
				</div>
				<div class="content row">
					<div class="col-md-3 col-sm-3"><label>Good</label></div>
					<div class="col-md-9 col-sm-9"><?=$review['good']?> </div>
				</div>
				<div class="content row">
					<div class="col-md-3 col-sm-3"><label>Bad</label></div>
					<div class="col-md-9 col-sm-9"><?=$review['bad']?></div>
				</div>
				<div class="content row">
					<div class="col-md-3 col-sm-3"><label>Best Features</label></div>
					<div class="col-md-9 col-sm-9"><?=$review['features']?></div>
				</div>

				<div class="content row">
					<div class="col-md-3 col-sm-3"><label>Culture</label></div>
					<div class="col-md-9 col-sm-9"><?=$review['culture']?></div>
				</div>


				<div class="counts row">

					<div class="col-md-3 col-sm-3"></div>
					<div class="col-md-9 col-sm-9">
						<p><span class="color-success"><?= $review['helpfulFlagCount']?></span> people found this helpful</p>
					</div>


					<div class="col-md-3 col-sm-3"></div>
					<div class="col-md-9 col-sm-9">
						<p><span class="color-danger"><?= $review['flagCount']?></span>  people flagged this review</p>
					</div>
					
				</div>
				

			</div>

		</div>
	</div>

</div>

<div class="modal-footer">
	
	<?php
		if($approveLink != "" && $this->session->userdata('role') == 2)
		{?>
			<a class="approveReview btn btn-success pull-left" data-id="<?= $review['id'] ?>"><?php echo $approveLink?></a>
		<?php
		}
	?>
	
	<a class="deleteReview btn btn-danger" data-id="<?= $review['id'] ?>">Delete</a>
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


</div> 



