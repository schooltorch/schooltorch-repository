







<div class="modal fade" id="review-details-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">

		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="headerName">Review Details</h4>
			</div>


			<div class="modal-html">
					

					
			</div>




		</div>
	</div>
</div>

<script>

$(document).ready(function(){

	$('#review-details-modal').on('show.bs.modal', function (e) {

		mainSelector  	= 	$(e.relatedTarget);

		reviewId 		=	$(mainSelector).attr('data-id');

		$.ajax({

	            url:globalSiteUrl+'admin/reviewManagement/view',
	            method:'GET',
	            data: {id:reviewId},
	            success:function(response)
	            {

	            	if(response)
	            	{
	            		$(".modal-html").html(response);
	            	}

	            	delteReview();
	            	approveReview();
	            	
	            },
	            error:function()
	            {
	            	console.log("The ajax request failed");
	            }
           
        });
	});

	
})

			

</script>