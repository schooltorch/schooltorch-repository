<?php
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'placeholder' =>'',
	'class'  => 'form-control input-lg',
	'size'	=> 30,
);

?>

<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<h4>Are you sure you want to delete your account?</h4>
			</div>
			<div class="form-group hide alert-danger" id="errorContainer"  style="padding: 10px;border-radius: 10px;">

   			</div>
			<?php echo form_open(site_url('auth/unregister'),array("class"=>"form","role"=>"form", "id" => "unregisterForm")); ?>
			<div class="form-group">
				<?php echo form_label('Password', $password['id']); ?>
				<?php echo form_password($password); ?>
				<span class="help-block error-block"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></span>
			</div>
			<?php echo form_submit(array('name'=> 'cancel','value'=>'Delete account','class'=>'btn btn-success'));  ?>
			<?php echo form_close(); ?>
		</div>
	</div><!--/row-->
</div><!--/container-->

<script type="text/javascript">
	
	$(document).ready(function(){

		$('#unregisterForm').unbind('submit');
		$('#unregisterForm').submit(function(event){

			event.preventDefault();

			var formObj 	= 	$(this);

		    var formURL 	= 	formObj.attr("action");

		    //alert(formURL);

		    var formData 	= 	new FormData(this);

			$.ajax({
		        url: formURL,
		    	type: 'POST',
		        data:  formData,
		        mimeType:"multipart/form-data",
		    	contentType: false,
		        cache: false,
		        processData:false,
		    	success: function(data, textStatus, jqXHR)
		    	{

		    		//alert(data);
 
		    		data 	=	JSON.parse(data);

		    		if(data.status == 'success')
		    		{
		    			if(typeof data.redirectUrl != 'undefined')
		    			{
		    				window.location.href 	=	data.redirectUrl;
		    			}
		    			else
		    			{
		    				window.location.reload();
		    			}
		    		}
		    		else if(data.status == 'partialFailure')
		    		{
		    			if(typeof data.redirectUrl != 'undefined')
		    			{
		    				window.location.href 	=	data.redirectUrl;
		    			}
		    		}
		    		else
		    		{
		    			if(typeof data.redirectUrl != 'undefined')
		    			{
		    				window.location.href 	=	data.redirectUrl;
		    			}
		    			else if(typeof data.errors != 'undefined')
		    			{
		    				if(typeof data.errors == 'string')
		    				{
		    					$('#errorContainer').html('');
		    					$('#errorContainer').html(data.errors).removeClass('hide');
		    				}
		    				else if(typeof data.errors == 'object')
		    				{
		    					$('#errorContainer').html('');
		    					$.each(data.errors, function(key, value){

		    						$('#errorContainer').append('<div>'+ value +'</div>').removeClass('hide');

		    					})
		    				}
		    			}
		    		}
		 			
		    	},
		     	error: function(jqXHR, textStatus, errorThrown) 
		     	{
		     		
		     	}          
		    });

		})

	});

</script>


