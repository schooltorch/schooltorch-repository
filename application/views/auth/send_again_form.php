<?php
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'placeholder' =>'',
	'class'  => 'form-control input-lg',
	'maxlength'	=> 80,
	'size'	=> 30,
);
?>

<div class="container">
	<div class="row">
		<div class="form-wrapper">
			<div class="form-signin-heading header-image">

    			<a href="<?=site_url()?>" title="Home"><img class="img-responsive" alt="" src="<?=base_url()?>assets/images/logo-black.png"></a>

   			 </div>
   			<div class="form-group">
   					Your account has not been verified. Please enter your email address to receive the verification email again
			</div>

			<?php echo form_open($this->uri->uri_string()); ?>
			<div class="form-group">
				<?php echo form_label('Email Address', $email['id']); ?>
				<?php echo form_input($email); ?>
				<span class="help-block error-block"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></span>
			</div>
			<?php echo form_submit(array('name'=> 'send','value'=>'Send','class'=>'btn btn-success'));  ?>
			<?php echo form_close(); ?>
		</div>
	</div><!--/row-->
</div><!--/container-->