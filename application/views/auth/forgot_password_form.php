<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'placeholder' =>'',
	'class'  => 'form-control input-lg',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
	

);
if ($this->config->item('use_username')) {
	$login_label = 'Email or login';
} else {
	$login_label = 'Email';
}
?>


<div class="container">
	<div class="">
		<div class="form-wrapper">
			<div class="form-signin-heading header-image">

    			<a href="site_url()" title="Home"><img class="img-responsive" alt="" src="<?=base_url()?>assets/images/logo-black.png"></a>

   			 </div>

			<div class="form-group">
				<?php echo form_open($this->uri->uri_string()); ?>
				<?php echo form_label($login_label, $login['id']); ?>
				<?php echo form_input($login); ?>
				<span class="help-block error-block"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></span>
			</div>
			<div class="form-group">
				<?php echo form_submit(array('name'=>'reset', 'value' => 'Get a new password','class' => 'btn btn-success btn-lg btn-block')); ?>
			</div>
			<?php echo form_close(); ?>
			<div class="form-group">
				 <a class="btn btn-lg btn-default btn-block" title="Login Page" href="<?=site_url()?>auth/login">Login Page</a>
			</div>
		</div>
	</div><!--/row-->
</div><!--/container-->


