
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="form-wrapper" style="text-align:center;">

				<div class="form-signin-heading header-image">
					<img src="<?=base_url()?>assets/images/logo-black.png" alt="School" class="img-responsive"/>
   				</div>
   				<div class="form-group">
   					<?php echo $message; ?>
				</div>

				<div class="form-group">
   					<a href="<?=site_url()?>" class="btn btn-link"> Home </a>
				</div>
			</div>
		</div>
	</div>
</div>