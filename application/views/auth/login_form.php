<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'placeholder' =>'Email Address',
	'class'  => 'form-control input-lg',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
	
	'required' => true
);

$mainClassName 		=	'container';
$fromController 	=	1;

if(!isset($login_by_username) || !isset($login_by_email))
{ 
	$fromController 	=	0;
	$mainClassName 		=	'';

	$login_by_username 	=	FALSE;
	$login_by_email 	=	TRUE;

	$login_by_username = ($this->config->item('login_by_username') AND
					$this->config->item('use_username'));
	$login_by_email = $this->config->item('login_by_email');
}

if(!isset($use_recaptcha))
{
	$use_recaptcha = $this->config->item('use_recaptcha');
	//$use_recaptcha 		=	FALSE;
}

if(!isset($show_captcha))
{
	$show_captcha = FALSE;
}

if(!isset($recaptcha_html))
{
	$recaptcha_html = '';
}

if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}

$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'placeholder' =>'Password',
	'class'  => 'form-control input-lg',
	'size'	=> 30,
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'placeholder' =>'',
	
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);

$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'placeholder' =>'',
	'class'  => 'form-control input-lg',
	'maxlength'	=> 8,
);
?>

<div class="<?=$mainClassName;?>">
	<div class="">
		<div class="form-wrapper">
		
			<?php echo form_open(site_url('auth/loginAjax'),array("class"=>"form","role"=>"form", "id" => "loginForm")); ?>

			<div class="form-signin-heading header-image" style="text-align:center">

				<h3>

					
					Sign in to SchoolTorch

					
				</h3>

   			 </div>
   			<div class="form-group hide alert-danger" id="errorContainer"  style="padding: 10px;border-radius: 10px;">

   			</div>


			<div class="form-group social-signin text-center">

				<div class="btn btn-default btn-lg facebook btn-block" onclick='window.location="<?=site_url('facebook/auth')?>"'><i class="icon icon-facebook"></i> Sign in with Facebook</div>

				<div class="btn btn-default btn-lg twitter btn-block" onclick='window.location="<?=site_url('twitter/auth')?>"'><i class="icon icon-twitter"></i> Sign in with Twitter</div>

				<div class="btn btn-default btn-lg google btn-block" onclick='window.location="<?=site_url('google/auth')?>"'><i class="icon icon-google"></i> Sign in with Google</div>

			</div>

			<div class="form-group social-or" style="text-align:center;">

				or
				
			</div>



			<div class="form-group">
			

					<!--[if lte IE 9]>
						<?php echo form_label($login_label, $login['id']); ?>
					<![endif]-->

				<?php echo form_input($login); ?>
				<span class="help-block error-block"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></span>
			</div>

			<div class="form-group">
				

					<!--[if lte IE 9]>
						<?php echo form_label('Password', $password['id']); ?>
					<![endif]-->



				<?php echo form_password($password); ?>
				<span class="help-block error-block"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></span>
			</div>

			

		
			<div class="form-group">
					<?php echo form_checkbox($remember); ?>
					<?php echo form_label('Remember me', $remember['id']); ?>

					<?php echo form_submit(array('name'=>'submit', 'value' => 'Sign In','class' =>'btn btn-success pull-right' )); ?>
					
			</div>
			
			

			<div class="form-group text-center">

				<?php echo anchor('/auth/forgot_password/', 'Forgot password'); ?>

			</div>

			<div class="form-group text-center">

				Not a member? <strong><?php
					if($fromController == 1)
					{
				?>
						<?php if ($this->config->item('allow_registration')) echo anchor('/auth/register/', 'Join now',array("class"=>"btn-link")); ?>
				<?php
					}
					else
					{
				?>
						<?php if ($this->config->item('allow_registration')) echo anchor('/auth/register/', 'Sign Up',array("class"=>"btn-link", "id"=>"registerUser")); ?>
				<?php
					}
				?>
			</strong>
			</div>
			
			<?php echo form_close(); ?>
			
		</div>
	</div><!--/row-->
</div><!--/container-->


<script type="text/javascript">
	function getCookie(name){
            var pattern = RegExp(name + "=.[^;]*")
            matched = document.cookie.match(pattern)
            if(matched){
                var cookie = matched[0].split('=')
                return cookie[1]
            }
            return false
        }
        function eraseCookieFromAllPaths(name) {
            // This function will attempt to remove a cookie from all paths.
            var pathBits = location.pathname.split('/');
            var pathCurrent = ' path=';

            // do a simple pathless delete first.
            document.cookie = name + '=; expires=Thu, 01-Jan-1970 00:00:01 GMT;';

            for (var i = 0; i < pathBits.length; i++) {
                pathCurrent += ((pathCurrent.substr(-1) != '/') ? '/' : '') + pathBits[i];
                document.cookie = name + '=; expires=Thu, 01-Jan-1970 00:00:01 GMT;' + pathCurrent + ';';
            }
        }
	$(document).ready(function(){

		$('#loginForm').unbind('submit');
		$('#loginForm').submit(function(event){

			event.preventDefault();

			var formObj 	= 	$(this);

		    var formURL 	= 	formObj.attr("action");

		    var formData 	= 	new FormData(this);
			if(getCookie("school_save_id") != ""){
				formData.append("school_new_save_id",getCookie("school_save_id"))
			}
			$.ajax({
		        url: formURL,
		    	type: 'POST',
		        data:  formData,
		        mimeType:"multipart/form-data",
		    	contentType: false,
		        cache: false,
		        processData:false,
		    	success: function(data, textStatus, jqXHR)
		    	{
		    		data 	=	JSON.parse(data);

		    		if(data.status == 'success')
		    		{
		    			if(typeof data.redirectUrl != 'undefined')
		    			{
		    				window.location.href 	=	data.redirectUrl;
		    			}
		    			else
		    			{
		    				if(getCookie("page_redirect") == "write_review"){
	                                                eraseCookieFromAllPaths("page_redirect");
	                                                window.location.href = "<?php echo site_url('reviews/createReview') ?>";
	                                            }
	                                            else{
	                                                window.location.reload();
	                                            }
		    			}
		    		}
		    		else if(data.status == 'partialFailure')
		    		{
		    			if(typeof data.redirectUrl != 'undefined')
		    			{
		    				window.location.href 	=	data.redirectUrl;
		    			}
		    		}
		    		else
		    		{
		    			if(typeof data.errors != 'undefined')
		    			{
		    				if(typeof data.errors == 'string')
		    				{
		    					$('#errorContainer').html('');
		    					$('#errorContainer').html(data.errors).removeClass('hide');
		    				}
		    				else if(typeof data.errors == 'object')
		    				{
		    					$('#errorContainer').html('');
		    					$.each(data.errors, function(key, value){

		    						$('#errorContainer').append('<div>'+ value +'</div>').removeClass('hide');

		    					})
		    				}
		    			}
		    		}
		 			
		    	},
		     	error: function(jqXHR, textStatus, errorThrown) 
		     	{
		     		
		     	}          
		    });

		})

	});
	function getCookie(name) {
		var dc = document.cookie;
		var prefix = name + "=";
		var begin = dc.indexOf("; " + prefix);
		if (begin == -1) {
			begin = dc.indexOf(prefix);
			if (begin != 0) return null;
		}
		else
		{
			begin += 2;
			var end = document.cookie.indexOf(";", begin);
			if (end == -1) {
			end = dc.length;
			}
		}
		return unescape(dc.substring(begin + prefix.length, end));
	} 
</script>
