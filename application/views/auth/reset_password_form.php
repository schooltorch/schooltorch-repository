<?php
$new_password = array(
	'name'	=> 'new_password',
	'id'	=> 'new_password',
	'placeholder' =>'',
	'class'  => 'form-control input-lg',
	'maxlength'	=> $this->config->item('password_max_length'),
	'size'	=> 30,
);
$confirm_new_password = array(
	'name'	=> 'confirm_new_password',
	'id'	=> 'confirm_new_password',
	'placeholder' =>'',
	'class'  => 'form-control input-lg',
	'maxlength'	=> $this->config->item('password_max_length'),
	'size' 	=> 30,
);
?>

<div class="container">
	<div class="row">
		<div class="form-wrapper">
			<div class="form-signin-heading header-image">

    			<a href="site_url()" title="Home"><img class="img-responsive" alt="" src="<?=base_url()?>assets/images/logo-black.png"></a>

   			 </div>
			<?php echo form_open($this->uri->uri_string()); ?>

			<div class="form-group">
				<?php echo form_label('New Password', $new_password['id']); ?>
				<?php echo form_password($new_password); ?>
				<span class="help-block error-block"><?php echo form_error($new_password['name']); ?><?php echo isset($errors[$new_password['name']])?$errors[$new_password['name']]:''; ?></span>

			</div>

			<div class="form-group">
				<?php echo form_label('Confirm New Password', $confirm_new_password['id']); ?>
				<?php echo form_password($confirm_new_password); ?>
				<span class="help-block error-block"><?php echo form_error($confirm_new_password['name']); ?><?php echo isset($errors[$confirm_new_password['name']])?$errors[$confirm_new_password['name']]:''; ?> </span>
			</div>
			<?php echo form_submit(array('name'=> 'change','value'=>'Change Password','class'=>'btn btn-success'));  ?>
			<?php echo form_close(); ?>
		</div>
	</div><!--/row-->
</div><!--/container-->


