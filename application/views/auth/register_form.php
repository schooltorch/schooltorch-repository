<?php

$fromController 	=	1;
$mainClassName 		=	'container';

if(!isset($use_username))
{
	$fromController 	=	0;
	$mainClassName 		=	'';
	$use_username = $this->config->item('use_username');
}

if(!isset($captcha_registration))
{
	$captcha_registration = $this->config->item('captcha_registration');
}

if(!isset($use_recaptcha))
{
	$use_recaptcha = $this->config->item('use_recaptcha');
}


if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'value' => set_value('username'),
		'class' =>  'form-control input-lg',
		'maxlength'	=> $this->config->item('username_max_length'),
		'placeholder' =>'Unsername',
		'size'	=> 30,
		'required' => true
	);
}

/*$fname = array(
		'name'	=> 'fname',
		'id'	=> 'fname',
		'value' => set_value('fname'),
		'class' =>  'form-control input-lg',
		'maxlength'	=> 80,
		'placeholder' =>'First Name',
		'size'	=> 30
	);

$lname = array(
		'name'	=> 'lname',
		'id'	=> 'lname',
		'value' => set_value('lname'),
		'class' =>  'form-control input-lg',
		'maxlength'	=> 80,
		'placeholder' =>'Last Name',
		'size'	=> 30
	);*/

$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'class' =>  'form-control input-lg',
	'value'	=> set_value('email'),
	'placeholder' =>'Email Address',
	'maxlength'	=> 80,
	'size'	=> 30,
	'type'  => 'email',
	'required'	=>true
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'class' =>  'form-control input-lg',
	'value' => set_value('password'),
	'placeholder' =>'Password',
	'maxlength'	=> $this->config->item('password_max_length'),
	'size'	=> 30,
	'required'	=>true
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'class' =>  'form-control input-lg',
	'value' => set_value('confirm_password'),
	'placeholder' =>'Confirm Password',
	'maxlength'	=> $this->config->item('password_max_length'),
	'size'	=> 30,
	'required'	=>true
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'class' =>  'form-control input-lg',
	'maxlength'	=> 8,
);
?>


<div class="<?=$mainClassName;?>">
	<div class="">
		<div class="form-signin-heading header-image"><h3 class="center-text">Sign Up for SchoolTorch</h3>

   			 </div>
		<div class="form-wrapper">
			


   			<div class="form-group hide alert-danger" id="registerErrorContainer" style="padding: 10px;border-radius: 10px;">

   			</div>

   			<div class="form-group hide alert-success" id="registerSuccess" style="padding: 10px;border-radius: 10px;">

   			</div>


			<?php echo form_open(site_url('auth/registerAjax'),array('class'=>"form bv-form", 'id' => 'registerForm', 'role'=>'form')); ?>

			<?php if ($use_username) { ?>
				<div class="form-group">

					<!--[if lte IE 9]>
					<?php echo form_label('Username', $username['id']); ?>
					<![endif]-->

					

					<?php echo form_input($username); ?>
					<span class="help-block error-block"><?php echo form_error($username['name']); ?><?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?></span>
				</div>

			<?php
				}
			?>

			<!--<div class="form-group">
			


				<!--[if lte IE 9]>
					<?php //echo form_label('Email Address', $fname['id']); ?>
				<![endif]

				<?php //echo form_input($fname); ?>
				<span class="help-block error-block"><?php //echo form_error($fname['name']); ?><?php //echo isset($errors[$fname['name']])?$errors[$fname['name']]:''; ?></span>
			</div>-->

			<!--<div class="form-group">
			


				<!--[if lte IE 9]>
					<?php //echo form_label('Email Address', $lname['id']); ?>
				<![endif]-->

				<?php //echo form_input($lname); ?>
				<!--<span class="help-block error-block"><?php //echo form_error($lname['name']); ?><?php //echo isset($errors[$lname['name']])?$errors[$lname['name']]:''; ?></span>
			</div>-->
			<div class="form-group">
			


				<!--[if lte IE 9]>
					<?php echo form_label('Email Address', $email['id']); ?>
				<![endif]-->

				<?php echo form_input($email); ?>
				<span class="help-block error-block"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></span>
			</div>

			<div class="form-group">
				

				<!--[if lte IE 9]>
					<?php echo form_label('Password', $password['id']); ?>
				<![endif]-->
				<?php echo form_password($password); ?>
				<span class="help-block error-block"><?php echo form_error($password['name']); ?></span>
			</div>
			<div class="form-group">

				<!--[if lte IE 9]>
					<?php echo form_label('Confirm Password', $confirm_password['id']); ?>
				<![endif]-->
				
				<?php echo form_password($confirm_password); ?>
				<span class="help-block error-block"><?php echo form_error($confirm_password['name']); ?></span>
			</div>

			<?php if ($captcha_registration) {
				if ($use_recaptcha) { ?>
			<div class="form-group">
				
				<div id="recaptcha_image"></div>
				<a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
				<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
				<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
				
			</div>

			<div class="form-group">
				
				<div class="recaptcha_only_if_image">Enter the words above</div>
				<div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
				
				<input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
				<span class="help-block error-block"><?php echo form_error('recaptcha_response_field'); ?></span>
				<?php echo $recaptcha_html; ?>
			</div>
			<?php }
			 else 
			 	{?>
					<div class="form-group">
							<p>Enter the code exactly as it appears:</p>
							<?php echo $captcha_html; ?>
					</div>
					<div class="form-group">
						<?php echo form_label('Confirmation Code', $captcha['id']); ?>
						<?php echo form_input($captcha); ?>
						<span class="help-block error-block"> <?php echo form_error($captcha['name']); ?> </span>
					</div>
			<?php }
			} ?>


			<!--<div class="form-group terms">
				
				<?php
				//$this->load->view('home/terms');
				?>

			</div>-->


			<div class="form-group disclaimer">
                            By creating an account, you agree to SchoolTorch's <a href="<?php echo site_url('terms'); ?>" target="_blank">Terms of Use</a> and <a href="<?php echo site_url('privacy'); ?>" target="_blank">Privacy Policy</a>.

			</div>





		<div class="form-group action">
		<?php echo form_submit(array('value'=>'Sign Up', 'id' => 'registerButton', 'class'=>'btn btn-success btn-lg btn-block','name'=>'register')); ?><br/>
		<br/><span class="login-return">Already registered? <a href="" class="loginCheck"><strong>Sign In</strong></a></span>
	</div>
		<?php
			if($fromController == 1)
			{
		?>
			<div class="form-group">
				<a href="<?php echo site_url("auth/login");  ?>" class="btn btn-lg btn-default btn-block">Back</a>
			</div>
		<?php
			}
		?>

		<?php echo form_close(); ?>

		</div>
	</div><!--/row-->
</div><!--/container-->

<script type="text/javascript">
	
	$(document).ready(function(){

		function resetForm()
		{
			$('#registerForm').find("input[type=text], textarea,input[type=email],input[type=password]").val("");
		}

		/*$('#registerForm').bootstrapValidator({

            message: 'This value is not valid',

            excluded: [':disabled', ':hidden', ':not(:visible)'],

            feedbackIcons: {
                valid: '',
                invalid: '',
                validating: ''
            },

            fields: {

            	'fname' :  	{
					                validators: {
					                    notEmpty: {
					                        message: 'The First Name is required'
					                    }
					                }
					            },
			    
				'lname'	: 	{
									validators: {
					                    notEmpty: {
					                        message: 'The Last Name is required'
					                    }
					                }
								},

            	'email' : {
				                validators: {

				                	 notEmpty: {
					                        message: 'The Email is required'
					                    }
				                }
				            },
	            'password' :  {
					                validators: {
					                    notEmpty: {
					                        message: 'The Password is required'
					                    }
					                }
					            },
	            'confirm_password' :  {
					                validators: {
					                    notEmpty: {
					                        message: 'The Confirm Password is required'
					                    }
					                }
					            }
				 }
           }
        });*/

		$('#registerForm').unbind('submit');
		$('#registerForm').submit(function(event){

			event.preventDefault();
			event.stopPropagation();

			var formObj 	= 	$(this);

			var formURL 	= 	formObj.attr("action");

			var formData 	= 	new FormData(this);

			$('#registerButton').attr('disabled', 'disabled').attr('value', 'Processing');

			$.ajax({
				url: formURL,
				type: 'POST',
				data:  formData,
			    mimeType:"multipart/form-data",
				contentType: false,
				cache: false,
			    processData:false,
				success: function(data, textStatus, jqXHR)
				{

					$('#registerButton').removeAttr('disabled').attr('value', 'Sign Up');

		    		data 	=	JSON.parse(data);

		    		if(data.status == 'success')
		    		{
		    			$('#registerErrorContainer').addClass('hide');

		    			if(typeof data.message != 'undefined')
			    		{
			    			$('#registerSuccess').html(data.message).removeClass('hide');
			    			//$('#registerSuccess').append('<div>Please <a class="btn-link loginCheck" id="launchLogin">Click Here</a> to Login.</div>');
			    		}
			    		else
			    		{
			    			//$('#registerSuccess').html('You have been registered successfully. Please <a class="btn-link" id="launchLogin">Click Here</a> to Login.').removeClass('hide');
			    		}
			    		resetForm();
			    		bindLoginPopupEvent();
		    		}
		    		else if(data.status == 'partialFailure')
		    		{
		    			if(typeof data.redirectUrl != 'undefined')
		    			{
		    				window.location.href 	=	data.redirectUrl;
		    			}
		    		}
		    		else
		    		{
		    			if(typeof data.errors != 'undefined')
		    			{
		    				$('#registerErrorContainer').slideUp(100);

		    				if(typeof data.errors == 'string')
		    				{
		    					$('#registerErrorContainer').html('');
		    					$('#registerErrorContainer').html(data.errors).removeClass('hide');
		    					$('#registerErrorContainer').slideDown(100);
		    				}
		    				else if(typeof data.errors == 'object')
		    				{
								$('#registerErrorContainer').html('');
								$.each(data.errors, function(key, value){

		    						$('#registerErrorContainer').append('<div>'+ value +'</div>').removeClass('hide');

								})
								$('#registerErrorContainer').slideDown(100);
		    				}
		    			}
		    		}


		    		console.log(data);
		 			
		    	},
		     	error: function(jqXHR, textStatus, errorThrown) 
		     	{
		     		
		     	}          
		    });

		});

		function bindLoginPopupEvent()
		{
			$('.loginCheck').unbind('click');
			$('.loginCheck').click(function(event){

				event.preventDefault();

				$('#loginPopup').trigger('click')

			})
		}

		bindLoginPopupEvent();

	});

</script>