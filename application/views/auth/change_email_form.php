<?php
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'class'  => 'form-control input-lg',
	'placeholder' =>'',
	'size'	=> 30,
);
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'placeholder' =>'',
	'class'  => 'form-control input-lg',
	'maxlength'	=> 80,
	'size'	=> 30,
);
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php echo form_open($this->uri->uri_string()); ?>
			<div class="form-group">
				<?php echo form_label('Password', $password['id']); ?>
				<?php echo form_password($password); ?>
				<span class="help-block error-block"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></span>

			</div>

			<div class="form-group">
				<?php echo form_label('New email address', $email['id']); ?>
				<?php echo form_input($email); ?>
				<span class="help-block error-block"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></span>
			</div>
			<?php echo form_submit(array('name'=> 'change','value'=>'Send confirmation email','class'=>'btn btn-success'));  ?>
			<?php echo form_close(); ?>
		</div>
	</div><!--/row-->
</div><!--/container-->

