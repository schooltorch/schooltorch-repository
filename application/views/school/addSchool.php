<div class="container">
	
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">
				Add School
			</h1>
			<a href="<?= site_url("admin/school/programs") ;?>" class="btn btn-success pull-right">Manage Programs</a>
		</div>
		
		<div class="col-md-6">
			<form role="form"  action="<?php site_url("admin/school/add") ;?>">
			  	<div class="form-group">
					<label for="name">Name</label>
					<input type="text" name="name" class="form-control input-lg" id="name" placeholder="Enter name of school">
				</div>
				<div class="form-group">
					<label for="location">Location</label>
					<input type="text" name="location" class="form-control input-lg" id="location" placeholder="Enter location">
				</div>
				<div class="form-group">
			    	<label for="state">State</label>
					<?php
						if(count($states) > 0)
						{
					?>
							<select class="form-control input-lg" name="state" id="state">
								<option selected="selected" value="0" disabled="disabled">Select State</option>
					<?php
							foreach($states as $key=>$value)
							{
					?>
								<option value="<?=$value['code']?>"><?=$value['name']?></option>
					<?php
							}
					?>
							</select>
					<?php
						}
						else
						{
					?>
						<select class="form-control input-lg" name="state" id="state">
							<option selected="selected" disabled="disabled">Select State</option>
						</select>
					<?php
						}
					?>
			  	</div>
				<div class="form-group">
			    	<label for="city">City</label>
			    	<select class="form-control input-lg" name="city" id="city" title="Select State to get the list of Cities">
							<option selected="selected" disabled="disabled">Select City</option>
					</select>
			    	
			  	</div>
			  	

				<div class="form-group">
			    	<label for="area">Area</label>
			    	<select class="form-control input-lg" name="area" id="area">
						  <option value="town">Town</option>
						  <option value="rural">Rural</option>
						  <option value="urban">Urban</option>
						  <option value="suburban">Suburban</option>
					</select>
			    	
			  	</div>

			  	<div class="form-group">
			    	<label for="latitude">Latitude</label>
			    	<input type="text" name="latitude" class="form-control input-lg" id="latitude" placeholder="Enter latitude">
			  	</div>
			  	<div class="form-group">
			    	<label for="longitude">Longitude</label>
			    	<input type="text" name="longitude" class="form-control input-lg" id="longitude" placeholder="Enter longitude">
			  	</div>

			  	<div class="form-group">
			    	<label for="programs">Programs</label>
			    	
			    	<?php
						if(count($programs) > 0)
						{
					?>
							<select class="form-control input-lg" name="programs" id="programs" multiple>
								<option selected="selected" value="0" disabled="disabled">Select Programs</option>
					<?php
							foreach($programs as $key=>$value)
							{
					?>
								<option value="<?=$value['id']?>"><?=$value['name']?></option>
					<?php
							}
					?>
							</select>
					<?php
						}
						else
						{
					?>
						<select class="form-control input-lg" name="programs" id="programs">
							<option selected="selected" disabled="disabled">Select Programs</option>
						</select>
					<?php
						}
					?>
			    	
			  	</div>

			  	<div class="form-group">
			    	<label for="enrollmentSize">Enrollment Size</label>
			    	<input type="text" name="enrollmentSize" class="form-control input-lg"  id="enrollmentSize" placeholder="Enter Enrollment Size">
			  	</div>

			  	<div class="form-group">
			    	<label for="tuition">Tuition</label>
			    	<input type="text" name="tuition" class="form-control input-lg"  id="tuition" placeholder="Enter tuition">
			  	</div>
			  	<div class="form-group">
			    	<label for="selectivity">Selectivity</label>
			    	<input type="text" name="selectivity" class="form-control input-lg"  id="selectivity" placeholder="Enter Selectivity">
			  	</div>

			  	<div class="form-group">
			    	<label for="demographicsMale">Demographics Male</label>
			    	<input type="text" name="demographicsMale" class="form-control input-lg"  id="demographicsMale" placeholder="Enter Demographics Male">
			    </div>
		    	<div class="form-group">
		    		<label for="demographicsFemale">Demographics Female</label>
			    	<input type="text" name="demographicsFemale" class="form-control input-lg"  id="demographicsFemale" placeholder="Enter Demographics Female">

		    	</div>

		    	<div class="form-group">
		    		<label for="demographicsInternational">Demographics International</label>
			    	<input type="text" name="demographicsInternational" class="form-control input-lg"  id="demographicsInternational" placeholder="Enter Demographics International">
				
		    	</div>

			  	<div class="form-group">
			    	<label for="keyFeatures">Key Features</label>
			    	
			    	<textarea class="form-control input-lg" rows="3" name="keyFeatures"  id="keyFeatures" placeholder="Enter key features"></textarea>
			  	</div>

			  	<div class="form-group">
			    	<label for="survey">Survey</label>
			    	<?php
						if(count($survey) > 0)
						{
					?>
							<select class="form-control input-lg" name="survey" id="survey">
								<option selected="selected" value="0" disabled="disabled">Select survey</option>
					<?php
							foreach($survey as $key=>$value)
							{
					?>
								<option value="<?=$value['id']?>"><?=$value['name']?></option>
					<?php
							}
					?>
							</select>
					<?php
						}
						else
						{
					?>
						<select class="form-control input-lg" name="survey" id="survey">
							<option selected="selected" disabled="disabled">Select survey</option>
						</select>
					<?php
						}
					?>
			  	</div>
			  	
				<div class="checkbox">
				    <label>
				      <input type="checkbox" name="isActive">Is Active
				    </label>
				 </div>
	  			 <button type="submit" class="btn btn-success">Submit</button>
	  			 <a href="<?= site_url("/admin/school") ?>" class="btn"> Cancel </a>
	  		</form>
	  	</div>
	</div><!--/row-->
</div><!--/container-->

<script>
	
	$(document).ready(function(e){


		$('#state').unbind('change');
		$('#state').change(function(event){
			
			if($('#state').val() != '0')
			{
				$('#city').html('<option selected="selected" disabled="disabled" value="0">Loading...</option>');

				$.ajax(
					{
						type: "GET",
						url: "<?= site_url('admin/school/getCities') ?>",
						data: { stateCode: $('#state').val()}
					}
				).done(function(data) {

					data 	=	JSON.parse(data);
					
			    	if(data.status == 'failure')
			    	{
			    		alert(data.message);
			    	}
			    	else
			    	{
			    		var optionsStr 	=	'';
			    		$.each(data.data, function(index, value){
							optionsStr	+=	'<option value="'+ value.city +'('+ value.zip +')" city="'+ value.city +'" lt="'+ value.latitude +'" lg="'+ value.longitude +'" zip="'+ value.zip +'">'+ value.city +'  ('+ value.zip +')</option>';
						});
			    		$('#city').html(optionsStr);
			    		
			    		$("#city option:first").attr('selected','selected');

			    		$('#city').trigger('change');
			    	}
			  	});

			}

		});

		$('#city').unbind('change');
		$('#city').change( function(event){

			if($('#city').val() != '')
			{
				var selector 	=	$('#city option[value="'+ $('#city').val() +'"]');
				
				if(selector.length > 0)
				{
					$('#latitude').val('');
					$('#longitude').val('');
					$('#latitude').val(selector.attr('lt'));
					$('#longitude').val(selector.attr('lg'));
				}
			}

		})


	})

</script>
