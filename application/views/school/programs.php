<div class="container">
	
	<div class="row">
		<div class="col-md-6">
			<h3> Programs </h3>

			<?php
				if($status == 'success')
				{
			?>
				<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Created Date</th>
						<th>Action</th>
					</tr>
			<?php
					foreach($data as $key=>$value)
					{
			?>
						<tr>
							<td><?=$value['id']?></td>
							<td><?=$value['name']?></td>
							<td><?=date('M d Y, h:i:s a', strtotime($value['createDate']))?></td>
							<td><a href="">Edit </a> | <a href="">Delete </a></td>
						</tr>
			<?php
					}
			?>
				</table>
			<?php
				echo $pagination_str;
				}
				else
				{
			?>
					No Programs.
			<?php
				}
			?>
			
		</div>

		<div class="col-md-6">
				<h3> Add Program</h3>
				<?php if(isset($error)){
				?>
					<span class="danger"><?=$error;?></span>
				<?php
				} ?>
				<form role="form"  action="<?= site_url("admin/school/addPrograms") ;?>">
				  	<div class="form-group">
						<input type="text" name="name" class="form-control input-lg" id="name" placeholder="Enter name of Programs">
					</div>

					<button type="submit" class="btn btn-success">Submit</button>
	  				<a href="<?= site_url("admin/school/add") ?>" class="btn"> Cancel </a>
	  	
				</form>
		</div>

	</div><!--/row-->
</div><!--/container-->

		

