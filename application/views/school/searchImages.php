<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">
				Search Images
			</h1>
		</div>
		<div class="col-md-6">

			<form role="form" class="search_form has-feedback" action="" method="GET">
				  <div class="form-group">
				  	<input type="text" class="form-control input-lg" id="search2" placeholder="Enter school name" autocomplete="on">
					
				  </div>
				   <a href=" <?= site_url("admin/school")  ?>" class="btn btn-default"> Back </a>
			</form>
		</div>
	</div><!--/row-->
</div><!--/container-->
<script>
		
	$(function() {
		$("#search2").autocomplete({

	      source: "<?=site_url('search');?>",
	      minLength: 1,
	      select: function( event, ui ) {
	      	window.location = '<?= site_url();?>admin/school/manageImages/'+ ui.item.id;
	      }
	    });	

  	});

</script>
