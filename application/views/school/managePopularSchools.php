<?php
	
	$school1 	= '';
	$school2 	= '';
	$school3 	= '';
	$school1Id  = '';
	$school2Id  = '';
	$school3Id  = '';
	$photo1  	= "";
	$photo2  	= "";
	$photo3  	= "";

	if(isset($data))
	{
		$school1 	= $data[0]['INSTNM'];
		$school2 	= $data[1]['INSTNM'];
		$school3 	= $data[2]['INSTNM'];
		$school1Id  = $data[0]['UNITID'];
		$school2Id  = $data[1]['UNITID'];
		$school3Id  = $data[2]['UNITID'];

		if(isset($data[0]['photo']) && $data[0]['photo'] != "" && $data[0]['photo'] != null)
		{
			$photo1 = $data[0]['photo'];
		}
		if(isset($data[1]['photo']) && $data[1]['photo'] != "" && $data[1]['photo'] != null)
		{
			$photo2 = $data[1]['photo'];
		}
		if(isset($data[2]['photo']) && $data[2]['photo'] != "" && $data[2]['photo'] != null)
		{
			$photo3 = $data[2]['photo'];
		}

	}
	
?>

<style>
	.popular-image img{
		height: 24px;
		width : 24px;
	}
</style>

<div class="container">
	<div class="row">

		<div class="col-md-12">

			<h1 class="page-header">Manage Popular schools</h1>

		</div>

		<form role="form" class="search_form has-feedback" action="" method="GET">

			<div class="col-md-12">
				<div class="col-md-6">
					<div class="form-group">
						<input type="text" class="form-control input-lg" data-image="popular-image1"  id="search2" placeholder="Enter school name to add" autocomplete="on" data-id="<?= $school1Id  ?>" value="<?= $school1 ?>">
					</div>
				</div>
				<div class="col-md-6">
					<div id="popular-image1"> 
						<?php
							if($photo1 != "")
							{?>
								<img src="<?= base_url() ?>/<?=$photo1 ?>" alt="Smiley face" height="42" width="42">
							<?
							}
						?>
				   	</div>
				</div>
			</div>	
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="form-group">
						<input type="text" class="form-control input-lg" data-image="popular-image2"  id="search3" placeholder="Enter school name" autocomplete="on" data-id="<?= $school2Id  ?>" value="<?= $school2 ?>">
					</div>
				</div>
				<div class="col-md-6">
					<div id="popular-image2"> 
						<?php
							if($photo2 != "")
							{?>
							 <img src="<?= base_url() ?>/<?=$photo2 ?>" alt="Smiley face" height="42" width="42">
							<?
							}
						?>
				   	</div>
				</div>
			</div>	
			<div class="col-md-12">
				<div class="col-md-6">
					  <div class="form-group">
					  	<input type="text" class="form-control input-lg" data-image="popular-image3" id="search4" placeholder="Enter school name" autocomplete="on" data-id="<?= $school3Id  ?>" value="<?= $school3 ?>">
					  </div>
				</div>

				<div class="col-md-6">
					<div id="popular-image3">
						<?php
							if($photo3 != "")
							{?>
							 <img src="<?= base_url() ?>/<?=$photo3 ?>" alt="Smiley face" height="42" width="42">
							<?
							}
						?>
				   	</div>
				</div>
			</div>	
			<div class="col-md-12">
				<div class="col-md-6">
					 <div class="form-group">
						<button type="button" class="btn btn-default" id="form-submit">Submit</button>
						<a href="<?= site_url("admin/school")  ?>" class="btn btn-link"> Cancel </a>
					</div>

				</div>
			</div>

		</form>
		
			
		</div>
	</div><!--/row-->
</div><!--/container-->


<script>
		
	$(function() {

		$("#search2,#search3,#search4").autocomplete({

	      source: "<?= site_url('search');?>",
	      minLength: 1,
	      select: function( event, ui ) {

	      	$(this).attr("data-id", ui.item.id);

	      	var imgCont  = 	$(this).attr("data-image");

	      	if(ui.item.photo == null || ui.item.photo == "")
	      	{
	      		 	$('#'+imgCont).html("");
	      	}
	      	else{
		      	var html = '<img src="'+'<?= base_url() ?>'+ui.item.photo +'" alt="Smiley face" height="42" width="42">';

		      	$('#'+imgCont).html(html);
	      	}

	      	//window.location = '<?= site_url();?>admin/school/manageImages/'+ ui.item.id;
	      }
	    });	


		$("#form-submit").on("click",function(){

			// here make the ajax call to save the schools

			//console.log($("#search2").attr('data-id'));
			//console.log($("#search3").attr('data-id'))
			//console.log($("#search4").attr('data-id'))

			var school1 = $("#search2").attr('data-id');
			var school2 = $("#search3").attr('data-id');
			var school3  = $("#search4").attr('data-id');

			if(school1 == "" || school1 == null || school2 == "" || school2 == null || school3 == "" || school3 == null)
			{


				alert("Please select schools for all the three fields !!!");
			}
			else{


			
	            $.ajax({
	                        url: '<?= site_url("admin/school/savePopularSchools") ?>',
	                        data: {school1:school1,school2:school2,school3:school3},
	                        type: 'POST',
	                        dataType: 'json',
	                        success:function(data)
	                        {
	                       		if(data.status== "success")
	                       		{
	                       			alert("Success");
	                       		}
	                       		else{
	                       			alert("Error");
	                       		}

	                       	}
	                     });

            }
		})


  	});// end of document ready




</script>
