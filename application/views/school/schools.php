<?php
	
	if(!isset($data))
	{
		$data 	=	array();
	}

?>

<div class="container">

	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">
				Schools
			</h1>
		</div>

		<div class="col-md-9">

			<?php
				$this->load->view("/admin/filter2");
			?>
		</div>
		<div class="col-md-3 pull-right">
			<a href="<?= site_url("admin/school/searchImages") ;?>" class="btn btn-link pull-right">Manage Images</a>
		</div>
		<div class="col-md-12">

			<?php

				if(isset($data) && count($data) > 0)
				{?>
					<div style="clear:both;" class="table-responsive custom-responsive-table">
						
							<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">
									<tr>
										<th>ID</th>
										<th>Name</th>
										
										
										<th>Action</th>
									</tr>
									<?php
									foreach($data as $key=>$value)
									{
									?>
									<tr>
										<td><div class="responsive-table-th"><label>ID</label></div> 
											<div class="responsive-table-td">
											<?=$value['UNITID']?>
											</div> 
										</td>
										<td><div class="responsive-table-th"><label>Name</label></div> 
											<div class="responsive-table-td">
											<?=$value['INSTNM']?>
											</div>
										</td>
										
										<td><div class="responsive-table-th"><label>Action</label></div> 
											<div class="responsive-table-td">
											 <a href="<? echo site_url('admin/school/manageImages') ?>/<?=$value['UNITID']?>" class="btn btn-link">Manage Images </a>
											</div> 
										</th>
									</tr>
								<?
									}
								?>
							</table>
					</div> <!-- table Wrapper -->

						<?php
							if(isset($pagination_str))
							{
								echo $pagination_str;
							}

				}
				else
				{?>
					<!-- <h3>No data found</h3> -->
					<div class="form-wrapper no-data">

						<div class="icon"></div>
					
						<div class="msg"> No Schools Present.</div>

					</div>

				<?php
				}
			?>

		</div>
	</div><!--/row-->
</div><!--/container-->
