<?php
	
	$noData 	=	1;

	if((isset($imageArr) && is_array($imageArr) && count($imageArr) > 0) || (isset($userImageArr) && is_array($userImageArr) && count($userImageArr) > 0))
	{
		$noData =	0;
	}

?>

<div class="container">
	<div class="row">

		<h1 class="page-header">
				<a data-id="<?=$id?>" class="btn btn-info pull-right fetchImages">Pull images again </a>
			<?= $name ?> </h1>

		<div class="col-md-12">
			
			<div class="row">

				<div class="col-md-12">
					
				
						<?php
						//print_r($imageArr);
						if($noData == 0)
						{
							if(isset($userImageArr) && is_array($userImageArr) && count($userImageArr) > 0)
							{
								
								?>
								

								<form class="form" role="form" action="<?= site_url("admin/school/saveImages/".$id) ?>" method="POST">
									<div class="row" id="mainImageContainer">


										

									
										<?php
											$temp ="";
											foreach ($userImageArr as $key => $value)
											{

												if(!in_array($value, $imageArr))
												{
												?>

													<div class="form-group col-md-2 img-selector">
														<label>
														<?php 
															
															if($value !=NULL ||$value != '' )
															{	
																$temp = "temp";
																?>
														    
														    
				 												


															
																	<div class="thumbnail">
																		<div class="img" data-image="" style="background: url('<?=locationPicsBaseUrl().$value?>') no-repeat center center transparent;">
																			
																		</div>
																		
																			

																		
																	</div>
																	<div class="checkbox">
																	   
																	      <input type="checkbox" name="url[]" value="<?= $value?>">
																	    
																	 </div>
																
														    <?php 
															}	
															
														?>
														</label>
									  				</div>
								  				
												<?php
												}
											}
										?>


									</div>
								<?php
								if($temp != "")
								{
									?>
									<hr/>
									
									<div class="form-group ">
					  					 <button type="submit" class="btn btn-success">Save Selected</button>
					  					 <a href=" <?= site_url("admin/school/searchImages/".$id)  ?>" class="btn btn-link"> Back </a>
					  				</div>

					  			<?php
					  			}
					  			else
					  			{?>

			  					<div class="form-wrapper no-data text-center">

									<div class="icon glyphicon glyphicon-exclamation-sign"></div>
								
									<div class="msg">No Images found</div>

									<a href=" <?= site_url("admin/school/searchImages/".$id)  ?>" class="btn btn-default"> Back </a>

								</div>



					  				 
					  			<?php
					  			}
					  			?>
				  				</form>
								<?php
							}
							else
							{
							?>
								<div class="form-wrapper no-data text-center">

									<div class="icon glyphicon glyphicon-exclamation-sign"></div>
								
									<div class="msg">No Images found</div>

									<a href=" <?= site_url("admin/school/searchImages/".$id)  ?>" class="btn btn-default"> Back </a>

								</div>
							<?php
							}
						}
						else
						{
						?>
							<div class="form-wrapper no-data text-center">

									<div class="icon glyphicon glyphicon-exclamation-sign"></div>
								
									<div class="msg">No Images found</div>

									<a href=" <?= site_url("admin/school/searchImages/".$id)  ?>" class="btn btn-default"> Back </a>

								</div>
						<?
						}
						?>
					
				</div>

			</div>

			<div class="row">

				<h1 class="page-header">
					Selected Images
				</h1>

				<div class="col-md-12">

					<?php
					if(isset($imageArr) && is_array($imageArr) && count($imageArr) > 0)
					{
								
								?>
								

								
									<div class="row">


										

									
										<?php
											$temp ="";
											foreach ($imageArr as $key => $value) {?>

												<div class="form-group col-md-2 img-selector">
													<label>
													<?php 
														
														if($value !=NULL ||$value != '' )
														{	
															$temp = "temp";
															?>
													    
													    
			 												


														
																<div class="thumbnail">
																	
																	<div class="img" data-image="" style="background: url('<?=locationPicsBaseUrl().$value?>') no-repeat center center transparent;">
																		
																	</div>																	
																</div>

																<a class="btn-link deleteImage" data-id="<?=$id?>" data-url="<?=$value?>">Delete Image</a>
															
													    <?php 
														}	
														
													?>
												</label>
								  				</div>
								  				
											<?php
											}
										?>


									</div>
								<?php
								if($temp != "")
								{
									?>
									<hr/>
									
					  			<?php
					  			}
					  			else
					  			{?>

					  			<div class="form-wrapper no-data text-center">

									<div class="icon glyphicon glyphicon-exclamation-sign"></div>
								
									<div class="msg">No Images found</div>

									<a href=" <?= site_url("admin/school/searchImages/".$id)  ?>" class="btn btn-default"> Back </a>

								</div>



					  				
					  			<?php
					  			}
							}
							else
							{
							?>

								<div class="form-wrapper no-data text-center">

									<div class="icon glyphicon glyphicon-exclamation-sign"></div>
								
									<div class="msg">No Images found</div>

									<a href=" <?= site_url("admin/school/searchImages/".$id)  ?>" class="btn btn-default"> Back </a>

								</div>


								
							<?php
							}
							?>

				</div>

			</div>

		</div>

	</div><!--/row-->
</div><!--/container-->

<script type="text/javascript">
	
	var locationBaseUrl 	=	"<?=base_url()?>";

	console.log(locationBaseUrl);

 	$(document).ready(function(){

 		$(document).on('click', '.deleteImage', function(event){

 			var selector 	=	$(this);

 			var schoolId 	=	$(selector).attr('data-id');

 			var url 		=	$(selector).attr('data-url');

 			if(confirm('Aru you sure you want to delete this image?'))
 			{
 				$.ajax({
                        url:"<?=site_url()?>"+'admin/school/deleteImage/'+ schoolId,
                        method:'POST',
                        data: {url:url},
                        success:function(response)
                        {
                            var result = JSON.parse(response);

                            if(result.status != "success")
                            {
                                alert('Something Went wrong!!!');
                            }
                            else
                            {
								location.reload();
                            }
                            
                        },
                        error:function()
                        {
                            console.log("The ajax request failed");
                        }
                   
                });
 				
 			}

 		});

		$(document).on('click', '.fetchImages', function(event){

 			var selector 	=	$(this);

 			var schoolId 	=	$(selector).attr('data-id');

 			$('.fetchImages').html('Processing...')

 			$('#mainImageContainer .dynamicallyAdded').remove();

 			$.ajax({
                    url:"<?=site_url()?>"+'admin/school/fetchImages/'+ schoolId,
                    method:'GET',
                    data: {},
                    success:function(response)
                    {
                        var result = JSON.parse(response);

                        if(typeof result.imageArr != 'undefined')
                        {

                        	var str 	=	'';

                        	$.each(result.imageArr, function(key, value){

                        		str 	+=	'<div class="form-group col-md-2 img-selector dynamicallyAdded">'+
												'<label>'+
													'<div class="thumbnail">'+
														'<div class="img" data-image="" style="background: url('+ locationBaseUrl+ value +') no-repeat center center transparent;">'+
																			
														'</div>'+
													'</div>'+

													'<div class="checkbox">'+
													   
													      '<input type="checkbox" name="url[]" value="'+ value +'">'+
													    
													'</div>'+

												'</label>'+
											'</div>';
                        	});

                        	$('#mainImageContainer').prepend(str);

                        }

                        $('.fetchImages').html('Pull images again ');
                        
                    },
                    error:function()
                    {
                    	$('.fetchImages').html('Pull images again ');
                        console.log("The ajax request failed");
                    }
               
            });

 		});

 	})

</script>