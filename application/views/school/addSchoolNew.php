<?php
	
	$this->load->config('schoolForm');

	$schoolVariables 	=	$this->config->item('addSchool');

	if(!isset($states))
	{
		$states 	=	array();
	}
	$temp_states 	=	array();
	foreach($states as $key=>$val)
	{
		$temp_states[$val['code']] 	=	$val['name'];
	}

	$states 	=	$temp_states;

?>

<div class="container">
	<div class="row">
		<div class="col-md-6">
			<form role="form"  action="<?php site_url("admin/school/add") ;?>">

				<?php
					foreach($schoolVariables as $key=>$value)
					{
				?>
						<div class="form-group">
							<label for="<?= $value['pName']?>"><?= $value['pLabel']?></label>

					<?php
						if(in_array($value['pType'], array('number','alpha')))
						{
					?>
							<input type="text" name="<?= $value['pName']?>" class="form-control input-lg" id="<?= $value['pName']?>" placeholder="<?= $value['desc']?>">
					<?php
						}
						else if(in_array($value['pType'], array('textarea')))
						{
					?>
							<textarea class="form-control input-lg" rows="3" name="<?= $value['pName']?>"  id="<?= $value['pName']?>" placeholder="<?= $value['desc']?>"></textarea>
					<?php
						}
						else if(in_array($value['pType'], array('select')))
						{
							if(!isset($value['pSubType']))
							{
								$value['pSubType'] 	=	'single';
							}

							if(!isset($value['isDefault']))
							{
								$value['isDefault'] 	=	'';
							}

							if(!isset($value['aValues']))
							{
								$value['aValues'] 	=	array();
							}

							if($value['pName'] == 'STABBR')
							{
								$value['aValues'] 	=	$states;
							}


					?>
							<select class="form-control input-lg" name="<?= $value['pName']?>" id="<?= $value['pName']?>">
					<?php
							if($value['isDefault'] != '')
							{
					?>
								<option selected="selected" value="0" disabled="disabled"><?=$value['isDefault']?></option>
					<?php
							}

							if(count($value['aValues']) > 0)
							{
								foreach($value['aValues'] as $selKey=>$selVal)
								{
					?>
									<option value="<?=$selKey?>"><?=$selVal?></option>
					<?php
								}
							}
					?>

							</select>

					<?php

						}
					?>



						</div>
				<?php
					}
				?>

				<button type="submit" class="btn btn-success">Submit</button>
	  			<a href="<?= site_url("/admin/school") ?>" class="btn"> Cancel </a>

			</form>
		</div>
	</div>
</div>

<script>
	
	$(document).ready(function(e){


		$('#STABBR').unbind('change');
		$('#STABBR').change(function(event){
			
			if($('#STABBR').val() != '0')
			{
				$('#CITY').html('<option selected="selected" disabled="disabled" value="0">Loading...</option>');

				$.ajax(
					{
						type: "GET",
						url: "<?= site_url('admin/school/getCities') ?>",
						data: { stateCode: $('#STABBR').val()}
					}
				).done(function(data) {

					data 	=	JSON.parse(data);
					
					if(data.status == 'failure')
					{
						alert(data.message);
					}
					else
					{
						var optionsStr 	=	'';
						$.each(data.data, function(index, value){
							optionsStr	+=	'<option value="'+ value.city +'('+ value.zip +')" city="'+ value.city +'" lt="'+ value.latitude +'" lg="'+ value.longitude +'" zip="'+ value.zip +'">'+ value.city +'  ('+ value.zip +')</option>';
						});
						$('#CITY').html(optionsStr);
						
						$("#CITY option:first").attr('selected','selected');

						$('#CITY').trigger('change');
			    	}
			  	});

			}

		});

		$('#CITY').unbind('change');
		$('#CITY').change( function(event){

			if($('#CITY').val() != '')
			{
				var selector 	=	$('#CITY option[value="'+ $('#CITY').val() +'"]');
				
				if(selector.length > 0)
				{
					$('#LATITUDE').val('');
					$('#LONGITUD').val('');
					$('#ZIP').val('');
					$('#LATITUDE').val(selector.attr('lt'));
					$('#LONGITUD').val(selector.attr('lg'));
					$('#ZIP').val(selector.attr('zip'));
				}
			}

		})


	})

</script>