<div class="container">
	<div class="row">
		
		<div class="col-md-8">
			<h3>Articles</h3>
			<ul>
				<?php
					if($data['count'] > 0)
					{
						foreach ($data['data'] as $key => $value) 
						{?>
							<li>
								<h2> <?= $value ['title'] ;?> </h2>
								<div class="content">
									<?= $value ['content'] ;?> 
								</div>
							</li>
						<?php	
						}

					}
				?>

			</ul>
		</div>
		<div class="col-md-4">
			<h3>Categories</h3>
		  	<ul>
				<li> News </li>
				<li> Technology </li>
				
			</ul>

		</div>

</div><!--/row-->
</div><!--/container-->
