<?php

  $category_ids = array();
  if(isset($is_create))
  {
  	if($is_create == 1)
  	{
  		$is_published = $this->input->post('is_published');
  		$is_favorite  = $this->input->post('is_favorite');
  		$action    	  = site_url()."blog/admin/add_article";
  		$title   	  = $this->input->post('title');
  		$content 	  = $this->input->post('content');
  		$user_id 	  = $this->input->post('user_id');

  		if($has_category == "yes")
  		{
  			//$category_id  =  $this->input->post('category_id');

  			$category_ids  = $this->input->post('category_id');
  		}

  	}
  	
  }
  else if(isset($is_edit))
  {
  	if($is_edit == 1 && $is_post != 1)
  	{
  		$is_published = $data['is_published'];
  		$is_favorite  = $data['is_favorite'];
  		$title   	  = $data['title'];
  		$content 	  = $data['content'];
  		$user_id	  = $data['user_id'];

  		if($has_category == "yes")
  		{
  			$category_ids  =  array_filter(explode('|',$data['category_id'])); 
  		}

  		

  		$action       = site_url()."blog/admin/update_article/".$id."/1";


  	}
  	if($is_edit == 1 && $is_post == 1)
  	{
  		$is_published =  $this->input->post('is_published');
  		$is_favorite  =  $this->input->post('is_favorite');
  		$title   	  =  $this->input->post('title');
  		$content 	  =  $this->input->post('content');
  		$user_id 	  = $this->input->post('user_id');

  		if($has_category == "yes")
  		{
  			$category_ids  =  $this->input->post('category_id');;
  		}

  		$action       = site_url()."blog/admin/update_article/".$id."/1";


  	}
  }
?>

<div class="container">
	<div class="row">

	<h3> Create Article</h3>
	<form action="<?php echo $action ?>" method="POST" class="form" role="form" >

		<div class="form-group">
			    <label for="title">Title <span style="color:red;">*</span></label>
				<input class="form-control" type="text" name="title" id="title" placeholder="Article Title" value="<?php echo $title ?>"/>
				<?php echo form_error('title'); ?>
		</div>
		<div class="form-group">
		<?php
			if(count($users) > 0)
			{?>

				<label for="user_id">User <span style="color:red;">*</span> </label>
				<select name="user_id" class="form-control" id="user_id" style="max-width:300px;">
				<?php
					foreach($users as $key2=>$row2)
					{
						if($user_id == $row2->id)
						{?>
								
								<option value="<?= $row2->id ?>" selected><?= $row2->username ?></option>
						<?php
						}
						else
						{?>
							<option value="<?= $row2->id ?>"><?= $row2->username ?></option>

						<?php
						}
						?>	
						
					<?php
					}
				?>

				</select>
				<?php echo form_error('user_id'); ?>
				
			<?
			}
		?>


		</div>
		<?php
			if($has_category == "yes")
			{
			?>
				<div class="form-group">
					<label for="category">Category</label>

				    <?php

					if(count($categories) > 0)
					{
						
						?>
						<select name="category_id[]" class="form-control" id="category_id" style="max-width:300px;" multiple>

							<?php

								foreach($categories as $key=>$row)
								{
									//if(isset($is_edit) || )
									{
										//if($row['id'] == $category_id)

										if(in_array($row['id'],$category_ids))
										
										{?>
											<option value="<?= $row['id'] ?>" data="<?=$row['id'] ?>" selected><?= $row['name']?></option>

										<?php
										}
										else
										{?>
											<option value="<?= $row['id'] ?>"><?= $row['name']?></option>
										<?php
										}
									}
									
								}
							?>
				
						</select>
					<?php
					}
					else{?>
						<select name="category_id" class="form-control" id="category_id" style="max-width:300px;">
								<option value="no_category">No category to Select</option>
						</select>
					<?php
					}	
					?>
					<?php echo form_error('category_id'); ?>
					
				</div>

			<?php
			}
		?>

		<div class="form-group">
				<label >Is Published</label>

				<?php
					if($is_published == 0)
					{?>
						<input type="checkbox" name="is_published" value="1">
					<?php
					}
					else
					{?>
						<input type="checkbox" name="is_published" value="1" checked>
					<?php
					}
				?>
				
				
		</div>
		<div class="form-group">
				<label >Is FAQ</label>

				<?php
					if($is_favorite	 == 0)
					{?>
						<input type="checkbox" name="is_favorite" value="1">
					<?php
					}
					else
					{?>
						<input type="checkbox" name="is_favorite" value="1" checked>
					<?php
					}
				?>
		</div>

		<div class="form-group">

			<label for="content">Content <span style="color:red;">*</span> </label>
		
			<textarea  name="content" class="form-control pagedown" id="content" rows="10" ><?php echo $content ?></textarea>
			<?php echo form_error('content'); ?>
		</div>

		<div class="form-group">
			<input type="submit" id="form_submit" class="btn btn-success" value="SUBMIT"/>
			
			<a href="<?php echo site_url() ?>blog/admin" class="btn"> CANCEL </a>
		</div>


	</form>

	
	</div><!--/row-->
</div><!--/container-->