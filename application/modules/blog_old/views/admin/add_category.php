<?php
		
if(isset($is_edit))
{
	$category_name  = $data['name'];
 	$is_active 	 	= $data['is_active'];
 	$action         =  site_url()."blog/admin/update_category/".$id;

}
else
{
	$category_name  = $this->input->post('category_name');
 	$is_active 	 	= $this->input->post('is_active');
 	$action         =  site_url()."blog/admin/add_category";

}

 
	
?>

<div class="container">
	<div class="row">

	<h3> Create Category</h3>
	<form name="create_category" action="<?= $action  ?>" method="POST" class="form" role="form">

		
		<div class="form-group">
			<label for="name">Category Name: </label>
			<input class="form-control" type="text" name="category_name" id="category_name" placeholder="Category Name" value="<?= $category_name; ?>">
			<?php echo form_error('category_name'); ?>
			
		</div>
		<div class="form-group">
				<label >Is Active</label>

				<?php
					if($is_active	== 2)
					{?>
						<input type="checkbox" name="is_active" value="1">
					<?php
					}
					else
					{?>
						<input type="checkbox" name="is_active" value="1" checked>
					<?php
					}
				?>
		</div>
		<div class="form-group submit_group">
			<input type="submit" id="add_category" class="btn btn-success" value="Add Category">
			<a href="<?php echo site_url() ?>blog/admin" class="btn"> CANCEL </a>
		</div>
	</form>

</div><!--/row-->
</div><!--/container-->
