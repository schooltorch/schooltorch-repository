<?php
	
	$article_col_width = 12;
	if($has_category == "yes")
	{
		$article_col_width = 8;
		$cat_col_width 	   = 4;
	}
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>Blog </h2>
		</div>
		
	</div>
	<div class="row">
		
		<div class="col-md-<?= $article_col_width ?>">
			<h3>Articles <?php  echo anchor(site_url()."blog/admin/add_article","Add Article",array('style'=>'float:right','class'=>'btn btn-success')) ?></h3>
			<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">
				<tr>
					<th>#</th>
					<th>Title</th>
					<th>User</th>
					<th>Published</th>
					<th>Favorite</th>
					<?php 
						if($has_category == "yes")
						{?>
							<th>Category</th>
						<?php
						}
					?>
					<th>Action</th>
				</tr>

				<?php
					if(sizeof($articles) > 0)
					{
						foreach($articles as $key=>$row)
						{
							$is_published = "No";
							$is_favorite  = "No";
							if($row['is_favorite'] == 1)
							{
								$is_favorite = "Yes";
							}

							if($row['is_published'] == 1)
							{
								$is_published = "Yes";
							}

							?>		
							<tr>
								<td><?=$key;?></td>
								<td><?=$row['title'];?></td>
								<td>
									<?php
										if(count($users) > 0)
										{

											if(isset($users[$row['user_id']]))
											{

												$user = $users[$row['user_id']]->username;

												
											}
											else
											{
												$user = "--";	
											}
										}
										else
										{
											$user = "--";	
										}

									?>
									<?= $user  ?>
 
								</td>
								<td><?=$is_published;?></td>
								<td><?=$is_favorite;?></td>
								
								<?php 
									if($has_category == "yes")
									{	

										//echo $row['category_id'];
										$temp_cat_id = array_filter(explode("|",$row['category_id']));

										$category = "";

										//print_r($temp_cat_id);

										if(count($temp_cat_id) > 0)
										{

											foreach ($temp_cat_id as  $value2) 
											{
												if(isset($categories[$value2]['name']))
												{
													$category.= $categories[$value2]['name'] . ",";
												}
												
											}

											if($category == "")
											{
												$category = "--";
											}

											
										}
										else
										{
											$category  = "--";
										}
										?>

										<td> <?= trim($category,",");?></td>

									<?
									}
								?>
								<td>
								  <?php echo anchor("blog/admin/update_article/".$key,"Edit") ?>|
								  <?php echo anchor("blog/admin/delete_article/".$key,"Delete") ?>
								 </td>
							</tr>
						<?php
						}
					}
					else
					{?>
				
						<tr>
							<td colspan="6"> No Articles Present</td>
						</tr>
					<?php
					}
				?>
			</table>

		</div>
		<?php

			if($has_category == "yes")
			{?>

			<div class="col-md-<?= $cat_col_width ?>">
				<h3>Categories  <?php  echo anchor(site_url()."blog/admin/add_category","Add Category",array('style'=>'float:right','class'=>'btn btn-success')) ?> </h3> </h3> 
				<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped">
				<?php
					if(sizeof($categories) > 0)
					{
						?>		
						<tr>
							<th><?php  echo anchor("/blog/admin/index","#")  ?></th>
							<th>Name</th>
							<th>Is Active</th>

							<th>Action</th>
						</tr>
						<?php
							foreach($categories as $key=>$row)
							{
								$is_active = "No";
								if($row['is_active'] == 1)
								{
									$is_active = "Yes";
								} 
								?>
								<tr>
									<td><?php echo anchor("/blog/admin/index/".$row['id'],$row['id']); ?> </td>
									<td><?php echo $row['name'] ;?> </td>
									<td><?=  $is_active ?> </td>
									<td>
										<div class="action">
											<?php echo  anchor("/blog/admin/update_category/".$row['id'],"Edit ")   ?> |
											<?php echo  anchor("/blog/admin/delete_category/".$row['id'],"Delete")   ?>
										</div>
									</td>
									
								</tr>
							<?php
							}
						?>

						<?php
					}
					else
					{?>
				
						<tr>
							<td colspan="3"> No Categories Present</td>
						</tr>
					<?php
					}
				?>
				</table>
			</div>
			<?php
			}
		?>

	</div><!--/row-->
</div><!--/container-->






