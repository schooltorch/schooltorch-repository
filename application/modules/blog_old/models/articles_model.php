<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Articles_model extends CI_Model{


	function getArticles()
	{

		$returnArray  = array(
								'status' => "success",
								'count'	 => 0,
								'data'   => array(),
							 );
		

		$articles = $this->db->select('content,title')
							->from('blog-articles')
								->where('is_deleted',NULL)
									->order_by('id','desc')
										->get()
											->result_array();

		if(count($articles)  == 0)
		{
			return $returnArray;

		}
		else
		{
			$articles_info = array();
			foreach ($articles as $key => $value) 
			{

				$articles_info[] = $value;
 				
			}

			return array(
							"status" => "success",
							"count"  => count($articles_info),
							"data"	 => $articles_info,
						);

		}


	}


	function getArticle($id)
	{
		/*
		$returnArray  = array(
								'status' => "success",
								'count'	 => 0,
								'data'   => array(),
							 );

		$articles = $this->db->select('*')
							->from('blog-articles')
								->where('is_deleted',NULL)
									->order_by('id','desc')
										->get()
											->result_array();*/


	}


	function categories()
	{

		$categories = array();

		$categories = $this->db->select('*')
							->from('blog-articles')
								->where('is_deleted',NULL)
									->order_by('id','desc')
										->get()
											->result_array();

	}
}
?>