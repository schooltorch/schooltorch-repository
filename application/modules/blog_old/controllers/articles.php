<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends CI_Controller {


	function __construct()
	{
		parent::__construct();

		$this->load->model("articles_model");
	}

	function index($id = 0)
	{
		$this->blog($id);
	}

	// Function to call with controller and the argument
	function _remap() {

        $id = $this->uri->segment(3);
        $this->index($id );

    }

	function blog($id = 0)
	{
		
		$data  = array();
		if($id == 0)
		{
			$data['data']  = $this->articles_model->getArticles();

			/*echo "<pre>";

			print_r($data);
			echo "</pre>";*/

			$this->load->view('common/inc_header',$data);	
			$this->load->view('/blog/blogs',$data);
			$this->load->view('common/inc_footer',$data);

		}
		else
		{
			$this->load->view('common/inc_header',$data);	
			$this->load->view('/blog/article',$data);
			$this->load->view('common/inc_footer',$data);

		}
	}

}
?>
