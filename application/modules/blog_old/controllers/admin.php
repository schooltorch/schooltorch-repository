<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	private $has_category = null ;//$this->config->item('has_category');

	function __construct()
	{
		parent::__construct();

		//$this->load->helper('gateway');

		$this->load->model('admin_model');
		$this->config->load('config');
		$this->has_category  = $this->config->item('has_category');


	}
	
	
	
	function index($id=0)
	{	
		$data['active_menu']	=	'blog';
		$data['subMenuActive']	=	'';

		$data['has_category']   = $this->has_category;

		if($this->has_category == "yes")
		{
			$data['categories'] = $this->admin_model->get_categories();
		}

		$data['users'] = $this->admin_model->get_users();
		

		$data['articles'] 	= $this->admin_model->get_articles($id);

		$this->load->view('common/inc_header',$data);	
		$this->load->view('admin/blog',$data);
		$this->load->view('common/inc_footer',$data);

	}



	function add_category()
	{	
		$data = array();

		$this->form_validation->set_rules('category_name', 'Category name', 'required');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red ;">', '</div>');

		

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('common/inc_header',$data);
			$this->load->view('admin/add_category',$data);
			$this->load->view('common/inc_footer',$data);
		}
		else
		{
			$this->admin_model->add_category($data);
			redirect('/blog/admin');
			
		}

	}

	function update_category($id = 0)
	{	
		$data = array();

		$this->form_validation->set_rules('category_name', 'Category name', 'required');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red ;">', '</div>');

		
		$data['id'] 	 = $id;
		$data['is_edit'] = 1;

		if ($this->form_validation->run() == FALSE)
		{

			$data['data'] = $this->admin_model->get_category($data);


			$this->load->view('common/inc_header',$data);
			$this->load->view('admin/add_category',$data);
			$this->load->view('common/inc_footer',$data);
		}
		else
		{

			//print_r($_POST);

			if($id != 0)
			{
				$this->admin_model->update_category($data);
			}else
			{
				echo "here";
			}
		
			redirect('/blog/admin');
			
		}

	}


	function delete_category($category_id){

		
		if($category_id != null && $category_id != ""){
			$result = $this->admin_model->delete_category($category_id);

		}
		redirect('/blog/admin');
	}


	function add_article(){
		
		$data = array();
		$data['active_menu']	=	'blog';
		$data['subMenuActive']	=	'';
		$data['is_create']		=   1;
		$data['has_category']   =   $this->has_category;

		if($this->has_category == "yes")
		{
			$data['categories'] = $this->admin_model->get_categories();
			$this->form_validation->set_rules('category_id', 'Category Id', 'required');

			//dump($data['categories']);
		}

		$data['users']  = $this->admin_model->get_users();

		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('content', 'content', 'required');
		$this->form_validation->set_rules('user_id', 'user_id', 'required');

		$this->form_validation->set_error_delimiters('<div class="error" style="color:red ;">', '</div>');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('common/inc_header',$data);
			$this->load->view('admin/add_article',$data);
			$this->load->view('common/inc_footer',$data);
		}
		else
		{
			
			/*echo  '<pre>';
			print_r($_POST);
			echo '</pre>';*/
			$this->admin_model->create_article($data);
			redirect('/blog/admin');
			
		}
	}


	function update_article($id = 0 ,$post = 0){

		$data['id'] = $id;
		$data['is_edit'] = 1;
		$data['has_category']	= $this->has_category;
		$data['is_post']		= $post;


		if($this->has_category == "yes")
		{
			$data['categories'] = $this->admin_model->get_categories();	
		}

		$data['users']  = $this->admin_model->get_users();

		
		if($id != 0 && $post == 0)
		{

			$temp = $this->admin_model->read_article($id);

			if($temp['status'] == "success")
			{
				$data['data']  = $temp['data'];
			}
			else
			{

				redirect("/blog/admin/update_article/".$id."/");

			}
		}
		
		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('content', 'content', 'required');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red ;">', '</div>');
	
				
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('common/inc_header',$data);
			$this->load->view('admin/add_article',$data);
			$this->load->view('common/inc_footer',$data);
		}
		else
		{
		
			$this->admin_model->update_article($data);

			redirect("/blog/admin/update_article/".$id);
			
		}
	}

	function delete_article($id = 0 ){
		
		$this->admin_model->delete_article($id);

		redirect("/blog/admin");
	}

   
}

/* End of file admin.php */
/* Location: ./application/modules/blog/controllers/admin.php */