<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['siteName']						=	"SchoolTorch";
$config['titleSeparator'] 				=   " | ";
$config['siteEmail']					=   "neil@gdiz.com";
$config['dateFormat']					=    "Y/m/d";
$config['timeFormat']					=    "H:i a";

$config['has_category']   				= 	"yes";   // used for blog

$config['previousPostsYears']   		= 	1;   // used for blog