<?php
		$config['csvImportData'] = 	array(
									'schools' 						=> 	'Institution Directory (hd2013)',
									'schoolCharacteristics' 		=>	'Institution Characteristics (ic2013)',
									'schoolPrograms' 				=> 	'Programs Offered via Distance (c2013dep)',
									'schoolHeadCount' 				=> 	'Head Count (effy2013)',
									'schoolCompletions' 			=> 	'Completions (c2013_a)',
									/*'schoolCompleters' 				=> 	'Completers (c_b)',
									'schoolCompletersAwardRace' 	=> 	'Completers by Award, Race (c_c)',
									'schoolInstructionalActivity'	=>	'Instructional Activity Data (efia)',
									'schoolStudentChargesProgram' 	=> 	'Student Charges by Program (ic_py)',*/
									'schoolStudentChargesYear' 		=> 	'Student Charges for Full Academic Year (ic2013_ay)'
								);

	
	$config['csvImportTableNames'] = 	array(
											'schools', 
											'schoolCharacteristics',
											'schoolHeadCount',
											'schoolPrograms',
											'schoolCompletions',
											'schoolCompleters',
											'schoolCompletersAwardRace',
											'schoolInstructionalActivity',
											'schoolStudentChargesProgram',
											'schoolStudentChargesYear',


											'schoolFrequencies:schools', 
											'schoolFrequencies:schoolCharacteristics', 
											'schoolFrequencies:schoolHeadCount', 
											'schoolFrequencies:schoolPrograms', 
											'schoolFrequencies:schoolCompletions', 
											'schoolFrequencies:schoolCompletersAwardRace',
											'schoolFrequencies:schoolInstructionalActivity',
											'schoolFrequencies:schoolStudentChargesProgram',
											
											
											'schoolVarlist:schools',
											'schoolVarlist:schoolCharacteristics',
											'schoolVarlist:schoolHeadCount',
											'schoolVarlist:schoolPrograms', 
											'schoolVarlist:schoolCompletions', 
											'schoolVarlist:schoolCompleters', 
											'schoolVarlist:schoolCompletersAwardRace', 
											'schoolVarlist:schoolInstructionalActivity',
											'schoolVarlist:schoolStudentChargesProgram',
											'schoolVarlist:schoolStudentChargesYear',

											'schoolStatistics:schoolCharacteristics', 
											'schoolStatistics:schoolPrograms',
											'schoolStatistics:schoolHeadCount',
											'schoolStatistics:schoolCompletions',
											'schoolStatistics:schoolCompleters',
											'schoolStatistics:schoolCompletersAwardRace',
											'schoolStatistics:schoolInstructionalActivity',
											'schoolStatistics:schoolStudentChargesProgram',
											'schoolStatistics:schoolStudentChargesYear',
										);




?>