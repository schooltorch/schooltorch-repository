<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*$config['students'] = array("1"=>"Up to 1,000",
							"2"=>"1001 to 2500",
							"3"=>"2501 to 5000",
							"4"=>"5001 to 10000",
							"5"=>"10001 to 20000");*/

$config['students'] = 	array(
								"1"=>"Up to 5K",
								"2"=>"5-10K",
								"3"=>"10-25K",
								"4"=>"25-40K",
								"5"=>">40K"
							);


$config['setting'] 	= array("11|12|13"=>"City",
							"21|22|23"=>"Suburban",
							"31|32|33"=>"Town",
							"41|42|43"=>"Rural");

$config['selectivity'] = array(
									"1"	=>	"Upto 500",
									"2"	=>	"500-550",
									"3"	=>	"550-600",
									"4"	=>	"600-700",
									"5"	=>	"700-800"
								);

/*$config['tuition'] 	= array(
								"L10000"		=>	"Up to $10K",
								"L20000|G10000"	=>	"$10-$20K",
								"L30000|G20000"	=>	"$20-30K",
								"L40000|G30000"	=>	"$30-40K",
								"G40000"		=>	">$40K"
							);*/

$config['tuition'] 	= array(
								"1"	=>	"Up to $10K",
								"2"	=>	"$10-$20K",
								"3"	=>	"$20-30K",
								"4"	=>	"$30-40K",
								"5"	=>	">$40K"
							);

/*$config['state'] 	= 	array(
                           		'Alabama'=>'AL',
								'Alaska'=>'AK',
								'Arizona'=>'AZ',
								'Arkansas'=>'AR',
								'California'=>'CA',
								'Colorado'=>'CO',
								'Connecticut'=>'CT',
								'Delaware'=>'DE',
								'Florida'=>'FL',
								'Georgia'=>'GA',
								'Hawaii'=>'HI',
								'Idaho'=>'ID',
								'Illinois'=>'IL',
								'Indiana'=>'IN',
								'Iowa'=>'IA',
								'Kansas'=>'KS',
								'Kentucky'=>'KY',
								'Louisiana'=>'LA',
								'Maine'=>'ME',
								'Maryland'=>'MD',
								'Massachusetts'=>'MA',
								'Michigan'=>'MI',
								'Minnesota'=>'MN',
								'Mississippi'=>'MS',
								'Missouri'=>'MO',
								'Montana'=>'MT',
								'Nebraska'=>'NE',
								'Nevada'=>'NV',
								'New Hampshire'=>'NH',
								'New Jersey'=>'NJ',
								'New Mexico'=>'NM',
								'New York'=>'NY',
								'North Carolina'=>'NC',
								'North Dakota'=>'ND',
								'Ohio'=>'OH',
								'Oklahoma'=>'OK',
								'Oregon'=>'OR',
								'Pennsylvania'=>'PA',
								'Rhode Island'=>'RI',
								'South Carolina'=>'SC',
								'South Dakota'=>'SD',
								'Tennessee'=>'TN',
								'Texas'=>'TX',
								'Utah'=>'UT',
								'Vermont'=>'VT',
								'Virginia'=>'VA',
								'Washington'=>'WA',
								'West Virginia'=>'WV',
								'Wisconsin'=>'WI',
								'Wyoming'=>'WY'
							);*/

$config['state'] 	= 	array(
                           		'1'	=>	'US Service schools',
								'2'	=>	'New England CT ME MA NH RI VT',
								'3'	=>	'Mid East DE DC MD NJ NY PA',
								'4'	=>	'Great Lakes IL IN MI OH WI',
								'5'	=>	'Plains IA KS MN MO NE ND SD',
								'6'	=>	'Southeast AL AR FL GA KY LA MS NC SC TN VA WV',
								'7'	=>	'Southwest AZ NM OK TX',
								'8'	=>	'Rocky Mountains CO ID MT UT WY',
								'9'	=>	'Far West AK CA HI NV OR WA',
								'10'=>	'Outlying areas AS FM GU MH MP PR PW VI',
								'1000'	=>	'Canada',
								'1001'	=>	'Australia',
								'1002'	=>	'New Zealand',
								'1003'	=>	'UK',
							);


$config['locale'] 	= array('11'=>'City',
							'12'=>'City',
							'13'=>'City',
							'15'=>'City',
							'21'=>'Suburban',
							'22'=>'Suburban',
							'23'=>'Suburban',
							'31'=>'Town',
							'32'=>'Town',
							'33'=>'Town',
							'41'=>'Rural',
							'42'=>'Rural',
							'43'=>'Rural'
							);