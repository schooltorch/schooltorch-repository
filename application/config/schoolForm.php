<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['addSchool']						=	array();

$config['addSchool']['UNITID']				=	array(
														'pName'		=>	'UNITID',
														'desc'		=>	'Unique Institute Id',
														'pLabel'	=>	'Institute Id',
														'pType'		=>	'number',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['INSTNM']				=	array(
														'pName'		=>	'INSTNM',
														'desc'		=>	'Institute Name',
														'pLabel'	=>	'Institute Name',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	120,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['ADDR']				=	array(
														'pName'		=>	'ADDR',
														'desc'		=>	'Institute Address',
														'pLabel'	=>	'Address',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	100,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['STABBR']				=	array(
														'pName'		=>	'STABBR',
														'desc'		=>	'State',
														'pLabel'	=>	'State',
														'pType'		=>	'select',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(),
														'isRequired'=>	1,
														'isDefault'	=>	'Select State'
													);

$config['addSchool']['CITY']				=	array(
														'pName'		=>	'CITY',
														'desc'		=>	'City',
														'pLabel'	=>	'City',
														'pType'		=>	'select',
														'min'		=>	0,
														'max'		=>	30,
														'aValues'	=>	array(),
														'isRequired'=>	1,
														'isDefault'	=>	'Select City'
													);

$config['addSchool']['LATITUDE']			=	array(
														'pName'		=>	'LATITUDE',
														'desc'		=>	'Latitude',
														'pLabel'	=>	'Latitude',
														'pType'		=>	'number',
														'min'		=>	0,
														'max'		=>	30,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['LONGITUD']			=	array(
														'pName'		=>	'LONGITUD',
														'desc'		=>	'Longitude',
														'pLabel'	=>	'Longitude',
														'pType'		=>	'number',
														'min'		=>	0,
														'max'		=>	30,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['ZIP']					=	array(
														'pName'		=>	'ZIP',
														'desc'		=>	'ZIP Code',
														'pLabel'	=>	'ZIP Code',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	10,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['FIPS']				=	array(
														'pName'		=>	'FIPS',
														'desc'		=>	'FIPS Code',
														'pLabel'	=>	'FIPS Code',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	2,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['OBEREG']				=	array(
														'pName'		=>	'OBEREG',
														'desc'		=>	'Geographic region code',
														'pLabel'	=>	'Geographic region code',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'0'	=>	'US Service schools',
																				'1'	=>	'New England CT ME MA NH RI VT',
																				'2'	=>	'Mid East DE DC MD NJ NY PA',
																				'3'	=>	'Great Lakes IL IN MI OH WI',
																				'4'	=>	'Plains IA KS MN MO NE ND SD',
																				'5'	=>	'Southeast AL AR FL GA KY LA MS NC SC TN VA WV',
																				'6'	=>	'Southwest AZ NM OK TX',
																				'7'	=>	'Rocky Mountains CO ID MT UT WY',
																				'8'	=>	'Far West AK CA HI NV OR WA',
																				'9'	=>	'Outlying areas AS FM GU MH MP PR PW VI'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select Geographic region code'
													);

$config['addSchool']['CHFNM']				=	array(
														'pName'		=>	'CHFNM',
														'desc'		=>	'Name of Chief Administrator',
														'pLabel'	=>	'Chief Administrator',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	50,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['CHFTITLE']			=	array(
														'pName'		=>	'CHFTITLE',
														'desc'		=>	'Title of Chief Administrator',
														'pLabel'	=>	'Title of Chief Administrator',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	50,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['GENTELE']				=	array(
														'pName'		=>	'GENTELE',
														'desc'		=>	'General information telephone number',
														'pLabel'	=>	'Telephone Number',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	20,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['FAXTELE']				=	array(
														'pName'		=>	'FAXTELE',
														'desc'		=>	'Fax Number',
														'pLabel'	=>	'Fax Number',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	20,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['EIN']					=	array(
														'pName'		=>	'EIN',
														'desc'		=>	'Internal Revenue Service Number',
														'pLabel'	=>	'Internal Revenue Service Number',
														'pType'		=>	'number',
														'min'		=>	0,
														'max'		=>	20,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['OPEID']				=	array(
														'pName'		=>	'OPEID',
														'desc'		=>	'OPE Id',
														'pLabel'	=>	'OPE Id',
														'pType'		=>	'number',
														'min'		=>	0,
														'max'		=>	20,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);


$config['addSchool']['OPEFLAG']				=	array(
														'pName'		=>	'OPEFLAG',
														'desc'		=>	'OPE Title IV eligibility',
														'pLabel'	=>	'OPE Title IV eligibility',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Participates in Title IV federal financial aid programs',
																				'2'	=>	'Branch campus of a main campus that participates in Title IV',
																				'3'	=>	'Deferment only - limited participation',
																				'4'	=>	'Not currently participating in Title IV, has an OPE ID number',
																				'5'	=>	'Not currently participating in Title IV, has an OPE ID number',
																				'6'	=>	'Not currently participating in Title IV, does not have OPE ID number',
																				'7'	=>	'Stopped participating during the survey year'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select OPE Title IV eligibility'
													);

$config['addSchool']['WEBADDR']				=	array(
														'pName'		=>	'WEBADDR',
														'desc'		=>	'Web Address',
														'pLabel'	=>	'Web Address',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	150,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['ADMINURL']			=	array(
														'pName'		=>	'ADMINURL',
														'desc'		=>	'Admissions office web address',
														'pLabel'	=>	'Admissions office web address',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	200,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['FAIDURL']				=	array(
														'pName'		=>	'FAIDURL',
														'desc'		=>	'Financial aid office web address',
														'pLabel'	=>	'Financial aid office web address',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	200,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['APPLURL']				=	array(
														'pName'		=>	'APPLURL',
														'desc'		=>	'Online application web address',
														'pLabel'	=>	'Online application web address',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	200,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['NPRICURL']			=	array(
														'pName'		=>	'NPRICURL',
														'desc'		=>	'Net price calculator web address',
														'pLabel'	=>	'Net price calculator web address',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	200,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['SECTOR']				=	array(
														'pName'		=>	'SECTOR',
														'desc'		=>	'Sector',
														'pLabel'	=>	'Sector',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'0'	=>	'Administrative Unit',
																				'1'	=>	'Public, 4-year or above',
																				'2'	=>	'Private not-for-profit, 4-year or above',
																				'3'	=>	'Private for-profit, 4-year or above',
																				'4'	=>	'Public, 2-year',
																				'5'	=>	'Private not-for-profit, 2-year',
																				'6'	=>	'Private for-profit, 2-year',
																				'7'	=>	'Public, less-than 2-year',
																				'8'	=>	'Private not-for-profit, less-than 2-year',
																				'9'	=>	'Private for-profit, less-than 2-year',
																				'99'=>	'Sector unknown (not active)'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select Sector'
													);

$config['addSchool']['ICLEVEL']				=	array(
														'pName'		=>	'ICLEVEL',
														'desc'		=>	'Level of institution',
														'pLabel'	=>	'Level of institution',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Four or more years',
																				'2'	=>	'At least 2 but less than 4 years',
																				'3'	=>	'Less than 2 years (below associate)',
																				'-3'=>	'{Not available}'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select Level of institution'
													);

$config['addSchool']['CONTROL']				=	array(
														'pName'		=>	'CONTROL',
														'desc'		=>	'Control of institution',
														'pLabel'	=>	'Control of institution',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Public',
																				'2'	=>	'Private not-for-profit',
																				'3'	=>	'Private for-profit',
																				'-3'=>	'{Not available}'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select Control of institution'
													);

$config['addSchool']['HLOFFER']				=	array(
														'pName'		=>	'HLOFFER',
														'desc'		=>	'Highest level of offering',
														'pLabel'	=>	'Highest level of offering',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Award of less than one academic year',
																				'2'	=>	'At least 1, but less than 2 academic yrs',
																				'3'	=>	'Associate\'s degree',
																				'4'	=>	'At least 2, but less than 4 academic yrs',
																				'5'	=>	'Bachelor\'s degree',
																				'6'	=>	'Postbaccalaureate certificate',
																				'7'	=>	'Master\'s degree',
																				'8'	=>	'Post-master\'s certificate',
																				'9'	=>	'Doctor\'s degree',
																				'-3'=>	'{Not available}'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select Highest level of offering'
													);

$config['addSchool']['UGOFFER']				=	array(
														'pName'		=>	'UGOFFER',
														'desc'		=>	'Undergraduate offering',
														'pLabel'	=>	'Undergraduate offering',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Undergraduate degree or certificate offering',
																				'2'	=>	'No undergraduate offering',
																				'-3'=>	'{Not available}'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select Undergraduate offering'
													);

$config['addSchool']['GROFFER']				=	array(
														'pName'		=>	'GROFFER',
														'desc'		=>	'Graduate offering',
														'pLabel'	=>	'Graduate offering',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Graduate degree or certificate offering',
																				'2'	=>	'No graduate offering',
																				'-3'=>	'{Not available}'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select Graduate offering'
													);

$config['addSchool']['HDEGOFR1']			=	array(
														'pName'		=>	'HDEGOFR1',
														'desc'		=>	'Highest degree offered',
														'pLabel'	=>	'Highest degree offered',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'11'=>	'Doctor\'s degree - research/scholarship and professional practice',
																				'12'=>	'Doctor\'s degree - research/scholarship',
																				'13'=>	'Doctor\'s degree -  professional practice',
																				'14'=>	'Doctor\'s degree - other',
																				'20'=>	'Master\'s degree',
																				'30'=>	'Bachelor\'s degree',
																				'40'=>	'Associate\'s degree',
																				'0'	=>	'Non-degree granting',
																				'-3'=>	'{Not available}'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select Highest degree offered'
													);

$config['addSchool']['DEGGRANT']			=	array(
														'pName'		=>	'DEGGRANT',
														'desc'		=>	'Degree-granting status',
														'pLabel'	=>	'Degree-granting status',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Degree-granting',
																				'2'	=>	'Nondegree-granting, primarily postsecondary',
																				'-3'=>	'{Not available}'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select Degree-granting status'
													);

$config['addSchool']['HBCU']				=	array(
														'pName'		=>	'HBCU',
														'desc'		=>	'Historically Black College or University',
														'pLabel'	=>	'Historically Black College or University',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Yes',
																				'2'	=>	'No'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['HOSPITAL']			=	array(
														'pName'		=>	'HOSPITAL',
														'desc'		=>	'Institution has hospital',
														'pLabel'	=>	'Institution has hospital',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Yes',
																				'2'	=>	'No',
																				'-1'=>	'Not reported',
																				'-2'=>	'Not applicable'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['MEDICAL']				=	array(
														'pName'		=>	'MEDICAL',
														'desc'		=>	'Institution grants a medical degree',
														'pLabel'	=>	'Institution grants a medical degree',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Yes',
																				'2'	=>	'No',
																				'-1'=>	'Not reported',
																				'-2'=>	'Not applicable'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['TRIBAL']				=	array(
														'pName'		=>	'TRIBAL',
														'desc'		=>	'Tribal college',
														'pLabel'	=>	'Tribal college',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Yes',
																				'2'	=>	'No'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['LOCALE']				=	array(
														'pName'		=>	'LOCALE',
														'desc'		=>	'Degree of urbanization',
														'pLabel'	=>	'Degree of urbanization',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'11'=>	'City: Large',
																				'12'=>	'City: Midsize',
																				'13'=>	'City: Small',
																				'21'=>	'Suburb: Large',
																				'22'=>	'Suburb: Midsize',
																				'23'=>	'Suburb: Small',
																				'31'=>	'Town: Fringe',
																				'32'=>	'Town: Distant',
																				'33'=>	'Town: Remote',
																				'41'=>	'Rural: Fringe',
																				'42'=>	'Rural: Distant',
																				'43'=>	'Rural: Remote',
																				'-3'=>	'{Not available}'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select Locality'
													);

$config['addSchool']['OPENPUBL']			=	array(
														'pName'		=>	'OPENPUBL',
														'desc'		=>	'Institution open to the general public',
														'pLabel'	=>	'Institution open to the general public',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Institution is open to the public',
																				'0'	=>	'Institution is not open to the public'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['ACT']					=	array(
														'pName'		=>	'ACT',
														'desc'		=>	'Status of institution',
														'pLabel'	=>	'Status of institution',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'A'	=>	'Active - institution active',
																				'N'	=>	'New (active) - added during the current year',
																				'R'	=>	'Restore (active) - restored to the current universe',
																				'M'	=>	'Closed in current year (active has data)',
																				'C'	=>	'Combined with other institution',
																				'D'	=>	'Delete out of business'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['ACT']					=	array(
														'pName'		=>	'ACT',
														'desc'		=>	'Status of institution',
														'pLabel'	=>	'Status of institution',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'A'	=>	'Active - institution active',
																				'N'	=>	'New (active) - added during the current year',
																				'R'	=>	'Restore (active) - restored to the current universe',
																				'M'	=>	'Closed in current year (active has data)',
																				'C'	=>	'Combined with other institution',
																				'D'	=>	'Delete out of business'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['NEWID']				=	array(
														'pName'		=>	'NEWID',
														'desc'		=>	'Unique Institute Id for merged schools',
														'pLabel'	=>	'Institute Id for merged schools',
														'pType'		=>	'number',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(),
														'isRequired'=>	0
													);

$config['addSchool']['DEATHYR']				=	array(
														'pName'		=>	'DEATHYR',
														'desc'		=>	'Year institution was deleted from IPEDS',
														'pLabel'	=>	'Year institution was deleted from IPEDS',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(),
														'isRequired'=>	0,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['CLOSEDAT']			=	array(
														'pName'		=>	'CLOSEDAT',
														'desc'		=>	'Date institution closed',
														'pLabel'	=>	'Date institution closed',
														'pType'		=>	'alpha',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(),
														'isRequired'=>	0
													);

$config['addSchool']['CYACTIVE']			=	array(
														'pName'		=>	'CYACTIVE',
														'desc'		=>	'Institution is active in current year',
														'pLabel'	=>	'Institution is active in current year',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Yes',
																				'3'	=>	'No, closed, combined, or out-of-scope'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['POSTSEC']				=	array(
														'pName'		=>	'POSTSEC',
														'desc'		=>	'Primarily postsecondary indicator',
														'pLabel'	=>	'Primarily postsecondary indicator',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Primarily postsecondary institution',
																				'2'	=>	'Not primarily postsecondary'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['PSEFLAG']				=	array(
														'pName'		=>	'PSEFLAG',
														'desc'		=>	'Postsecondary institution indicator',
														'pLabel'	=>	'Postsecondary institution indicator',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Active postsecondary institution',
																				'2'	=>	'Not primarily postsecondary or open to public',
																				'3'	=>	'Not active'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['PSET4FLG']			=	array(
														'pName'		=>	'PSET4FLG',
														'desc'		=>	'Postsecondary and Title IV institution indicator',
														'pLabel'	=>	'Postsecondary and Title IV institution indicator',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Title IV postsecondary institution',
																				'2'	=>	'Non-Title IV postsecondary institution',
																				'3'	=>	'Title IV NOT primarily postsecondary institution',
																				'4'	=>	'Non-Title IV NOT primarily postsecondary institution',
																				'5'	=>	'Title IV postsecondary institution that is NOT open to the public',
																				'6'	=>	'Non-Title IV postsecondary institution that is NOT open to the public',
																				'9'	=>	'Institution is not active in current universe'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['RPTMTH']				=	array(
														'pName'		=>	'RPTMTH',
														'desc'		=>	'Reporting method for student charges, graduation rates, retention rates and student financial aid indicator',
														'pLabel'	=>	'Reporting method for student charges, graduation rates, retention rates and student financial aid',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Student charges for full academic year and fall GR/SFA/retention rate cohort',
																				'3'	=>	'Student charges for full academic year and full-year GR/SFA/retention rate cohort',
																				'2'	=>	'Student charges by program and full-year GR/SFA/retention rate cohort',
																				'-1'=>	'Not reported',
																				'-2'=>	'Not applicable'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['IALIAS']				=	array(
														'pName'		=>	'IALIAS',
														'desc'		=>	'Institution name alias',
														'pLabel'	=>	'Institution name alias',
														'pType'		=>	'textarea',
														'min'		=>	0,
														'max'		=>	2000,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['INSTCAT']				=	array(
														'pName'		=>	'INSTCAT',
														'desc'		=>	'Institutional category',
														'pLabel'	=>	'Institutional category',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Degree-granting, graduate with no undergraduate degrees',
																				'2'	=>	'Degree-granting, primarily baccalaureate or above',
																				'3'	=>	'Degree-granting, not primarily baccalaureate or above',
																				'4'	=>	'Degree-granting, associate\'s and certificates',
																				'5'	=>	'Nondegree-granting, above the baccalaureate',
																				'6'	=>	'Nondegree-granting, sub-baccalaureate',
																				'-1'=>	'Not reported',
																				'-2'=>	'Not applicable'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['CCBASIC']				=	array(
														'pName'		=>	'CCBASIC',
														'desc'		=>	'Carnegie Classification 2010: Basic',
														'pLabel'	=>	'Carnegie Classification 2010: Basic',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Associate\'s--Public Rural-serving Small',
																				'2'	=>	'Associate\'s--Public Rural-serving Medium',
																				'3'	=>	'Associate\'s--Public Rural-serving Large',
																				'4'	=>	'Associate\'s--Public Suburban-serving Single Campus',
																				'5'	=>	'Associate\'s--Public Suburban-serving Multicampus',
																				'6'	=>	'Associate\'s--Public Urban-serving Single Campus',
																				'7'	=>	'Associate\'s--Public Urban-serving Multicampus',
																				'8'	=>	'Associate\'s--Public Special Use',
																				'9'	=>	'Associate\'s--Private Not-for-profit',
																				'10'=>	'Associate\'s--Private For-profit',
																				'11'=>	'Associate\'s--Public 2-year colleges under 4-year universities',
																				'12'=>	'Associate\'s--Public 4-year Primarily Associate\'s',
																				'13'=>	'Associate\'s--Private Not-for-profit 4-year Primarily Associate\'s',
																				'14'=>	'Associate\'s--Private For-profit 4-year Primarily Associate\'s',
																				'15'=>	'Research Universities (very high research activity)',
																				'16'=>	'Research Universities (high research activity)',
																				'17'=>	'Doctoral/Research Universities',
																				'18'=>	'Master\'s Colleges and Universities (larger programs)',
																				'19'=>	'Master\'s Colleges and Universities (medium programs)',
																				'20'=>	'Master\'s Colleges and Universities (smaller programs)',
																				'21'=>	'Baccalaureate Colleges--Arts & Sciences',
																				'22'=>	'Baccalaureate Colleges--Diverse Fields',
																				'23'=>	'Baccalaureate/Associate\'s Colleges',
																				'24'=>	'Theological seminaries, Bible colleges, and other faith-related institutions',
																				'25'=>	'Medical schools and medical centers',
																				'26'=>	'Other health professions schools',
																				'27'=>	'Schools of engineering',
																				'28'=>	'Other technology-related schools',
																				'29'=>	'Schools of business and management',
																				'30'=>	'Schools of art, music, and design',
																				'31'=>	'Schools of law',
																				'32'=>	'Other special-focus institutions',
																				'33'=>	'Tribal Colleges',
																				'-3'=>	'Not applicable, not in Carnegie universe (not accredited or nondegree-granting)'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['CCIPUG']				=	array(
														'pName'		=>	'CCIPUG',
														'desc'		=>	'Carnegie Classification 2010: Undergraduate Instructional Program',
														'pLabel'	=>	'Carnegie Classification 2010: Undergraduate Instructional Program',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Associates',
																				'2'	=>	'Associates Dominant',
																				'3'	=>	'Arts & sciences focus, no graduate coexistence',
																				'4'	=>	'Arts & sciences focus, some graduate coexistence',
																				'5'	=>	'Arts & sciences focus, high graduate coexistence',
																				'6'	=>	'Arts & sciences plus professions, no graduate coexistence',
																				'7'	=>	'Arts & sciences plus professions, some graduate coexistence',
																				'8'	=>	'Arts & sciences plus professions, high graduate coexistence',
																				'9'	=>	'Balanced arts & sciences/professions, no graduate coexistence',
																				'10'=>	'Balanced arts & sciences/professions, some graduate coexistence',
																				'11'=>	'Balanced arts & sciences/professions, high graduate coexistence',
																				'12'=>	'Professions plus arts & sciences, no graduate coexistence',
																				'13'=>	'Professions plus arts & sciences, some graduate coexistence',
																				'14'=>	'Professions plus arts & sciences, high graduate coexistence',
																				'15'=>	'Professions focus, no graduate coexistence',
																				'16'=>	'Professions focus, some graduate coexistence',
																				'17'=>	'Professions focus, high graduate coexistence',
																				'0'	=>	'Not classified',
																				'-1'=>	'Not applicable, graduate institution',
																				'-2'=>	'Not applicable, special focus institution',
																				'-3'=>	'Not applicable, not in Carnegie universe (not accredited or nondegree-granting)'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['CCIPGRAD']			=	array(
														'pName'		=>	'CCIPGRAD',
														'desc'		=>	'Carnegie Classification 2010: Graduate Instructional Program',
														'pLabel'	=>	'Carnegie Classification 2010: Graduate Instructional Program',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Single postbaccalaureate (education)',
																				'2'	=>	'Single postbaccalaureate (business)',
																				'3'	=>	'Single postbaccalaureate (other field)',
																				'4'	=>	'Postbaccalaureate comprehensive',
																				'5'	=>	'Postbaccalaureate, arts & sciences dominant',
																				'6'	=>	'Postbaccalaureate with arts & sciences (education dominant)',
																				'7'	=>	'Postbaccalaureate with arts & sciences (business dominant)',
																				'8'	=>	'Postbaccalaureate with arts & sciences (other dominant fields)',
																				'9'	=>	'Postbaccalaureate professional (education dominant)',
																				'10'=>	'Postbaccalaureate professional (business dominant)',
																				'11'=>	'Postbaccalaureate professional (other dominant fields)',
																				'12'=>	'Single doctoral (education)',
																				'13'=>	'Single doctoral (other field)',
																				'14'=>	'Comprehensive doctoral with medical/veterinary',
																				'15'=>	'Comprehensive doctoral (no medical/veterinary)',
																				'16'=>	'Doctoral, humanities/social sciences dominant',
																				'17'=>	'STEM dominant',
																				'18'=>	'Doctoral, professional dominant',
																				'-1'=>	'Not applicable',
																				'-2'=>	'Not applicable, special focus institution',
																				'-3'=>	'Not applicable, not in Carnegie universe (not accredited or nondegree-granting)'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['CCUGPROF']			=	array(
														'pName'		=>	'CCUGPROF',
														'desc'		=>	'Carnegie Classification 2010: Undergraduate Profile',
														'pLabel'	=>	'Carnegie Classification 2010: Undergraduate Profile',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Higher part-time two-year',
																				'2'	=>	'Mixed part/full-time two-year',
																				'3'	=>	'Medium full-time two-year',
																				'4'	=>	'Higher full-time two-year',
																				'5'	=>	'Higher part-time four-year',
																				'6'	=>	'Medium full-time four-year, inclusive',
																				'7'	=>	'Medium full-time four-year, selective, lower transfer-in',
																				'8'	=>	'Medium full-time four-year, selective, higher transfer-in',
																				'9'	=>	'Full-time four-year, inclusive',
																				'10'=>	'Full-time four-year, selective, lower transfer-in',
																				'11'=>	'Full-time four-year, selective, higher transfer-in',
																				'12'=>	'Full-time four-year, more selective, lower transfer-in',
																				'13'=>	'Full-time four-year, more selective, higher transfer-in',
																				'0'	=>	'Not classified',
																				'-1'=>	'Not applicable',
																				'-2'=>	'Not applicable, special focus institution',
																				'-3'=>	'Not applicable, not in Carnegie universe (not accredited or nondegree-granting)'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['CCENRPRF']			=	array(
														'pName'		=>	'CCENRPRF',
														'desc'		=>	'Carnegie Classification 2010: Enrollment Profile',
														'pLabel'	=>	'Carnegie Classification 2010: Enrollment Profile',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Exclusively undergraduate two-year',
																				'2'	=>	'Exclusively undergraduate four-year',
																				'3'	=>	'Very high undergraduate',
																				'4'	=>	'High undergraduate',
																				'5'	=>	'Majority undergraduate',
																				'6'	=>	'Majority graduate/professional',
																				'7'	=>	'Exclusively graduate/professional',
																				'0'	=>	'Not classified',
																				'-3'=>	'Not applicable, not in Carnegie universe (not accredited or nondegree-granting)'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['CCSIZSET']			=	array(
														'pName'		=>	'CCSIZSET',
														'desc'		=>	'Carnegie Classification 2010: Size and Setting',
														'pLabel'	=>	'Carnegie Classification 2010: Size and Setting',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Very small two-year',
																				'2'	=>	'Small two-year',
																				'3'	=>	'Medium two-year',
																				'4'	=>	'Large two-year',
																				'5'	=>	'Very large two-year',
																				'6'	=>	'Very small four-year, primarily nonresidential',
																				'7'	=>	'Very small four-year, primarily residential',
																				'8'	=>	'Very small four-year, highly residential',
																				'9'	=>	'Small four-year, primarily nonresidential',
																				'10'=>	'Small four-year, primarily residential',
																				'11'=>	'Small four-year, highly residential',
																				'12'=>	'Medium four-year, primarily nonresidential',
																				'13'=>	'Medium four-year, primarily residential',
																				'14'=>	'Medium four-year, highly residential',
																				'15'=>	'Large four-year, primarily nonresidential',
																				'16'=>	'Large four-year, primarily residential',
																				'17'=>	'Large four-year, highly residential',
																				'18'=>	'Exclusively graduate/professional',
																				'-2'=>	'Not applicable, special focus institution',
																				'-3'=>	'Not applicable, not in Carnegie universe (not accredited or nondegree-granting)'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['CARNEGIE']			=	array(
														'pName'		=>	'CARNEGIE',
														'desc'		=>	'Carnegie Classification 2000',
														'pLabel'	=>	'Carnegie Classification 2000',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'15'=>	'Doctoral/Research Universities--Extensive',
																				'16'=>	'Doctoral/Research Universities--Intensive',
																				'21'=>	'Masters Colleges and Universities I',
																				'22'=>	'Masters Colleges and Universities II',
																				'31'=>	'Baccalaureate Colleges--Liberal Arts',
																				'32'=>	'Baccalaureate Colleges--General',
																				'33'=>	'Baccalaureate/Associates Colleges',
																				'40'=>	'Associates Colleges',
																				'51'=>	'Theological seminaries and other specialized faith-related institutions',
																				'52'=>	'Medical schools and medical centers',
																				'53'=>	'Other separate health profession schools',
																				'54'=>	'Schools of engineering and technology',
																				'55'=>	'Schools of business and management',
																				'56'=>	'Schools of art, music, and design',
																				'57'=>	'Schools of law',
																				'58'=>	'Teachers colleges',
																				'59'=>	'Other specialized institutions',
																				'60'=>	'Tribal colleges',
																				'-3'=>	'{Item not available}'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['LANDGRNT']			=	array(
														'pName'		=>	'LANDGRNT',
														'desc'		=>	'Land Grant Institution',
														'pLabel'	=>	'Land Grant Institution',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Land Grant Institution',
																				'2'	=>	'Not a Land Grant Institution'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['INSTSIZE']			=	array(
														'pName'		=>	'INSTSIZE',
														'desc'		=>	'Institution size category',
														'pLabel'	=>	'Institution size category',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Under 1,000',
																				'2'	=>	'1,000 - 4,999',
																				'3'	=>	'5,000 - 9,999',
																				'4'	=>	'10,000 - 19,999',
																				'5'	=>	'20,000 and above',
																				'-1'=>	'Not reported',
																				'-2'=>	'Not applicable'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['CBSA']			=	array(
														'pName'		=>	'CBSA',
														'desc'		=>	'Core Based Statistical Area (CBSA)',
														'pLabel'	=>	'Core Based Statistical Area (CBSA)',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	10,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['CBSATYPE']			=	array(
														'pName'		=>	'CBSATYPE',
														'desc'		=>	'CBSA Type Metropolitan or Micropolitan',
														'pLabel'	=>	'CBSA Type Metropolitan or Micropolitan',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Metropolitan Statistical Area',
																				'2'	=>	'Micropolitan Statistical Area',
																				'-2'=>	'Not applicable'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['CSA']					=	array(
														'pName'		=>	'CSA',
														'desc'		=>	'Combined Statistical Area (CSA)',
														'pLabel'	=>	'Combined Statistical Area (CSA)',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	10,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['NECTA']				=	array(
														'pName'		=>	'NECTA',
														'desc'		=>	'New England City and Town Area (NECTA)',
														'pLabel'	=>	'New England City and Town Area (NECTA)',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	10,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['F1SYSTYP']			=	array(
														'pName'		=>	'F1SYSTYP',
														'desc'		=>	'Multi-institution or multi-campus organization',
														'pLabel'	=>	'Multi-institution or multi-campus organization',
														'pType'		=>	'select',
														'pSubType'	=>	'single',
														'min'		=>	-1,
														'max'		=>	-1,
														'aValues'	=>	array(
																				'1'	=>	'Institution is part of a multi-institution or multi-campus organization',
																				'2'	=>	'Institution is NOT part of a multi-institution or multi-campus organization',
																				'-1'=>	'Not reported',
																				'-2'=>	'Not applicable'
																			),
														'isRequired'=>	1,
														'isDefault'	=>	'Select'
													);

$config['addSchool']['F1SYSNAM']			=	array(
														'pName'		=>	'F1SYSNAM',
														'desc'		=>	'Name of multi-institution or multi-campus organization',
														'pLabel'	=>	'Name of multi-institution or multi-campus organization',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	100,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['F1SYSCOD']			=	array(
														'pName'		=>	'F1SYSCOD',
														'desc'		=>	'Identification number of multi-institution or multi-campus organization',
														'pLabel'	=>	'Identification number of multi-institution or multi-campus organization',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	10,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['COUNTYCD']			=	array(
														'pName'		=>	'COUNTYCD',
														'desc'		=>	'FIPS County code',
														'pLabel'	=>	'FIPS County code',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	5,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['COUNTYNM']			=	array(
														'pName'		=>	'COUNTYNM',
														'desc'		=>	'County name',
														'pLabel'	=>	'County name',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	30,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

$config['addSchool']['CNGDSTCD']			=	array(
														'pName'		=>	'CNGDSTCD',
														'desc'		=>	'Congressional district code',
														'pLabel'	=>	'Congressional district code',
														'pType'		=>	'alpha',
														'min'		=>	0,
														'max'		=>	5,
														'aValues'	=>	array(),
														'isRequired'=>	1
													);

