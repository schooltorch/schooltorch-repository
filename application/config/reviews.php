<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['reviewUserType']						=	array(
															1 => 'Student',
															2 => 'Alum',
															3 => 'Other',
															);

$config['reviewGradYear']						=	array(
															1 => 'In School',
															2 => 'N/A',
														 );

$config['reviewFeatures']						=	array(
															1 => 'Location',
															2 => 'Campus/Facilities',
															3 => 'Cost',
															4 => 'Professors',
															5 => 'Students',
															6 => 'Courses',
															7 => 'Alumni network',
															8 => 'Career prospects',
														 );
$config['reviewCulture']						=	array(
															1 => 'Academics',
															2 => 'Sports',
															3 => 'Parties',
															4 => 'Arts',
															5 => 'Politics',
															6 => 'Careers'
														 );
