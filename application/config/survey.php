<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['surveyOptions']		=	array(	
										1 => array(
													1 => "Masters (Arts/Humanities)",
													2 => "Masters (Science/Engr.)",
													3 => "Masters (Business)",
													4 => "PhD",
													5 => "Other graduate degree",
													6 => "Undergrad degree",
													),
										3 => array(
													1 => "Yes",
													2 => "No",
													),
										4 => array(
													5 => "Loved it",
													4 => "Really like it",
													3 => "Like it",
													2 => "Didn’t like it",
													1 => "Hated it",
												  ),
										5 => array(
													1 => "Location/campus life",
													2 => "Good infrastructure & facilities",
													3 => "High calibre of professors",
													4 => "Strong quality of students",
													5 => "Great learning environment",
													6 => "Excellent courses",
													7 => "Strong alumni network",
												  ),
										6 => array(
													1 => "Career focused",
													2 => "Budget conscious",
													3 => "Prestige patrons",
													4 => "Research geeks",
													5 => "Art & culture fiends",
													6 => "Party animals",
													7 => "Sports & outdoor enthusiasts",
													8 => "Knowledge seeker"
												  ),
										7 => array(
													1 => "Self-funded",
													2 => "Employer",
													//3 => "Like it",
													4 => "On campus job",
													5 => "School assistantships/scholarships",
													6 => "External loans"
												  ),

										8 => array(
													1 => "Will get a job but not my top preference",
													2 => "Concerned about job prospects at graduation",
													3 => "Will get the job I want"
												  ),
										9 => array(
													1 => "Top-tier firms",
													2 => "Average reputation",
													3 => "Lesser known brands/employers"
												  ),


						    );
