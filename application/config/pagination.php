<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['pageConfig'] 	=	array();

$config['pageConfig']['num_links'] 			= 	5;
$config['pageConfig']['first_link'] 		= 	'First';
$config['pageConfig']['last_link'] 			= 	'Last';
$config['pageConfig']['use_page_numbers'] 	= 	TRUE;
$config['pageConfig']['full_tag_open'] 		= 	'<ul class="pagination">';
$config['pageConfig']['full_tag_close'] 	= 	'</ul>';

$config['pageConfig']['num_tag_open'] 		= 	'<li>';
$config['pageConfig']['num_tag_close'] 		= 	'</li>';

$config['pageConfig']['cur_tag_open'] 		= 	"<li class='active'><a href='#'>";
$config['pageConfig']['cur_tag_close'] 		= 	'</a></li>';

$config['pageConfig']['prev_tag_open'] 		= 	'<li>';
$config['pageConfig']['prev_tag_close'] 	= 	'</li>';

$config['pageConfig']['next_tag_open'] 		= 	'<li>';
$config['pageConfig']['next_tag_close'] 	= 	'</li>';

$config['pageConfig']['first_tag_open'] 	= 	'<li>';
$config['pageConfig']['first_tag_close'] 	= 	'</li>';

$config['pageConfig']['last_tag_open'] 		= 	'<li>';
$config['pageConfig']['last_tag_close']	 	= 	'</li>';
?>